import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class AppTheme {
  static final global =
      ThemeData(/*textTheme: _textTheme(),*/ appBarTheme: _appBarTheme());

  // static TextTheme _textTheme() {
  //   final basicTheme = ThemeData.light().textTheme;
  //
  //   return TextTheme(
  //       headline1: basicTheme.headline1
  //           ?.copyWith(fontSize: 36.0, fontFamily: 'Poppins', height: 1.1),
  //       headline2: basicTheme.headline2?.copyWith(
  //           fontSize: 26.0,
  //           color: AppColors.darkIndigo,
  //           fontFamily: 'PoppinsMedium'),
  //       headline3: basicTheme.headline3?.copyWith(
  //           fontSize: 16.0,
  //           color: AppColors.darkIndigo,
  //           fontFamily: 'PoppinsMedium'),
  //       headline4: basicTheme.headline4?.copyWith(
  //           fontSize: 18.0,
  //           color: AppColors.darkIndigo,
  //           fontFamily: 'PoppinsMedium'),
  //       headline5: basicTheme.headline5?.copyWith(
  //           fontSize: 22.0,
  //           color: AppColors.darkIndigo,
  //           fontFamily: 'PoppinsSemiBold'),
  //       headline6: basicTheme.headline6?.copyWith(
  //           fontSize: 17.0,
  //           color: AppColors.darkIndigo,
  //           fontFamily: 'PoppinsSemiBold'),
  //       bodyText1: basicTheme.bodyText1?.copyWith(
  //           fontSize: 16.0, color: AppColors.darkIndigo, fontFamily: 'Roboto'),
  //       bodyText2: basicTheme.bodyText2?.copyWith(
  //           fontSize: 14.0, color: AppColors.darkIndigo, fontFamily: 'Roboto'),
  //       subtitle1: basicTheme.subtitle1?.copyWith(
  //           fontSize: 16.0, color: AppColors.coldGrey, fontFamily: 'Roboto'),
  //       subtitle2: basicTheme.subtitle2?.copyWith(
  //           fontSize: 14.0, color: AppColors.coldGrey, fontFamily: 'Roboto'),
  //       button: basicTheme.button?.copyWith(
  //           fontSize: 15.0,
  //           color: Colors.white,
  //           fontFamily: 'PoppinsSemiBold'));
  // }

  static AppBarTheme _appBarTheme() {
    return AppBarTheme(
        brightness: Brightness.light,
        color: Colors.white,
        iconTheme: IconThemeData(color: AppColors.darkGrey));
  }
}

class AppColors {
  static final darkIndigo = Color(0xFF0B2444);
  static final strongPink = Color(0xFFFB1470);
  static final coldGrey = Color(0xFF8591A1);
  static final avantGard = Color(0xFF00947C);
  static final paleGrey = Color(0xFFF3F4F7);
  static final paleColdGrey = Color(0xFFF5F6F7);
  static final lightColdGrey = Color(0xFFE6E8EB);
  static final skyStrong = Color(0xFF243179);
  static final sky = Color(0xFF2a69da);
  static final skyLight = Color(0xFF58a6ef);
  static final skyPale = Color(0xff7fa4e8);
  static final purple = Color(0xFFb01ed9);
  static final grey = Color(0xFF6D6C6D);
  static final lightGrey = Color(0xFF899099);
  static final lightGrey2 = Color(0xFFBEC4CC);
  static final darkGrey = Color(0xFF484c51);
  static final darkPurple = Color(0xFF6c00c3);
  static final grayDark = Color(0xFF707780);
  static final errorRed = Color(0xFFb20a0a);
  static final confirmGreen = Color(0x4Ef4fffd);
  static final casper = Color(0xFFB1BCBE);
}
