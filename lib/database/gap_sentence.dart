import 'package:sqflite/sqflite.dart';

class GapSentence{

  Future insertGapsentence(Database db) async {
    await db.transaction((txn) async {
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [1, 'Auf dem %s stehen %s', 1, 2]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [3, 'Das %s fährt über die %s', 3, 4]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [5, 'Das %s frisst %s', 5, 6]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [7, 'Das %s wird %s', 7, 8]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [9, 'Das Kind %s in der %s', 9, 10]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [11, 'Das %s hat die Farbe %s', 11, 12]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [13, 'Der %s hat %s Fell', 13, 14]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [15, 'Wir gehen %s in den %s', 15, 16]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [17, 'Der %s malt ein %s', 17, 18]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [19, 'Der %s schmeckt nach %s', 19, 20]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [21, 'Der %s spricht %s', 21, 22]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [23, 'Der Anwalt %s den %s', 23, 24]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [25, 'Die %s halten sich an die %s', 25, 26]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [27, 'Die %s hilft dem %s', 27, 28]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [29, 'Die %s ist um %s Uhr', 29, 30]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [31, 'Die %s wurde %s', 31, 32]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [33, 'Die Großmutter %s %s', 33, 34]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [35, 'Du %s auf dem %s', 35, 36]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [37, 'Er %s heute %s', 37, 38]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [39, 'Es ist sehr %s im %s', 39, 40]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [41, 'Ich %s meine %s', 41, 42]);
      await txn.execute(
          "INSERT INTO gapsentence ('gapsentence_id', 'gapsentence', 'answer_id1', 'answer_id2') values (?, ?, ?, ?)",
          [43, 'Sie %s die Schule %s Sommer', 43, 44]);
    });
  }
}