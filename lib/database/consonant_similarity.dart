import 'package:sqflite/sqflite.dart';

class ConsonantSimilarity{

  Future insertConsonantSimilarity(Database db) async {
    await db.transaction((txn) async {
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0020.mp3", 2]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0020.mp3", 2]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0021.mp3", 2]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0021.mp3", 2]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0022.mp3", 1]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0022.mp3", 1]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0023.mp3", 3]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0023.mp3", 3]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0024.mp3", 4]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0024.mp3", 4]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0025.mp3", 4]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0025.mp3", 4]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0026.mp3", 3]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0026.mp3", 3]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0027.mp3", 4]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0027.mp3", 4]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0028.mp3", 2]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0028.mp3", 2]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0029.mp3", 2]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0029.mp3", 2]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0030.mp3", 3]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0030.mp3", 3]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0031.mp3", 4]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0031.mp3", 4]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0032.mp3", 1]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0032.mp3", 1]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0033.mp3", 3]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0033.mp3", 3]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0034.mp3", 1]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0034.mp3", 1]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0035.mp3", 1]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0035.mp3", 1]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0036.mp3", 1]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0036.mp3", 1]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0037.mp3", 2]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0037.mp3", 2]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/m0038.mp3", 1]
      );
      await txn.execute(
          "INSERT INTO consonant_similarity ('res_id', 'sim_group') values (?, ?)",
          ["raw/f0038.mp3", 1]
      );
    });
  }
}