import 'package:audis/database/model/sample.dart';

class SampleConsonant extends Sample{
  
  String consonant = '', alternateConsonant = '', minPair = '';
  int? startEnd;

  SampleConsonant(String res_id, String content,int syllables,int mood, int speed,int type,int speaker,
      String consonant, String alternateConsonant, String minPair, int startEnd) : super(res_id, content, syllables, mood, speed, type, speaker) {
    this.consonant = consonant;
    this.alternateConsonant = alternateConsonant;
    this.minPair = minPair;
    this.startEnd = startEnd;
  }

  String getConsonant() { return consonant; }
  String getAlternateConsonant() { return alternateConsonant; }
  String getMinPair() { return minPair; }
  int? getStartEnd() { return startEnd; }

  static SampleConsonant fromJson(Map<String, Object?> json) => SampleConsonant(
    json['res_id'] as String,
    json['content'] as String,
    json['syllables'] as int,
    json['mood'] as int,
    json['speed'] as int,
    json['type'] as int,
    json['speaker'] as int,
    json['consonant']  == null ? '' : json['consonant'] as String,
    json['alternateConsonant'] == null ? '' : json['alternateConsonant'] as String,
    json['minPair'] == null ? '' : json['minPair'] as String,
    json['startEnd'] as int,
  );

  static String fromJson2(Map<String, Object?> json) => (
      json['consonant'] as String
  );

}