import 'package:audis/database/model/sample.dart';

class GapSentenceModel extends Sample {

  int? gapsentence_id;
  String? gapsentence;
  int? answer_id1;
  int? answer_id2;

  /// @param res_id    ID of the resource in R.raw
  /// @param content   Description of the sample
  /// @param syllables number of syllables
  /// @param mood      0 = neutral; 1 = happy; 2 = sad
  /// @param speed     0 = slow; 1 = fast
  /// @param type      0 = word; 1 = sentence; 2 = question
  /// @param speaker   0 = male; 1 = female
  GapSentenceModel(String res_id, String content, int syllables, int mood, int speed, int type, int speaker,
      int gapsentence_id, String gapsentence,int answer_id1,int answer_id2) : super(res_id, content, syllables, mood, speed, type, speaker) {
    this.gapsentence_id = gapsentence_id;
    this.gapsentence = gapsentence;
    this.answer_id1 = answer_id1;
    this.answer_id2 = answer_id2;
  }
  static GapSentenceModel fromJson(Map<String, Object?> json) => GapSentenceModel(
    json['res_id'] as String,
    json['content'] as String,
    json['syllables'] as int,
    json['mood'] as int,
    json['speed'] as int,
    json['type'] as int,
    json['speaker'] as int,
    json['gapsentence_id'] as int,
    json['gapsentence'] == null ? '' : json['gapsentence'] as String,
    json['answer_id1'] as int,
    json['answer_id2'] as int,
  );

  int? getGapsentence_id(){
    return gapsentence_id;
  }
  String? getGapsentece(){
    return gapsentence;
  }
  int? getAnswer_id1(){
    return answer_id1;
  }
  int? getAnswer_id2(){
    return answer_id2;
  }
}
