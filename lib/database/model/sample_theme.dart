import 'package:audis/database/model/sample.dart';

class SampleTheme extends Sample {

  int? theme_id;
  String? theme;

  /// @param res_id    ID of the resource in R.raw
  /// @param content   Description of the sample
  /// @param syllables number of syllables
  /// @param mood      0 = neutral; 1 = happy; 2 = sad
  /// @param speed     0 = slow; 1 = fast
  /// @param type      0 = word; 1 = sentence; 2 = question
  /// @param speaker   0 = male; 1 = female

  SampleTheme(String res_id, String content, int syllables, int mood, int speed, int type, int speaker,int theme_id, String theme)
  : super(res_id, content, syllables, mood, speed, type, speaker){
    this.theme_id = theme_id;
    this.theme = theme;
  }

  static SampleTheme fromJson(Map<String, Object?> json) => SampleTheme(
    json['res_id'] as String,
    json['content'] as String,
    json['syllables'] as int,
    json['mood'] as int,
    json['speed'] as int,
    json['type'] as int,
    json['speaker'] as int,
    json['theme_id']  as int,
    json['theme'] as String,
  );

  int? getThemeId(){
    return theme_id;
  }

  String? getTheme(){
    return (theme);
  }
}