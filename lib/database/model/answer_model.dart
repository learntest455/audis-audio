class AnswerModel{
  int answer_id;
  String answer;

  AnswerModel(this.answer_id, this.answer);

  static String fromJson(Map<String, Object?> json) => (
      json['answer'] as String
  );

}