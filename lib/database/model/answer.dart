class Answer {

  //text on Button
  String text = '';

  //some tasks require multiple answers in the correct order... this will hold the answers
  //position in the sequence as given from the user
  int order = -1;

  //this would be the correct order - as given by the task...
  int correctOrder = -1;


  static int expectedNumOfAnswers(List<Answer> answers) {
    if (answers == null) return 0;
    int num = 0;
    for(int i = 0; i< answers.length; i++) {
      if (answers[i].getCorrectOrder() != -1)
        num++;
    }
    return num;
  }


  /// Creates a new Answer Object for a questions with multiple answers that have 2 be in coorect order...
  /// @param answerText
  /// @param correctOrder
  Answer.constructor1(String answerText, int correctOrder){
    text = answerText;
    this.correctOrder = correctOrder;
  }

  /// Creates a new Answer Object for a binary question
  /// @param answerText - the text on the button
  /// @param correctAnswer - is this the correct answer to the questioin/task/exercise?
  Answer.constructor2(String answerText, bool correctAnswer){
    text = answerText;
    this.correctOrder = correctAnswer ? 0 : -1;
  }

  String getText(){
    return text;
  }

  //for a task with only one correct answer -1 is unselected, everything else is selected
  bool isSelected(){
    return (order != -1);
  }

  void setSelected(bool selected){
    if (!selected) order = -1;
    else order = 0;
  }

  //set & get the position in the sequence of answers the user gave...
  void setOrder(int sequencePosition){
    order = sequencePosition;
  }

  int getOrder(){
    return order;
  }

  int getCorrectOrder() {
    return correctOrder;
  }

  bool isCorrect() {
    return (order == correctOrder);
  }

}