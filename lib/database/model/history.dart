class History {
  late String timestamp;
  int? levelId;
  int? score;
  int? maxScore;
  String? content;
  String? answer;
  String? correctAnswer;

  History(String timestamp, int levelId, int score, int maxScore,
      String content, String answer, String correctAnswer) {
    this.timestamp = timestamp;
    this.levelId = levelId;
    this.score = score;
    this.maxScore = maxScore;
    this.content = content;
    this.answer = answer;
    this.correctAnswer = correctAnswer;
  }

  static History fromJson(Map<String, Object?> json) => History(
        json['timestamp'] == null ? '' : json['timestamp'] as String,
        json['level_id'] == null ? 0 : json['level_id'] as int,
        json['score'] == null ? 0 : json['score'] as int,
        json['maxScore'] == null ? 0 : json['maxScore'] as int,
        json['content'] == null ? '' : json['content'] as String,
        json['answer'] == null ? '' : json['answer'] as String,
        json['correctAnswer'] == null ? '' : json['correctAnswer'] as String,
      );

  static int fromJson2(Map<String, Object?> json) =>
      (json['score'] == null ? 0 : json['score'] as int);

  String getTimestamp() {
    return timestamp;
  }

  int? getLevelId() {
    return levelId;
  }

  int? getScore() {
    return score;
  }

  int? getMaxScore() {
    return maxScore;
  }

  String? getContent() {
    return content;
  }

  String? getAnswer() {
    return answer;
  }

  String? getCorrectAnswer() {
    return correctAnswer;
  }
}
