//=> speed {0 = slow; 1 = fast}
class Speed {
  static const int SLOW = 0;
  static const int FAST = 1;
}