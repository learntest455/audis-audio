class Sample{
  String res_id;
  String content;
  int syllables;
  int mood;
  int speed;
  int type;
  int speaker;

  /// @param res_id ID of the resource in R.raw
  /// @param content Description of the sample
  /// @param syllables number of syllables
  /// @param mood 0 = neutral; 1 = happy; 2 = sad
  /// @param speed 0 = slow; 1 = fast
  /// @param type 0 = word; 1 = sentence; 2 = question
  /// @param speaker 0 = male; 1 = female

  Sample(this.res_id, this.content, this.syllables, this.mood, this.speed, this.type, this.speaker);

  static Sample fromJson(Map<String, Object?> json) => Sample(
   json['res_id'] as String,
   json['content'] as String,
   json['syllables'] as int,
   json['mood'] as int,
   json['speed'] as int,
   json['type'] as int,
   json['speaker'] as int,
  );


  String getRes_id() {
    return res_id;
  }

  String getContent() {
    return content;
  }

  int getSyllables() {
    return syllables;
  }

  int getMood() {
    return mood;
  }

  int getSpeed() {
    return speed;
  }

  int getType() {
    return type;
  }

  int getSpeaker() {
    return speaker;
  }

}