class Rhyme{
  String res_id;
  int rhyme_group;

  Rhyme(this.res_id, this.rhyme_group);

  static int fromJson(Map<String, Object?> json) => (
      json['rhyme_group'] as int
  );

}