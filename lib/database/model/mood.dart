//=> mood {0 = neutral; 1 = fröhloch; 2 = traurig}
class Mood {
  static const int NEUTRAL = 0;
  static const int HAPPY = 1;
  static const int SAD = 2;
}