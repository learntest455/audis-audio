//=> type {0 = word; 1 = sentence; 2 = question}
class Type {
  static const int WORD = 0;
  static const int SENTENCE = 1;
  static const int QUESTION = 2;
  static const int EXP_QUESTION = 3;
  static const int NONSENSE = 4;
  static const int NUMBER = 5;
  static const int Noise = 6;
}