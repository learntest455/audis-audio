import 'package:sqflite/sqflite.dart';

import 'model/mood.dart';
import 'model/type.dart';
import 'model/speaker.dart';
import 'model/speed.dart';

class Numbers{

  Future insertNumbers(Database db) async {
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1854.mp3", "13", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1855.mp3", "14", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1856.mp3", "15", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1857.mp3", "16", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1858.mp3", "17", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1859.mp3", "18", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1860.mp3", "19", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1861.mp3", "20", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1862.mp3", "21", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1863.mp3", "22", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1864.mp3", "23", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1865.mp3", "24", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1866.mp3", "25", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1867.mp3", "26", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1868.mp3", "27", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1869.mp3", "28", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1870.mp3", "29", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1871.mp3", "30", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1872.mp3", "31", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1873.mp3", "32", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1874.mp3", "33", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1875.mp3", "34", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1876.mp3", "35", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1877.mp3", "36", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1878.mp3", "37", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1879.mp3", "38", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1880.mp3", "39", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1881.mp3", "40", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1882.mp3", "41", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1883.mp3", "42", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1884.mp3", "43", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1885.mp3", "44", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1886.mp3", "45", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1887.mp3", "46", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1888.mp3", "47", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1889.mp3", "48", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1890.mp3", "49", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1891.mp3", "50", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1892.mp3", "51", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1893.mp3", "52", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1894.mp3", "53", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1895.mp3", "54", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1896.mp3", "55", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1897.mp3", "56", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1898.mp3", "57", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1899.mp3", "58", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1900.mp3", "59", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1901.mp3", "60", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1902.mp3", "61", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1903.mp3", "62", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1904.mp3", "63", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1905.mp3", "64", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1906.mp3", "65", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1907.mp3", "66", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1908.mp3", "67", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1909.mp3", "68", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1910.mp3", "69", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1911.mp3", "70", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1912.mp3", "71", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1913.mp3", "72", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1914.mp3", "73", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1915.mp3", "74", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1916.mp3", "75", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1917.mp3", "76", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1918.mp3", "77", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1919.mp3", "78", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1920.mp3", "79", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1921.mp3", "80", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1922.mp3", "81", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1923.mp3", "82", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1924.mp3", "83", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1925.mp3", "84", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1926.mp3", "85", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1927.mp3", "86", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1928.mp3", "87", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1929.mp3", "88", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1930.mp3", "89", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1931.mp3", "90", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1932.mp3", "91", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1933.mp3", "92", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1934.mp3", "93", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1935.mp3", "94", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1936.mp3", "95", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1937.mp3", "96", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1938.mp3", "97", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1939.mp3", "98", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1940.mp3", "99", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.MALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1854.mp3", "13", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1855.mp3", "14", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1856.mp3", "15", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1857.mp3", "16", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1858.mp3", "17", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1859.mp3", "18", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1860.mp3", "19", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1861.mp3", "20", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1862.mp3", "21", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1863.mp3", "22", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1864.mp3", "23", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1865.mp3", "24", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1866.mp3", "25", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1867.mp3", "26", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1868.mp3", "27", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1869.mp3", "28", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1870.mp3", "29", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1871.mp3", "30", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1872.mp3", "31", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1873.mp3", "32", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1874.mp3", "33", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1875.mp3", "34", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1876.mp3", "35", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1877.mp3", "36", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1878.mp3", "37", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1879.mp3", "38", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1880.mp3", "39", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1881.mp3", "40", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1882.mp3", "41", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1883.mp3", "42", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1884.mp3", "43", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1885.mp3", "44", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1886.mp3", "45", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1887.mp3", "46", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1888.mp3", "47", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1889.mp3", "48", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1890.mp3", "49", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1891.mp3", "50", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1892.mp3", "51", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1893.mp3", "52", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1894.mp3", "53", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1895.mp3", "54", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1896.mp3", "55", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1897.mp3", "56", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1898.mp3", "57", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1899.mp3", "58", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1900.mp3", "59", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1901.mp3", "60", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1902.mp3", "61", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1903.mp3", "62", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1904.mp3", "63", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1905.mp3", "64", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1906.mp3", "65", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1907.mp3", "66", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1908.mp3", "67", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1909.mp3", "68", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1910.mp3", "69", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1911.mp3", "70", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1912.mp3", "71", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1913.mp3", "72", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1914.mp3", "73", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1915.mp3", "74", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1916.mp3", "75", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1917.mp3", "76", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1918.mp3", "77", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1919.mp3", "78", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1920.mp3", "79", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1921.mp3", "80", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1922.mp3", "81", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1923.mp3", "82", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1924.mp3", "83", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1925.mp3", "84", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1926.mp3", "85", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1927.mp3", "86", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1928.mp3", "87", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1929.mp3", "88", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1930.mp3", "89", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1931.mp3", "90", 2, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1932.mp3", "91", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1933.mp3", "92", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1934.mp3", "93", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1935.mp3", "94", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1936.mp3", "95", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1937.mp3", "96", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1938.mp3", "97", 5, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1939.mp3", "98", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1940.mp3", "99", 4, Mood.NEUTRAL, Speed.SLOW, Type.NUMBER, Speaker.FEMALE]
    );
    });
  }
}
