import 'package:sqflite/sqflite.dart';

class Consonants{

  Future insertConsonants(Database db) async {
    await db.transaction((txn) async {
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0081.mp3", 'b', 'd', 'Dach', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0085.mp3", 'b', 'h', 'Hahn', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0087.mp3", 'b', 'k', 'kalt', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0088.mp3", 'b', 'h', 'Hall', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0090.mp3", 'b', 'r', 'Rand', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0091.mp3", 'b', 'd', 'Dank', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0082.mp3", 'b', 'h', 'Haar', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0093.mp3", 'b', 't', 'Teer', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0094.mp3", 'b', 'h', 'hart', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0097.mp3", 'b', 'p', 'Pass', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0100.mp3", 'b', 'l', 'Lauch', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0105.mp3", 'b', 'r', 'Raum', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0108.mp3", 'b', 'w', 'Wein', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0111.mp3", 'b', 'f', 'Fett', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0118.mp3", 'b', 'g', 'Gier', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0119.mp3", 'b', 'm', 'mild', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0124.mp3", 'b', 'g', 'glatt', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0135.mp3", 'b', 'r', 'Rock', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0138.mp3", 'b', 't', 'Tod', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0139.mp3", 'b', 'm', 'Mord', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0141.mp3", 'b', 'r', 'Ross', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0143.mp3", 'b', 'r', 'Rauch', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0145.mp3", 'b', 'g', 'Graf', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0155.mp3", 'b', 'f', 'Frust', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0156.mp3", 'b', 't', 'Tuch', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0158.mp3", 'b', 's', 'Sucht', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0159.mp3", 'b', 'r', 'rund', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0166.mp3", 'b', 'k', 'Kuss', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0171.mp3", 'd', 'b', 'Bach', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0173.mp3", 'd', 'l', 'Lachs', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0174.mp3", 'd', 'k', 'Kamm', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0175.mp3", 'd', 'b', 'Bank', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0176.mp3", 'd', 'w', 'warm', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0177.mp3", 'd', 'r', 'Reich', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0179.mp3", 'd', 'l', 'Licht', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0180.mp3", 'd', 's', 'Sieb', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0182.mp3", 'd', 'r', 'Ring', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0184.mp3", 'd', 'k', 'Koch', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0185.mp3", 'd', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0187.mp3", 'd', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0190.mp3", 'd', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0193.mp3", 'd', 'g', 'Grad', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0194.mp3", 'd', 'r', 'Rang', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0195.mp3", 'd', 'sch', 'Schreck', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0198.mp3", 'd', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0199.mp3", 'd', 'l', 'Luft', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0200.mp3", 'd', 'k', 'Kunst', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0202.mp3", 'd', 'w', 'Wurst', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0226.mp3", 'f', 'sch', 'Schach', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0229.mp3", 'f', 'n', 'nackt', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0230.mp3", 'f', 'sch', 'Schall', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0233.mp3", 'f', 'h', 'Hass', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0234.mp3", 'f', 'm', 'Mast', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0236.mp3", 'f', 'l', 'Lachs', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0238.mp3", 'f', 's', 'See', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0244.mp3", 'f', 'w', 'Welt', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0246.mp3", 'f', 'h', 'hell', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0247.mp3", 'f', 'p', 'Pelz', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0251.mp3", 'f', 'g', 'gern', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0253.mp3", 'f', 'n', 'Nest', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0254.mp3", 'f', 'b', 'Bett', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0258.mp3", 'f', 't', 'Tisch', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0261.mp3", 'f', 'k', 'Kloß', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0264.mp3", 'f', 'sch', 'Schlucht', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0265.mp3", 'f', 'k', 'klug', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0269.mp3", 'f', 'sch', 'Schluss', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0271.mp3", 'f', 'g', 'Glut', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0272.mp3", 'f', 'sch', 'schön', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0276.mp3", 'f', 'r', 'rau', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0283.mp3", 'f', 't', 'trist', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0284.mp3", 'f', 'r', 'roh', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0286.mp3", 'f', 'r', 'Rost', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0291.mp3", 'f', 'l', 'Luchs', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0295.mp3", 'f', 'r', 'Ruß', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0299.mp3", 'g', 'r', 'Rang', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0300.mp3", 'g', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0303.mp3", 'g', 'm', 'Maß', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0305.mp3", 'g', 'r', 'Rast', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0313.mp3", 'g', 'z', 'Zelt', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0315.mp3", 'g', 'k', 'Kern', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0323.mp3", 'g', 'b', 'Bier', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0324.mp3", 'g', 'l', 'Lift', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0327.mp3", 'g', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0328.mp3", 'g', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0329.mp3", 'g', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0330.mp3", 'g', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0331.mp3", 'g', 'f', 'Flut', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0332.mp3", 'g', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0334.mp3", 'g', 'w', 'Wolf', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0336.mp3", 'g', 't', 'Trab', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0337.mp3", 'g', 'd', 'Draht', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0338.mp3", 'g', 'b', 'brav', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0339.mp3", 'g', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0342.mp3", 'g', 'r', 'rau', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0342.mp3", 'g', 'r', 'rau', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0343.mp3", 'g', 'r', 'Riff', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0347.mp3", 'g', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0349.mp3", 'g', 'r', 'Ruß', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0351.mp3", 'g', 'w', 'Wut', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0352.mp3", 'h', 'n', 'Narr', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0357.mp3", 'h', 'b', 'Bahn', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0358.mp3", 'h', 'w', 'Wall', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0360.mp3", 'h', 'l', 'Land', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0364.mp3", 'h', 'l', 'lang', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0365.mp3", 'h', 'b', 'Bart', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0367.mp3", 'h', 'f', 'Fass', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0369.mp3", 'h', 'm', 'Maus', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0372.mp3", 'h', 'l', 'laut', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0373.mp3", 'h', 'r', 'Recht', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0374.mp3", 'h', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0376.mp3", 'h', 'w', 'Welt', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0378.mp3", 'h', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0380.mp3", 'h', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0381.mp3", 'h', 'w', 'Wert', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0382.mp3", 'h', 'l', 'leer', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0383.mp3", 'h', 'm', 'März', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0384.mp3", 'h', 'n', 'neu', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0386.mp3", 'h', 'b', 'Bier', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0389.mp3", 'h', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0391.mp3", 'h', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0398.mp3", 'h', 'm', 'Mund', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0400.mp3", 'h', 'w', 'Wut', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0413.mp3", 'k', 'b', 'bald', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0414.mp3", 'k', 'd', 'Damm', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0425.mp3", 'k', 'l', 'Lauf', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0429.mp3", 'k', 'r', 'Reim', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0432.mp3", 'k', 'g', 'gern', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0434.mp3", 'k', 'f', 'fies', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0435.mp3", 'k', 'r', 'Rind', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0438.mp3", 'k', 's', 'Sinn', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0449.mp3", 'k', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0455.mp3", 'k', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0456.mp3", 'k', 'l', 'Leid', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0461.mp3", 'k', 'l', 'Los', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0465.mp3", 'k', 'n', 'nie', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0467.mp3", 'k', 'd', 'doch', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0472.mp3", 'k', 't', 'Topf', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0474.mp3", 'k', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0476.mp3", 'k', 'p', 'Post', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0478.mp3", 'k', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0479.mp3", 'k', 'sch', 'Schrank', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0482.mp3", 'k', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0482.mp3", 'k', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0483.mp3", 'k', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0484.mp3", 'k', 'p', 'Preis', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0486.mp3", 'k', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0488.mp3", 'k', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0492.mp3", 'k', 'sch', 'Schuh', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0494.mp3", 'k', 'p', 'Pult', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0495.mp3", 'k', 'd', 'Dunst', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0497.mp3", 'k', 'b', 'Bus', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0499.mp3", 'l', 'w', 'Wachs', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0500.mp3", 'l', 's', 'Sack', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0501.mp3", 'l', 'r', 'Rahm', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0502.mp3", 'l', 'd', 'Damm', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0504.mp3", 'l', 'h', 'Hand', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0508.mp3", 'l', 'h', 'Hang', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0510.mp3", 'l', 'm', 'Mast', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0512.mp3", 'l', 'r', 'Raub', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0513.mp3", 'l', 'b', 'Bauch', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0514.mp3", 'l', 'k', 'Kauf', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0515.mp3", 'l', 'r', 'raus', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0516.mp3", 'l', 'm', 'Maut', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0519.mp3", 'l', 'm', 'Meer', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0521.mp3", 'l', 's', 'seicht', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0522.mp3", 'l', 'n', 'Neid', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0523.mp3", 'l', 'r', 'Reim', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0524.mp3", 'l', 'n', 'nicht', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0526.mp3", 'l', 's', 'Sieb', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0531.mp3", 'l', 'g', 'Gift', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0534.mp3", 'l', 'n', 'noch', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0538.mp3", 'l', 't', 'Ton', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0539.mp3", 'l', 'r', 'Rock', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0541.mp3", 'l', 'm', 'Moos', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0543.mp3", 'l', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0547.mp3", 'm', 'n', 'Nacht', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0550.mp3", 'm', 'r', 'Reis', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0553.mp3", 'm', 'w', 'wann', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0557.mp3", 'm', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0559.mp3", 'm', 'h', 'Herz', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0561.mp3", 'm', 'g', 'Gas', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0563.mp3", 'm', 'l', 'Last', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0565.mp3", 'm', 'h', 'Haus', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0566.mp3", 'm', 'l', 'laut', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0569.mp3", 'm', 'h', 'Herr', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0574.mp3", 'm', 'n', 'nett', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0578.mp3", 'm', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0580.mp3", 'm', 'b', 'Bild', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0582.mp3", 'm', 'l', 'List', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0589.mp3", 'm', null, null, 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0592.mp3", 'm', 'r', 'Rohr', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0593.mp3", 'm', 'l', 'Los', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0594.mp3", 'm', 'w', 'Wort', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0599.mp3", 'm', 'w', 'wund', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0602.mp3", 'm', 'h', 'Hut', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0605.mp3", 'n', 'm', 'Macht', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0607.mp3", 'n', 'f', 'Fakt', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0610.mp3", 'n', 'r', 'Rat', 0]
      );
      await txn.execute(
          "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
          ["raw/m0612.mp3", 'n', 'h', 'Haar', 0]
      );

      _insertConsonants2(txn);

      // insert all consonants rows for female samples based on the male ones by matching content...
      await txn.execute(
          "INSERT INTO consonants SELECT females.res_id, males.consonant, males.alternateConsonant,"
              " males.minPair, males.startEnd FROM (SELECT * FROM samples NATURAL JOIN consonants WHERE speaker = 0 AND type = 0) "
              "AS males JOIN (SELECT * FROM samples WHERE speaker <> 0 AND type = 0) AS females ON males.content = females.content");
    });
  }

  Future _insertConsonants2(Transaction db) async {
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0614.mp3", 'n', 'h', 'Hass', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0616.mp3", 'n', 'l', 'Leid', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0617.mp3", 'n', 'r', 'Rest', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0618.mp3", 'n', 'm', 'Mett', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0619.mp3", 'n', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0620.mp3", 'n', 'h', 'Heu', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0621.mp3", 'n', 'l', 'Licht', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0623.mp3", 'n', 'l', 'Loch', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0626.mp3", 'n', 'r', 'rot', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0629.mp3", 'n', 'b', 'Bus', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0644.mp3", 'p', 't', 'Takt', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0648.mp3", 'p', 's', 'Sarg', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0651.mp3", 'p', 'b', 'Bass', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0655.mp3", 'p', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0656.mp3", 'p', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0657.mp3", 'p', 't', 'Test', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0670.mp3", 'p', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0672.mp3", 'p', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0673.mp3", 'p', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0677.mp3", 'p', 'k', 'Kost', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0678.mp3", 'p', 'k', 'Kreis', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0680.mp3", 'p', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0685.mp3", 'p', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0686.mp3", 'p', 'k', 'Kult', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0687.mp3", 'p', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0693.mp3", 'r', 'n', 'Naht', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0699.mp3", 'r', 'l', 'Land', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0700.mp3", 'r', 'g', 'Gang', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0704.mp3", 'r', 'g', 'Gast', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0706.mp3", 'r', 'g', 'grau', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0707.mp3", 'r', 'l', 'Laub', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0708.mp3", 'r', 't', 'Traum', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0710.mp3", 'r', 'l', 'Laus', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0711.mp3", 'r', 'h', 'Hecht', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0718.mp3", 'r', 't', 'Tee', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0719.mp3", 'r', 'd', 'Deich', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0720.mp3", 'r', 'l', 'Leim', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0721.mp3", 'r', 'p', 'Preis', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0725.mp3", 'r', 'n', 'Nest', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0729.mp3", 'r', 'g', 'Griff', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0730.mp3", 'r', 'k', 'Kind', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0731.mp3", 'r', 'd', 'Ding', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0733.mp3", 'r', 'b', 'Bock', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0734.mp3", 'r', 'f', 'froh', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0735.mp3", 'r', 'm', 'Moor', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0740.mp3", 'r', 'f', 'Frost', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0741.mp3", 'r', 'n', 'Not', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0746.mp3", 'r', 'b', 'bunt', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0747.mp3", 's', 'p', 'Pack', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0748.mp3", 's', 'h', 'Haft', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0750.mp3", 's', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0753.mp3", 's', 'w', 'Wand', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0754.mp3", 's', 'p', 'Park', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0755.mp3", 's', 'sch', 'Schatz', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0756.mp3", 's', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0758.mp3", 'sch', 'f', 'Fach', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0759.mp3", 'sch', 'm', 'Macht', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0760.mp3", 'sch', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0761.mp3", 'sch', 'f', 'Fall', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0762.mp3", 'sch', 'h', 'Haar', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0764.mp3", 'sch', 's', 'Satz', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0766.mp3", 'sch', 'b', 'Baum', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0768.mp3", 'sch', 'w', 'weich', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0769.mp3", 'sch', 'w', 'Wein', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0771.mp3", 'sch', 'h', 'Herz', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0772.mp3", 'sch', 'h', 'Heu', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0774.mp3", 'sch', 's', 'Sicht', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0776.mp3", 'sch', 'r', 'Riff', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0777.mp3", 'sch', 'w', 'wild', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0785.mp3", 'sch', 'l', 'Lauch', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0788.mp3", 'sch', 'f', 'Flucht', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0789.mp3", 'sch', 'f', 'Fluss', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0792.mp3", 'sch', 'm', 'März', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0802.mp3", 'sch', 's', 'Sohn', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0804.mp3", 'sch', 'k', 'krank', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0806.mp3", 'sch', 'd', 'Dreck', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0811.mp3", 'sch', 'k', 'Kuh', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0819.mp3", 'sch', 'n', 'Nuss', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0822.mp3", 'sch', 'w', 'warm', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0825.mp3", 'sch', 'w', 'weiß', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0826.mp3", 'sch', 'w', 'Wert', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0832.mp3", 's', 'f', 'Fee', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0835.mp3", 's', 'b', 'Beil', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0837.mp3", 's', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0841.mp3", 's', 'sch', 'Schicht', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0842.mp3", 's', 'h', 'Hieb', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0843.mp3", 's', 'k', 'Kinn', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0845.mp3", 's', 'w', 'Witz', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0850.mp3", 's', 'sch', 'schon', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0879.mp3", 'sch', 't', 'Tat', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0886.mp3", 'sch', 't', 'taub', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0893.mp3", 'sch', 't', 'Tier', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0912.mp3", 'sch', 't', 'Turm', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0913.mp3", 's', 'b', 'Bucht', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0920.mp3", 't', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0921.mp3", 't', 'p', 'Pakt', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0922.mp3", 't', 's', 'Saal', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0931.mp3", 't', 'b', 'Bad', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0932.mp3", 't', 'sch', 'Staub', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0935.mp3", 't', 'r', 'Reh', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0938.mp3", 't', 'h', 'Herr', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0939.mp3", 't', 'd', 'Deich', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0941.mp3", 't', 'b', 'Beil', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0947.mp3", 't', 'p', 'Pest', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0949.mp3", 't', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0951.mp3", 't', 'h', 'hier', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0952.mp3", 't', 'f', 'Fisch', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0955.mp3", 't', 'b', 'Boot', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0957.mp3", 't', 'l', 'Lohn', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0959.mp3", 't', 'k', 'Kopf', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0960.mp3", 't', 'r', 'Rohr', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0963.mp3", 't', 'r', 'Raum', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0966.mp3", 't', 'r', 'Ritt', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0969.mp3", 't', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0970.mp3", 't', null, null, 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0971.mp3", 't', 'b', 'Buch', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0973.mp3", 't', 'k', 'Kür', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0976.mp3", 't', 'w', 'Wurm', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0989.mp3", 'w', 'f', 'Fach', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0990.mp3", 'w', 'l', 'Lachs', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0991.mp3", 'w', 's', 'Saal', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0992.mp3", 'w', 'h', 'Hahn', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0993.mp3", 'w', 'b', 'bald', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0994.mp3", 'w', 'f', 'Fall', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0995.mp3", 'w', 's', 'Sand', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0998.mp3", 'w', 'sch', 'Schwarm', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1007.mp3", 'w', 'sch', 'Scheich', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1009.mp3", 'w', 'sch', 'Schein', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1011.mp3", 'w', 'sch', 'Schweiß', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1012.mp3", 'w', 'l', 'Leid', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1014.mp3", 'w', 'f', 'Feld', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1017.mp3", 'w', 'sch', 'Schwert', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1021.mp3", 'w', 'sch', 'Schild', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1022.mp3", 'w', 'r', 'Rind', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1026.mp3", 'w', 's', 'Sitz', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1027.mp3", 'w', 'g', 'Golf', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1029.mp3", 'w', 'm', 'Mord', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1030.mp3", 'w', 'f', 'Frack', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1031.mp3", 'w', 'h', 'Hund', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1033.mp3", 'w', 't', 'Turm', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1034.mp3", 'w', 'd', 'Durst', 0]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1036.mp3", 'w', 'm', 'Mut', 0]
    );
    _insertConsonants3(db);
  }

  Future _insertConsonants3(Transaction db) async {
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0039.mp3", 'l', 's', 'Aas', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0040.mp3", 's', 'l', 'Aal', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0062.mp3", 'm', 't', 'Art', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0072.mp3", 'ch', 's', 'aus', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0075.mp3", 's', 'ch', 'auch', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0081.mp3", 'ch', 's', 'Bass', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0088.mp3", 'l', 'd', 'Bald', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0090.mp3", 'd', 'k', 'Bank', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0091.mp3", 'k', 'd', 'Band', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0097.mp3", 's', 'ch', 'Bach', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0100.mp3", 'ch', 'm', 'Baum', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0105.mp3", 'm', 'ch', 'Bauch', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0111.mp3", 't', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0141.mp3", 's', 'k', 'Bock', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0143.mp3", 'ch', 't', 'Braut', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0144.mp3", 't', 'ch', 'Brauch', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0147.mp3", 'f', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0166.mp3", 's', 'sch', 'Busch', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0167.mp3", 'sch', 's', 'Bus', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0168.mp3", 'f', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0174.mp3", 'm', 'n', 'dann', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0176.mp3", 'm', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0180.mp3", 'b', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0182.mp3", 'ng', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0184.mp3", 'ch', 't', 'Docht', 1]
    );

    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0185.mp3", 't', 'ch', 'doch', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0190.mp3", 'f', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0194.mp3", 'ng', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0198.mp3", 'k', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0226.mp3", 'ch', 's', 'Fass', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0230.mp3", 'l', 'ch', 'Fach', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0233.mp3", 's', 'ch', 'Fach', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0234.mp3", 't', 's', 'Fass', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0244.mp3", 'd', 'l', 'Fell', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0246.mp3", 'l', 'd', 'Feld', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0247.mp3", 's', 'l', 'Fell', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0258.mp3", 'sch', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0259.mp3", 'k', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0260.mp3", 'sch', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0265.mp3", 'g', 't', 'Flut', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0271.mp3", 't', 'g', 'Flug', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0282.mp3", 'sch', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0285.mp3", 'sch', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0299.mp3", 'ng', 's', 'Gans', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0300.mp3", 's', 'g', 'Gang', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0312.mp3", 'b', 'd', 'Geld', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0313.mp3", 'd', 'b', 'gelb', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0328.mp3", 'ch', 's', 'Gleis', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0329.mp3", 's', 'ch', 'gleich', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0332.mp3", 'd', 'f', 'Golf', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0334.mp3", 'f', 'd', 'Gold', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0336.mp3", 'b', 'd', 'Grad', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0337.mp3", 'd', 'b', 'Grab', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0338.mp3", 'f', 's', 'Gras', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0341.mp3", 's', 'f', 'Graf', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0343.mp3", 'f', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0358.mp3", 'l', 's', 'Hals', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0360.mp3", 'd', 'ng', 'Hang', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0364.mp3", 'ng', 'd', 'Hand', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0367.mp3", 's', 'l', 'Hall', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0369.mp3", 's', 't', 'Haut', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0372.mp3", 't', 's', 'Haus', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0376.mp3", 'd', 'm', 'Helm', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0378.mp3", 'm', 'd', 'Held', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0425.mp3", 'f', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0435.mp3", 'd', 'n', 'Kinn', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0438.mp3", 'n', 'd', 'Kind', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0449.mp3", 'ng', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0456.mp3", 'd', 'n', 'klein', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0458.mp3", 'n', 'd', 'Kleid', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0472.mp3", 'f', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0474.mp3", 'b', 'k', 'Kork', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0475.mp3", 'k', 'b', 'Korb', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0499.mp3", 's', 'k', 'Lack', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0500.mp3", 'k', 's', 'Lachs', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0504.mp3", 'd', 'ng', 'lang', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0508.mp3", 'ng', 'd', 'Land', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0509.mp3", 'm', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0512.mp3", 'b', 't', 'laut', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0513.mp3", 'ch', 'f', 'Lauf', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0514.mp3", 'f', 'ch', 'Lauch', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0515.mp3", 's', 'f', 'Lauf', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0516.mp3", 't', 'b', 'Laub', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0522.mp3", 'd', 'm', 'Leim', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0523.mp3", 'm', 'd', 'Leid', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0526.mp3", 'b', 'd', 'Lied', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0529.mp3", 'd', 'b', 'lieb', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0534.mp3", 'ch', 'k', 'Lok', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0538.mp3", 'n', 's', 'Los', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0539.mp3", 'k', 'ch', 'Loch', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0541.mp3", 's', 'n', 'Lohn', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0553.mp3", 'n', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0556.mp3", 'k', 't', 'Markt', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0557.mp3", 't', 'k', 'Mark', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0565.mp3", 's', 't', 'Maut', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0566.mp3", 't', 's', 'Maus', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0570.mp3", 'l', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0578.mp3", 'ch', 'd', 'mild', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0580.mp3", 'd', 'ch', 'Milch', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0633.mp3", 'l', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0685.mp3", 's', 't', 'Pult', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0686.mp3", 't', 's', 'Puls', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0699.mp3", 'd', 'ng', 'Rang', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0700.mp3", 'ng', 'd', 'Rand', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0707.mp3", 'b', 'm', 'Raum', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0708.mp3", 'm', 'b', 'Raub', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0710.mp3", 's', 'm', 'Raum', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0719.mp3", 'ch', 's', 'Reis', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0720.mp3", 'm', 's', 'Reis', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0721.mp3", 's', 'ch', 'Reich', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0730.mp3", 'd', 'ng', 'Ring', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0731.mp3", 'ng', 'd', 'Rind', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0733.mp3", 'k', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0744.mp3", 'f', 'm', 'Ruhm', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0745.mp3", 'm', 'f', 'Ruf', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0758.mp3", 'ch', 't', 'Schacht', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0759.mp3", 't', 'ch', 'Schach', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0768.mp3", 'ch', 'n', 'Schein', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0769.mp3", 'n', 'ch', 'Scheich', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0776.mp3", 'f', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0777.mp3", 'd', 'f', 'Schilf', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0778.mp3", 'f', 'd', 'Schild', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0780.mp3", 'f', 'g', 'Schlag', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0782.mp3", 'g', 'f', 'Schlaf', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0806.mp3", 'k', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0824.mp3", 'n', 's', 'Schweiß', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0825.mp3", 's', 'n', 'Schwein', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0831.mp3", 'ng', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0835.mp3", 'l', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0842.mp3", 'b', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0863.mp3", 'l', 's', 'Spieß', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0868.mp3", 's', 'l', 'Spiel', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0875.mp3", 'ng', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0879.mp3", 't', 'b', 'Stab', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0880.mp3", 'b', 't', 'Staat', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0885.mp3", 't', 'k', 'stark', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0886.mp3", 'b', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0890.mp3", 'n', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0908.mp3", 'm', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0909.mp3", 'k', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0910.mp3", 'l', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0912.mp3", 'm', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0920.mp3", 'g', 't', 'Tat', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0922.mp3", 'l', 'g', 'Tag', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0931.mp3", 't', 'g', 'Tag', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0932.mp3", 'b', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0939.mp3", 'ch', 'l', 'Teil', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0940.mp3", 'g', 'ch', 'Teich', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0941.mp3", 'l', 'ch', 'Teich', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0952.mp3", 'sch', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0955.mp3", 'd', 'n', 'Ton', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0957.mp3", 'n', 'd', 'Tod', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0959.mp3", 'f', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0989.mp3", 'ch', 'n', 'wann', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0991.mp3", 'l', 'n', 'Wahn', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0992.mp3", 'n', 'l', 'Wahl', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0993.mp3", 'd', 'l', 'Wall', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0994.mp3", 'l', 'd', 'Wald', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m0995.mp3", 'd', 'n', 'wann', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1007.mp3", 'ch', 's', 'weiß', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1009.mp3", 'n', 'ch', 'weich', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1011.mp3", 's', 'ch', 'weich', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1012.mp3", 't', 's', 'weiß', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1015.mp3", 'k', 't', 'Wert', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1017.mp3", 't', 'k', 'Werk', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1027.mp3", 'f', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1031.mp3", 'd', 'sch', 'Wunsch', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1032.mp3", 'sch', 'd', 'wund', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1038.mp3", 'l', 'm', 'zahm', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1039.mp3", 'm', 'n', 'Zahn', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1040.mp3", 'n', 'l', 'Zahl', 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1052.mp3", 'l', null, null, 1]
    );
    await db.execute(
        "INSERT INTO consonants ('res_id', 'consonant', 'alternateConsonant', 'minPair', 'startEnd') values (?, ?, ?, ?, ?)",
        ["raw/m1060.mp3", 'ng', null, null, 1]
    );
  }
}