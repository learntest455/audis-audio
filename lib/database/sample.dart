import 'package:audis/database/sample_child.dart';
import 'package:audis/database/sample_female.dart';
import 'model/mood.dart';
import 'model/type.dart';
import 'model/speaker.dart';
import 'model/speed.dart';
import 'package:sqflite/sqflite.dart';

class Sample{

  Future<void> createSampleTable(Database db) async {
    //nonsense words
    //=> mood {0 = neutral; Mood.HAPPY = fröhloch; 2 = traurig}
    //=> speed {0 = slow; Mood.HAPPY = fast}
    //=> type {0 = word; Mood.HAPPY = sentence; 2 = question}
    //=> speaker {0 = male, Mood.HAPPY = female, 2 = child}

    _insertNonsenseWords(db);

    _insertFirst200SamplesMale(db);
    _insertSample201to400Male(db);
    _insertSample401to600Male(db);


    _insertSamples601to800Male(db);
    _insertSamples801to1000Male(db);
    _insertSamples1001to1200Male(db);
    _insertSamples1201to1400Male(db);
    _insertSamples1401to1600Male(db);
    _insertSamples1601toEndMale(db);
    SampleFemale().createSampleTableFemale(db);
    SampleChild().insertSamplesOfChild(db);

    await db.transaction((txn) async {
        await txn.execute(
            "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
            ["raw/noise_restaurant.mp3", 'Noise_Restaurant', 0, 0, 0, Type.Noise, 0]);
        await txn.execute(
            "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
            ["raw/noise_pool.mp3", 'Noise_Pool', 0, 0, 0, Type.Noise, 0]);
        await txn.execute(
            "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
            ["raw/noise_shoppingmall.mp3", 'Noise_ShoppingMall', 0, 0, 0, Type.Noise, 0]);
        await txn.execute(
            "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
            ["raw/noise_car.mp3", 'Noise_Car', 0, 0, 0, Type.Noise, 0]);
      });
    }
  }

  Future _insertNonsenseWords(Database db) async {
    await db.transaction((txn) async {
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0001.mp3", 'Bat', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0002.mp3", 'Bet', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0003.mp3", 'Bit', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0004.mp3", 'Bot', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0005.mp3", 'But', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0006.mp3", 'Bät', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0007.mp3", 'Böt', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0008.mp3", 'Büt', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0009.mp3", 'Baat', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0010.mp3", 'Beet', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0011.mp3", 'Biit', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0012.mp3", 'Boot', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0013.mp3", 'Buut', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0014.mp3", 'Bäät', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0015.mp3", 'Bööt', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0016.mp3", 'Büüt', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0017.mp3", 'Baut', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0018.mp3", 'Beit', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0019.mp3", 'Beut', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0020.mp3", 'Aba', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0021.mp3", 'Ada', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0022.mp3", 'Afa', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0023.mp3", 'Aga', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0024.mp3", 'Aha', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0025.mp3", 'Aja', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0026.mp3", 'Aka', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0027.mp3", 'Ala', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0028.mp3", 'Ama', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0029.mp3", 'Ana', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0030.mp3", 'Apa', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0031.mp3", 'Ara', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0032.mp3", 'Asa', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0033.mp3", 'Ata', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0034.mp3", 'Awa', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0035.mp3", 'Acha', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0036.mp3", 'Ascha', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0037.mp3", 'Anga', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0038.mp3", 'Aßa', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.MALE]);

      //-------------------FEMALE-----------------------------------------
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0001.mp3", 'Bat', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0002.mp3", 'Bet', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0003.mp3", 'Bit', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0004.mp3", 'Bot', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0005.mp3", 'But', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0006.mp3", 'Bät', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0007.mp3", 'Böt', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0008.mp3", 'Büt', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0009.mp3", 'Baat', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0010.mp3", 'Beet', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0011.mp3", 'Biit', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0012.mp3", 'Boot', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0013.mp3", 'Buut', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0014.mp3", 'Bäät', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0015.mp3", 'Bööt', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0016.mp3", 'Büüt', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0017.mp3", 'Baut', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0018.mp3", 'Beit', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0019.mp3", 'Beut', Mood.HAPPY, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0020.mp3", 'Aba', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f002Mood.HAPPY.mp3", 'Ada', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0022.mp3", 'Afa', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0023.mp3", 'Aga', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0024.mp3", 'Aha', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0025.mp3", 'Aja', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0026.mp3", 'Aka', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0027.mp3", 'Ala', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0028.mp3", 'Ama', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0029.mp3", 'Ana', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0030.mp3", 'Apa', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0031.mp3", 'Ara', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0032.mp3", 'Asa', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0033.mp3", 'Ata', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0034.mp3", 'Awa', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0035.mp3", 'Acha', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0036.mp3", 'Ascha', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0037.mp3", 'Anga', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/f0038.mp3", 'Aßa', Mood.SAD, 0, 0, Type.NONSENSE, Speaker.FEMALE]);
    });
  }

  //Male realword samples
  Future _insertFirst200SamplesMale(Database db) async{
    //real words
    await db.transaction((txn) async {
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0039.mp3", 'Aal', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0040.mp3", 'Aas', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0041.mp3", 'Abendkleid', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0042.mp3", 'Abteilung', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0043.mp3", 'Achterbahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0044.mp3", 'Afrika', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0045.mp3", 'Akt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0046.mp3", 'Akustiker', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0047.mp3", 'Allergie', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0048.mp3", 'Altenpfleger', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0049.mp3", 'Altstadt', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0050.mp3", 'Amerika', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0051.mp3", 'Ampel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0052.mp3", 'Amt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0053.mp3", 'Ananas', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0054.mp3", 'Angst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0055.mp3", 'Anwalt', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0056.mp3", 'Anzeige', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0057.mp3", 'Anzug', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0058.mp3", 'Apfel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0059.mp3", 'Apfelsaft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0060.mp3", 'Apotheke', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0061.mp3", 'April', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0062.mp3", 'Arm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0063.mp3", 'Armband', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0064.mp3", 'Armlehne', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0065.mp3", 'Art', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0066.mp3", 'Arzt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0067.mp3", 'Arztpraxis', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0068.mp3", 'Asien', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0069.mp3", 'Ast', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0070.mp3", 'Astronaut', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0071.mp3", 'Atemnot', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0072.mp3", 'auch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0073.mp3", 'Auge', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0074.mp3", 'August', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0075.mp3", 'aus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0076.mp3", 'Ausflugsziel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0077.mp3", 'Australien', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0078.mp3", 'Auto', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0079.mp3", 'Autobahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0080.mp3", 'Axt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0081.mp3", 'Bach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0082.mp3", 'Bäcker', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0083.mp3", 'Backofen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0084.mp3", 'Badesee', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0085.mp3", 'Bahn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0086.mp3", 'Bahnhof', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0087.mp3", 'bald', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0088.mp3", 'Ball', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0089.mp3", 'Banane', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0090.mp3", 'Band', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0091.mp3", 'Bank', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0092.mp3", 'bar', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0093.mp3", 'Bär', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0094.mp3", 'Bart', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0095.mp3", 'Base', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0096.mp3", 'Basketball', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0097.mp3", 'Bass', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0098.mp3", 'Batterie', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0099.mp3", 'Bauarbeiter', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0100.mp3", 'Bauch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0101.mp3", 'Bauchschmerzen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0102.mp3", 'Bauer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0103.mp3", 'Bauernhof', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0104.mp3", 'Bauklötze', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0105.mp3", 'Baum', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0106.mp3", 'Bausparen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0107.mp3", 'Baustelle', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0108.mp3", 'Bein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0109.mp3", 'Berg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0110.mp3", 'Bergsteigen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0111.mp3", 'Bett', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0112.mp3", 'Bettdecke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0113.mp3", 'Beutel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0114.mp3", 'Beuteltier', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0115.mp3", 'Bewerbung', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0116.mp3", 'Bibliothek', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0117.mp3", 'Bienenkorb', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0118.mp3", 'Bier', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0119.mp3", 'Bild', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0120.mp3", 'Bilderbuch', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0121.mp3", 'Bildschirm', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0122.mp3", 'Birne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0123.mp3", 'Blase', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0124.mp3", 'Blatt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0125.mp3", 'blau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0126.mp3", 'Blech', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0127.mp3", 'Blei', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0128.mp3", 'Blitz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0129.mp3", 'Blick', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      //await txn.execute(
      //     "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
      //     ["raw/m0130 + ", 'Blitz', 1, Mood.NEUTRAL, "+ Speed.SLOW + ", " + Type.WORD.mp3", " + Speaker.MALE + ")");   <-- ist auch Blitz, wie m0128
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0131.mp3", 'Block', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0132.mp3", 'Blumenbeet', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0133.mp3", 'Blumentopf', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0134.mp3", 'Bluse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0135.mp3", 'Bock', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0136.mp3", 'Boden', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0137.mp3", 'Bohrmaschine', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0138.mp3", 'Boot', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0139.mp3", 'Bord', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0140.mp3", 'böse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0141.mp3", 'Boss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0142.mp3", 'Bratensatz', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0143.mp3", 'Brauch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0144.mp3", 'Braut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0145.mp3", 'brav', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0146.mp3", 'Brett', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0147.mp3", 'Brief', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0148.mp3", 'Briefkasten', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0149.mp3", 'Brillenglas', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0150.mp3", 'Brot', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0151.mp3", 'Brötchen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0152.mp3", 'Brotkorb', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0153.mp3", 'Bruch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0154.mp3", 'Bruder', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0155.mp3", 'Brust', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0156.mp3", 'Buch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0157.mp3", 'Bücher', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0158.mp3", 'Bucht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0159.mp3", 'Bund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0160.mp3", 'Bundestag', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0161.mp3", 'Buntstift', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0162.mp3", 'Burg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0163.mp3", 'Bürgersteig', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0164.mp3", 'Burggraben', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0165.mp3", 'Büro', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0166.mp3", 'Bus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0167.mp3", 'Busch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0168.mp3", 'Chef', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0169.mp3", 'Chor', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0170.mp3", 'Computer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0171.mp3", 'Dach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0172.mp3", 'Dachboden', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0173.mp3", 'Dachs', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0174.mp3", 'Damm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0175.mp3", 'Dank', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0176.mp3", 'Darm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0177.mp3", 'Deich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0178.mp3", 'Dezember', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0179.mp3", 'dicht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0180.mp3", 'Dieb', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0181.mp3", 'Dienstag', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0182.mp3", 'Ding', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0183.mp3", 'Disziplin', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0184.mp3", 'doch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0185.mp3", 'Docht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0186.mp3", 'Dokument', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0187.mp3", 'Dom', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0188.mp3", 'Donner', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0189.mp3", 'Donnerstag', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0190.mp3", 'Dorf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0191.mp3", 'Dose', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0192.mp3", 'Drache', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0193.mp3", 'Draht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0194.mp3", 'Drang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0195.mp3", 'Dreck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0196.mp3", 'Drehstul', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0197.mp3", 'Dreirad', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0198.mp3", 'Druck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0199.mp3", 'Duft', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0200.mp3", 'Dunst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    });
  }

  Future _insertSample201to400Male(Database db) async{
    await db.transaction((txn) async {
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0201.mp3", 'Durchschnittswert', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0202.mp3", 'Durst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0203.mp3", 'Dusche', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0204.mp3", 'Duschvorhang', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0205.mp3", 'Ehebett', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0206.mp3", 'Ei', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0207.mp3", 'Einbruch', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0208.mp3", 'Eis', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0209.mp3", 'Eisbär', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0210.mp3", 'Eisdiele', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0211.mp3", 'Eisenbahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0212.mp3", 'Eiskugel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0213.mp3", 'Eiskunstlauf', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0214.mp3", 'Elektriker', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0215.mp3", 'Elfen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0216.mp3", 'Ende', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0217.mp3", 'Enkel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0218.mp3", 'Ente', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0219.mp3", 'Ententeich', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0220.mp3", 'Ertxneere', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0221.mp3", 'Erdgeschoss', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0222.mp3", 'Erzieherin', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0223.mp3", 'Ethik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0224.mp3", 'Europa', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0225.mp3", 'Fabrik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0226.mp3", 'Fach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0227.mp3", 'Fahrbahn', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0228.mp3", 'Fahrradständer', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0229.mp3", 'Fakt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0230.mp3", 'Fall', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0231.mp3", 'Fallschirmsprung', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0232.mp3", 'Familie', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0233.mp3", 'Fass', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0234.mp3", 'fast', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0235.mp3", 'Faust', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0236.mp3", 'Fax', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0237.mp3", 'Februar', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0238.mp3", 'Fee', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0239.mp3", 'Feierabend', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0240.mp3", 'Feiertag', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0241.mp3", 'Feige', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0242.mp3", 'Feigenbaum', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0243.mp3", 'Feind', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0244.mp3", 'Feld', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0245.mp3", 'Feldsalat', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0246.mp3", 'Fell', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0247.mp3", 'Fels', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0248.mp3", 'Fenster', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0249.mp3", 'Fensterbrett', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0250.mp3", 'Ferien', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0251.mp3", 'fern', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0252.mp3", 'Fernseher', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0253.mp3", 'Fest', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0254.mp3", 'Fett', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0255.mp3", 'Feuerwehr', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0256.mp3", 'Film', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0257.mp3", 'Finger', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0258.mp3", 'Fisch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0259.mp3", 'Fleck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0260.mp3", 'Fleisch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0261.mp3", 'Floß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0262.mp3", 'Flöte', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0263.mp3", 'Fluch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0264.mp3", 'Flucht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0265.mp3", 'Flug', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0266.mp3", 'Flughafen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0267.mp3", 'Flugzeug', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0268.mp3", 'Flur', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0269.mp3", 'Fluss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0270.mp3", 'Flussufer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0271.mp3", 'Flut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0272.mp3", 'Föhn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0273.mp3", 'Form', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0274.mp3", 'Fotograf', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0275.mp3", 'Fracht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0276.mp3", 'Frau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0277.mp3", 'frech', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0278.mp3", 'Freitag', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0279.mp3", 'Freizeit', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0280.mp3", 'Fremdsprache', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0281.mp3", 'Freund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0282.mp3", 'frisch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0283.mp3", 'Frist', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0284.mp3", 'froh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0285.mp3", 'Frosch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0286.mp3", 'Frost', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0287.mp3", 'Frucht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0288.mp3", 'Frühjahrsputz', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0289.mp3", 'Frühling', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0290.mp3", 'Frühstück', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0291.mp3", 'Fuchs', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0292.mp3", 'Füller', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0293.mp3", 'Funk', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0294.mp3", 'Furcht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0295.mp3", 'Fuß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0296.mp3", 'Fußball', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0297.mp3", 'Futter', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0298.mp3", 'Gabel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0299.mp3", 'Gang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0300.mp3", 'Gans', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0301.mp3", 'Gartenhaus', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0302.mp3", 'Gärtner', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0303.mp3", 'Gas', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0304.mp3", 'Gase', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0305.mp3", 'Gast', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0306.mp3", 'Geburtstag', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0307.mp3", 'Gedicht', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0308.mp3", 'Gefängnis', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0309.mp3", 'Gegner', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0310.mp3", 'Geige', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0311.mp3", 'Geist', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0312.mp3", 'gelb', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0313.mp3", 'Geld', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0314.mp3", 'Gericht', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0315.mp3", 'gern', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0316.mp3", 'Gesang', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0317.mp3", 'Geschäft', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0318.mp3", 'Geschichte', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0319.mp3", 'Gesetzbuch', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0320.mp3", 'Gesundheit', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0321.mp3", 'Getränk', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0322.mp3", 'Gewichtheben', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0323.mp3", 'Gier', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0324.mp3", 'Gift', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0325.mp3", 'Gips', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0326.mp3", 'Glanz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0327.mp3", 'Glas', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0328.mp3", 'gleich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0329.mp3", 'Gleis', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0330.mp3", 'Glück', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0331.mp3", 'Glut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0332.mp3", 'Gold', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0333.mp3", 'Goldschmied', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0334.mp3", 'Golf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0335.mp3", 'Gott', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0336.mp3", 'Grab', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0337.mp3", 'Grad', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0338.mp3", 'Graf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0339.mp3", 'Gramm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0340.mp3", 'Grafik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0341.mp3", 'Gras', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0342.mp3", 'grau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0343.mp3", 'Griff', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0344.mp3", 'Grillkohle', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0345.mp3", 'groß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0346.mp3", 'grün', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0347.mp3", 'Grund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0348.mp3", 'Gruppe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0349.mp3", 'Gruß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0350.mp3", 'Gummiband', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0351.mp3", 'gut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0352.mp3", 'Haar', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0353.mp3", 'Haarklammer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0354.mp3", 'Hafen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0355.mp3", 'Haftnotiz', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0356.mp3", 'Hagelkorn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0357.mp3", 'Hahn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0358.mp3", 'Hall', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0359.mp3", 'Hammer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0360.mp3", 'Hand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0361.mp3", 'Handtasche', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0362.mp3", 'Handtuch', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0363.mp3", 'Handwerker', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0364.mp3", 'Hang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0365.mp3", 'hart', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0366.mp3", 'Hase', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0367.mp3", 'Hass', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0368.mp3", 'Hauptgang', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0369.mp3", 'Haus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0370.mp3", 'Hausaufgaben', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0371.mp3", 'Hausschlüssel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0372.mp3", 'Haut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0373.mp3", 'Hecht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0374.mp3", 'Heft', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0375.mp3", 'Heizung', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0376.mp3", 'Held', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0377.mp3", 'heller', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0378.mp3", 'Helm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0379.mp3", 'Hemd', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0380.mp3", 'Herbst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0381.mp3", 'Herd', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0382.mp3", 'Herr', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0383.mp3", 'Herz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0384.mp3", 'Heu', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0385.mp3", 'Hexe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0386.mp3", 'hier', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0387.mp3", 'Himbeere', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0388.mp3", 'Himmel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0389.mp3", 'Hirn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0390.mp3", 'Hitze', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0391.mp3", 'Holz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0392.mp3", 'Hörgerät', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0393.mp3", 'Hose', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0394.mp3", 'Hotel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0395.mp3", 'Hotelbett', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0396.mp3", 'Hubschrauber', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0397.mp3", 'Huhn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0398.mp3", 'Hund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0399.mp3", 'Hürdenlauf', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
      await txn.execute(
          "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
          ["raw/m0400.mp3", 'Hut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    });
  }

  Future _insertSample401to600Male(Database  db) async{
    await db.transaction((txn) async {
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0401.mp3", 'Insekt', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0402.mp3", 'Instrument', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0403.mp3", 'Internet', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0404.mp3", 'Jacke', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0405.mp3", 'Jahrgang', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0406.mp3", 'Januar', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0407.mp3", 'Juli', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0408.mp3", 'Juni', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0409.mp3", 'Kabel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0410.mp3", 'Kaffee', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0411.mp3", 'Kaffeefleck', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0412.mp3", 'Kaffeemaschine', 5, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0413.mp3", 'kalt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0414.mp3", 'Kamm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0415.mp3", 'Kanne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0416.mp3", 'Kantine', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0417.mp3", 'Karte', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0418.mp3", 'Kartenspiel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0419.mp3", 'Kartoffel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0420.mp3", 'Käse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0421.mp3", 'Kasse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0422.mp3", 'Kassenbon', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0423.mp3", 'Katalog', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0424.mp3", 'Katze', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0425.mp3", 'Kauf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0426.mp3", 'Kauffrau', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0427.mp3", 'Kaufhaus', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0428.mp3", 'Kaufmann', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0429.mp3", 'Keim', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0430.mp3", 'Keller', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0431.mp3", 'Kellner', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0432.mp3", 'Kern', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0433.mp3", 'Kerze', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0434.mp3", 'Kies', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0435.mp3", 'Kind', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0436.mp3", 'Kindergarten', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0437.mp3", 'Kinderlied', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0438.mp3", 'Kinn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0439.mp3", 'Kino', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0440.mp3", 'Kirche', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0441.mp3", 'Kirchturmuhr', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0442.mp3", 'Kirsche', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0443.mp3", 'Kirschkuchen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0444.mp3", 'Kissenschlacht', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0445.mp3", 'Kiste', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0446.mp3", 'Kittel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0447.mp3", 'Kiwi', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0448.mp3", 'Klammer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0449.mp3", 'Klang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0450.mp3", 'klar', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0451.mp3", 'Klasse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0452.mp3", 'Klassik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0453.mp3", 'Klavier', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0454.mp3", 'Klebeband', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0455.mp3", 'Klee', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0456.mp3", 'Kleid', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0457.mp3", 'Kleiderschrank', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0458.mp3", 'klein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0459.mp3", 'Kleingeld', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0460.mp3", 'Klingelton', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0461.mp3", 'Kloß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0462.mp3", 'klug', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0463.mp3", 'Knall', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0464.mp3", 'Knecht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0465.mp3", 'Knie', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0466.mp3", 'Kobold', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0467.mp3", 'Koch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0468.mp3", 'Kochlöffel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0469.mp3", 'Kofferraum', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0470.mp3", 'Kollege', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0471.mp3", 'Kompass', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0472.mp3", 'Kopf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0473.mp3", 'Kopfhörer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0474.mp3", 'Korb', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0475.mp3", 'Kork', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0476.mp3", 'Kost', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0477.mp3", 'Krach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0478.mp3", 'Kran', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0479.mp3", 'krank', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0480.mp3", 'Krankenhaus', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0481.mp3", 'Krankenschwester', 4,Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0482.mp3", 'Kranz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0483.mp3", 'Krebs', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0484.mp3", 'Kreis', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0485.mp3", 'Kreisverkehr', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0486.mp3", 'Kreuz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0487.mp3", 'Kreuzfahrtschiff', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0488.mp3", 'Krieg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0489.mp3", 'Küche', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0490.mp3", 'Küchentisch', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0491.mp3", 'Kugelschreiber', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0492.mp3", 'Kuh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0493.mp3", 'Kühlschrank', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0494.mp3", 'Kult', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0495.mp3", 'Kunst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0496.mp3", 'Kur', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0497.mp3", 'Kuss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0498.mp3", 'Küste', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0499.mp3", 'Lachs', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0500.mp3", 'Lack', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0501.mp3", 'lahm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0502.mp3", 'Lamm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0503.mp3", 'Lampenschirm', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0504.mp3", 'Land', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0505.mp3", 'Landebahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0506.mp3", 'Landkarte', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0507.mp3", 'Landwirtschaft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0508.mp3", 'lang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0509.mp3", 'Lärm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0510.mp3", 'Last', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0511.mp3", 'Lastwagen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0512.mp3", 'Laub', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0513.mp3", 'Lauch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0514.mp3", 'Lauf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0515.mp3", 'Laus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0516.mp3", 'laut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0517.mp3", 'Lautstärke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0518.mp3", 'Leberwurst', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0519.mp3", 'leer', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0520.mp3", 'Lehrer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0521.mp3", 'leicht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0522.mp3", 'Leid', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0523.mp3", 'Leim', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0524.mp3", 'Licht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0525.mp3", 'Lichtschalter', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0526.mp3", 'lieb', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0527.mp3", 'Liebe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0528.mp3", 'Liebespaar', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0529.mp3", 'Lied', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0530.mp3", 'Liegestuhl', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0531.mp3", 'Lift', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0532.mp3", 'Lineal', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0533.mp3", 'Lippenstift', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0534.mp3", 'Loch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0535.mp3", 'Locher', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0536.mp3", 'Löffel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0537.mp3", 'Logik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0538.mp3", 'Lohn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0539.mp3", 'Lok', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0540.mp3", 'Lokführer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0541.mp3", 'Los', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0542.mp3", 'Lückentext', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0543.mp3", 'Luft', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0544.mp3", 'Lupe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0545.mp3", 'Lust', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0546.mp3", 'Lutscher', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0547.mp3", 'Macht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0548.mp3", 'Magie', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0549.mp3", 'Mai', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0550.mp3", 'Mais', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0551.mp3", 'Mandarine', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0552.mp3", 'Mango', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0553.mp3", 'Mann', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0554.mp3", 'Marathon', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0555.mp3", 'Märchenbuch', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0556.mp3", 'Mark', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0557.mp3", 'Markt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0558.mp3", 'Marktplatz', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0559.mp3", 'März', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0560.mp3", 'Maskenball', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0561.mp3", 'Maß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0562.mp3", 'Masse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0563.mp3", 'Mast', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0564.mp3", 'Matratze', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0565.mp3", 'Maus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0566.mp3", 'Maut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0567.mp3", 'Medien', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0568.mp3", 'Mediziner', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0569.mp3", 'Meer', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0570.mp3", 'Mehl', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0571.mp3", 'Melodie', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0572.mp3", 'Melone', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0573.mp3", 'Messer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0574.mp3", 'Mett', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0575.mp3", 'Metzger', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0576.mp3", 'Mikroskop', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0577.mp3", 'Mikrowelle', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0578.mp3", 'Milch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0579.mp3", 'Milchkaffee', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0580.mp3", 'mild', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0581.mp3", 'Militär', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0582.mp3", 'Mist', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0583.mp3", 'Mittagspause', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0584.mp3", 'Mitternacht', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0585.mp3", 'Mittwoch', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0586.mp3", 'Mixer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0587.mp3", 'Moderation', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0588.mp3", 'Monat', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0589.mp3", 'Mönch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0590.mp3", 'Mond', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0591.mp3", 'Montag', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0592.mp3", 'Moor', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0593.mp3", 'Moos', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0594.mp3", 'Mord', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0595.mp3", 'Motorrad', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0596.mp3", 'Möwe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0597.mp3", 'Mückenstich', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0598.mp3", 'Mülleimer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0599.mp3", 'Mund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0600.mp3", 'Münze', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     });
  }

  Future _insertSamples601to800Male(Database db) async{
    await db.transaction((txn) async {
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0601.mp3", 'Muschel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0602.mp3", 'Mut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0603.mp3", 'Mütze', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0604.mp3", 'Nachrichten', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0605.mp3", 'Nacht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0606.mp3", 'Nachtisch', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0607.mp3", 'nackt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0608.mp3", 'Nagel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0609.mp3", 'Nagellack', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0610.mp3", 'Naht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0611.mp3", 'Name', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0612.mp3", 'Narr', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0613.mp3", 'Nase', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0614.mp3", 'nass', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0615.mp3", 'Neffe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0616.mp3", 'Neid', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0617.mp3", 'Nest', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0618.mp3", 'nett', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0619.mp3", 'Netz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0620.mp3", 'neu', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0621.mp3", 'nicht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0622.mp3", 'Nichte', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0623.mp3", 'noch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0624.mp3", 'Nonne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0625.mp3", 'Norden', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0626.mp3", 'Not', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0627.mp3", 'November', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0628.mp3", 'Nudel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0629.mp3", 'Nuss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0630.mp3", 'Obstwiese', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0631.mp3", 'Ohr', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0632.mp3", 'Oktober', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0633.mp3", 'Öl', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0634.mp3", 'Oma', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0635.mp3", 'Onkel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0636.mp3", 'Opa', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0637.mp3", 'orange', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    // await txn.execute(
    //     "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
    //     ["raw/m0638.mp3", '???', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);      <--- there is no m0638...
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0639.mp3", 'Ort', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0640.mp3", 'Osten', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0641.mp3", 'Ozean', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0642.mp3", 'Päckchen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0643.mp3", 'Paket', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0644.mp3", 'Pakt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0645.mp3", 'Panne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0646.mp3", 'Papierkorb', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0647.mp3", 'Paradies', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0648.mp3", 'Park', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0649.mp3", 'Parkdeck', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0650.mp3", 'Parklücke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0651.mp3", 'Pass', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0652.mp3", 'Passwörter', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0653.mp3", 'Patenkind', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0654.mp3", 'Pause', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0655.mp3", 'Pech', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0656.mp3", 'Pelz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0657.mp3", 'Pest', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0658.mp3", 'Pfad', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0659.mp3", 'Pfand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0660.mp3", 'Pfanne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0661.mp3", 'Pfau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0662.mp3", 'Pfeil', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0663.mp3", 'Pferd', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0664.mp3", 'Pferdestall', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0665.mp3", 'Pfiff', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0666.mp3", 'Pfirsich', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0667.mp3", 'Pflasterstein', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0668.mp3", 'Pflaume', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0669.mp3", 'Pfund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0670.mp3", 'Pilz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0671.mp3", 'Pinselstrich', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0672.mp3", 'Plan', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0673.mp3", 'Platz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0674.mp3", 'Plätzchen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0675.mp3", 'Polizei', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0676.mp3", 'Pose', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0677.mp3", 'Post', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0678.mp3", 'Preis', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0679.mp3", 'Priester', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0680.mp3", 'Prinz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0681.mp3", 'Prinzessin', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0682.mp3", 'Programmheft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0683.mp3", 'Prüfung', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0684.mp3", 'Prüfungsangst', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0685.mp3", 'Puls', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0686.mp3", 'Pult', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0687.mp3", 'Punkt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0688.mp3", 'Puppe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0689.mp3", 'Puppenhaus', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0690.mp3", 'Quittung', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0691.mp3", 'Quizsendung', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0692.mp3", 'Rabatt', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0693.mp3", 'Rad', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0694.mp3", 'Radio', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0695.mp3", 'Radmutter', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0696.mp3", 'Radrennen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0697.mp3", 'Radweg', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0698.mp3", 'Rakete', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0699.mp3", 'Rand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0700.mp3", 'Rang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0701.mp3", 'Rasen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0702.mp3", 'Rasierschaum', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0703.mp3", 'Rasse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0704.mp3", 'Rast', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0705.mp3", 'Rathaus', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0706.mp3", 'rau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0707.mp3", 'Raub', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0708.mp3", 'Raum', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0709.mp3", 'Raumstation', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0710.mp3", 'raus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0711.mp3", 'Recht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0712.mp3", 'Redaktion', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0713.mp3", 'Rednerpult', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0714.mp3", 'Regalbrett', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0715.mp3", 'Regen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0716.mp3", 'Regenjacke', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0717.mp3", 'Regenwurm', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0718.mp3", 'Reh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0719.mp3", 'Reich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0720.mp3", 'Reim', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0721.mp3", 'Reis', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0722.mp3", 'Reisen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0723.mp3", 'Reisepass', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0724.mp3", 'Reiz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0725.mp3", 'Rest', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0726.mp3", 'Rettung', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0727.mp3", 'Richter', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0728.mp3", 'Riesenrad', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0729.mp3", 'Riff', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0730.mp3", 'Rind', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0731.mp3", 'Ring', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0732.mp3", 'Ritter', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0733.mp3", 'Rock', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0734.mp3", 'roh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0735.mp3", 'Rohr', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0736.mp3", 'Rolltreppe', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0737.mp3", 'rosa', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0738.mp3", 'Rose', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0739.mp3", 'Rosenkohl', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0740.mp3", 'Rost', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0741.mp3", 'rot', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0742.mp3", 'Rucksack', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0743.mp3", 'Ruderboot', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0744.mp3", 'Ruf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0745.mp3", 'Ruhm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0746.mp3", 'rund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0747.mp3", 'Sack', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0748.mp3", 'Saft', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0749.mp3", 'Sage', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0750.mp3", 'Salz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0751.mp3", 'Salzstreuer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0752.mp3", 'Samstag', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0753.mp3", 'Sand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0754.mp3", 'Sarg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0755.mp3", 'Satz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0756.mp3", 'Sau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0757.mp3", 'Säulengang', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0758.mp3", 'Schach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0759.mp3", 'Schacht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0760.mp3", 'Schaf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0761.mp3", 'Schall', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0762.mp3", 'Schar', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0763.mp3", 'scharf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0764.mp3", 'Schatz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0765.mp3", 'Schaukelpferd', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0766.mp3", 'Schaum', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0767.mp3", 'Scheck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0768.mp3", 'Scheich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0769.mp3", 'Schein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0770.mp3", 'Schere', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0771.mp3", 'Scherz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0772.mp3", 'scheu', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0773.mp3", 'Scheune', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0774.mp3", 'Schicht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0775.mp3", 'Schicksalsschlag',3,Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0776.mp3", 'Schiff', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0777.mp3", 'Schild', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0778.mp3", 'Schilf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0779.mp3", 'Schlacht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0780.mp3", 'Schlaf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0781.mp3", 'Schlafzimmer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0782.mp3", 'Schlag', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0783.mp3", 'Schlamm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0784.mp3", 'schlank', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0785.mp3", 'Schlauch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0786.mp3", 'Schleim', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0787.mp3", 'Schloss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0788.mp3", 'Schlucht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0789.mp3", 'Schluss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0790.mp3", 'Schlüssel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0791.mp3", 'schmal', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0792.mp3", 'Schmerz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0793.mp3", 'Schmetterling', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0794.mp3", 'Schmied', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0795.mp3", 'Schmutz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0796.mp3", 'Schneckenhaus', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0797.mp3", 'Schnee', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0798.mp3", 'Schneebesen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0799.mp3", 'schneller', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0800.mp3", 'Schnitt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
     });
  }

  Future _insertSamples801to1000Male(Database db) async{
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0801.mp3", 'Schokolade', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0802.mp3", 'schon', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0803.mp3", 'Schornstein', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0804.mp3", 'Schrank', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0805.mp3", 'Schraube', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0806.mp3", 'Schreck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0807.mp3", 'Schrei', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0808.mp3", 'Schreibtisch', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0809.mp3", 'Schrift', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0810.mp3", 'Schrott', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0811.mp3", 'Schuh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0812.mp3", 'Schuhgeschäft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0813.mp3", 'Schulabschluss', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0814.mp3", 'Schuld', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0815.mp3", 'Schule', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0816.mp3", 'Schulhof', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0817.mp3", 'Schulleiter', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0818.mp3", 'Schuppe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0819.mp3", 'Schuss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0820.mp3", 'Schutz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0821.mp3", 'Schwanz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0822.mp3", 'Schwarm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0823.mp3", 'schwarz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0824.mp3", 'Schwein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0825.mp3", 'Schweiß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0826.mp3", 'Schwert', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0827.mp3", 'Schwester', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0828.mp3", 'Schwiegereltern', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0829.mp3", 'Schwimmbad', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0830.mp3", 'schwimmen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0831.mp3", 'Schwung', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0832.mp3", 'See', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0833.mp3", 'Seefahrt', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0834.mp3", 'Seife', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0835.mp3", 'Seil', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0836.mp3", 'Sekretärin', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0837.mp3", 'Sekt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0838.mp3", 'Sender', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0839.mp3", 'September', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0840.mp3", 'Sicherheit', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0841.mp3", 'Sicht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0842.mp3", 'Sieb', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0843.mp3", 'Sinn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0844.mp3", 'Sinnfrage', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0845.mp3", 'Sitz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0846.mp3", 'Sitzkissen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0847.mp3", 'Sitznachbar', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0848.mp3", 'Ski', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0849.mp3", 'Sofa', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0850.mp3", 'Sohn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0851.mp3", 'Sommer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0852.mp3", 'Sommerfest', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0853.mp3", 'Sonne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0854.mp3", 'Sonnenschirm', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0855.mp3", 'Sonnenuhr', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0856.mp3", 'Sonntag', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0857.mp3", 'Spalt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0858.mp3", 'Spaß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0859.mp3", 'Specht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0860.mp3", 'Speck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0861.mp3", 'Speer', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0862.mp3", 'Speerwurf', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0863.mp3", 'Spiel', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0864.mp3", 'Spielfeld', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0865.mp3", 'Spielplatz', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0866.mp3", 'Spielstraße', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0867.mp3", 'Spielzeug', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0868.mp3", 'Spieß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0869.mp3", 'Spind', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0870.mp3", 'spitz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0871.mp3", 'Sport', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0872.mp3", 'Sportverein', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0873.mp3", 'Sprache', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0874.mp3", 'Spruch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0875.mp3", 'Sprung', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0876.mp3", 'Spüle', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0877.mp3", 'Spülmittel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0878.mp3", 'Spur', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0879.mp3", 'Staat', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0880.mp3", 'Stab', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0881.mp3", 'Stabhochsprung', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0882.mp3", 'Stadt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0883.mp3", 'Stamm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0884.mp3", 'Stand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0885.mp3", 'Start', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0886.mp3", 'Staub', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0887.mp3", 'Steckdose', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0888.mp3", 'Stecknadel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0889.mp3", 'Stein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0890.mp3", 'Stern', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0891.mp3", 'Stich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0892.mp3", 'Stiel', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0893.mp3", 'Stier', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0894.mp3", 'Stift', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0895.mp3", 'Stimmgabel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0896.mp3", 'Stock', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0897.mp3", 'Stolz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0898.mp3", 'Stoß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0899.mp3", 'stramm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0900.mp3", 'Strand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0901.mp3", 'Strandkorb', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0902.mp3", 'Strandurlaub', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0903.mp3", 'Straße', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0904.mp3", 'Straßenbahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0905.mp3", 'Stress', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0906.mp3", 'Strich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0907.mp3", 'Stroh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0908.mp3", 'Strom', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0909.mp3", 'Stück', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0910.mp3", 'Stuhl', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0911.mp3", 'Stundenplan', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0912.mp3", 'Sturm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0913.mp3", 'Sucht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0914.mp3", 'Süden', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0915.mp3", 'Supermarkt', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0916.mp3", 'Suppe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0917.mp3", 'Süßigkeit', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0918.mp3", 'Tacker', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0919.mp3", 'Tafel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0920.mp3", 'Tag', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0921.mp3", 'Takt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0922.mp3", 'Tal', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0923.mp3", 'Tankstelle', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0924.mp3", 'Tanne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0925.mp3", 'Tannenbaum', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0926.mp3", 'Tante', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0927.mp3", 'Taschendieb', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0928.mp3", 'Taschentuch', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0929.mp3", 'Tasse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0930.mp3", 'Tastatur', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0931.mp3", 'Tat', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0932.mp3", 'taub', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0933.mp3", 'Team', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0934.mp3", 'Technik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0935.mp3", 'Tee', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0936.mp3", 'Teebeutel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0937.mp3", 'Teekanne', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0938.mp3", 'Teer', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0939.mp3", 'Teich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0940.mp3", 'Teig', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0941.mp3", 'Teil', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0942.mp3", 'Telefon', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0943.mp3", 'Teller', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0944.mp3", 'Temperatur', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0945.mp3", 'Tennis', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0946.mp3", 'Teppich', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0947.mp3", 'Test', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0948.mp3", 'Textmarker', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0949.mp3", 'Thron', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0950.mp3", 'Tiefkühlfach', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0951.mp3", 'Tier', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0952.mp3", 'Tisch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0953.mp3", 'Tischdecke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0954.mp3", 'Tochter', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0955.mp3", 'Tod', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0956.mp3", 'Tomate', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0957.mp3", 'Ton', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0958.mp3", 'Tonne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0959.mp3", 'Topf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0960.mp3", 'Tor', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0961.mp3", 'Tortenguss', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0962.mp3", 'Traktor', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0963.mp3", 'Traum', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0964.mp3", 'Traumberuf', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0965.mp3", 'treu', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0966.mp3", 'Tritt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0967.mp3", 'Trommel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0968.mp3", 'Trompete', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0969.mp3", 'Tropf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0970.mp3", 'Trotz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0971.mp3", 'Tuch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0972.mp3", 'Tulpe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0973.mp3", 'Tür', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0974.mp3", 'türkis', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0975.mp3", 'Türklinke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0976.mp3", 'Turm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0977.mp3", 'Turnen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0978.mp3", 'Uhr', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0979.mp3", 'Unterricht', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0980.mp3", 'Urlaub', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0981.mp3", 'Urlaubszeit', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0982.mp3", 'Vase', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0983.mp3", 'viel', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0984.mp3", 'violett', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0985.mp3", 'Vogel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0986.mp3", 'Vogelnest', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0987.mp3", 'Vorlage', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0988.mp3", 'Vorspeise', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0989.mp3", 'wach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0990.mp3", 'Wachs', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0991.mp3", 'Wahl', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0992.mp3", 'Wahn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0993.mp3", 'Wald', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0994.mp3", 'Wall', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0995.mp3", 'Wand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0996.mp3", 'wandern', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0997.mp3", 'Wanne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0998.mp3", 'warm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m0999.mp3", 'Warteschlange', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1000.mp3", 'Waschbecken', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    });
  }

  Future _insertSamples1001to1200Male(Database db) async{
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1001.mp3", 'Wäschekorb', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1002.mp3", 'Wasser', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1003.mp3", 'Wasserhahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1004.mp3", 'Wasserkocher', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1005.mp3", 'Wasserwaage', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1006.mp3", 'Weg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1007.mp3", 'weich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1008.mp3", 'Weidenzaun', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1009.mp3", 'Wein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1010.mp3", 'Weintrauben', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1011.mp3", 'weiß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1012.mp3", 'weit', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1013.mp3", 'Wellen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1014.mp3", 'Welt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1015.mp3", 'Werk', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1016.mp3", 'Werkzeug', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1017.mp3", 'Wert', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1018.mp3", 'Wespennest', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1019.mp3", 'Westen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1020.mp3", 'Wettkampf', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1021.mp3", 'wild', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1022.mp3", 'Wind', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1023.mp3", 'Windmühle', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1024.mp3", 'Winter', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1025.mp3", 'Wissenschaft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1026.mp3", 'Witz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1027.mp3", 'Wolf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1028.mp3", 'Wolken', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1029.mp3", 'Wort', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1030.mp3", 'Wrack', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1031.mp3", 'wund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1032.mp3", 'Wunsch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1033.mp3", 'Wurm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1034.mp3", 'Wurst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1035.mp3", 'Wüste', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1036.mp3", 'Wut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1037.mp3", 'Yoga', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1038.mp3", 'Zahl', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1039.mp3", 'zahm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1040.mp3", 'Zahn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1041.mp3", 'Zahnbürste', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1042.mp3", 'Zahnschmerzen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1043.mp3", 'Zange', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1044.mp3", 'zart', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1045.mp3", 'Zauberer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1046.mp3", 'Zeh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1047.mp3", 'Zeit', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1048.mp3", 'Zeitung', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1049.mp3", 'Zelt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1050.mp3", 'Zettel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1051.mp3", 'Zeug', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1052.mp3", 'Ziel', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1053.mp3", 'Zirkus', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1054.mp3", 'Zitrone', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1055.mp3", 'Zoll', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1056.mp3", 'Zoo', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1057.mp3", 'Zopf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1058.mp3", 'Zug', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1059.mp3", 'Zugbrücke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1060.mp3", 'Zwang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1061.mp3", 'Zweck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1062.mp3", 'Zwerg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.MALE]);
    //sentence
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1063.mp3", 'Auf dem Balkon stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1064.mp3", 'Auf dem Balkon stehen Kisten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1065.mp3", 'Auf dem Boden stehen Bilder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1066.mp3", 'Auf dem Boden stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1067.mp3", 'Auf dem Boden stehen Bücher', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1068.mp3", 'Auf dem Boden stehen Kisten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1069.mp3", 'Auf dem Boden stehen Ordner', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1070.mp3", 'Auf dem Regal stehen Bilder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1071.mp3", 'Auf dem Regal stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1072.mp3", 'Auf dem Regal stehen Bücher', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1073.mp3", 'Auf dem Regal stehen Fotos', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1074.mp3", 'Auf dem Regal stehen Kisten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1075.mp3", 'Auf dem Regal stehen Ordner', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1076.mp3", 'Auf dem Schrank stehen Bilder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1077.mp3", 'Auf dem Schrank stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1078.mp3", 'Auf dem Schrank stehen Bücher', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1079.mp3", 'Auf dem Schrank stehen Fotos', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1080.mp3", 'Auf dem Schrank stehen Kisten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1081.mp3", 'Auf dem Schrank stehen Ordner', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1082.mp3", 'Auf dem Tisch stehen Bilder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1083.mp3", 'Auf dem Tisch stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1084.mp3", 'Auf dem Tisch stehen Bücher', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1085.mp3", 'Auf dem Tisch stehen Fotos', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1086.mp3", 'Auf dem Tisch stehen Kisten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1087.mp3", 'Auf dem Tisch stehen Ordner', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1088.mp3", 'Das Auto fährt über die Ampel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1089.mp3", 'Das Auto fährt über die Autobahn', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1090.mp3", 'Das Auto fährt über die Brücke', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1091.mp3", 'Das Auto fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1092.mp3", 'Das Auto fährt über die Straße', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1093.mp3", 'Das Fahrrad fährt über die Ampel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1094.mp3", 'Das Fahrrad fährt über die Brücke', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1095.mp3", 'Das Fahrrad fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1096.mp3", 'Das Fahrrad fährt über die Straße', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1097.mp3", 'Das Mädchen fährt über die Ampel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1098.mp3", 'Das Mädchen fährt über die Autobahn', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1099.mp3", 'Das Mädchen fährt über die Brücke', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1100.mp3", 'Das Mädchen fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1101.mp3", 'Das Mädchen fährt über die Straße', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1102.mp3", 'Das Taxi fährt über die Ampel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1103.mp3", 'Das Taxi fährt über die Autobahn', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1104.mp3", 'Das Taxi fährt über die Brücke', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1105.mp3", 'Das Taxi fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1106.mp3", 'Das Taxi fährt über die Straße', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1107.mp3", 'Das Pferd frisst Futter', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1108.mp3", 'Das Pferd frisst Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1109.mp3", 'Das Pferd frisst Heu', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1110.mp3", 'Das Pferd frisst Pflanzen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1111.mp3", 'Das Rind frisst Futter', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1112.mp3", 'Das Rind frisst Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1113.mp3", 'Das Rind frisst Heu', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1114.mp3", 'Das Rind frisst Pflanzen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1115.mp3", 'Das Schaf frisst Futter', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1116.mp3", 'Das Schaf frisst Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1117.mp3", 'Das Schaf frisst Heu', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1118.mp3", 'Das Schaf frisst Pflanzen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1119.mp3", 'Das Tier frisst Futter', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1120.mp3", 'Das Tier frisst Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1121.mp3", 'Das Tier frisst Heu', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1122.mp3", 'Das Tier frisst Pflanzen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1123.mp3", 'Das Apartment wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1124.mp3", 'Das Apartment wird geräumt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1125.mp3", 'Das Apartment wird gestrichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1126.mp3", 'Das Apartment wird renoviert', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1127.mp3", 'Das Apartment wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1128.mp3", 'Das Gebäude wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1129.mp3", 'Das Gebäude wird gebaut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1130.mp3", 'Das Gebäude wird geräumt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1131.mp3", 'Das Gebäude wird gestrichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1132.mp3", 'Das Gebäude wird renoviert', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1133.mp3", 'Das Gebäude wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1134.mp3", 'Das Haus wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1135.mp3", 'Das Haus wird gebaut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1136.mp3", 'Das Haus wird geräumt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1137.mp3", 'Das Haus wird gestrichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1138.mp3", 'Das Haus wird renoviert', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1139.mp3", 'Das Haus wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1140.mp3", 'Das Hotel wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1141.mp3", 'Das Hotel wird gebaut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1142.mp3", 'Das Hotel wird geräumt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1143.mp3", 'Das Hotel wird gestrichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1144.mp3", 'Das Hotel wird renoviert', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1145.mp3", 'Das Hotel wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1146.mp3", 'Das Viertel wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1147.mp3", 'Das Viertel wird gebaut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1148.mp3", 'Das Viertel wird geräumt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1149.mp3", 'Das Viertel wird renoviert', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1150.mp3", 'Das Viertel wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1151.mp3", 'Das Kind lernt in der Küche', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1152.mp3", 'Das Kind lernt in der Pause', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1153.mp3", 'Das Kind lernt in der Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1154.mp3", 'Das Kind lernt in der Sonne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1155.mp3", 'Das Kind rennt in der Küche', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1156.mp3", 'Das Kind rennt in der Pause', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1157.mp3", 'Das Kind rennt in der Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1158.mp3", 'Das Kind rennt in der Sonne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1159.mp3", 'Das Kind sitzt in der Küche', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1160.mp3", 'Das Kind sitzt in der Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1161.mp3", 'Das Kind sitzt in der Sonne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1162.mp3", 'Das Kind spielt in der Küche', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1163.mp3", 'Das Kind spielt in der Pause', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1164.mp3", 'Das Kind spielt in der Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1165.mp3", 'Das Kind spielt in der Sonne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1166.mp3", 'Das Auto hat die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1167.mp3", 'Das Auto hat die Farbe grau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1168.mp3", 'Das Auto hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1169.mp3", 'Das Auto hat die Farbe schwarz', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1170.mp3", 'Das Auto hat die Farbe weiß', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    // TODO: fix samples m1171 & 1172 and add them (-> "das haus hat die farbe grau/rot")
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1173.mp3", 'Das Haus hat die Farbe weiß', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1174.mp3", 'Das Hemd hat die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1175.mp3", 'Das Hemd hat die Farbe grau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1176.mp3", 'Das Hemd hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1177.mp3", 'Das Hemd hat die Farbe schwarz', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1178.mp3", 'Das Hemd hat die Farbe weiß', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1179.mp3", 'Das Leder hat die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1180.mp3", 'Das Leder hat die Farbe grau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1181.mp3", 'Das Leder hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1182.mp3", 'Das Leder hat die Farbe schwarz', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1183.mp3", 'Das Leder hat die Farbe weiß', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1184.mp3", 'Das Sofa hat die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1185.mp3", 'Das Sofa hat die Farbe grau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1186.mp3", 'Das Sofa hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1187.mp3", 'Das Sofa hat die Farbe schwarz', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1188.mp3", 'Das Sofa hat die Farbe weiß', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1189.mp3", 'Der Bär hat braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1190.mp3", 'Der Bär hat dichtes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1191.mp3", 'Der Bär hat dunkles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1192.mp3", 'Der Bär hat helles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1193.mp3", 'Der Bär hat langes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1194.mp3", 'Der Bär hat warmes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1195.mp3", 'Der Biber hat braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1196.mp3", 'Der Biber hat dichtes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1197.mp3", 'Der Biber hat dunkles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1198.mp3", 'Der Biber hat helles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1199.mp3", 'Der Biber hat langes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1200.mp3", 'Der Biber hat warmes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    });
  }

  Future _insertSamples1201to1400Male(Database db) async{
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1201.mp3", 'Der Hase hat braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1202.mp3", 'Der Hase hat dichtes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1203.mp3", 'Der Hase hat dunkles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1204.mp3", 'Der Hase hat helles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1205.mp3", 'Der Hase hat langes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1206.mp3", 'Der Hase hat warmes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1207.mp3", 'Der Hund hat braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1208.mp3", 'Der Hund hat dichtes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1209.mp3", 'Der Hund hat dunkles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1210.mp3", 'Der Hund hat helles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1211.mp3", 'Der Hund hat langes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1212.mp3", 'Der Hund hat warmes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1213.mp3", 'Wir gehen heute in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1214.mp3", 'Wir gehen heute in den Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1215.mp3", 'Wir gehen heute in den Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1216.mp3", 'Wir gehen heute in den Zirkus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1217.mp3", 'Wir gehen heute in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1218.mp3", 'Wir gehen Montag in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1219.mp3", 'Wir gehen Montag in den Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1220.mp3", 'Wir gehen Montag in den Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1221.mp3", 'Wir gehen Montag in den Zirkus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1222.mp3", 'Wir gehen Montag in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1223.mp3", 'Wir gehen morgen in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1224.mp3", 'Wir gehen morgen in den Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1225.mp3", 'Wir gehen morgen in den Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1226.mp3", 'Wir gehen morgen in den Zirkus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1227.mp3", 'Wir gehen morgen in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1228.mp3", 'Wir gehen Samstag in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1229.mp3", 'Wir gehen Samstag in den Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1230.mp3", 'Wir gehen Samstag in den Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1231.mp3", 'Wir gehen Samstag in den Zirkus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1232.mp3", 'Wir gehen Samstag in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1233.mp3", 'Wir gehen Sonntag in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1234.mp3", 'Wir gehen Sonntag in den Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1235.mp3", 'Wir gehen Sonntag in den Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1236.mp3", 'Wir gehen Sonntag in den Zirkus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1237.mp3", 'Wir gehen Sonntag in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1238.mp3", 'Der Junge malt ein Auto', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1239.mp3", 'Der Junge malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1240.mp3", 'Der Junge malt ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1241.mp3", 'Der Junge malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1242.mp3", 'Der Junge malt ein Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1243.mp3", 'Der Künstler malt ein Auto', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1244.mp3", 'Der Künstler malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1245.mp3", 'Der Künstler malt ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1246.mp3", 'Der Künstler malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1247.mp3", 'Der Künstler malt ein Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1248.mp3", 'Der Maler malt ein Auto', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1249.mp3", 'Der Maler malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1250.mp3", 'Der Maler malt ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1251.mp3", 'Der Maler malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1252.mp3", 'Der Maler malt ein Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1253.mp3", 'Der Mann malt ein Auto', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1254.mp3", 'Der Mann malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]); // TODO: fix sample
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1255.mp3", 'Der Mann malt ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1256.mp3", 'Der Mann malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1257.mp3", 'Der Mann malt ein Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1258.mp3", 'Der Schüler malt ein Auto', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1259.mp3", 'Der Schüler malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1260.mp3", 'Der Schüler malt ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1261.mp3", 'Der Schüler malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1262.mp3", 'Der Schüler malt ein Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1263.mp3", 'Der Joghurt schmeckt nach Erdbeere', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1264.mp3", 'Der Joghurt schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1265.mp3", 'Der Joghurt schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1266.mp3", 'Der Joghurt schmeckt nach Vanille', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1267.mp3", 'Der Kuchen schmeckt nach Erdbeere', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1268.mp3", 'Der Kuchen schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1269.mp3", 'Der Kuchen schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1270.mp3", 'Der Kuchen schmeckt nach Vanille', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1271.mp3", 'Der Nachtisch schmeckt nach Erdbeere', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1272.mp3", 'Der Nachtisch schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1273.mp3", 'Der Nachtisch schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1274.mp3", 'Der Nachtisch schmeckt nach Vanille', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1275.mp3", 'Der Saft schmeckt nach Erdbeere', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1276.mp3", 'Der Saft schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1277.mp3", 'Der Saft schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1278.mp3", 'Der Tee schmeckt nach Erdbeere', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1279.mp3", 'Der Tee schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1280.mp3", 'Der Tee schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1281.mp3", 'Der Tee schmeckt nach Vanille', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1282.mp3", 'Der Anwalt spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1283.mp3", 'Der Anwalt spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1284.mp3", 'Der Anwalt spricht laut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1285.mp3", 'Der Anwalt spricht leise', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1286.mp3", 'Der Anwalt spricht schnell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1287.mp3", 'Der Lehrer spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1288.mp3", 'Der Lehrer spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1289.mp3", 'Der Lehrer spricht laut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1290.mp3", 'Der Lehrer spricht leise', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1291.mp3", 'Der Lehrer spricht schnell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1292.mp3", 'Der Redner spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1293.mp3", 'Der Redner spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1294.mp3", 'Der Redner spricht laut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1295.mp3", 'Der Redner spricht leise', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1296.mp3", 'Der Redner spricht schnell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1297.mp3", 'Der Richter spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1298.mp3", 'Der Richter spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1299.mp3", 'Der Richter spricht laut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1300.mp3", 'Der Richter spricht leise', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1301.mp3", 'Der Richter spricht schnell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1302.mp3", 'Der Schüler spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1303.mp3", 'Der Schüler spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1304.mp3", 'Der Schüler spricht laut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1305.mp3", 'Der Schüler spricht leise', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1306.mp3", 'Der Schüler spricht schnell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1307.mp3", 'Der Anwalt bearbeitet den Auftrag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1308.mp3", 'Der Anwalt bearbeitet den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1309.mp3", 'Der Anwalt gewinnt den Auftrag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1310.mp3", 'Der Anwalt gewinnt den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1311.mp3", 'Der Anwalt gewinnt den Klienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1312.mp3", 'Der Anwalt gewinnt den Prozess', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1313.mp3", 'Der Anwalt übernimmt den Auftrag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1314.mp3", 'Der Anwalt übernimmt den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1315.mp3", 'Der Anwalt übernimmt den Klienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1316.mp3", 'Der Anwalt verliert den Auftrag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1317.mp3", 'Der Anwalt verliert den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1318.mp3", 'Der Anwalt verliert den Klienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1319.mp3", 'Der Anwalt verliert den Prozess', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1320.mp3", 'Die Angestellten halten sich an die Pläne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1321.mp3", 'Die Angestellten halten sich an die Regeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1322.mp3", 'Die Angestellten halten sich an die Richtlinien', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1323.mp3", 'Die Angestellten halten sich an die Vorgaben', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1324.mp3", 'Die Arbeiter halten sich an die Pläne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1325.mp3", 'Die Arbeiter halten sich an die Regeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1326.mp3", 'Die Arbeiter halten sich an die Richtlinien', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1327.mp3", 'Die Arbeiter halten sich an die Vorgaben', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1328.mp3", 'Die Bauern halten sich an die Gesetze', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1329.mp3", 'Die Bauern halten sich an die Pläne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1330.mp3", 'Die Bauern halten sich an die Richtlinien', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1331.mp3", 'Die Bauern halten sich an die Vorgaben', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1332.mp3", 'Die Bürger halten sich an die Gesetze', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1333.mp3", 'Die Bürger halten sich an die Regeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1334.mp3", 'Die Bürger halten sich an die Richtlinien', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1335.mp3", 'Die Menschen halten sich an die Gesetze', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1336.mp3", 'Die Menschen halten sich an die Regeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1337.mp3", 'Die Menschen halten sich an die Richtlinien', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1338.mp3", 'Die Ärztin hilft dem Kind', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1339.mp3", 'Die Ärztin hilft dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1340.mp3", 'Die Ärztin hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1341.mp3", 'Die Ärztin hilft dem Patienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1342.mp3", 'Die Ärztin hilft dem Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1343.mp3", 'Die Frau hilft dem Kind', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1344.mp3", 'Die Frau hilft dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1345.mp3", 'Die Frau hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1346.mp3", 'Die Frau hilft dem Patienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1347.mp3", 'Die Frau hilft dem Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1348.mp3", 'Die Medizin hilft dem Kind', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1349.mp3", 'Die Medizin hilft dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1350.mp3", 'Die Medizin hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1351.mp3", 'Die Medizin hilft dem Patienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1352.mp3", 'Die Medizin hilft dem Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1353.mp3", 'Die Polizei hilft dem Kind', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1354.mp3", 'Die Polizei hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1355.mp3", 'Die Polizei hilft dem Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1356.mp3", 'Die Therapie hilft dem Kind', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1357.mp3", 'Die Therapie hilft dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1358.mp3", 'Die Therapie hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1359.mp3", 'Die Therapie hilft dem Patienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1360.mp3", 'Die Therapie hilft dem Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1361.mp3", 'Die Abfahrt ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1362.mp3", 'Die Abfahrt ist um drei Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1363.mp3", 'Die Abfahrt ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1364.mp3", 'Die Abfahrt ist um neun Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1365.mp3", 'Die Abfahrt ist um sechs Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1366.mp3", 'Die Ankunft ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1367.mp3", 'Die Ankunft ist um drei Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1368.mp3", 'Die Ankunft ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1369.mp3", 'Die Ankunft ist um neun Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1370.mp3", 'Die Ankunft ist um sechs Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1371.mp3", 'Die Probe ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1372.mp3", 'Die Probe ist um drei Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1373.mp3", 'Die Probe ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1374.mp3", 'Die Probe ist um neun Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1375.mp3", 'Die Probe ist um sechs Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1376.mp3", 'Die Prüfung ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1377.mp3", 'Die Prüfung ist um drei Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1378.mp3", 'Die Prüfung ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1379.mp3", 'Die Prüfung ist um neun Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1380.mp3", 'Die Prüfung ist um sechs Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1381.mp3", 'Die Vorstellung ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1382.mp3", 'Die Vorstellung ist um drei Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1383.mp3", 'Die Vorstellung ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1384.mp3", 'Die Vorstellung ist um neun Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1385.mp3", 'Die Vorstellung ist um sechs Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1386.mp3", 'Die Miete wurde bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1387.mp3", 'Die Miete wurde erhöht', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1388.mp3", 'Die Miete wurde gesenkt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1389.mp3", 'Die Rechnung wurde beglichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1390.mp3", 'Die Rechnung wurde bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1391.mp3", 'Die Rechnung wurde erhöht', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1392.mp3", 'Die Rechnung wurde gesenkt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1393.mp3", 'Die Steuer wurde bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1394.mp3", 'Die Steuer wurde erhöht', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1395.mp3", 'Die Steuer wurde gesenkt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1396.mp3", 'Die Strafe wurde beglichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    // Sample nicht vorhanden
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1397.mp3", 'Die Strafe wurde bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1398.mp3", 'Die Strafe wurde erhöht', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1399.mp3", 'Die Strafe wurde gesenkt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1400.mp3", 'Die Großmutter kauft Mützen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    });
  }

  Future _insertSamples1401to1600Male(Database db) async {
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1401.mp3", 'Die Großmutter kauft Pullover', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1402.mp3", 'Die Großmutter kauft Schals', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1403.mp3", 'Die Großmutter kauft Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1404.mp3", 'Die Großmutter stopft Mützen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1405.mp3", 'Die Großmutter stopft Pullover', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1406.mp3", 'Die Großmutter stopft Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1407.mp3", 'Die Großmutter strickt Mützen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1408.mp3", 'Die Großmutter strickt Pullover', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1409.mp3", 'Die Großmutter strickt Schals', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1410.mp3", 'Die Großmutter strickt Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1411.mp3", 'Die Großmutter verschenkt Mützen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1412.mp3", 'Die Großmutter verschenkt Pullover', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1413.mp3", 'Die Großmutter verschenkt Schals', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1414.mp3", 'Die Großmutter verschenkt Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1415.mp3", 'Du liest auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1416.mp3", 'Du liest auf dem Papier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1417.mp3", 'Du liest auf dem Umschlag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1418.mp3", 'Du liest auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1419.mp3", 'Du malst auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1420.mp3", 'Du malst auf dem Brief', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1421.mp3", 'Du malst auf dem Papier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1422.mp3", 'Du malst auf dem Umschlag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1423.mp3", 'Du malst auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1424.mp3", 'Du notierst auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1425.mp3", 'Du notierst auf dem Papier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1426.mp3", 'Du notierst auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1427.mp3", 'Du schreibst auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1428.mp3", 'Du schreibst auf dem Brief', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1429.mp3", 'Du schreibst auf dem Papier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1430.mp3", 'Du schreibst auf dem Umschlag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1431.mp3", 'Du schreibst auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1432.mp3", 'Du zeichnest auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1433.mp3", 'Du zeichnest auf dem Papier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1434.mp3", 'Du zeichnest auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1435.mp3", 'Er isst heute Eintopf', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1436.mp3", 'Er isst heute Gemüse', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1437.mp3", 'Er isst heute Kartoffeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1438.mp3", 'Er isst heute Nudeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1439.mp3", 'Er isst heute Reis', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1440.mp3", 'Er isst heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1441.mp3", 'Er kauft heute Gemüse', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1442.mp3", 'Er kauft heute Kartoffeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1443.mp3", 'Er kauft heute Nudeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1444.mp3", 'Er kauft heute Reis', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1445.mp3", 'Er kauft heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1446.mp3", 'Er kocht heute Eintopf', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1447.mp3", 'Er kocht heute Gemüse', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1448.mp3", 'Er kocht heute Kartoffeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1449.mp3", 'Er kocht heute Nudeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1450.mp3", 'Er kocht heute Reis', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1451.mp3", 'Er kocht heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1452.mp3", 'Er serviert heute Eintopf', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1453.mp3", 'Er serviert heute Gemüse', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1454.mp3", 'Er serviert heute Kartoffeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1455.mp3", 'Er serviert heute Nudeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1456.mp3", 'Er serviert heute Reis', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1457.mp3", 'Er serviert heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1458.mp3", 'Es ist sehr heiß im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1459.mp3", 'Es ist sehr heiß im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1460.mp3", 'Es ist sehr heiß im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1461.mp3", 'Es ist sehr heiß im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1462.mp3", 'Es ist sehr heiß im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1463.mp3", 'Es ist sehr kalt im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1464.mp3", 'Es ist sehr kalt im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1465.mp3", 'Es ist sehr kalt im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1466.mp3", 'Es ist sehr kalt im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1467.mp3", 'Es ist sehr kalt im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1468.mp3", 'Es ist sehr laut im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1469.mp3", 'Es ist sehr laut im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1470.mp3", 'Es ist sehr laut im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1471.mp3", 'Es ist sehr laut im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1472.mp3", 'Es ist sehr laut im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1473.mp3", 'Es ist sehr leer im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1474.mp3", 'Es ist sehr leer im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1475.mp3", 'Es ist sehr leer im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1476.mp3", 'Es ist sehr leer im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1477.mp3", 'Es ist sehr leer im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1478.mp3", 'Es ist sehr ruhig im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1479.mp3", 'Es ist sehr ruhig im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1480.mp3", 'Es ist sehr ruhig im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1481.mp3", 'Es ist sehr ruhig im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1482.mp3", 'Es ist sehr ruhig im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1483.mp3", 'Es ist sehr voll im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1484.mp3", 'Es ist sehr voll im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1485.mp3", 'Es ist sehr voll im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1486.mp3", 'Es ist sehr voll im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1487.mp3", 'Es ist sehr voll im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1488.mp3", 'Ich belüge meine Brüder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1489.mp3", 'Ich belüge meine Eltern', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1490.mp3", 'Ich belüge meine Freunde', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1491.mp3", 'Ich belüge meine Freundin', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1492.mp3", 'Ich belüge meine Kollegen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1493.mp3", 'Ich beschenke meine Brüder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1494.mp3", 'Ich beschenke meine Eltern', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1495.mp3", 'Ich beschenke meine Freunde', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1496.mp3", 'Ich beschenke meine Freundin', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1497.mp3", 'Ich verrate meine Brüder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1498.mp3", 'Ich verrate meine Eltern', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1499.mp3", 'Ich verrate meine Freunde', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1500.mp3", 'Ich verrate meine Freundin', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1501.mp3", 'Ich verrate meine Kollegen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1502.mp3", 'Ich versetze meine Brüder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1503.mp3", 'Ich versetze meine Eltern', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1504.mp3", 'Ich versetze meine Freunde', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1505.mp3", 'Ich versetze meine Freundin', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1506.mp3", 'Ich versetze meine Kollegen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1507.mp3", 'Sie beendet die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1508.mp3", 'Sie beendet die Schule erst im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1509.mp3", 'Sie beendet die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1510.mp3", 'Sie beendet die Schule noch im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1511.mp3", 'Sie beginnt die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1512.mp3", 'Sie beginnt die Schule erst im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1513.mp3", 'Sie beginnt die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1514.mp3", 'Sie beginnt die Schule noch im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1515.mp3", 'Sie verlässt die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1516.mp3", 'Sie verlässt die Schule erst im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1517.mp3", 'Sie verlässt die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1518.mp3", 'Sie verlässt die Schule noch im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1519.mp3", 'Sie wechselt die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1520.mp3", 'Sie wechselt die Schule erst im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1521.mp3", 'Sie wechselt die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1522.mp3", 'Sie wechselt die Schule noch im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE,Speaker.MALE]);
    //Question
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1523.mp3", 'Baut der Vogel ein Nest', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1524.mp3", 'Dreht sich die Erde', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1525.mp3", 'Erreicht er das Ziel', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1526.mp3", 'Fährt das Auto über die Straße', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1527.mp3", 'Fährt der Zug täglich', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1528.mp3", 'Fährt sie immer mit dem Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1529.mp3", 'Frisst das Pferd Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1530.mp3", 'Gehen wir Montag in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1531.mp3", 'Gewinnen wir das Spiel', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1532.mp3", 'Haben Spinnen lange Beine', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1533.mp3", 'Hängt das Bild an der Wand', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1534.mp3", 'Hast du gut geschlafen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1535.mp3", 'Hat das Auto die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1536.mp3", 'Hat der Bär braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1537.mp3", 'Hilft die Medizin dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1538.mp3", 'Hörst du das Radio rauschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1539.mp3", 'Isst sie gerne Eis', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1540.mp3", 'Ist der Anstrich frisch', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1541.mp3", 'Ist die Abfahrt um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1542.mp3", 'Ist die Kasse geschlossen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1543.mp3", 'Ist es sehr voll im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1544.mp3", 'Kann ich das Buch ausleihen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1545.mp3", 'Kennst du den neuen Schüler', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1546.mp3", 'Kocht er heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1547.mp3", 'Kommst du heute zu mir', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1548.mp3", 'Leuchtet die Ampel grün', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1549.mp3", 'Lief die Sendung im Fernsehen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1550.mp3", 'Malt der Junge ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1551.mp3", 'Makierst du die Wörter', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1552.mp3", 'Quietscht die Tür', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1553.mp3", 'Regnet es viel', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1554.mp3", 'Schmeckt der Kaffee gut', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1555.mp3", 'Schmeckt der Tee nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1556.mp3", 'Sind die Blätter grün', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1557.mp3", 'Spielt er mit dem Ball', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1558.mp3", 'Spricht der Lehrer deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1559.mp3", 'Stehen Blumen auf dem Regal', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1560.mp3", 'Strickt die Großmutter Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1561.mp3", 'Trinkst du deinen Kaffee schwarz', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1562.mp3", 'Übernimmt der Anwalt den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1563.mp3", 'Verlief das Treffen gut', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1564.mp3", 'Wann möchstest du aufstehen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1565.mp3", 'Wann wurde der Film veröffentlicht', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1566.mp3", 'Was möchtest du essen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1567.mp3", 'Wechselt sie die Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1568.mp3", 'Welche Note hast du bekommen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1569.mp3", 'Wie heißt der kleine Junge', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1570.mp3", 'Wie lang ist der Stau', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1571.mp3", 'Wieso bist du so spät', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1572.mp3", 'Wird das Gebäude abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1573.mp3", 'Wo muss ich abbiegen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1574.mp3", 'Wo wohnst du', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1575.mp3", 'Wurde der Tisch gedeckt', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1576.mp3", 'Wurde die Strafe bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1577.mp3", 'Wurden die Karten gemischt', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.MALE]);
    // sentence happy
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1578.mp3", 'Auf dem Balkon stehen Blumen', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1579.mp3", 'Auf dem Regal stehen Bücher', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1580.mp3", 'Das Auto fährt über die Brücke', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1581.mp3", 'Das Fahrrad fährt über die Kreuzung', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1582.mp3", 'Das Pferd frisst Heu', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1583.mp3", 'Das Schaf frisst Gras', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1584.mp3", 'Das Gebäude wird abgebrochen', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1585.mp3", 'Das Haus wird verkauft', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1586.mp3", 'Das Kind lernt in der Schule', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1587.mp3", 'Das Kind spielt in der Pause', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1588.mp3", 'Das Haus hat die Farbe rot', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1589.mp3", 'Das Hemd hat die Farbe blau', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1590.mp3", 'Der Bär hat braunes Fell', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1591.mp3", 'Der Hund hat langes Fell', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1592.mp3", 'Wir gehen heute in den Garten', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1593.mp3", 'Wir gehen Sonntag in den Zoo', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1594.mp3", 'Der Junge malt ein Schiff', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1595.mp3", 'Der Künstler malt ein Bild', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1596.mp3", 'Der Kuchen schmeckt nach Orange', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1597.mp3", 'Der Tee schmeckt nach Früchten', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1598.mp3", 'Der Redner spricht deutlich', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1599.mp3", 'Der Richter spricht langsam', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1600.mp3", 'Der Anwalt gewinnt den Prozess', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    });
  }

  Future _insertSamples1601toEndMale(Database db) async {
   await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1601.mp3", 'Der Anwalt übernimmt den Fall', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1602.mp3", 'Die Arbeiter halten sich an die Pläne', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1603.mp3", 'Die Bürger halten sich an die Gesetze', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1604.mp3", 'Die Ärztin hilft dem Kranken', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1605.mp3", 'Die Polizei hilft dem Menschen', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1606.mp3", 'Die Probe ist um fünf Uhr', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1607.mp3", 'Die Prüfung ist um acht Uhr', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1608.mp3", 'Die Miete wurde bezahlt', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1609.mp3", 'Die Steuer wurde gesenkt', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1610.mp3", 'Die Großmutter stopft Socken', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1611.mp3", 'Die Großmutter strickt Mützen', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1612.mp3", 'Du notierst auf dem Block', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1613.mp3", 'Du schreibst auf dem Zettel', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1614.mp3", 'Er isst heute Kartoffeln', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1615.mp3", 'Er kocht heute Suppe', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1616.mp3", 'Es ist sehr heiß im Zug', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1617.mp3", 'Es ist sehr ruhig im Park', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1618.mp3", 'Ich belüge meine Eltern', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1619.mp3", 'Ich versetze meine Freunde', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1620.mp3", 'Sie beginnt die Schule diesen Sommer', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1621.mp3", 'Sie wechselt die Schule nächsten Sommer', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    //Word Happy
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1622.mp3", 'Abendkleid', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1623.mp3", 'Achterbahn', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1624.mp3", 'Apfelsaft', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1625.mp3", 'Backofen', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1626.mp3", 'Baustelle', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1627.mp3", 'Blumenbeet', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1628.mp3", 'Briefkasten', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1629.mp3", 'Dachboden', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1630.mp3", 'Dezember', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1631.mp3", 'Eisenbahn', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1632.mp3", 'Ententeich', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1633.mp3", 'Erdgeschoss', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1634.mp3", 'Fensterbrett', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1635.mp3", 'Flughafen', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1636.mp3", 'Fremdsprache', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1637.mp3", 'Gartenhaus', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1638.mp3", 'Geschichte', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1639.mp3", 'Handwerker', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1640.mp3", 'Hörgerät', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1641.mp3", 'Hürdenlauf', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1642.mp3", 'Instrument', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1643.mp3", 'Kartenspiel', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1644.mp3", 'Kofferraum', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1645.mp3", 'Kreisverkehr', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1646.mp3", 'Landwirtschaft', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1647.mp3", 'Lippenstift', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1648.mp3", 'Lückentext', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1649.mp3", 'Märchenbuch', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1650.mp3", 'Mitternacht', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1651.mp3", 'Nachrichten', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1652.mp3", 'Nagellack', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1653.mp3", 'Obstwiese', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1654.mp3", 'Papierkorb', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1655.mp3", 'Pferdestall', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1656.mp3", 'Pinselstrich', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1657.mp3", 'Rasierschaum', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1658.mp3", 'Raumstation', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1659.mp3", 'Regenwurm', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1660.mp3", 'Schmetterling', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1661.mp3", 'Straßenbahn', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1662.mp3", 'Supermarkt', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1663.mp3", 'Tankstelle', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1664.mp3", 'Teebeutel', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1665.mp3", 'Tischdecke', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1666.mp3", 'Urlaubszeit', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1667.mp3", 'Vogelnest', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1668.mp3", 'Waschbecken', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1669.mp3", 'Weintrauben', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1670.mp3", 'Windmühle', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1671.mp3", 'Zahnschmerzen', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.MALE]);
    //sentence sad
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1672.mp3", 'Auf dem Balkon stehen Blumen', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1673.mp3", 'Auf dem Regal stehen Bücher', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1674.mp3", 'Das Auto fährt über die Brücke', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1675.mp3", 'Das Fahrrad fährt über die Kreuzung', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1676.mp3", 'Das Pferd frisst Heu', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1677.mp3", 'Das Schaf frisst Gras', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1678.mp3", 'Das Gebäude wird abgebrochen', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1679.mp3", 'Das Haus wird verkauft', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1680.mp3", 'Das Kind lernt in der Schule', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1681.mp3", 'Das Kind spielt in der Pause', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1682.mp3", 'Das Haus hat die Farbe rot', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1683.mp3", 'Das Hemd hat die Farbe blau', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1684.mp3", 'Der Bär hat braunes Fell', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1685.mp3", 'Der Hund hat langes Fell', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1686.mp3", 'Wir gehen heute in den Garten', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1687.mp3", 'Wir gehen Sonntag in den Zoo', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1688.mp3", 'Der Junge malt ein Schiff', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1689.mp3", 'Der Künstler malt ein Bild', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1690.mp3", 'Der Kuchen schmeckt nach Orange', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1691.mp3", 'Der Tee schmeckt nach Früchten', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1692.mp3", 'Der Redner spricht deutlich', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1693.mp3", 'Der Richter spricht langsam', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1694.mp3", 'Der Anwalt gewinnt den Prozess', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1695.mp3", 'Der Anwalt übernimmt den Fall', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1696.mp3", 'Die Arbeiter halten sich an die Pläne', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1697.mp3", 'Die Bürger halten sich an die Gesetze', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1698.mp3", 'Die Ärztin hilft dem Kranken', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1699.mp3", 'Die Polizei hilft dem Menschen', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1700.mp3", 'Die Probe ist um fünf Uhr', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1701.mp3", 'Die Prüfung ist um acht Uhr', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1702.mp3", 'Die Miete wurde bezahlt', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1703.mp3", 'Die Steuer wurde gesenkt', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1704.mp3", 'Die Großmutter stopft Socken', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1705.mp3", 'Die Großmutter strickt Mützen', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1706.mp3", 'Du notierst auf dem Block', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1707.mp3", 'Du schreibst auf dem Zettel', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1708.mp3", 'Er isst heute Kartoffeln', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1709.mp3", 'Er kocht heute Suppe', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1710.mp3", 'Es ist sehr heiß im Zug', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1711.mp3", 'Es ist sehr ruhig im Park', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1712.mp3", 'Ich belüge meine Eltern', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1713.mp3", 'Ich versetze meine Freunde', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1714.mp3", 'Sie beginnt die Schule diesen Sommer', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1715.mp3", 'Sie wechselt die Schule nächsten Sommer', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.MALE]);
    //Word SAD
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1716.mp3", 'Abendkleid', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1717.mp3", 'Achterbahn', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1718.mp3", 'Apfelsaft', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1719.mp3", 'Backofen', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1720.mp3", 'Baustelle', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1721.mp3", 'Blumenbeet', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1722.mp3", 'Briefkasten', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1723.mp3", 'Dachboden', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1724.mp3", 'Dezember', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1725.mp3", 'Eisenbahn', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1726.mp3", 'Ententeich', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1727.mp3", 'Erdgeschoss', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1728.mp3", 'Fensterbrett', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1729.mp3", 'Flughafen', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1730.mp3", 'Fremdsprache', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1731.mp3", 'Gartenhaus', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1732.mp3", 'Geschichte', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1733.mp3", 'Handwerker', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1734.mp3", 'Hörgerät', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1735.mp3", 'Hürdenlauf', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1736.mp3", 'Instrument', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1737.mp3", 'Kartenspiel', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1738.mp3", 'Kofferraum', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1739.mp3", 'Kreisverkehr', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1740.mp3", 'Landwirtschaft', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1741.mp3", 'Lippenstift', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1742.mp3", 'Lückentext', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1743.mp3", 'Märchenbuch', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1744.mp3", 'Mitternacht', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1745.mp3", 'Nachrichten', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1746.mp3", 'Nagellack', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1747.mp3", 'Obstwiese', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1748.mp3", 'Papierkorb', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1749.mp3", 'Pferdestall', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1750.mp3", 'Pinselstrich', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1751.mp3", 'Rasierschaum', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1752.mp3", 'Raumstation', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1753.mp3", 'Regenwurm', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1754.mp3", 'Schmetterling', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1755.mp3", 'Straßenbahn', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1756.mp3", 'Supermarkt', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1757.mp3", 'Tankstelle', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1758.mp3", 'Teebeutel', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1759.mp3", 'Tischdecke', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1760.mp3", 'Urlaubszeit', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1761.mp3", 'Vogelnest', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1762.mp3", 'Waschbecken', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1763.mp3", 'Weintrauben', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1764.mp3", 'Windmühle', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1765.mp3", 'Zahnschmerzen', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.MALE]);
    //Question
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1766.mp3", 'Auf dem Balkon stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1767.mp3", 'Auf dem Regal stehen Bücher', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1768.mp3", 'Das Auto fährt über die Brücke', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1769.mp3", 'Das Fahrrad fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1770.mp3", 'Das Pferd frisst Heu', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1771.mp3", 'Das Schaf frisst Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1772.mp3", 'Das Gebäude wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1773.mp3", 'Das Haus wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1774.mp3", 'Das Kind lernt in der Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1775.mp3", 'Das Kind spielt in der Pause', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1776.mp3", 'Das Haus hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1777.mp3", 'Das Hemd hat die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1778.mp3", 'Der Bär hat braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1779.mp3", 'Der Hund hat langes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1780.mp3", 'Wir gehen heute in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1781.mp3", 'Wir gehen Sonntag in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1782.mp3", 'Der Junge malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1783.mp3", 'Der Künstler malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1784.mp3", 'Der Kuchen schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1785.mp3", 'Der Tee schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1786.mp3", 'Der Redner spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1787.mp3", 'Der Richter spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1788.mp3", 'Der Anwalt gewinnt den Prozess', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1789.mp3", 'Der Anwalt übernimmt den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1790.mp3", 'Die Arbeiter halten sich an die Pläne', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1791.mp3", 'Die Bürger halten sich an die Gesetze', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1792.mp3", 'Die Ärztin hilft dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1793.mp3", 'Die Polizei hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1794.mp3", 'Die Probe ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1795.mp3", 'Die Prüfung ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1796.mp3", 'Die Miete wurde bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1797.mp3", 'Die Steuer wurde gesenkt', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1798.mp3", 'Die Großmutter stopft Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1799.mp3", 'Die Großmutter strickt Mützen', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1800.mp3", 'Du notierst auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1801.mp3", 'Du schreibst auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1802.mp3", 'Er isst heute Kartoffeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1803.mp3", 'Er kocht heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1804.mp3", 'Es ist sehr heiß im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1805.mp3", 'Es ist sehr ruhig im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1806.mp3", 'Ich belüge meine Eltern', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1807.mp3", 'Ich versetze meine Freunde', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1808.mp3", 'Sie beginnt die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1809.mp3", 'Sie wechselt die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.MALE]);

    //sentence fast
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1810.mp3", 'Auf dem Balkon stehen Blumen', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1811.mp3", 'Auf dem Regal stehen Bücher', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1812.mp3", 'Das Auto fährt über die Brücke', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1813.mp3", 'Das Fahrrad fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1814.mp3", 'Das Pferd frisst Heu', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1815.mp3", 'Das Schaf frisst Gras', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1816.mp3", 'Das Gebäude wird abgebrochen', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1817.mp3", 'Das Haus wird verkauft', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1818.mp3", 'Das Kind lernt in der Schule', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1819.mp3", 'Das Kind spielt in der Pause', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1820.mp3", 'Das Haus hat die Farbe rot', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1821.mp3", 'Das Hemd hat die Farbe blau', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1822.mp3", 'Der Bär hat braunes Fell', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1823.mp3", 'Der Hund hat langes Fell', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1824.mp3", 'Wir gehen heute in den Garten', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1825.mp3", 'Wir gehen Sonntag in den Zoo', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1826.mp3", 'Der Junge malt ein Schiff', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1827.mp3", 'Der Künstler malt ein Bild', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1828.mp3", 'Der Kuchen schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1829.mp3", 'Der Tee schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1830.mp3", 'Der Redner spricht deutlich', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1831.mp3", 'Der Richter spricht langsam', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1832.mp3", 'Der Anwalt gewinnt den Prozess', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1833.mp3", 'Der Anwalt übernimmt den Fall', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1834.mp3", 'Die Arbeiter halten sich an die Pläne', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1835.mp3", 'Die Bürger halten sich an die Gesetze', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1836.mp3", 'Die Ärztin hilft dem Kranken', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1837.mp3", 'Die Polizei hilft dem Menschen', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1838.mp3", 'Die Probe ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1839.mp3", 'Die Prüfung ist um acht Uhr', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1840.mp3", 'Die Miete wurde bezahlt', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1841.mp3", 'Die Steuer wurde gesenkt', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1842.mp3", 'Die Großmutter stopft Socken', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1843.mp3", 'Die Großmutter strickt Mützen', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1844.mp3", 'Du notierst auf dem Block', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1845.mp3", 'Du schreibst auf dem Zettel', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1846.mp3", 'Er isst heute Kartoffeln', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1847.mp3", 'Er kocht heute Suppe', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1848.mp3", 'Es ist sehr heiß im Zug', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1849.mp3", 'Es ist sehr ruhig im Park', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1850.mp3", 'Ich belüge meine Eltern', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1851.mp3", 'Ich versetze meine Freunde', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1852.mp3", 'Sie beginnt die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/m1853.mp3", 'Sie wechselt die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.MALE]);
    });
  }
