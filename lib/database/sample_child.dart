import 'model/mood.dart';
import 'model/type.dart';
import 'model/speaker.dart';
import 'model/speed.dart';
import 'package:sqflite/sqflite.dart';

class SampleChild{
  
  Future insertSamplesOfChild(Database db) async {
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k1.mp3", 'Auf dem Balkon stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k2.mp3", 'Auf dem Regal stehen Bücher', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k3.mp3", 'Das Auto fährt über die Brücke', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k4.mp3", 'Das Fahrrad fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k5.mp3", 'Das Pferd frisst Heu', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k6.mp3", 'Das Schaf frisst Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k7.mp3", 'Das Gebäude wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k8.mp3", 'Das Haus wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k9.mp3", 'Das Kind lernt in der Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k10.mp3", 'Das Kind spielt in der Pause', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k11.mp3", 'Das Haus hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k12.mp3", 'Das Hemd hat die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k13.mp3", 'Der Bär hat braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k14.mp3", 'Der Hund hat langes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k15.mp3", 'Wir gehen heute in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k16.mp3", 'Wir gehen Sonntag in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k17.mp3", 'Der Junge malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k18.mp3", 'Der Künstler malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k19.mp3", 'Der Kuchen schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k20.mp3", 'Der Tee schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k21.mp3", 'Der Redner spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k22.mp3", 'Der Richter spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k23.mp3", 'Der Anwalt gewinnt den Prozess', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k24.mp3", 'Der Anwalt übernimmt den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k25.mp3", 'Die Arbeiter halten sich an die Pläne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k26.mp3", 'Die Bürger halten sich an die Gesetze', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k27.mp3", 'Die Ärztin hilft dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k28.mp3", 'Die Polizei hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k29.mp3", 'Die Probe ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k30.mp3", 'Die Prüfung ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k31.mp3", 'Die Miete wurde bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k32.mp3", 'Die Steuer wurde gesenkt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k33.mp3", 'Die Großmutter stopft Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k34.mp3", 'Die Großmutter strickt Mützen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k35.mp3", 'Du notierst auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k36.mp3", 'Du schreibst auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k37.mp3", 'Er isst heute Kartoffeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k38.mp3", 'Er kocht heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k39.mp3", 'Es ist sehr heiß im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k40.mp3", 'Es ist sehr ruhig im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k41.mp3", 'Ich belüge meine Eltern', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k42.mp3", 'Ich versetze meine Freunde', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k43.mp3", 'Sie beginnt die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k44.mp3", 'Sie wechselt die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k45.mp3", 'Abendkleid', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k46.mp3", 'Achterbahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k47.mp3", 'Apfelsaft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k48.mp3", 'Backofen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k49.mp3", 'Baustelle', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k50.mp3", 'Blumenbeet', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k51.mp3", 'Briefkasten', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k52.mp3", 'Dachboden', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k53.mp3", 'Dezember', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k54.mp3", 'Eisenbahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k55.mp3", 'Ententeich', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k56.mp3", 'Erdgeschoss', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k57.mp3", 'Fensterbrett', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k58.mp3", 'Flughafen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k59.mp3", 'Fremdsprache', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k60.mp3", 'Gartenhaus', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k61.mp3", 'Geschichte', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k62.mp3", 'Handwerker', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k63.mp3", 'Hörgerät', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k64.mp3", 'Hürdenlauf', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k65.mp3", 'Instrument', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k66.mp3", 'Kartenspiel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k67.mp3", 'Kofferraum', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k68.mp3", 'Kreisverkehr', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k69.mp3", 'Landwirtschaft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k70.mp3", 'Lippenstift', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k71.mp3", 'Lückentext', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k72.mp3", 'Märchenbuch', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k73.mp3", 'Mitternacht', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k74.mp3", 'Nachrichten', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k75.mp3", 'Nagellack', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k76.mp3", 'Obstwiese', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k77.mp3", 'Papierkorb', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k78.mp3", 'Pferdestall', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k79.mp3", 'Pinselstrich', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k80.mp3", 'Rasierschaum', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k81.mp3", 'Raumstation', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k82.mp3", 'Regenwurm', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k83.mp3", 'Schmetterling', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k84.mp3", 'Straßenbahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k85.mp3", 'Supermarkt', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k86.mp3", 'Tankstelle', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k87.mp3", 'Teebeutel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k88.mp3", 'Tischdecke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k89.mp3", 'Urlaubszeit', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k90.mp3", 'Vogelnest', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k91.mp3", 'Waschbecken', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k92.mp3", 'Weintrauben', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k93.mp3", 'Windmühle', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/k94.mp3", 'Zahnschmerzen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.CHILD]);
    });
  }
  
}