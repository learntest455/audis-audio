import 'package:sqflite/sqflite.dart';

class Answers{

  Future insertAnswer(Database db) async {
    await db.transaction((txn) async {
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [1, 'Regal']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [1, 'Tisch']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [1, 'Boden']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [1, 'Schrank']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [1, 'Balkon']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [2, 'Bilder']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [2, 'Blumen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [2, 'Fotos']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [2, 'Bücher']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [2, 'Ordner']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [2, 'Kisten']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [3, 'Auto']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [3, 'Mädchen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [3, 'Fahrrad']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [3, 'Taxi']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [4, 'Straße']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [4, 'Brücke']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [4, 'Autobahn']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [4, 'Ampel']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [4, 'Kreuzung']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [5, 'Pferd']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [5, 'Schaf']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [5, 'Tier']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [5, 'Rind']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [6, 'Gras']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [6, 'Futter']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [6, 'Heu']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [6, 'Pflanzen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [7, 'Gebäude']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [7, 'Haus']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [7, 'Hotel']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [7, 'Viertel']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [7, 'Apartment']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [8, 'abgebrochen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [8, 'renoviert']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [8, 'gestrichen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [8, 'gebaut']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [8, 'verkauft']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [8, 'geräumt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [9, 'lernt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [9, 'spielt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [9, 'rennt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [9, 'sitzt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [10, 'Sonne']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [10, 'Pause']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [10, 'Schule']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [10, 'Küche']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [11, 'Auto']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [11, 'Haus']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [11, 'Leder']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [11, 'Sofa']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [11, 'Hemd']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [12, 'rot']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [12, 'schwarz']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [12, 'grau']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [12, 'weiß']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [12, 'blau']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [13, 'Bär']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [13, 'Hund']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [13, 'Hase']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [13, 'Biber']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [14, 'braunes']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [14, 'helles']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [14, 'dunkles']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [14, 'warmes']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [14, 'langes']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [14, 'dichtes']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [15, 'Montag']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [15, 'heute']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [15, 'morgen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [15, 'Samstag']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [15, 'Sonntag']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [16, 'Zoo']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [16, 'Park']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [16, 'Laden']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [16, 'Garten']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [16, 'Zirkus']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [17, 'Junge']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [17, 'Künstler']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [17, 'Mann']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [17, 'Schüler']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [17, 'Maler']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [18, 'Haus']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [18, 'Tier']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [18, 'Auto']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [18, 'Schiff']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [18, 'Bild']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [19, 'Tee']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [19, 'Kuchen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [19, 'Nachtisch']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [19, 'Saft']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [19, 'Joghurt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [20, 'Orange']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [20, 'Erdbeere']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [20, 'Vanille']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [20, 'Früchten']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [21, 'Lehrer']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [21, 'Schüler']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [21, 'Redner']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [21, 'Anwalt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [21, 'Richter']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [22, 'deutlich']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [22, 'schnell']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [22, 'laut']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [22, 'leise']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [22, 'langsam']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [23, 'übernimmt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [23, 'gewinnt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [23, 'verliert']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [23, 'bearbeitet']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [24, 'Fall']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [24, 'Prozess']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [24, 'Auftrag']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [24, 'Klienten']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [25, 'Angestellten']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [25, 'Arbeiter']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [25, 'Bauern']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [25, 'Menschen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [25, 'Bürger']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [26, 'Vorgaben']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [26, 'Pläne']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [26, 'Regeln']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [26, 'Gesetze']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [26, 'Richtlinien']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [27, 'Medizin']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [27, 'Ärztin']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [27, 'Polizei']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [27, 'Frau']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [27, 'Therapie']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [28, 'Kind']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [28, 'Patienten']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [28, 'Tier']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [28, 'Menschen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [28, 'Kranken']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [29, 'Abfahrt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [29, 'Ankunft']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [29, 'Vorstellung']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [29, 'Prüfung']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [29, 'Probe']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [30, 'acht']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [30, 'drei']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [30, 'neun']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [30, 'fünf']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [30, 'sechs']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [31, 'Strafe']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [31, 'Rechnung']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [31, 'Miete']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [31, 'Steuer']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [32, 'bezahlt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [32, 'erhöht']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [32, 'beglichen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [32, 'gesenkt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [33, 'verschenkt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [33, 'strickt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [33, 'kauft']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [33, 'stopft']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [34, 'Socken']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [34, 'Pullover']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [34, 'Schals']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [34, 'Mützen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [35, 'schreibst']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [35, 'malst']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [35, 'liest']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [35, 'zeichnest']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [35, 'notierst']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [36, 'Zettel']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [36, 'Umschlag']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [36, 'Block']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [36, 'Brief']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [36, 'Papier']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [37, 'kocht']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [37, 'isst']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [37, 'kauft']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [37, 'serviert']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [38, 'Suppe']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [38, 'Reis']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [38, 'Nudeln']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [38, 'Kartoffeln']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [38, 'Eintopf']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [38, 'Gemüse']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [39, 'voll']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [39, 'leer']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [39, 'kalt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [39, 'ruhig']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [39, 'laut']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [39, 'heiß']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [40, 'Park']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [40, 'Bus']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [40, 'Laden']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [40, 'Zoo']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [40, 'Zug']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [41, 'versetze']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [41, 'belüge']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [41, 'beschenke']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [41, 'verrate']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [42, 'Eltern']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [42, 'Freunde']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [42, 'Kollegen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [42, 'Brüder']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [42, 'Freundin']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [43, 'verlässt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [43, 'wechselt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [43, 'beginnt']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [43, 'beendet']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [44, 'diesen']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [44, 'nächsten']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [44, 'noch im']);
      await txn.execute(
          "INSERT INTO answer ('answer_id', 'answer') values (?, ?)",
          [44, 'erst im']);
    });
  }
}