import 'model/mood.dart';
import 'model/type.dart';
import 'model/speaker.dart';
import 'model/speed.dart';
import 'package:sqflite/sqflite.dart';

class SampleFemale{

  void createSampleTableFemale(Database db) {
    _insertFirst200SamplesFemale(db);
    _insertSample201to400Female(db);
    _insertSample401to600Female(db);

    _insertSamples601to800Female(db);
    _insertSamples801to1000Female(db);
    _insertSamples1000to1062Female(db);

    _insertSamples1063to1200Female(db);
    _insertSamples1201to1400Female(db);
    _insertSamples1401to1600Female(db);

    _insertSamples1601toEndFemale(db);
  }
  //Female realword samples
  Future _insertFirst200SamplesFemale(Database db) async{
    await db.transaction((txn) async {
    //real words
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0039.mp3", 'Aal', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0040.mp3", 'Aas', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0041.mp3", 'Abendkleid', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0042.mp3", 'Abteilung', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0043.mp3", 'Achterbahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0044.mp3", 'Afrika', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0045.mp3", 'Akt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0046.mp3", 'Akustiker', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0047.mp3", 'Allergie', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0048.mp3", 'Altenpfleger', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0049.mp3", 'Altstadt', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0050.mp3", 'Amerika', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0051.mp3", 'Ampel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0052.mp3", 'Amt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0053.mp3", 'Ananas', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0054.mp3", 'Angst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0055.mp3", 'Anwalt', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0056.mp3", 'Anzeige', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0057.mp3", 'Anzug', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0058.mp3", 'Apfel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0059.mp3", 'Apfelsaft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0060.mp3", 'Apotheke', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0061.mp3", 'April', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0062.mp3", 'Arm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0063.mp3", 'Armband', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0064.mp3", 'Armlehne', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0065.mp3", 'Art', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0066.mp3", 'Arzt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0067.mp3", 'Arztpraxis', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0068.mp3", 'Asien', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0069.mp3", 'Ast', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0070.mp3", 'Astronaut', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0071.mp3", 'Atemnot', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0072.mp3", 'auch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0073.mp3", 'Auge', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0074.mp3", 'August', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0075.mp3", 'aus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0076.mp3", 'Ausflugsziel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0077.mp3", 'Australien', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0078.mp3", 'Auto', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0079.mp3", 'Autobahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0080.mp3", 'Axt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0081.mp3", 'Bach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0082.mp3", 'Bäcker', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0083.mp3", 'Backofen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0084.mp3", 'Badesee', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0085.mp3", 'Bahn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0086.mp3", 'Bahnhof', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0087.mp3", 'bald', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0088.mp3", 'Ball', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0089.mp3", 'Banane', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0090.mp3", 'Band', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0091.mp3", 'Bank', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0092.mp3", 'bar', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0093.mp3", 'Bär', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0094.mp3", 'Bart', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0095.mp3", 'Base', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0096.mp3", 'Basketball', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0097.mp3", 'Bass', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0098.mp3", 'Batterie', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0099.mp3", 'Bauarbeiter', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0100.mp3", 'Bauch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0101.mp3", 'Bauchschmerzen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0102.mp3", 'Bauer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0103.mp3", 'Bauernhof', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0104.mp3", 'Bauklötze', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0105.mp3", 'Baum', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0106.mp3", 'Bausparen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0107.mp3", 'Baustelle', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0108.mp3", 'Bein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0109.mp3", 'Berg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0110.mp3", 'Bergsteigen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0111.mp3", 'Bett', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0112.mp3", 'Bettdecke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0113.mp3", 'Beutel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0114.mp3", 'Beuteltier', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0115.mp3", 'Bewerbung', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0116.mp3", 'Bibliothek', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0117.mp3", 'Bienenkorb', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0118.mp3", 'Bier', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0119.mp3", 'Bild', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0120.mp3", 'Bilderbuch', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0121.mp3", 'Bildschirm', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0122.mp3", 'Birne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0123.mp3", 'Blase', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0124.mp3", 'Blatt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0125.mp3", 'blau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0126.mp3", 'Blech', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0127.mp3", 'Blei', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]); //bis hier richtig
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0128.mp3", 'Blick', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0129.mp3", 'Blitz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0130.mp3", 'Block', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0131.mp3", 'Blumenbeet', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0132.mp3", 'Blumentopf', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0133.mp3", 'Bluse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0134.mp3", 'Bock', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0135.mp3", 'Boden', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0136.mp3", 'Bohrmaschine', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0137.mp3", 'Boot', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0138.mp3", 'Bord', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0139.mp3", 'böse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0140.mp3", 'Boss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0141.mp3", 'Bratensatz', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0142.mp3", 'Brauch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0143.mp3", 'Braut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0144.mp3", 'brav', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0145.mp3", 'Brett', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0146.mp3", 'Brief', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0147.mp3", 'Briefkasten', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0148.mp3", 'Brillenglas', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0149.mp3", 'Brot', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0150.mp3", 'Brötchen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0151.mp3", 'Brotkorb', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0152.mp3", 'Bruch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0153.mp3", 'Bruder', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0154.mp3", 'Brust', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0155.mp3", 'Buch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0156.mp3", 'Bücher', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0157.mp3", 'Bucht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0158.mp3", 'Bund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0159.mp3", 'Bundestag', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0160.mp3", 'Buntstift', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0161.mp3", 'Burg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0162.mp3", 'Bürgersteig', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0163.mp3", 'Burggraben', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0164.mp3", 'Büro', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0165.mp3", 'Bus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0166.mp3", 'Busch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0167.mp3", 'Chef', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0168.mp3", 'Chor', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0169.mp3", 'Computer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0170.mp3", 'Dach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0171.mp3", 'Dachboden', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0172.mp3", 'Dachs', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0173.mp3", 'Damm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0174.mp3", 'Dank', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0175.mp3", 'Darm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0176.mp3", 'Deich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0177.mp3", 'Dezember', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0178.mp3", 'dicht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0179.mp3", 'Dieb', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0180.mp3", 'Dienstag', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0181.mp3", 'Ding', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0182.mp3", 'Disziplin', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0183.mp3", 'doch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0184.mp3", 'Docht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0185.mp3", 'Dokument', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0186.mp3", 'Dom', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0187.mp3", 'Donner', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0188.mp3", 'Donnerstag', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0189.mp3", 'Dorf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0190.mp3", 'Dose', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0191.mp3", 'Drache', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0192.mp3", 'Draht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0193.mp3", 'Drang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0194.mp3", 'Dreck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0195.mp3", 'Drehstul', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0196.mp3", 'Dreirad', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0197.mp3", 'Druck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0198.mp3", 'Duft', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0199.mp3", 'Dunst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    });
  }

  Future _insertSample201to400Female(Database db) async {
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0200.mp3", 'Durchschnittswert', 3,Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0201.mp3", 'Durst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0202.mp3", 'Dusche', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0203.mp3", 'Duschvorhang', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0204.mp3", 'Ehebett', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0205.mp3", 'Ei', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0206.mp3", 'Einbruch', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0207.mp3", 'Eis', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0208.mp3", 'Eisbär', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0210.mp3", 'Eisdiele', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0211.mp3", 'Eisenbahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0212.mp3", 'Eiskugel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0213.mp3", 'Eiskunstlauf', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0214.mp3", 'Elektriker', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0215.mp3", 'Elfen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0216.mp3", 'Ende', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0217.mp3", 'Enkel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0218.mp3", 'Ente', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0219.mp3", 'Ententeich', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0220.mp3", 'Erdbeere', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0221.mp3", 'Erdgeschoss', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0222.mp3", 'Erzieherin', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0223.mp3", 'Ethik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0224.mp3", 'Europa', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0225.mp3", 'Fabrik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0226.mp3", 'Fach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0227.mp3", 'Fahrbahn', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0228.mp3", 'Fahrradständer', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0229.mp3", 'Fakt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0230.mp3", 'Fall', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0231.mp3", 'Fallschirmsprung', 3,Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0232.mp3", 'Familie', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0233.mp3", 'Fass', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0234.mp3", 'fast', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0235.mp3", 'Faust', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0236.mp3", 'Fax', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0237.mp3", 'Februar', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0238.mp3", 'Fee', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0239.mp3", 'Feierabend', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0240.mp3", 'Feiertag', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0241.mp3", 'Feige', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0242.mp3", 'Feigenbaum', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0243.mp3", 'Feind', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0244.mp3", 'Feld', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0245.mp3", 'Feldsalat', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0246.mp3", 'Fell', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0247.mp3", 'Fels', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0248.mp3", 'Fenster', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0249.mp3", 'Fensterbrett', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0250.mp3", 'Ferien', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0251.mp3", 'fern', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0252.mp3", 'Fernseher', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0253.mp3", 'Fest', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0254.mp3", 'Fett', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0255.mp3", 'Feuerwehr', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0256.mp3", 'Film', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0257.mp3", 'Finger', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0258.mp3", 'Fisch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0259.mp3", 'Fleck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0260.mp3", 'Fleisch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0261.mp3", 'Floß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0262.mp3", 'Flöte', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0263.mp3", 'Fluch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0264.mp3", 'Flucht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0265.mp3", 'Flug', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0266.mp3", 'Flughafen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0267.mp3", 'Flugzeug', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0268.mp3", 'Flur', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0269.mp3", 'Fluss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0270.mp3", 'Flussufer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0271.mp3", 'Flut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0272.mp3", 'Föhn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0273.mp3", 'Form', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0274.mp3", 'Fotograf', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0275.mp3", 'Fracht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0276.mp3", 'Frau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0277.mp3", 'frech', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0278.mp3", 'Freitag', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0279.mp3", 'Freizeit', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0280.mp3", 'Fremdsprache', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0281.mp3", 'Freund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0282.mp3", 'frisch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0283.mp3", 'Frist', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0284.mp3", 'froh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0285.mp3", 'Frosch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0286.mp3", 'Frost', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0287.mp3", 'Frucht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0288.mp3", 'Frühjahrsputz', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0289.mp3", 'Frühling', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0290.mp3", 'Frühstück', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0291.mp3", 'Fuchs', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0292.mp3", 'Füller', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0293.mp3", 'Funk', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0294.mp3", 'Furcht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0295.mp3", 'Fuß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0296.mp3", 'Fußball', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0297.mp3", 'Futter', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0298.mp3", 'Gabel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0299.mp3", 'Gang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0300.mp3", 'Gans', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0301.mp3", 'Gartenhaus', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0302.mp3", 'Gärtner', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0303.mp3", 'Gas', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0304.mp3", 'Gase', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0305.mp3", 'Gast', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0306.mp3", 'Geburtstag', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0307.mp3", 'Gedicht', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0308.mp3", 'Gefängnis', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0309.mp3", 'Gegner', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0310.mp3", 'Geige', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0311.mp3", 'Geist', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0312.mp3", 'gelb', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0313.mp3", 'Geld', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0314.mp3", 'Gericht', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0315.mp3", 'gern', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0316.mp3", 'Gesang', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0317.mp3", 'Geschäft', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0318.mp3", 'Geschichte', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0319.mp3", 'Gesetzbuch', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0320.mp3", 'Gesundheit', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0321.mp3", 'Getränk', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0322.mp3", 'Gewichtheben', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0323.mp3", 'Gier', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0324.mp3", 'Gift', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0325.mp3", 'Gips', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0326.mp3", 'Glanz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0327.mp3", 'Glas', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0328.mp3", 'gleich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0329.mp3", 'Gleis', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0330.mp3", 'Glück', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0331.mp3", 'Glut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0332.mp3", 'Gold', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0333.mp3", 'Goldschmied', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0334.mp3", 'Golf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0335.mp3", 'Gott', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0336.mp3", 'Grab', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0337.mp3", 'Grad', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0338.mp3", 'Graf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0339.mp3", 'Gramm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0340.mp3", 'Grafik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0341.mp3", 'Gras', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0342.mp3", 'grau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0343.mp3", 'Griff', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0344.mp3", 'Grillkohle', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0345.mp3", 'groß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0346.mp3", 'grün', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0347.mp3", 'Grund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0348.mp3", 'Gruppe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0349.mp3", 'Gruß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0350.mp3", 'Gummiband', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0351.mp3", 'gut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0352.mp3", 'Haar', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0353.mp3", 'Haarklammer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0354.mp3", 'Hafen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0355.mp3", 'Haftnotiz', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0356.mp3", 'Hagelkorn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0357.mp3", 'Hahn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0358.mp3", 'Hall', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0359.mp3", 'Hammer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0360.mp3", 'Hand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0361.mp3", 'Handtasche', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0362.mp3", 'Handtuch', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0363.mp3", 'Handwerker', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0364.mp3", 'Hang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0365.mp3", 'hart', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0366.mp3", 'Hase', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0367.mp3", 'Hass', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0368.mp3", 'Hauptgang', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0369.mp3", 'Haus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0370.mp3", 'Hausaufgaben', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0371.mp3", 'Hausschlüssel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0372.mp3", 'Haut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0373.mp3", 'Hecht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0374.mp3", 'Heft', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0375.mp3", 'Heizung', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0376.mp3", 'Held', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0377.mp3", 'heller', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0378.mp3", 'Helm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0379.mp3", 'Hemd', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0380.mp3", 'Herbst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0381.mp3", 'Herd', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0382.mp3", 'Herr', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0383.mp3", 'Herz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0384.mp3", 'Heu', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0385.mp3", 'Hexe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0386.mp3", 'hier', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0387.mp3", 'Himbeere', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0388.mp3", 'Himmel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0389.mp3", 'Hirn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0390.mp3", 'Hitze', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0391.mp3", 'Holz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0392.mp3", 'Hörgerät', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0393.mp3", 'Hose', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0394.mp3", 'Hotel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0395.mp3", 'Hotelbett', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0396.mp3", 'Hubschrauber', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0397.mp3", 'Huhn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0398.mp3", 'Hund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0399.mp3", 'Hürdenlauf', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0400.mp3", 'Hut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    });
  }

  Future _insertSample401to600Female(Database db) async {
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0401.mp3", 'Insekt', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0402.mp3", 'Instrument', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0403.mp3", 'Internet', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0404.mp3", 'Jacke', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0405.mp3", 'Jahrgang', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0406.mp3", 'Januar', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0407.mp3", 'Juli', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0408.mp3", 'Juni', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0409.mp3", 'Kabel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0410.mp3", 'Kaffee', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0411.mp3", 'Kaffeefleck', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0412.mp3", 'Kaffeemaschine', 5, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0413.mp3", 'kalt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0414.mp3", 'Kamm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0415.mp3", 'Kanne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0416.mp3", 'Kantine', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0417.mp3", 'Karte', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0418.mp3", 'Kartenspiel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0419.mp3", 'Kartoffel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0420.mp3", 'Käse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0421.mp3", 'Kasse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0422.mp3", 'Kassenbon', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0423.mp3", 'Katalog', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0424.mp3", 'Katze', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0425.mp3", 'Kauf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0426.mp3", 'Kauffrau', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0427.mp3", 'Kaufhaus', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0428.mp3", 'Kaufmann', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0429.mp3", 'Keim', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0430.mp3", 'Keller', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0431.mp3", 'Kellner', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0432.mp3", 'Kern', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0433.mp3", 'Kerze', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0434.mp3", 'Kies', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0435.mp3", 'Kind', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0436.mp3", 'Kindergarten', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0437.mp3", 'Kinderlied', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0438.mp3", 'Kinn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0439.mp3", 'Kino', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0440.mp3", 'Kirche', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0441.mp3", 'Kirchturmuhr', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0442.mp3", 'Kirsche', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0443.mp3", 'Kirschkuchen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0444.mp3", 'Kissenschlacht', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0445.mp3", 'Kiste', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0446.mp3", 'Kittel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0447.mp3", 'Kiwi', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0448.mp3", 'Klammer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0449.mp3", 'Klang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0450.mp3", 'klar', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0451.mp3", 'Klasse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0452.mp3", 'Klassik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0453.mp3", 'Klavier', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0454.mp3", 'Klebeband', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0455.mp3", 'Klee', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0456.mp3", 'Kleid', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0457.mp3", 'Kleiderschrank', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0458.mp3", 'klein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0459.mp3", 'Kleingeld', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0460.mp3", 'Klingelton', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0461.mp3", 'Kloß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0462.mp3", 'klug', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0463.mp3", 'Knall', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0464.mp3", 'Knecht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0465.mp3", 'Knie', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0466.mp3", 'Kobold', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0467.mp3", 'Koch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0468.mp3", 'Kochlöffel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0469.mp3", 'Kofferraum', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0470.mp3", 'Kollege', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0471.mp3", 'Kompass', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0472.mp3", 'Kopf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0473.mp3", 'Kopfhörer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0474.mp3", 'Korb', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0475.mp3", 'Kork', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0476.mp3", 'Kost', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0477.mp3", 'Krach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0478.mp3", 'Kran', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0479.mp3", 'krank', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0480.mp3", 'Krankenhaus', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0481.mp3", 'Krankenschwester', 4,Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0482.mp3", 'Kranz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0483.mp3", 'Krebs', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0484.mp3", 'Kreis', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0485.mp3", 'Kreisverkehr', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0486.mp3", 'Kreuz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0487.mp3", 'Kreuzfahrtschiff', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0488.mp3", 'Krieg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0489.mp3", 'Küche', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0490.mp3", 'Küchentisch', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0491.mp3", 'Kugelschreiber', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0492.mp3", 'Kuh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0493.mp3", 'Kühlschrank', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0494.mp3", 'Kult', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0495.mp3", 'Kunst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0496.mp3", 'Kur', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0497.mp3", 'Kuss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0498.mp3", 'Küste', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0499.mp3", 'Lachs', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0500.mp3", 'Lack', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0501.mp3", 'lahm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0502.mp3", 'Lamm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0503.mp3", 'Lampenschirm', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0504.mp3", 'Land', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0505.mp3", 'Landebahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0506.mp3", 'Landkarte', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0507.mp3", 'Landwirtschaft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0508.mp3", 'lang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0509.mp3", 'Lärm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0510.mp3", 'Last', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0511.mp3", 'Lastwagen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0512.mp3", 'Laub', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0513.mp3", 'Lauch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0514.mp3", 'Lauf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0515.mp3", 'Laus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0516.mp3", 'laut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0517.mp3", 'Lautstärke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0518.mp3", 'Leberwurst', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0519.mp3", 'leer', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0520.mp3", 'Lehrer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0521.mp3", 'leicht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0522.mp3", 'Leid', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0523.mp3", 'Leim', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0524.mp3", 'Licht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0525.mp3", 'Lichtschalter', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0526.mp3", 'lieb', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0527.mp3", 'Liebe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0528.mp3", 'Liebespaar', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0529.mp3", 'Lied', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0530.mp3", 'Liegestuhl', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0531.mp3", 'Lift', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0532.mp3", 'Lineal', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0533.mp3", 'Lippenstift', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0534.mp3", 'Loch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0535.mp3", 'Locher', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0536.mp3", 'Löffel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0537.mp3", 'Logik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0538.mp3", 'Lohn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0539.mp3", 'Lok', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0540.mp3", 'Lokführer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0541.mp3", 'Los', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0542.mp3", 'Lückentext', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0543.mp3", 'Luft', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0544.mp3", 'Lupe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0545.mp3", 'Lust', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0546.mp3", 'Lutscher', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0547.mp3", 'Macht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0548.mp3", 'Magie', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0549.mp3", 'Mai', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0550.mp3", 'Mais', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0551.mp3", 'Mandarine', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0552.mp3", 'Mango', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0553.mp3", 'Mann', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0554.mp3", 'Marathon', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0555.mp3", 'Märchenbuch', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0556.mp3", 'Mark', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0557.mp3", 'Markt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0558.mp3", 'Marktplatz', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0559.mp3", 'März', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0560.mp3", 'Maskenball', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0561.mp3", 'Maß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0562.mp3", 'Masse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0563.mp3", 'Mast', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0564.mp3", 'Matratze', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0565.mp3", 'Maus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0566.mp3", 'Maut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0567.mp3", 'Medien', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0568.mp3", 'Mediziner', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0569.mp3", 'Meer', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0570.mp3", 'Mehl', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0571.mp3", 'Melodie', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0572.mp3", 'Melone', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0573.mp3", 'Messer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0574.mp3", 'Mett', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0575.mp3", 'Metzger', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0576.mp3", 'Mikroskop', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0577.mp3", 'Mikrowelle', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0578.mp3", 'Milch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0579.mp3", 'Milchkaffee', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0580.mp3", 'mild', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0581.mp3", 'Militär', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0582.mp3", 'Mist', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0583.mp3", 'Mittagspause', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0584.mp3", 'Mitternacht', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0585.mp3", 'Mittwoch', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0586.mp3", 'Mixer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0587.mp3", 'Moderation', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0588.mp3", 'Monat', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0589.mp3", 'Mönch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0590.mp3", 'Mond', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0591.mp3", 'Montag', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0592.mp3", 'Moor', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0593.mp3", 'Moos', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0594.mp3", 'Mord', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0595.mp3", 'Motorrad', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0596.mp3", 'Möwe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    //await txn.execute(
    //     "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
    //     ["raw/f0597.mp3", 'Mückenstich', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);  <-- there is no mückenstich
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0597.mp3", 'Mülleimer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0598.mp3", 'Mund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0599.mp3", 'Münze', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    });
  }

  Future _insertSamples601to800Female(Database db) async {
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0600.mp3", 'Muschel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0601.mp3", 'Mut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0602.mp3", 'Mütze', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0603.mp3", 'Nachrichten', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0604.mp3", 'Nacht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0605.mp3", 'Nachtisch', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0606.mp3", 'nackt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0607.mp3", 'Nagel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0608.mp3", 'Nagellack', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0609.mp3", 'Naht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0610.mp3", 'Name', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0611.mp3", 'Narr', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0612.mp3", 'Nase', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0613.mp3", 'nass', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0614.mp3", 'Neffe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0615.mp3", 'Neid', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0616.mp3", 'Nest', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0617.mp3", 'nett', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0618.mp3", 'Netz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0619.mp3", 'neu', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0620.mp3", 'nicht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0621.mp3", 'Nichte', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0622.mp3", 'noch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0623.mp3", 'Nonne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0624.mp3", 'Norden', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0625.mp3", 'Not', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0626.mp3", 'November', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0627.mp3", 'Nudel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0628.mp3", 'Nuss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0629.mp3", 'Obstwiese', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0630.mp3", 'Ohr', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0631.mp3", 'Oktober', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0632.mp3", 'Öl', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0633.mp3", 'Oma', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0634.mp3", 'Onkel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0635.mp3", 'Opa', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0636.mp3", 'orange', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0637.mp3", 'Orchester', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0638.mp3", 'Ort', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0639.mp3", 'Osten', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0640.mp3", 'Ozean', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0641.mp3", 'Päckchen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0642.mp3", 'Paket', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0643.mp3", 'Pakt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0644.mp3", 'Panne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0645.mp3", 'Papierkorb', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0646.mp3", 'Paradies', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0647.mp3", 'Park', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0648.mp3", 'Parkdeck', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0649.mp3", 'Parklücke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0650.mp3", 'Pass', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0651.mp3", 'Passwörter', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0652.mp3", 'Patenkind', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0653.mp3", 'Pause', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0654.mp3", 'Pech', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0655.mp3", 'Pelz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0656.mp3", 'Pest', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0657.mp3", 'Pfad', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0658.mp3", 'Pfand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0659.mp3", 'Pfanne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0660.mp3", 'Pfau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0661.mp3", 'Pfeil', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0662.mp3", 'Pferd', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0663.mp3", 'Pferdestall', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0664.mp3", 'Pfiff', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0665.mp3", 'Pfirsich', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0666.mp3", 'Pflasterstein', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0667.mp3", 'Pflaume', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0668.mp3", 'Pfund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0669.mp3", 'Pilz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0670.mp3", 'Pinselstrich', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0671.mp3", 'Plan', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0672.mp3", 'Platz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0673.mp3", 'Plätzchen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0674.mp3", 'Polizei', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0675.mp3", 'Pose', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0676.mp3", 'Post', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0677.mp3", 'Preis', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0678.mp3", 'Priester', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0679.mp3", 'Prinz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0680.mp3", 'Prinzessin', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0681.mp3", 'Programmheft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0682.mp3", 'Prüfung', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0683.mp3", 'Prüfungsangst', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0684.mp3", 'Puls', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0685.mp3", 'Pult', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0686.mp3", 'Punkt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0687.mp3", 'Puppe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0688.mp3", 'Puppenhaus', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0689.mp3", 'Quittung', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0690.mp3", 'Quizsendung', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0691.mp3", 'Rabatt', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0692.mp3", 'Rad', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0693.mp3", 'Radio', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0694.mp3", 'Radmutter', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0695.mp3", 'Radrennen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0696.mp3", 'Radweg', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0697.mp3", 'Rakete', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0698.mp3", 'Rand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0699.mp3", 'Rang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0700.mp3", 'Rasen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0701.mp3", 'Rasierschaum', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0702.mp3", 'Rasse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0703.mp3", 'Rast', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0704.mp3", 'Rathaus', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0705.mp3", 'rau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0706.mp3", 'Raub', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0707.mp3", 'Raum', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]); //erste Version Raum
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0708.mp3", 'Raum', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]); //zweite Version Raum
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0709.mp3", 'Raumstation', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0710.mp3", 'raus', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0711.mp3", 'Recht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0712.mp3", 'Redaktion', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0713.mp3", 'Rednerpult', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0714.mp3", 'Regalbrett', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0715.mp3", 'Regen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0716.mp3", 'Regenjacke', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0717.mp3", 'Regenwurm', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0718.mp3", 'Reh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0719.mp3", 'Reich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0720.mp3", 'Reim', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0721.mp3", 'Reis', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0722.mp3", 'Reisen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0723.mp3", 'Reisepass', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0724.mp3", 'Reiz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0725.mp3", 'Rest', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0726.mp3", 'Rettung', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0727.mp3", 'Richter', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0728.mp3", 'Riesenrad', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0729.mp3", 'Riff', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0730.mp3", 'Rind', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0731.mp3", 'Ring', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0732.mp3", 'Ritter', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0733.mp3", 'Rock', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0734.mp3", 'roh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0735.mp3", 'Rohr', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0736.mp3", 'Rolltreppe', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0737.mp3", 'rosa', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0738.mp3", 'Rose', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0739.mp3", 'Rosenkohl', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0740.mp3", 'Rost', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0741.mp3", 'rot', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0742.mp3", 'Rucksack', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0743.mp3", 'Ruderboot', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0744.mp3", 'Ruf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0745.mp3", 'Ruhm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0746.mp3", 'rund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0747.mp3", 'Sack', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0748.mp3", 'Saft', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0749.mp3", 'Sage', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0750.mp3", 'Salz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0751.mp3", 'Salzstreuer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0752.mp3", 'Samstag', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0753.mp3", 'Sand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0754.mp3", 'Sarg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0755.mp3", 'Satz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0756.mp3", 'Sau', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0757.mp3", 'Säulengang', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0758.mp3", 'Schach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0759.mp3", 'Schacht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0760.mp3", 'Schaf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0761.mp3", 'Schall', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0762.mp3", 'Schar', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0763.mp3", 'scharf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0764.mp3", 'Schatz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0765.mp3", 'Schaukelpferd', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0766.mp3", 'Schaum', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0767.mp3", 'Scheck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0768.mp3", 'Scheich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0769.mp3", 'Schein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0770.mp3", 'Schere', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0771.mp3", 'Scherz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0772.mp3", 'scheu', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0773.mp3", 'Scheune', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0774.mp3", 'Schicht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0775.mp3", 'Schicksalsschlag',3,Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0776.mp3", 'Schiff', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0777.mp3", 'Schild', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0778.mp3", 'Schilf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0779.mp3", 'Schlacht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0780.mp3", 'Schlaf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0781.mp3", 'Schlafzimmer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0782.mp3", 'Schlag', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0783.mp3", 'Schlamm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0784.mp3", 'schlank', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0785.mp3", 'Schlauch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0786.mp3", 'Schleim', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0787.mp3", 'Schloss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0788.mp3", 'Schlucht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0789.mp3", 'Schluss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0790.mp3", 'Schlüssel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0791.mp3", 'schmal', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0792.mp3", 'Schmerz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0793.mp3", 'Schmetterling', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0794.mp3", 'Schmied', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0795.mp3", 'Schmutz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0796.mp3", 'Schneckenhaus', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0797.mp3", 'Schnee', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0798.mp3", 'Schneebesen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0799.mp3", 'schneller', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0800.mp3", 'Schnitt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    });
  }

  Future _insertSamples801to1000Female(Database db) async {
      await db.transaction((txn) async {
      await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0801.mp3", 'Schokolade', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0802.mp3", 'schon', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0803.mp3", 'Schornstein', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0804.mp3", 'Schrank', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0805.mp3", 'Schraube', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0806.mp3", 'Schreck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0807.mp3", 'Schrei', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0808.mp3", 'Schreibtisch', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0809.mp3", 'Schrift', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0810.mp3", 'Schrott', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0811.mp3", 'Schuh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0812.mp3", 'Schuhgeschäft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0813.mp3", 'Schulabschluss', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0814.mp3", 'Schuld', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0815.mp3", 'Schule', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0816.mp3", 'Schulhof', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0817.mp3", 'Schulleiter', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0818.mp3", 'Schuppe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0819.mp3", 'Schuss', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0820.mp3", 'Schutz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0821.mp3", 'Schwanz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0822.mp3", 'Schwarm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0823.mp3", 'schwarz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0824.mp3", 'Schwein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0825.mp3", 'Schweiß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0826.mp3", 'Schwert', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0827.mp3", 'Schwester', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0828.mp3", 'Schwiegereltern', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0829.mp3", 'Schwimmbad', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0830.mp3", 'schwimmen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0831.mp3", 'Schwung', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0832.mp3", 'See', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0833.mp3", 'Seefahrt', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0834.mp3", 'Seife', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0835.mp3", 'Seil', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0836.mp3", 'Sekretärin', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0837.mp3", 'Sekt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0838.mp3", 'Sender', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0839.mp3", 'September', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0840.mp3", 'Sicherheit', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0841.mp3", 'Sicht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0842.mp3", 'Sieb', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0843.mp3", 'Sinn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0844.mp3", 'Sinnfrage', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0845.mp3", 'Sitz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0846.mp3", 'Sitzkissen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0847.mp3", 'Sitznachbar', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0848.mp3", 'Ski', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0849.mp3", 'Sofa', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0850.mp3", 'Sohn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0851.mp3", 'Sommer', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0852.mp3", 'Sommerfest', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0853.mp3", 'Sonne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0854.mp3", 'Sonnenschirm', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0855.mp3", 'Sonnenuhr', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0856.mp3", 'Sonntag', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0857.mp3", 'Spalt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0858.mp3", 'Spaß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0859.mp3", 'Specht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0860.mp3", 'Speck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0861.mp3", 'Speer', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0862.mp3", 'Speerwurf', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0863.mp3", 'Spiel', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0864.mp3", 'Spielfeld', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0865.mp3", 'Spielplatz', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0866.mp3", 'Spielstraße', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0867.mp3", 'Spielzeug', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0868.mp3", 'Spieß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0869.mp3", 'Spind', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0870.mp3", 'spitz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0871.mp3", 'Sport', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0872.mp3", 'Sportverein', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0873.mp3", 'Sprache', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0874.mp3", 'Spruch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0875.mp3", 'Sprung', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0876.mp3", 'Spüle', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0877.mp3", 'Spülmittel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0878.mp3", 'Spur', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0879.mp3", 'Staat', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0880.mp3", 'Stab', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0881.mp3", 'Stabhochsprung', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0882.mp3", 'Stadt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0883.mp3", 'Stamm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0884.mp3", 'Stand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0885.mp3", 'Start', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0886.mp3", 'Staub', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0887.mp3", 'Steckdose', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0888.mp3", 'Stecknadel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0889.mp3", 'Stein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0890.mp3", 'Stern', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0891.mp3", 'Stich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0892.mp3", 'Stiel', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0893.mp3", 'Stier', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0894.mp3", 'Stift', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0895.mp3", 'Stimmgabel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0896.mp3", 'Stock', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0897.mp3", 'Stolz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0898.mp3", 'Stoß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0899.mp3", 'stramm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0900.mp3", 'Strand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0901.mp3", 'Strandkorb', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0902.mp3", 'Strandurlaub', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0903.mp3", 'Straße', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0904.mp3", 'Straßenbahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0905.mp3", 'Stress', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0906.mp3", 'Strich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0907.mp3", 'Stroh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0908.mp3", 'Strom', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0909.mp3", 'Stück', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0910.mp3", 'Stuhl', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0911.mp3", 'Stundenplan', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0912.mp3", 'Sturm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0913.mp3", 'Sucht', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0914.mp3", 'Süden', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0915.mp3", 'Supermarkt', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0916.mp3", 'Suppe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0917.mp3", 'Süßigkeit', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0918.mp3", 'Tacker', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0919.mp3", 'Tafel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0920.mp3", 'Tag', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0921.mp3", 'Takt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0922.mp3", 'Tal', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0923.mp3", 'Tankstelle', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0924.mp3", 'Tanne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0925.mp3", 'Tannenbaum', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0926.mp3", 'Tante', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0927.mp3", 'Taschendieb', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0928.mp3", 'Taschentuch', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0929.mp3", 'Tasse', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0930.mp3", 'Tastatur', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0931.mp3", 'Tat', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0932.mp3", 'taub', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0933.mp3", 'Team', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0934.mp3", 'Technik', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0935.mp3", 'Tee', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0936.mp3", 'Teebeutel', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0937.mp3", 'Teekanne', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0938.mp3", 'Teer', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0939.mp3", 'Teich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0940.mp3", 'Teig', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0941.mp3", 'Teil', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0942.mp3", 'Telefon', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0943.mp3", 'Teller', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0944.mp3", 'Temperatur', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0945.mp3", 'Tennis', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0946.mp3", 'Teppich', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0947.mp3", 'Test', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0948.mp3", 'Textmarker', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0949.mp3", 'Thron', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0950.mp3", 'Tiefkühlfach', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0951.mp3", 'Tier', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0952.mp3", 'Tisch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0953.mp3", 'Tischdecke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0954.mp3", 'Tochter', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0955.mp3", 'Tod', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0956.mp3", 'Tomate', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0957.mp3", 'Ton', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0958.mp3", 'Tonne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0959.mp3", 'Topf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0960.mp3", 'Tor', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0961.mp3", 'Tortenguss', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0962.mp3", 'Traktor', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0963.mp3", 'Traum', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0964.mp3", 'Traumberuf', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0965.mp3", 'treu', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0966.mp3", 'Tritt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0967.mp3", 'Trommel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0968.mp3", 'Trompete', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0969.mp3", 'Tropf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0970.mp3", 'Trotz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0971.mp3", 'Tuch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0972.mp3", 'Tulpe', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0973.mp3", 'Tür', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0974.mp3", 'türkis', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0975.mp3", 'Türklinke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0976.mp3", 'Turm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0977.mp3", 'Turnen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0978.mp3", 'Uhr', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0979.mp3", 'Unterricht', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0980.mp3", 'Urlaub', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0981.mp3", 'Urlaubszeit', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0982.mp3", 'Vase', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0983.mp3", 'viel', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0984.mp3", 'violett', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0985.mp3", 'Vogel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0986.mp3", 'Vogelnest', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0987.mp3", 'Vorlage', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0988.mp3", 'Vorspeise', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0989.mp3", 'wach', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0990.mp3", 'Wachs', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0991.mp3", 'Wahl', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0992.mp3", 'Wahn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0993.mp3", 'Wald', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0994.mp3", 'Wall', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0995.mp3", 'Wand', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0996.mp3", 'wandern', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0997.mp3", 'Wanne', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0998.mp3", 'warm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f0999.mp3", 'Warteschlange', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1000.mp3", 'Waschbecken', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    });
  }

  Future _insertSamples1000to1062Female(Database db) async {
  await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1001.mp3", 'Wäschekorb', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1002.mp3", 'Wasser', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1003.mp3", 'Wasserhahn', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1004.mp3", 'Wasserkocher', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1005.mp3", 'Wasserwaage', 4, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1006.mp3", 'Weg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1007.mp3", 'weich', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1008.mp3", 'Weidenzaun', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1009.mp3", 'Wein', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1010.mp3", 'Weintrauben', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1011.mp3", 'weiß', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1012.mp3", 'weit', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1013.mp3", 'Wellen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1014.mp3", 'Welt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1015.mp3", 'Werk', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1016.mp3", 'Werkzeug', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1017.mp3", 'Wert', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1018.mp3", 'Wespennest', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1019.mp3", 'Westen', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1020.mp3", 'Wettkampf', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1021.mp3", 'wild', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1022.mp3", 'Wind', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1023.mp3", 'Windmühle', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1024.mp3", 'Winter', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1025.mp3", 'Wissenschaft', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1026.mp3", 'Witz', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1027.mp3", 'Wolf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1028.mp3", 'Wolken', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1029.mp3", 'Wort', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1030.mp3", 'Wrack', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1031.mp3", 'wund', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1032.mp3", 'Wunsch', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1033.mp3", 'Wurm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1034.mp3", 'Wurst', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1035.mp3", 'Wüste', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1036.mp3", 'Wut', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1037.mp3", 'Yoga', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1038.mp3", 'Zahl', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1039.mp3", 'zahm', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1040.mp3", 'Zahn', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1041.mp3", 'Zahnbürste', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1042.mp3", 'Zahnschmerzen', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1043.mp3", 'Zange', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1044.mp3", 'zart', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1045.mp3", 'Zauberer', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1046.mp3", 'Zeh', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1047.mp3", 'Zeit', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1048.mp3", 'Zeitung', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1049.mp3", 'Zelt', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1050.mp3", 'Zettel', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1051.mp3", 'Zeug', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1052.mp3", 'Ziel', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1053.mp3", 'Zirkus', 2, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1054.mp3", 'Zitrone', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1055.mp3", 'Zoll', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1056.mp3", 'Zoo', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1057.mp3", 'Zopf', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1058.mp3", 'Zug', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1059.mp3", 'Zugbrücke', 3, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1060.mp3", 'Zwang', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1061.mp3", 'Zweck', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1062.mp3", 'Zwerg', 1, Mood.NEUTRAL, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    });
  }

  Future _insertSamples1063to1200Female(Database db) async {
    await db.transaction((txn) async {
    //sentence
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1063.mp3", 'Auf dem Balkon stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1064.mp3", 'Auf dem Balkon stehen Kisten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1065.mp3", 'Auf dem Boden stehen Bilder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1066.mp3", 'Auf dem Boden stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1067.mp3", 'Auf dem Boden stehen Bücher', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1068.mp3", 'Auf dem Boden stehen Kisten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1069.mp3", 'Auf dem Boden stehen Ordner', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1070.mp3", 'Auf dem Regal stehen Bilder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1071.mp3", 'Auf dem Regal stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1072.mp3", 'Auf dem Regal stehen Bücher', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1073.mp3", 'Auf dem Regal stehen Fotos', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1074.mp3", 'Auf dem Regal stehen Kisten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1075.mp3", 'Auf dem Regal stehen Ordner', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1076.mp3", 'Auf dem Schrank stehen Bilder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1077.mp3", 'Auf dem Schrank stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1078.mp3", 'Auf dem Schrank stehen Bücher', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1079.mp3", 'Auf dem Schrank stehen Fotos', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1080.mp3", 'Auf dem Schrank stehen Kisten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1081.mp3", 'Auf dem Schrank stehen Ordner', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1082.mp3", 'Auf dem Tisch stehen Bilder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1083.mp3", 'Auf dem Tisch stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1084.mp3", 'Auf dem Tisch stehen Bücher', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1085.mp3", 'Auf dem Tisch stehen Fotos', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1086.mp3", 'Auf dem Tisch stehen Kisten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1087.mp3", 'Auf dem Tisch stehen Ordner', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1088.mp3", 'Das Auto fährt über die Ampel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1089.mp3", 'Das Auto fährt über die Autobahn', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1090.mp3", 'Das Auto fährt über die Brücke', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1091.mp3", 'Das Auto fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1092.mp3", 'Das Auto fährt über die Straße', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1093.mp3", 'Das Fahrrad fährt über die Ampel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1094.mp3", 'Das Fahrrad fährt über die Brücke', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1095.mp3", 'Das Fahrrad fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1096.mp3", 'Das Fahrrad fährt über die Straße', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1097.mp3", 'Das Mädchen fährt über die Ampel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1098.mp3", 'Das Mädchen fährt über die Autobahn', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1099.mp3", 'Das Mädchen fährt über die Brücke', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1100.mp3", 'Das Mädchen fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1101.mp3", 'Das Mädchen fährt über die Straße', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1102.mp3", 'Das Taxi fährt über die Ampel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1103.mp3", 'Das Taxi fährt über die Autobahn', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1104.mp3", 'Das Taxi fährt über die Brücke', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1105.mp3", 'Das Taxi fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1106.mp3", 'Das Taxi fährt über die Straße', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1107.mp3", 'Das Pferd frisst Futter', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1108.mp3", 'Das Pferd frisst Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1109.mp3", 'Das Pferd frisst Heu', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1110.mp3", 'Das Pferd frisst Pflanzen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1111.mp3", 'Das Rind frisst Futter', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1112.mp3", 'Das Rind frisst Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1113.mp3", 'Das Rind frisst Heu', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1114.mp3", 'Das Rind frisst Pflanzen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1115.mp3", 'Das Schaf frisst Futter', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1116.mp3", 'Das Schaf frisst Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1117.mp3", 'Das Schaf frisst Heu', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1118.mp3", 'Das Schaf frisst Pflanzen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1119.mp3", 'Das Tier frisst Futter', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1120.mp3", 'Das Tier frisst Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1121.mp3", 'Das Tier frisst Heu', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1122.mp3", 'Das Tier frisst Pflanzen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1123.mp3", 'Das Apartment wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1124.mp3", 'Das Apartment wird geräumt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1125.mp3", 'Das Apartment wird gestrichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1126.mp3", 'Das Apartment wird renoviert', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1127.mp3", 'Das Apartment wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1128.mp3", 'Das Gebäude wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1129.mp3", 'Das Gebäude wird gebaut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1130.mp3", 'Das Gebäude wird geräumt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1131.mp3", 'Das Gebäude wird gestrichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1132.mp3", 'Das Gebäude wird renoviert', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1133.mp3", 'Das Gebäude wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1134.mp3", 'Das Haus wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1135.mp3", 'Das Haus wird gebaut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1136.mp3", 'Das Haus wird geräumt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1137.mp3", 'Das Haus wird gestrichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1138.mp3", 'Das Haus wird renoviert', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1139.mp3", 'Das Haus wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1140.mp3", 'Das Hotel wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1141.mp3", 'Das Hotel wird gebaut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1142.mp3", 'Das Hotel wird geräumt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1143.mp3", 'Das Hotel wird gestrichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1144.mp3", 'Das Hotel wird renoviert', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1145.mp3", 'Das Hotel wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1146.mp3", 'Das Viertel wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1147.mp3", 'Das Viertel wird gebaut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1148.mp3", 'Das Viertel wird geräumt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1149.mp3", 'Das Viertel wird renoviert', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1150.mp3", 'Das Viertel wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1151.mp3", 'Das Kind lernt in der Küche', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1152.mp3", 'Das Kind lernt in der Pause', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1153.mp3", 'Das Kind lernt in der Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1154.mp3", 'Das Kind lernt in der Sonne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1155.mp3", 'Das Kind rennt in der Küche', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1156.mp3", 'Das Kind rennt in der Pause', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1157.mp3", 'Das Kind rennt in der Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1158.mp3", 'Das Kind rennt in der Sonne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1159.mp3", 'Das Kind sitzt in der Küche', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1160.mp3", 'Das Kind sitzt in der Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1161.mp3", 'Das Kind sitzt in der Sonne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1162.mp3", 'Das Kind spielt in der Küche', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1163.mp3", 'Das Kind spielt in der Pause', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1164.mp3", 'Das Kind spielt in der Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1165.mp3", 'Das Kind spielt in der Sonne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1166.mp3", 'Das Auto hat die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1167.mp3", 'Das Auto hat die Farbe grau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1168.mp3", 'Das Auto hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1169.mp3", 'Das Auto hat die Farbe schwarz', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1170.mp3", 'Das Auto hat die Farbe weiß', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1171.mp3", 'Das Haus hat die Farbe grau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1172.mp3", 'Das Haus hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1173.mp3", 'Das Haus hat die Farbe weiß', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1174.mp3", 'Das Hemd hat die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1175.mp3", 'Das Hemd hat die Farbe grau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1176.mp3", 'Das Hemd hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1177.mp3", 'Das Hemd hat die Farbe schwarz', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1178.mp3", 'Das Hemd hat die Farbe weiß', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1179.mp3", 'Das Leder hat die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1180.mp3", 'Das Leder hat die Farbe grau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1181.mp3", 'Das Leder hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1182.mp3", 'Das Leder hat die Farbe schwarz', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1183.mp3", 'Das Leder hat die Farbe weiß', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1184.mp3", 'Das Sofa hat die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1185.mp3", 'Das Sofa hat die Farbe grau', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1186.mp3", 'Das Sofa hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1187.mp3", 'Das Sofa hat die Farbe schwarz', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1188.mp3", 'Das Sofa hat die Farbe weiß', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1189.mp3", 'Der Bär hat braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1190.mp3", 'Der Bär hat dichtes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1191.mp3", 'Der Bär hat dunkles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1192.mp3", 'Der Bär hat helles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1193.mp3", 'Der Bär hat langes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1194.mp3", 'Der Bär hat warmes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1195.mp3", 'Der Biber hat braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1196.mp3", 'Der Biber hat dichtes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1197.mp3", 'Der Biber hat dunkles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1198.mp3", 'Der Biber hat helles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1199.mp3", 'Der Biber hat langes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1200.mp3", 'Der Biber hat warmes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    });
  }

  Future _insertSamples1201to1400Female(Database db) async {
  await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1201.mp3", 'Der Hase hat braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1202.mp3", 'Der Hase hat dichtes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1203.mp3", 'Der Hase hat dunkles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1204.mp3", 'Der Hase hat helles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1205.mp3", 'Der Hase hat langes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1206.mp3", 'Der Hase hat warmes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1207.mp3", 'Der Hund hat braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1208.mp3", 'Der Hund hat dichtes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1209.mp3", 'Der Hund hat dunkles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1210.mp3", 'Der Hund hat helles Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1211.mp3", 'Der Hund hat langes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1212.mp3", 'Der Hund hat warmes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1213.mp3", 'Wir gehen heute in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1214.mp3", 'Wir gehen heute in den Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1215.mp3", 'Wir gehen heute in den Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1216.mp3", 'Wir gehen heute in den Zirkus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1217.mp3", 'Wir gehen heute in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1218.mp3", 'Wir gehen Montag in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1219.mp3", 'Wir gehen Montag in den Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1220.mp3", 'Wir gehen Montag in den Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1221.mp3", 'Wir gehen Montag in den Zirkus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1222.mp3", 'Wir gehen Montag in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1223.mp3", 'Wir gehen morgen in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1224.mp3", 'Wir gehen morgen in den Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1225.mp3", 'Wir gehen morgen in den Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1226.mp3", 'Wir gehen morgen in den Zirkus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1227.mp3", 'Wir gehen morgen in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1228.mp3", 'Wir gehen Samstag in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1229.mp3", 'Wir gehen Samstag in den Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1230.mp3", 'Wir gehen Samstag in den Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1231.mp3", 'Wir gehen Samstag in den Zirkus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1232.mp3", 'Wir gehen Samstag in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1233.mp3", 'Wir gehen Sonntag in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1234.mp3", 'Wir gehen Sonntag in den Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1235.mp3", 'Wir gehen Sonntag in den Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1236.mp3", 'Wir gehen Sonntag in den Zirkus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1237.mp3", 'Wir gehen Sonntag in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1238.mp3", 'Der Junge malt ein Auto', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1239.mp3", 'Der Junge malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1240.mp3", 'Der Junge malt ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1241.mp3", 'Der Junge malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1242.mp3", 'Der Junge malt ein Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1243.mp3", 'Der Künstler malt ein Auto', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1244.mp3", 'Der Künstler malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1245.mp3", 'Der Künstler malt ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1246.mp3", 'Der Künstler malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1247.mp3", 'Der Künstler malt ein Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1248.mp3", 'Der Maler malt ein Auto', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1249.mp3", 'Der Maler malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1250.mp3", 'Der Maler malt ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1251.mp3", 'Der Maler malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1252.mp3", 'Der Maler malt ein Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1253.mp3", 'Der Mann malt ein Auto', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1254.mp3", 'Der Mann malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1255.mp3", 'Der Mann malt ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1256.mp3", 'Der Mann malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1257.mp3", 'Der Mann malt ein Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1258.mp3", 'Der Schüler malt ein Auto', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1259.mp3", 'Der Schüler malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1260.mp3", 'Der Schüler malt ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1261.mp3", 'Der Schüler malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1262.mp3", 'Der Schüler malt ein Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1263.mp3", 'Der Joghurt schmeckt nach Erdbeere', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1264.mp3", 'Der Joghurt schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1265.mp3", 'Der Joghurt schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1266.mp3", 'Der Joghurt schmeckt nach Vanille', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1267.mp3", 'Der Kuchen schmeckt nach Erdbeere', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1268.mp3", 'Der Kuchen schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1269.mp3", 'Der Kuchen schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1270.mp3", 'Der Kuchen schmeckt nach Vanille', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1271.mp3", 'Der Nachtisch schmeckt nach Erdbeere', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1272.mp3", 'Der Nachtisch schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1273.mp3", 'Der Nachtisch schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1274.mp3", 'Der Nachtisch schmeckt nach Vanille', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1275.mp3", 'Der Saft schmeckt nach Erdbeere', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1276.mp3", 'Der Saft schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1277.mp3", 'Der Saft schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1278.mp3", 'Der Tee schmeckt nach Erdbeere', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1279.mp3", 'Der Tee schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1280.mp3", 'Der Tee schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1281.mp3", 'Der Tee schmeckt nach Vanille', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1282.mp3", 'Der Anwalt spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1283.mp3", 'Der Anwalt spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1284.mp3", 'Der Anwalt spricht laut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1285.mp3", 'Der Anwalt spricht leise', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1286.mp3", 'Der Anwalt spricht schnell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1287.mp3", 'Der Lehrer spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1288.mp3", 'Der Lehrer spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1289.mp3", 'Der Lehrer spricht laut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1290.mp3", 'Der Lehrer spricht leise', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1291.mp3", 'Der Lehrer spricht schnell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1292.mp3", 'Der Redner spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1293.mp3", 'Der Redner spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1294.mp3", 'Der Redner spricht laut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1295.mp3", 'Der Redner spricht leise', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1296.mp3", 'Der Redner spricht schnell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1297.mp3", 'Der Richter spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1298.mp3", 'Der Richter spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1299.mp3", 'Der Richter spricht laut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1300.mp3", 'Der Richter spricht leise', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1301.mp3", 'Der Richter spricht schnell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1302.mp3", 'Der Schüler spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1303.mp3", 'Der Schüler spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1304.mp3", 'Der Schüler spricht laut', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1305.mp3", 'Der Schüler spricht leise', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1306.mp3", 'Der Schüler spricht schnell', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1307.mp3", 'Der Anwalt bearbeitet den Auftrag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1308.mp3", 'Der Anwalt bearbeitet den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1309.mp3", 'Der Anwalt gewinnt den Auftrag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1310.mp3", 'Der Anwalt gewinnt den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1311.mp3", 'Der Anwalt gewinnt den Klienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1312.mp3", 'Der Anwalt gewinnt den Prozess', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1313.mp3", 'Der Anwalt übernimmt den Auftrag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1314.mp3", 'Der Anwalt übernimmt den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1315.mp3", 'Der Anwalt übernimmt den Klienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1316.mp3", 'Der Anwalt verliert den Auftrag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1317.mp3", 'Der Anwalt verliert den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1318.mp3", 'Der Anwalt verliert den Klienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1319.mp3", 'Der Anwalt verliert den Prozess', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1320.mp3", 'Die Angestellten halten sich an die Pläne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1321.mp3", 'Die Angestellten halten sich an die Regeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1322.mp3", 'Die Angestellten halten sich an die Richtlinien', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1323.mp3", 'Die Angestellten halten sich an die Vorgaben', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1324.mp3", 'Die Arbeiter halten sich an die Pläne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1325.mp3", 'Die Arbeiter halten sich an die Regeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1326.mp3", 'Die Arbeiter halten sich an die Richtlinien', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1327.mp3", 'Die Arbeiter halten sich an die Vorgaben', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1328.mp3", 'Die Bauern halten sich an die Gesetze', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1329.mp3", 'Die Bauern halten sich an die Pläne', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1330.mp3", 'Die Bauern halten sich an die Richtlinien', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1331.mp3", 'Die Bauern halten sich an die Vorgaben', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1332.mp3", 'Die Bürger halten sich an die Gesetze', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1333.mp3", 'Die Bürger halten sich an die Regeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1334.mp3", 'Die Bürger halten sich an die Richtlinien', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1335.mp3", 'Die Menschen halten sich an die Gesetze', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1336.mp3", 'Die Menschen halten sich an die Regeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1337.mp3", 'Die Menschen halten sich an die Richtlinien', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1338.mp3", 'Die Ärztin hilft dem Kind', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1339.mp3", 'Die Ärztin hilft dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1340.mp3", 'Die Ärztin hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1341.mp3", 'Die Ärztin hilft dem Patienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1342.mp3", 'Die Ärztin hilft dem Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1343.mp3", 'Die Frau hilft dem Kind', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1344.mp3", 'Die Frau hilft dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1345.mp3", 'Die Frau hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1346.mp3", 'Die Frau hilft dem Patienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1347.mp3", 'Die Frau hilft dem Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1348.mp3", 'Die Medizin hilft dem Kind', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1349.mp3", 'Die Medizin hilft dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1350.mp3", 'Die Medizin hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1351.mp3", 'Die Medizin hilft dem Patienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1352.mp3", 'Die Medizin hilft dem Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1353.mp3", 'Die Polizei hilft dem Kind', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1354.mp3", 'Die Polizei hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1355.mp3", 'Die Polizei hilft dem Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1356.mp3", 'Die Therapie hilft dem Kind', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1357.mp3", 'Die Therapie hilft dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1358.mp3", 'Die Therapie hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1359.mp3", 'Die Therapie hilft dem Patienten', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1360.mp3", 'Die Therapie hilft dem Tier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1361.mp3", 'Die Abfahrt ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1362.mp3", 'Die Abfahrt ist um drei Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1363.mp3", 'Die Abfahrt ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1364.mp3", 'Die Abfahrt ist um neun Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1365.mp3", 'Die Abfahrt ist um sechs Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1366.mp3", 'Die Ankunft ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1367.mp3", 'Die Ankunft ist um drei Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1368.mp3", 'Die Ankunft ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1369.mp3", 'Die Ankunft ist um neun Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1370.mp3", 'Die Ankunft ist um sechs Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1371.mp3", 'Die Probe ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1372.mp3", 'Die Probe ist um drei Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1373.mp3", 'Die Probe ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1374.mp3", 'Die Probe ist um neun Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1375.mp3", 'Die Probe ist um sechs Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1376.mp3", 'Die Prüfung ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1377.mp3", 'Die Prüfung ist um drei Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1378.mp3", 'Die Prüfung ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1379.mp3", 'Die Prüfung ist um neun Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1380.mp3", 'Die Prüfung ist um sechs Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1381.mp3", 'Die Vorstellung ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1382.mp3", 'Die Vorstellung ist um drei Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1383.mp3", 'Die Vorstellung ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1384.mp3", 'Die Vorstellung ist um neun Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1385.mp3", 'Die Vorstellung ist um sechs Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1386.mp3", 'Die Miete wurde bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1387.mp3", 'Die Miete wurde erhöht', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1388.mp3", 'Die Miete wurde gesenkt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1389.mp3", 'Die Rechnung wurde beglichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1390.mp3", 'Die Rechnung wurde bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1391.mp3", 'Die Rechnung wurde erhöht', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1392.mp3", 'Die Rechnung wurde gesenkt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1393.mp3", 'Die Steuer wurde bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1394.mp3", 'Die Steuer wurde erhöht', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1395.mp3", 'Die Steuer wurde gesenkt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1396.mp3", 'Die Strafe wurde beglichen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1397.mp3", 'Die Strafe wurde bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1398.mp3", 'Die Strafe wurde erhöht', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1399.mp3", 'Die Strafe wurde gesenkt', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1400.mp3", 'Die Großmutter kauft Mützen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
   });
 }

  Future _insertSamples1401to1600Female(Database db) async {
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1401.mp3", 'Die Großmutter kauft Pullover', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1402.mp3", 'Die Großmutter kauft Schals', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1403.mp3", 'Die Großmutter kauft Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1404.mp3", 'Die Großmutter stopft Mützen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1405.mp3", 'Die Großmutter stopft Pullover', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1406.mp3", 'Die Großmutter stopft Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1407.mp3", 'Die Großmutter strickt Mützen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1408.mp3", 'Die Großmutter strickt Pullover', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1409.mp3", 'Die Großmutter strickt Schals', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1410.mp3", 'Die Großmutter strickt Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1411.mp3", 'Die Großmutter verschenkt Mützen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1412.mp3", 'Die Großmutter verschenkt Pullover', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1413.mp3", 'Die Großmutter verschenkt Schals', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1414.mp3", 'Die Großmutter verschenkt Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1415.mp3", 'Du liest auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1416.mp3", 'Du liest auf dem Papier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1417.mp3", 'Du liest auf dem Umschlag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1418.mp3", 'Du liest auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1419.mp3", 'Du malst auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1420.mp3", 'Du malst auf dem Brief', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1421.mp3", 'Du malst auf dem Papier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1422.mp3", 'Du malst auf dem Umschlag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1423.mp3", 'Du malst auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1424.mp3", 'Du notierst auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1425.mp3", 'Du notierst auf dem Papier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1426.mp3", 'Du notierst auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1427.mp3", 'Du schreibst auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1428.mp3", 'Du schreibst auf dem Brief', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1429.mp3", 'Du schreibst auf dem Papier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1430.mp3", 'Du schreibst auf dem Umschlag', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1431.mp3", 'Du schreibst auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1432.mp3", 'Du zeichnest auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1433.mp3", 'Du zeichnest auf dem Papier', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1434.mp3", 'Du zeichnest auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1435.mp3", 'Er isst heute Eintopf', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1436.mp3", 'Er isst heute Gemüse', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1437.mp3", 'Er isst heute Kartoffeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1438.mp3", 'Er isst heute Nudeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1439.mp3", 'Er isst heute Reis', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1440.mp3", 'Er isst heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1441.mp3", 'Er kauft heute Gemüse', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1442.mp3", 'Er kauft heute Kartoffeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1443.mp3", 'Er kauft heute Nudeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1444.mp3", 'Er kauft heute Reis', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1445.mp3", 'Er kauft heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1446.mp3", 'Er kocht heute Eintopf', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1447.mp3", 'Er kocht heute Gemüse', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1448.mp3", 'Er kocht heute Kartoffeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1449.mp3", 'Er kocht heute Nudeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1450.mp3", 'Er kocht heute Reis', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1451.mp3", 'Er kocht heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1452.mp3", 'Er serviert heute Eintopf', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1453.mp3", 'Er serviert heute Gemüse', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1454.mp3", 'Er serviert heute Kartoffeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1455.mp3", 'Er serviert heute Nudeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1456.mp3", 'Er serviert heute Reis', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1457.mp3", 'Er serviert heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1458.mp3", 'Es ist sehr heiß im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1459.mp3", 'Es ist sehr heiß im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1460.mp3", 'Es ist sehr heiß im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1461.mp3", 'Es ist sehr heiß im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1462.mp3", 'Es ist sehr heiß im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1463.mp3", 'Es ist sehr kalt im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1464.mp3", 'Es ist sehr kalt im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1465.mp3", 'Es ist sehr kalt im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1466.mp3", 'Es ist sehr kalt im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1467.mp3", 'Es ist sehr kalt im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1468.mp3", 'Es ist sehr laut im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1469.mp3", 'Es ist sehr laut im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1470.mp3", 'Es ist sehr laut im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1471.mp3", 'Es ist sehr laut im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1472.mp3", 'Es ist sehr laut im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1473.mp3", 'Es ist sehr leer im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1474.mp3", 'Es ist sehr leer im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1475.mp3", 'Es ist sehr leer im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1476.mp3", 'Es ist sehr leer im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1477.mp3", 'Es ist sehr leer im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1478.mp3", 'Es ist sehr ruhig im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1479.mp3", 'Es ist sehr ruhig im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1480.mp3", 'Es ist sehr ruhig im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1481.mp3", 'Es ist sehr ruhig im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1482.mp3", 'Es ist sehr ruhig im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1483.mp3", 'Es ist sehr voll im Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1484.mp3", 'Es ist sehr voll im Laden', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1485.mp3", 'Es ist sehr voll im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1486.mp3", 'Es ist sehr voll im Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1487.mp3", 'Es ist sehr voll im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1488.mp3", 'Ich belüge meine Brüder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1489.mp3", 'Ich belüge meine Eltern', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1490.mp3", 'Ich belüge meine Freunde', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1491.mp3", 'Ich belüge meine Freundin', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1492.mp3", 'Ich belüge meine Kollegen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1493.mp3", 'Ich beschenke meine Brüder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1494.mp3", 'Ich beschenke meine Eltern', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1495.mp3", 'Ich beschenke meine Freunde', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1496.mp3", 'Ich beschenke meine Freundin', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1497.mp3", 'Ich verrate meine Brüder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1498.mp3", 'Ich verrate meine Eltern', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1499.mp3", 'Ich verrate meine Freunde', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1500.mp3", 'Ich verrate meine Freundin', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1501.mp3", 'Ich verrate meine Kollegen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1502.mp3", 'Ich versetze meine Brüder', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1503.mp3", 'Ich versetze meine Eltern', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1504.mp3", 'Ich versetze meine Freunde', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1505.mp3", 'Ich versetze meine Freundin', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1506.mp3", 'Ich versetze meine Kollegen', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1507.mp3", 'Sie beendet die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1508.mp3", 'Sie beendet die Schule erst im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1509.mp3", 'Sie beendet die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1510.mp3", 'Sie beendet die Schule noch im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1511.mp3", 'Sie beginnt die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1512.mp3", 'Sie beginnt die Schule erst im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1513.mp3", 'Sie beginnt die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1514.mp3", 'Sie beginnt die Schule noch im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1515.mp3", 'Sie verlässt die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1516.mp3", 'Sie verlässt die Schule erst im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1517.mp3", 'Sie verlässt die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1518.mp3", 'Sie verlässt die Schule noch im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1519.mp3", 'Sie wechselt die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1520.mp3", 'Sie wechselt die Schule erst im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1521.mp3", 'Sie wechselt die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1522.mp3", 'Sie wechselt die Schule noch im Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    //Question
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1523.mp3", 'Baut der Vogel ein Nest', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1524.mp3", 'Dreht sich die Erde', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1525.mp3", 'Erreicht er das Ziel', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1526.mp3", 'Fährt das Auto über die Straße', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1527.mp3", 'Fährt der Zug täglich', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1528.mp3", 'Fährt sie immer mit dem Bus', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1529.mp3", 'Frisst das Pferd Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1530.mp3", 'Gehen wir Montag in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1531.mp3", 'Gewinnen wir das Spiel', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1532.mp3", 'Haben Spinnen lange Beine', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1533.mp3", 'Hängt das Bild an der Wand', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1534.mp3", 'Hast du gut geschlafen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1535.mp3", 'Hat das Auto die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1536.mp3", 'Hat der Bär braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1537.mp3", 'Hilft die Medizin dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1538.mp3", 'Hörst du das Radio rauschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1539.mp3", 'Isst sie gerne Eis', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1540.mp3", 'Ist der Anstrich frisch', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1541.mp3", 'Ist die Abfahrt um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1542.mp3", 'Ist die Kasse geschlossen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1543.mp3", 'Ist es sehr voll im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1544.mp3", 'Kann ich das Buch ausleihen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1545.mp3", 'Kennst du den neuen Schüler', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1546.mp3", 'Kocht er heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1547.mp3", 'Kommst du heute zu mir', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1548.mp3", 'Leuchtet die Ampel grün', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1549.mp3", 'Lief die Sendung im Fernsehen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1550.mp3", 'Malt der Junge ein Haus', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1551.mp3", 'Makierst du die Wörter', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1552.mp3", 'Quietscht die Tür', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1553.mp3", 'Regnet es viel', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1554.mp3", 'Schmeckt der Kaffee gut', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1555.mp3", 'Schmeckt der Tee nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1556.mp3", 'Sind die Blätter grün', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1557.mp3", 'Spielt er mit dem Ball', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1558.mp3", 'Spricht der Lehrer deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1559.mp3", 'Stehen Blumen auf dem Regal', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1560.mp3", 'Strickt die Großmutter Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1561.mp3", 'Trinkst du deinen Kaffee schwarz', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1562.mp3", 'Übernimmt der Anwalt den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1563.mp3", 'Verlief das Treffen gut', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1564.mp3", 'Wann möchstest du aufstehen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1565.mp3", 'Wann wurde der Film veröffentlicht', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1566.mp3", 'Was möchtest du essen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1567.mp3", 'Wechselt sie die Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1568.mp3", 'Welche Note hast du bekommen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1569.mp3", 'Wie heißt der kleine Junge', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1570.mp3", 'Wie lang ist der Stau', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1571.mp3", 'Wieso bist du so spät', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1572.mp3", 'Wird das Gebäude abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1573.mp3", 'Wo muss ich abbiegen', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1574.mp3", 'Wo wohnst du', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1575.mp3", 'Wurde der Tisch gedeckt', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1576.mp3", 'Wurde die Strafe bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1577.mp3", 'Wurden die Karten gemischt', 0, Mood.NEUTRAL, Speed.SLOW, Type.EXP_QUESTION, Speaker.FEMALE]);
    // sentence happy
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1578.mp3", 'Auf dem Balkon stehen Blumen', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1579.mp3", 'Auf dem Regal stehen Bücher', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1580.mp3", 'Das Auto fährt über die Brücke', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1581.mp3", 'Das Fahrrad fährt über die Kreuzung', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1582.mp3", 'Das Pferd frisst Heu', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1583.mp3", 'Das Schaf frisst Gras', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1584.mp3", 'Das Gebäude wird abgebrochen', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1585.mp3", 'Das Haus wird verkauft', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1586.mp3", 'Das Kind lernt in der Schule', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1587.mp3", 'Das Kind spielt in der Pause', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1588.mp3", 'Das Haus hat die Farbe rot', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1589.mp3", 'Das Hemd hat die Farbe blau', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1590.mp3", 'Der Bär hat braunes Fell', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1591.mp3", 'Der Hund hat langes Fell', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1592.mp3", 'Wir gehen heute in den Garten', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1593.mp3", 'Wir gehen Sonntag in den Zoo', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1594.mp3", 'Der Junge malt ein Schiff', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1595.mp3", 'Der Künstler malt ein Bild', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1596.mp3", 'Der Kuchen schmeckt nach Orange', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1597.mp3", 'Der Tee schmeckt nach Früchten', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1598.mp3", 'Der Redner spricht deutlich', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1599.mp3", 'Der Richter spricht langsam', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1600.mp3", 'Der Anwalt gewinnt den Prozess', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    });
  }

  Future _insertSamples1601toEndFemale(Database db) async {
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1601.mp3", 'Der Anwalt übernimmt den Fall', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1602.mp3", 'Die Arbeiter halten sich an die Pläne', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1603.mp3", 'Die Bürger halten sich an die Gesetze', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1604.mp3", 'Die Ärztin hilft dem Kranken', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1605.mp3", 'Die Polizei hilft dem Menschen', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1606.mp3", 'Die Probe ist um fünf Uhr', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1607.mp3", 'Die Prüfung ist um acht Uhr', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1608.mp3", 'Die Miete wurde bezahlt', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1609.mp3", 'Die Steuer wurde gesenkt', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1610.mp3", 'Die Großmutter stopft Socken', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1611.mp3", 'Die Großmutter strickt Mützen', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1612.mp3", 'Du notierst auf dem Block', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1613.mp3", 'Du schreibst auf dem Zettel', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1614.mp3", 'Er isst heute Kartoffeln', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1615.mp3", 'Er kocht heute Suppe', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1616.mp3", 'Es ist sehr heiß im Zug', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1617.mp3", 'Es ist sehr ruhig im Park', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1618.mp3", 'Ich belüge meine Eltern', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1619.mp3", 'Ich versetze meine Freunde', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1620.mp3", 'Sie beginnt die Schule diesen Sommer', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1621.mp3", 'Sie wechselt die Schule nächsten Sommer', 0, Mood.HAPPY, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    //Word Happy
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1622.mp3", 'Abendkleid', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1623.mp3", 'Achterbahn', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1624.mp3", 'Apfelsaft', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1625.mp3", 'Backofen', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1626.mp3", 'Baustelle', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1627.mp3", 'Blumenbeet', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1628.mp3", 'Briefkasten', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1629.mp3", 'Dachboden', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1630.mp3", 'Dezember', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1631.mp3", 'Eisenbahn', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1632.mp3", 'Ententeich', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1633.mp3", 'Erdgeschoss', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1634.mp3", 'Fensterbrett', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1635.mp3", 'Flughafen', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1636.mp3", 'Fremdsprache', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1637.mp3", 'Gartenhaus', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1638.mp3", 'Geschichte', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1639.mp3", 'Handwerker', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1640.mp3", 'Hörgerät', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1641.mp3", 'Hürdenlauf', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1642.mp3", 'Instrument', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1643.mp3", 'Kartenspiel', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1644.mp3", 'Kofferraum', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1645.mp3", 'Kreisverkehr', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1646.mp3", 'Landwirtschaft', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1647.mp3", 'Lippenstift', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1648.mp3", 'Lückentext', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1649.mp3", 'Märchenbuch', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1650.mp3", 'Mitternacht', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1651.mp3", 'Nachrichten', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1652.mp3", 'Nagellack', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1653.mp3", 'Obstwiese', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1654.mp3", 'Papierkorb', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1655.mp3", 'Pferdestall', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1656.mp3", 'Pinselstrich', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1657.mp3", 'Rasierschaum', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1658.mp3", 'Raumstation', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1659.mp3", 'Regenwurm', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1660.mp3", 'Schmetterling', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1661.mp3", 'Straßenbahn', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1662.mp3", 'Supermarkt', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1663.mp3", 'Tankstelle', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1664.mp3", 'Teebeutel', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1665.mp3", 'Tischdecke', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1666.mp3", 'Urlaubszeit', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1667.mp3", 'Vogelnest', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1668.mp3", 'Waschbecken', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1669.mp3", 'Weintrauben', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1670.mp3", 'Windmühle', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1671.mp3", 'Zahnschmerzen', 3, Mood.HAPPY, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    //sentence sad
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1672.mp3", 'Auf dem Balkon stehen Blumen', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1673.mp3", 'Auf dem Regal stehen Bücher', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1674.mp3", 'Das Auto fährt über die Brücke', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1675.mp3", 'Das Fahrrad fährt über die Kreuzung', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1676.mp3", 'Das Pferd frisst Heu', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1677.mp3", 'Das Schaf frisst Gras', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1678.mp3", 'Das Gebäude wird abgebrochen', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1679.mp3", 'Das Haus wird verkauft', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1680.mp3", 'Das Kind lernt in der Schule', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1681.mp3", 'Das Kind spielt in der Pause', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1682.mp3", 'Das Haus hat die Farbe rot', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1683.mp3", 'Das Hemd hat die Farbe blau', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1684.mp3", 'Der Bär hat braunes Fell', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1685.mp3", 'Der Hund hat langes Fell', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1686.mp3", 'Wir gehen heute in den Garten', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1687.mp3", 'Wir gehen Sonntag in den Zoo', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1688.mp3", 'Der Junge malt ein Schiff', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1689.mp3", 'Der Künstler malt ein Bild', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1690.mp3", 'Der Kuchen schmeckt nach Orange', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1691.mp3", 'Der Tee schmeckt nach Früchten', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1692.mp3", 'Der Redner spricht deutlich', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1693.mp3", 'Der Richter spricht langsam', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1694.mp3", 'Der Anwalt gewinnt den Prozess', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1695.mp3", 'Der Anwalt übernimmt den Fall', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1696.mp3", 'Die Arbeiter halten sich an die Pläne', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1697.mp3", 'Die Bürger halten sich an die Gesetze', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1698.mp3", 'Die Ärztin hilft dem Kranken', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1699.mp3", 'Die Polizei hilft dem Menschen', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1700.mp3", 'Die Probe ist um fünf Uhr', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1701.mp3", 'Die Prüfung ist um acht Uhr', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1702.mp3", 'Die Miete wurde bezahlt', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1703.mp3", 'Die Steuer wurde gesenkt', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1704.mp3", 'Die Großmutter stopft Socken', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1705.mp3", 'Die Großmutter strickt Mützen', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1706.mp3", 'Du notierst auf dem Block', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1707.mp3", 'Du notierst auf dem Zettel', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1708.mp3", 'Er isst heute Kartoffeln', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1709.mp3", 'Er kocht heute Suppe', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1710.mp3", 'Es ist sehr heiß im Zug', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1711.mp3", 'Es ist sehr ruhig im Park', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1712.mp3", 'Ich belüge meine Eltern', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1713.mp3", 'Ich versetze meine Freunde', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1714.mp3", 'Sie beginnt die Schule diesen Sommer', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1715.mp3", 'Sie wechselt die Schule nächsten Sommer', 0, Mood.SAD, Speed.SLOW, Type.SENTENCE, Speaker.FEMALE]);
    //Word SAD
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1716.mp3", 'Abendkleid', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1717.mp3", 'Achterbahn', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1718.mp3", 'Apfelsaft', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1719.mp3", 'Backofen', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1720.mp3", 'Baustelle', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1721.mp3", 'Blumenbeet', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1722.mp3", 'Briefkasten', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1723.mp3", 'Dachboden', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1724.mp3", 'Dezember', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1725.mp3", 'Eisenbahn', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1726.mp3", 'Ententeich', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1727.mp3", 'Erdgeschoss', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1728.mp3", 'Fensterbrett', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1729.mp3", 'Flughafen', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1730.mp3", 'Fremdsprache', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1731.mp3", 'Gartenhaus', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1732.mp3", 'Geschichte', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1733.mp3", 'Handwerker', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1734.mp3", 'Hörgerät', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1735.mp3", 'Hürdenlauf', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1736.mp3", 'Instrument', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1737.mp3", 'Kartenspiel', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1738.mp3", 'Kofferraum', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1739.mp3", 'Kreisverkehr', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1740.mp3", 'Landwirtschaft', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1741.mp3", 'Lippenstift', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1742.mp3", 'Lückentext', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1743.mp3", 'Märchenbuch', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1744.mp3", 'Mitternacht', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1745.mp3", 'Nachrichten', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1746.mp3", 'Nagellack', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1747.mp3", 'Obstwiese', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1748.mp3", 'Papierkorb', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1749.mp3", 'Pferdestall', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1750.mp3", 'Pinselstrich', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1751.mp3", 'Rasierschaum', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1752.mp3", 'Raumstation', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1753.mp3", 'Regenwurm', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1754.mp3", 'Schmetterling', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1755.mp3", 'Straßenbahn', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1756.mp3", 'Supermarkt', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1757.mp3", 'Tankstelle', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1758.mp3", 'Teebeutel', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1759.mp3", 'Tischdecke', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1760.mp3", 'Urlaubszeit', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1761.mp3", 'Vogelnest', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1762.mp3", 'Waschbecken', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1763.mp3", 'Weintrauben', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1764.mp3", 'Windmühle', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1765.mp3", 'Zahnschmerzen', 3, Mood.SAD, Speed.SLOW, Type.WORD, Speaker.FEMALE]);
    //Question
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1766.mp3", 'Auf dem Balkon stehen Blumen', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1767.mp3", 'Auf dem Regal stehen Bücher', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1768.mp3", 'Das Auto fährt über die Brücke', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1769.mp3", 'Das Fahrrad fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1770.mp3", 'Das Pferd frisst Heu', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1771.mp3", 'Das Schaf frisst Gras', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1772.mp3", 'Das Gebäude wird abgebrochen', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1773.mp3", 'Das Haus wird verkauft', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1774.mp3", 'Das Kind lernt in der Schule', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1775.mp3", 'Das Kind spielt in der Pause', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1776.mp3", 'Das Haus hat die Farbe rot', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1777.mp3", 'Das Hemd hat die Farbe blau', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1778.mp3", 'Der Bär hat braunes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1779.mp3", 'Der Hund hat langes Fell', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1780.mp3", 'Wir gehen heute in den Garten', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1781.mp3", 'Wir gehen Sonntag in den Zoo', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1782.mp3", 'Der Junge malt ein Schiff', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1783.mp3", 'Der Künstler malt ein Bild', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1784.mp3", 'Der Kuchen schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1785.mp3", 'Der Tee schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1786.mp3", 'Der Redner spricht deutlich', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1787.mp3", 'Der Richter spricht langsam', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1788.mp3", 'Der Anwalt gewinnt den Prozess', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1789.mp3", 'Der Anwalt übernimmt den Fall', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1790.mp3", 'Die Arbeiter halten sich an die Pläne', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1791.mp3", 'Die Bürger halten sich an die Gesetze', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1792.mp3", 'Die Ärztin hilft dem Kranken', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1793.mp3", 'Die Polizei hilft dem Menschen', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1794.mp3", 'Die Probe ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1795.mp3", 'Die Prüfung ist um acht Uhr', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1796.mp3", 'Die Miete wurde bezahlt', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1797.mp3", 'Die Steuer wurde gesenkt', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1798.mp3", 'Die Großmutter stopft Socken', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1799.mp3", 'Die Großmutter strickt Mützen', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1800.mp3", 'Du notierst auf dem Block', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1801.mp3", 'Du schreibst auf dem Zettel', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1802.mp3", 'Er isst heute Kartoffeln', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1803.mp3", 'Er kocht heute Suppe', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1804.mp3", 'Es ist sehr heiß im Zug', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1805.mp3", 'Es ist sehr ruhig im Park', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1806.mp3", 'Ich belüge meine Eltern', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1807.mp3", 'Ich versetze meine Freunde', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1808.mp3", 'Sie beginnt die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1809.mp3", 'Sie wechselt die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.SLOW, Type.QUESTION, Speaker.FEMALE]);

    //sentence fast
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1810.mp3", 'Auf dem Balkon stehen Blumen', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1811.mp3", 'Auf dem Regal stehen Bücher', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1812.mp3", 'Das Auto fährt über die Brücke', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1813.mp3", 'Das Fahrrad fährt über die Kreuzung', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1814.mp3", 'Das Pferd frisst Heu', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1815.mp3", 'Das Schaf frisst Gras', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1816.mp3", 'Das Gebäude wird abgebrochen', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1817.mp3", 'Das Haus wird verkauft', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1818.mp3", 'Das Kind lernt in der Schule', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1819.mp3", 'Das Kind spielt in der Pause', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1820.mp3", 'Das Haus hat die Farbe rot', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1821.mp3", 'Das Hemd hat die Farbe blau', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1822.mp3", 'Der Bär hat braunes Fell', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1823.mp3", 'Der Hund hat langes Fell', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1824.mp3", 'Wir gehen heute in den Garten', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1825.mp3", 'Wir gehen Sonntag in den Zoo', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1826.mp3", 'Der Junge malt ein Schiff', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1827.mp3", 'Der Künstler malt ein Bild', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1828.mp3", 'Der Kuchen schmeckt nach Orange', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1829.mp3", 'Der Tee schmeckt nach Früchten', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1830.mp3", 'Der Redner spricht deutlich', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1831.mp3", 'Der Richter spricht langsam', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1832.mp3", 'Der Anwalt gewinnt den Prozess', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1833.mp3", 'Der Anwalt übernimmt den Fall', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1834.mp3", 'Die Arbeiter halten sich an die Pläne', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1835.mp3", 'Die Bürger halten sich an die Gesetze', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1836.mp3", 'Die Ärztin hilft dem Kranken', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1837.mp3", 'Die Polizei hilft dem Menschen', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1838.mp3", 'Die Probe ist um fünf Uhr', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1839.mp3", 'Die Prüfung ist um acht Uhr', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1840.mp3", 'Die Miete wurde bezahlt', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1841.mp3", 'Die Steuer wurde gesenkt', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1842.mp3", 'Die Großmutter stopft Socken', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1843.mp3", 'Die Großmutter strickt Mützen', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1844.mp3", 'Du notierst auf dem Block', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1845.mp3", 'Du schreibst auf dem Zettel', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1846.mp3", 'Er isst heute Kartoffeln', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1847.mp3", 'Er kocht heute Suppe', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1848.mp3", 'Es ist sehr heiß im Zug', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1849.mp3", 'Es ist sehr ruhig im Park', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1850.mp3", 'Ich belüge meine Eltern', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1851.mp3", 'Ich versetze meine Freunde', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1852.mp3", 'Sie beginnt die Schule diesen Sommer', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    await txn.execute(
        "INSERT INTO samples ('res_id', 'content', 'syllables', 'mood', 'speed', 'type', 'speaker') values (?, ?, ?, ?, ?, ?, ?)",
        ["raw/f1853.mp3", 'Sie wechselt die Schule nächsten Sommer', 0, Mood.NEUTRAL, Speed.FAST, Type.SENTENCE, Speaker.FEMALE]);
    });
  }

}