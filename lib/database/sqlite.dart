import 'package:audis/database/answer.dart';
import 'package:audis/database/consonants.dart';
import 'package:audis/database/gap_sentence.dart';
import 'package:audis/database/rhymes.dart';
import 'package:audis/database/sample.dart';
import 'package:audis/database/sample_sentence.dart';
import 'package:audis/database/sample_themes.dart';
import 'package:audis/database/theme.dart';
import 'package:audis/database/vowels.dart';
import 'package:audis/utils/shared_preferences.dart';
import "package:path/path.dart";
import 'package:sqflite/sqflite.dart';
import 'package:vm_service/vm_service.dart';

import 'consonant_similarity.dart';
import 'model/history.dart';
import 'model/history.dart';
import 'numbers.dart';

class SqliteDB {
  static final SqliteDB instance = SqliteDB._init();

  static Database? _database;

  static final String createTableVowels = "CREATE TABLE vowels (res_id TEXT, vowel TEXT, length INTEGER, alternative TEXT, minimalpair TEXT, FOREIGN KEY (res_id) REFERENCES samples(res_id))";
  static final String createTableSamples = "CREATE TABLE samples (res_id TEXT PRIMARY KEY, content TEXT, syllables INTEGER, mood INTEGER, speed INTEGER, type INTEGER, speaker INTEGER)";
  static final String createTableConsonantSimilarity = "CREATE TABLE consonant_similarity (res_id TEXT, sim_group INTEGER, FOREIGN KEY (res_id) REFERENCES samples(res_id))";
  static final String createTableRhymes = "CREATE TABLE rhymes (res_id TEXT, rhyme_group INTEGER, FOREIGN KEY (res_id) REFERENCES samples(res_id))";

  static final String createTableConsonants = "CREATE TABLE consonants (res_id TEXT, consonant TEXT, alternateConsonant TEXT, minPair Text, startEnd INTEGER, FOREIGN KEY (res_id) REFERENCES samples(res_id))";
  static final String createTableTheme = "CREATE TABLE theme (theme_id INTEGER PRIMARY KEY,theme TEXT NOT NULL)";
  static final String createTableSampleThemes = "CREATE TABLE samplethemes (res_id TEXT, theme_id INTEGER, FOREIGN KEY (res_id) REFERENCES samples(res_id), FOREIGN KEY (theme_id) REFERENCES theme(theme_id))";
  static final String createTableAnswer = "CREATE TABLE answer (answer_id INTEGER, answer Text, PRIMARY KEY(answer_ID,answer))";
  static final String createTableGapsentence = "CREATE TABLE gapsentence (gapsentence_id INTEGER PRIMARY KEY, gapsentence TEXT, answer_id1 INTEGER, answer_id2 INTEGER)";
  static final String createTableSampleSentence = "CREATE TABLE samplesentence (res_id TEXT, gapsentence_id INTEGER, FOREIGN KEY (res_id) REFERENCES samples(res_id), FOREIGN KEY (gapsentence_id) REFERENCES gapsentence(gapsentence_id))";
  //
  static final String createTableHistory = "CREATE TABLE history (timestamp DATETIME DEFAULT (datetime('now','localtime')), level_id INTEGER, score INTEGER, maxScore INTEGER, content TEXT, answer TEXT,correctAnswer Text, PRIMARY KEY(timestamp))";
  static final String createTableUnlocks = "CREATE TABLE unlocks (taskgroup_id INTEGER PRIMARY KEY)";

  SqliteDB._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('HörtrainingDB.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    print('dpPath:: ' + dbPath.toString());
    final path = join(dbPath, filePath);
    print('path:: ' + path.toString());
    return await openDatabase(path, version: 1, onCreate: _createTables);
  }

  Future _createTables(Database db, int version) async {
    await db.execute(createTableVowels);
    await db.execute(createTableSamples);
    await db.execute(createTableConsonantSimilarity);
    await db.execute(createTableConsonants);
    await db.execute(createTableTheme);
    await db.execute(createTableSampleThemes);
    await db.execute(createTableAnswer);
    await db.execute(createTableGapsentence);
    await db.execute(createTableSampleSentence);
    await db.execute(createTableRhymes);
    await db.execute(createTableHistory);
    await db.execute(createTableUnlocks);

    Vowels().insertVowels(db);
    ConsonantSimilarity().insertConsonantSimilarity(db);
    Consonants().insertConsonants(db);
    Theme().insertTheme(db);
    Numbers().insertNumbers(db);
    SampleThemes().insertSampleThemes(db);
    GapSentence().insertGapsentence(db);
    Answers().insertAnswer(db);
    SampleSentence().insertSampleSentence(db);
    Rhymes().insertRhymes(db);
    Sample().createSampleTable(db);
  }

  // Future<Note> readNote(int id) async {
  //   final db = await instance.database;
  //
  //   final maps = await db.query(
  //     tableNotes,
  //     columns: NoteFields.values,
  //     where: '${NoteFields.id} = ?',
  //     whereArgs: [id],
  //   );
  //
  //   if (maps.isNotEmpty) {
  //     return Note.fromJson(maps.first);
  //   } else {
  //     throw Exception('ID $id not found');
  //   }
  // }

  void readAllVowels() async {
    final db = await instance.database;

    // final orderBy = '${NoteFields.time} ASC';
    // final result =
    //     await db.rawQuery('SELECT * FROM $tableNotes ORDER BY $orderBy');

    final result = await db.query('vowels');
    print("vowels table:: " + result.toString());

    // return result.map((json) => Note.fromJson(json)).toList();
  }

  // Future<int> update(Note note) async {
  //   final db = await instance.database;
  //
  //   return db.update(
  //     tableNotes,
  //     note.toJson(),
  //     where: '${NoteFields.id} = ?',
  //     whereArgs: [note.id],
  //   );
  // }
  //
  // Future<int> delete(int id) async {
  //   final db = await instance.database;
  //
  //   return await db.delete(
  //     tableNotes,
  //     where: '${NoteFields.id} = ?',
  //     whereArgs: [id],
  //   );
  // }

  Future writeToHistory(int levelID,int points, int maxScore) async {
    final db = await instance.database;
    await db.execute("INSERT INTO history (level_id, score, maxScore) VALUES (?, ?, ?)",[levelID, points, maxScore]);
  }

  Future writeToHistoryWithErrorLog(int levelID,int points, int maxScore,String content, String answer, String correctAnswer) async {
    final db = await instance.database;
    await db.execute("INSERT INTO history (level_id, score, maxScore, content, answer, correctAnswer) VALUES (?, ?, ?, ?, ?, ?)",
    [levelID, points, maxScore, content, answer, correctAnswer]);
  }

  Future<bool> isUnlocked(int taskGroupId) async {
    final db = await instance.database;
    List<Map> results = await db.rawQuery('SELECT * FROM unlocks WHERE taskgroup_id = ?', [taskGroupId]);

    if(results.first.isNotEmpty){
      return true;
    }else{
      return false;
    }
    // Cursor cursor = getDatabase().query("unlocks",new String[]{"*"},"taskgroup_id = ?",new String[]{String.valueOf(taskgroup_id)},null,null,null);
    // if(cursor.moveToFirst()){
    //   return true;
    // }else {
    //   return false;}
  }

  Future<void> updateUnlocked(List<String> unlocks) async {
    final db = await instance.database;
    db.delete("unlocks");

    for (int i = 0; i < unlocks.length; i++) {
      if (unlocks[i] != "") {
        int s = int.parse(unlocks[i]);
        db.execute('INSERT INTO unlocks (taskgroup_id) VALUES (?)', [s]);
      }
    }
  }


  Future<List<History>> getHistoriesToSynchronize() async {
    List<History> list = [];
    // SharedPreferences sp = c.getSharedPreferences("history",0);
    // int lastTimestamp = sp.getLong("timestamp",0);
    int? lastTimestamp = await getTimestamp();

    Timestamp ts = Timestamp(timestamp: lastTimestamp);
    final db = await instance.database;
    final results = await db.rawQuery('SELECT * FROM history WHERE timestamp > ?', [ts]);
    list.addAll(results.map((json) => History.fromJson(json)).toList());
    print("List => ${list.length}");

    // Cursor cursor = getDatabase().query("history",new String[]{"*"},"timestamp > ?",new String[]{String.valueOf(ts)},null,null,null);
    List<History> histories = [];
      for (int i = 0; i < list.length; i++) {
        histories.add(list[i]);
      }
    return histories;
  }

  Future close() async {
    final db = await instance.database;

    db.close();
  }

  //Score
    Future<int> getHighestScore(int exerciseID) async {
    List<int> history = [];
    int highScore = 0;
    final db = await instance.database;
    final results = await db.rawQuery('SELECT score FROM history WHERE level_id = ? ORDER BY score DESC' ,[exerciseID]);
    history.addAll(results.map((json) => History.fromJson2(json)));

    print('history length: ${history.length}');
    if(history.length > 0)
      highScore = history[0];
    print('highScore: $highScore');
    return highScore;
  }

}

