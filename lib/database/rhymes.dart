import 'package:sqflite/sqflite.dart';

class Rhymes{

  Future insertRhymes(Database db) async {
    await db.transaction((txn) async {
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0039.mp3", 36]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0040.mp3", 48]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0045.mp3", 40]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0062.mp3", 71]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0065.mp3", 66]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0069.mp3", 55]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0072.mp3", 29]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0075.mp3", 4]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0081.mp3", 34]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0085.mp3", 37]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0087.mp3", 20]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0088.mp3", 3]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0090.mp3", 15]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0091.mp3", 28]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0092.mp3", 68]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0093.mp3", 38]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0094.mp3", 66]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0095.mp3", 67]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0097.mp3", 39]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0100.mp3", 29]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0105.mp3", 14]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0108.mp3", 17]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0111.mp3", 51]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0118.mp3", 43]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0119.mp3", 63]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0123.mp3", 67]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0125.mp3", 45]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0127.mp3", 54]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0128.mp3", 26]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0131.mp3", 52]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0135.mp3", 52]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0138.mp3", 7]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0139.mp3", 58]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0143.mp3", 29]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0144.mp3", 62]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0145.mp3", 69]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0146.mp3", 51]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0150.mp3", 7]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0158.mp3", 53]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0159.mp3", 2]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0166.mp3", 42]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0169.mp3", 59]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0171.mp3", 34]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0174.mp3", 25]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0175.mp3", 28]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0176.mp3", 71]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0177.mp3", 49]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0179.mp3", 50]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0184.mp3", 47]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0191.mp3", 12]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0193.mp3", 13]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0194.mp3", 57]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0195.mp3", 23]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0206.mp3", 54]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0208.mp3", 35]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0226.mp3", 34]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0229.mp3", 40]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0230.mp3", 3]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0233.mp3", 39]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0234.mp3", 55]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0238.mp3", 30]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0244.mp3", 60]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0251.mp3", 61]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0253.mp3", 65]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0254.mp3", 51]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0259.mp3", 23]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0261.mp3", 24]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0264.mp3", 53]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0268.mp3", 16]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0269.mp3", 42]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0271.mp3", 6]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0275.mp3", 46]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0276.mp3", 45]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0284.mp3", 44]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0286.mp3", 9]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0287.mp3", 53]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0299.mp3", 57]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0300.mp3", 70]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0303.mp3", 48]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0304.mp3", 67]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0305.mp3", 55]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0313.mp3", 60]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0315.mp3", 61]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0323.mp3", 43]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0324.mp3", 31]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0326.mp3", 70]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0327.mp3", 48]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0328.mp3", 49]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0329.mp3", 35]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0331.mp3", 6]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0337.mp3", 13]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0338.mp3", 69]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0339.mp3", 25]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0341.mp3", 48]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0342.mp3", 45]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0343.mp3", 27]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0345.mp3", 24]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0347.mp3", 2]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0348.mp3", 19]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0351.mp3", 6]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0352.mp3", 68]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0357.mp3", 37]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0358.mp3", 3]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0360.mp3", 15]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0364.mp3", 57]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0365.mp3", 66]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0366.mp3", 67]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0367.mp3", 39]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0369.mp3", 4]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0372.mp3", 62]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0373.mp3", 41]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0376.mp3", 60]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0377.mp3", 11]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0381.mp3", 33]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0382.mp3", 38]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0383.mp3", 21]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0384.mp3", 64]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0386.mp3", 43]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0393.mp3", 12]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0398.mp3", 2]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0400.mp3", 6]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0413.mp3", 20]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0414.mp3", 25]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0415.mp3", 5]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0421.mp3", 10]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0429.mp3", 8]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0430.mp3", 11]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0432.mp3", 61]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0435.mp3", 18]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0449.mp3", 57]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0450.mp3", 68]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0451.mp3", 10]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0455.mp3", 30]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0456.mp3", 22]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0458.mp3", 17]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0461.mp3", 24]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0463.mp3", 3]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0464.mp3", 41]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0467.mp3", 47]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0472.mp3", 56]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0476.mp3", 9]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0477.mp3", 34]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0478.mp3", 37]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0479.mp3", 28]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0482.mp3", 70]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0484.mp3", 35]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0496.mp3", 16]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0497.mp3", 42]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0502.mp3", 25]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0504.mp3", 15]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0508.mp3", 57]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0510.mp3", 55]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0512.mp3", 72]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0513.mp3", 29]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0515.mp3", 4]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0516.mp3", 62]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0519.mp3", 38]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0522.mp3", 22]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0523.mp3", 8]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0524.mp3", 50]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0531.mp3", 31]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0534.mp3", 47]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0538.mp3", 1]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0539.mp3", 52]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0541.mp3", 24]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0547.mp3", 46]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0549.mp3", 54]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0550.mp3", 35]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0559.mp3", 21]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0561.mp3", 48]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0562.mp3", 10]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0563.mp3", 55]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0565.mp3", 4]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0566.mp3", 62]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0569.mp3", 38]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0574.mp3", 51]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0580.mp3", 63]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0592.mp3", 59]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0593.mp3", 24]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0594.mp3", 58]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0599.mp3", 2]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0602.mp3", 6]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0605.mp3", 46]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0607.mp3", 40]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0610.mp3", 13]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0612.mp3", 68]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0613.mp3", 67]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0614.mp3", 39]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0616.mp3", 22]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0617.mp3", 65]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0618.mp3", 51]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0620.mp3", 64]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0621.mp3", 50]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0623.mp3", 47]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0626.mp3", 7]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0629.mp3", 42]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0631.mp3", 59]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0639.mp3", 58]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0644.mp3", 40]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0645.mp3", 5]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0651.mp3", 39]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0657.mp3", 65]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0658.mp3", 13]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0659.mp3", 15]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0660.mp3", 5]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0661.mp3", 45]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0663.mp3", 33]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0665.mp3", 27]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0669.mp3", 2]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0672.mp3", 37]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0676.mp3", 12]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0677.mp3", 9]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0678.mp3", 35]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0688.mp3", 19]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0693.mp3", 13]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0699.mp3", 15]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0700.mp3", 57]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0703.mp3", 10]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0704.mp3", 55]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0706.mp3", 45]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0707.mp3", 72]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0708.mp3", 14]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0710.mp3", 4]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0711.mp3", 41]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0718.mp3", 30]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0719.mp3", 49]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0720.mp3", 8]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0721.mp3", 35]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0725.mp3", 65]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0729.mp3", 27]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0730.mp3", 18]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0733.mp3", 52]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0734.mp3", 44]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0735.mp3", 59]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0738.mp3", 12]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0740.mp3", 9]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0741.mp3", 7]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0746.mp3", 2]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0753.mp3", 15]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0756.mp3", 45]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0758.mp3", 34]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0759.mp3", 46]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0760.mp3", 69]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0761.mp3", 3]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0762.mp3", 68]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0766.mp3", 14]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0767.mp3", 23]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0768.mp3", 49]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0769.mp3", 17]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0771.mp3", 21]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0772.mp3", 64]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0774.mp3", 50]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0776.mp3", 27]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0777.mp3", 63]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0779.mp3", 46]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0780.mp3", 69]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0783.mp3", 25]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0784.mp3", 28]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0785.mp3", 29]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0786.mp3", 8]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0788.mp3", 53]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0789.mp3", 42]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0791.mp3", 36]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0792.mp3", 21]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0797.mp3", 30]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0799.mp3", 11]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0802.mp3", 1]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0804.mp3", 28]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0806.mp3", 23]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0807.mp3", 54]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0809.mp3", 31]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0818.mp3", 19]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0819.mp3", 42]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0821.mp3", 70]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0822.mp3", 71]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0824.mp3", 17]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0825.mp3", 35]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0826.mp3", 33]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0832.mp3", 30]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0841.mp3", 50]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0845.mp3", 26]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0850.mp3", 1]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0857.mp3", 20]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0858.mp3", 48]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0859.mp3", 41]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0860.mp3", 23]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0861.mp3", 38]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0863.mp3", 32]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0869.mp3", 18]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0870.mp3", 26]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0871.mp3", 58]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0878.mp3", 16]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0879.mp3", 13]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0883.mp3", 25]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0884.mp3", 15]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0885.mp3", 66]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0886.mp3", 72]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0889.mp3", 17]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0890.mp3", 61]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0892.mp3", 32]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0893.mp3", 43]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0894.mp3", 31]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0896.mp3", 52]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0898.mp3", 24]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0899.mp3", 25]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0900.mp3", 15]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0907.mp3", 44]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0913.mp3", 53]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0916.mp3", 19]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0921.mp3", 40]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0922.mp3", 36]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0924.mp3", 5]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0929.mp3", 10]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0931.mp3", 13]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0932.mp3", 72]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0935.mp3", 30]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0938.mp3", 38]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0939.mp3", 49]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0943.mp3", 11]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0947.mp3", 65]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0949.mp3", 1]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0951.mp3", 43]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0955.mp3", 7]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0957.mp3", 1]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0959.mp3", 56]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0960.mp3", 59]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0963.mp3", 14]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0965.mp3", 64]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0969.mp3", 56]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0978.mp3", 16]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0982.mp3", 67]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0983.mp3", 32]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0989.mp3", 34]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0991.mp3", 36]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0992.mp3", 37]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0993.mp3", 20]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0994.mp3", 3]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0995.mp3", 15]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0997.mp3", 5]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m0998.mp3", 71]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1007.mp3", 49]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1009.mp3", 17]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1011.mp3", 35]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1012.mp3", 22]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1014.mp3", 60]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1017.mp3", 33]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1021.mp3", 63]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1022.mp3", 18]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1026.mp3", 26]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1029.mp3", 58]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1031.mp3", 2]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1036.mp3", 6]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1038.mp3", 36]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1040.mp3", 37]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1044.mp3", 66]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1046.mp3", 30]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1047.mp3", 22]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1049.mp3", 60]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1052.mp3", 32]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1056.mp3", 44]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1057.mp3", 56]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1060.mp3", 57]);
      await txn.execute(
          "INSERT INTO rhymes ('res_id', 'rhyme_group') values (?, ?)",
          ["raw/m1061.mp3", 23]);
      await txn.execute(
          "INSERT INTO rhymes SELECT females.res_id, males.rhyme_group FROM "
              "(SELECT * FROM samples NATURAL JOIN rhymes WHERE speaker = 0 AND type = 0) AS males "
              "JOIN (SELECT * FROM samples WHERE speaker != 0 AND type = 0) AS females ON males.content = females.content");
    });
  }
}