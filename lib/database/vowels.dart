import 'package:sqflite/sqflite.dart';

class Vowels{

  Future insertVowels(Database db) async {
    await db.transaction((txn) async {
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0001.mp3", "a", 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0002.mp3", "e", 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0003.mp3", "i", 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0004.mp3", "o", 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0005.mp3", "u", 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0006.mp3", "ä", 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0007.mp3", "ö", 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0008.mp3", "ü", 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0009.mp3", "a", 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0010.mp3", "e", 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0011.mp3", "i", 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0012.mp3", "o", 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0013.mp3", "u", 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0014.mp3", "ä", 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0015.mp3", "ö", 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0016.mp3", "ü", 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0017.mp3", "au", 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0018.mp3", "ei", 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0019.mp3", "eu", 1, null, null]
      );
      //real-words
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0081.mp3", "a", 0, 'au', 'Bauch']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0085.mp3", 'a', 1, 'ei', 'Bein']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0087.mp3", 'a', 0, 'i', 'Bild']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0090.mp3", 'a', 0, 'u', 'Bund']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0097.mp3", 'a', 0, 'u', 'Bus']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0100.mp3", 'au', 1, 'a', 'Bach']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0105.mp3", 'au', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0108.mp3", 'ei', 1, 'a', 'Bahn']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0118.mp3", 'i', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0119.mp3", 'i', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0126.mp3", 'e', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0129.mp3", 'i', 0, 'o', 'Block']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0131.mp3", 'o', 0, 'i', 'Blick']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0138.mp3", 'o', 1, 'e', 'Beet']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0141.mp3", 'o', 0, 'a', 'Bass']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0143.mp3", 'au', 1, 'u', 'Bruch']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0144.mp3", 'au', 1, 'o', 'Brot']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0145.mp3", 'a', 1, 'i', 'Brief']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0146.mp3", 'e', 0, 'au', 'Braut']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0147.mp3", 'i', 1, 'a', 'brav']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0150.mp3", 'o', 1, 'au', 'Braut']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0153.mp3", 'u', 0, 'au', 'Brauch']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0156.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0159.mp3", 'u', 0, 'a', 'Band']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0166.mp3", 'u', 0, 'i', 'Biss']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0168.mp3", 'e', 0, 'i', 'Schiff']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0171.mp3", 'a', 0, 'o', 'doch']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0174.mp3", 'a', 0, 'u', 'dumm']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0177.mp3", 'ei', 1, 'a', 'Dach']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0179.mp3", 'i', 0, 'o', 'Docht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0180.mp3", 'i', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0184.mp3", 'o', 0, 'a', 'Dach']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0185.mp3", 'o', 0, 'i', 'dicht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0187.mp3", 'o', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0193.mp3", 'a', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0195.mp3", 'e', 0, 'u', 'Druck']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0198.mp3", 'u', 0, 'e', 'Dreck']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0230.mp3", 'a', 0, 'e', 'Fell']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0234.mp3", 'a', 0, 'e', 'Fest']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0235.mp3", 'au', 1, 'e', 'Fest']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0236.mp3", 'a', 0, 'u', 'Fuchs']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0238.mp3", 'e', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0243.mp3", 'ei', 1, 'u', 'Fund']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0244.mp3", 'e', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0246.mp3", 'e', 0, 'a', 'Fall']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0253.mp3", 'e', 0, 'a', 'fast']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0256.mp3", 'i', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0258.mp3", 'i', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0260.mp3", 'ei', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0261.mp3", 'o', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0263.mp3", 'u', 1, 'a', 'flach']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0265.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0269.mp3", 'u', 0, 'ei', 'Fleiß']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0271.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0272.mp3", 'ö', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0275.mp3", 'a', 0, 'u', 'Frucht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0276.mp3", 'au', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0281.mp3", 'eu', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0282.mp3", 'i', 0, 'o', 'Frosch']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0283.mp3", 'i', 0, 'o', 'Frost']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0285.mp3", 'o', 0, 'i', 'frisch']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0287.mp3", 'u', 0, 'a', 'Fracht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0291.mp3", 'u', 0, 'a', 'Fax']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0293.mp3", 'u', 0, 'i', 'Fink']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0295.mp3", 'u', 1, 'i', 'fies']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0299.mp3", 'a', 0, 'o', 'Gong']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0303.mp3", 'a', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0305.mp3", 'a', 0, 'ei', 'Geist']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0311.mp3", 'ei', 1, 'a', 'Gast']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0313.mp3", 'e', 0, 'o', 'Gold']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0323.mp3", 'i', 1, 'a', 'gar']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0327.mp3", 'a', 1, 'ei', 'Gleis']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0329.mp3", 'ei', 1, 'a', 'Glas']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0330.mp3", 'ü', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0331.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0332.mp3", 'o', 0, 'e', 'Geld']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0337.mp3", 'a', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0341.mp3", 'a', 1, 'o', 'groß']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0345.mp3", 'o', 1, 'a', 'Gras']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0346.mp3", 'ü', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0349.mp3", 'u', 1, 'a', 'Gras']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0351.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0352.mp3", 'a', 1, 'e', 'Herr']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0357.mp3", 'a', 1, 'u', 'Huhn']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0358.mp3", 'a', 0, 'e', 'hell']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0360.mp3", 'a', 0, 'u', 'Hund']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0367.mp3", 'a', 0, 'au', 'Haus']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0369.mp3", 'au', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0372.mp3", 'au', 1, 'u', 'Hut']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0374.mp3", 'e', 0, 'a', 'Haft']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0378.mp3", 'e', 0, 'a', 'Halm']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0386.mp3", 'i', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0391.mp3", 'o', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0397.mp3", 'u', 1, 'a', 'Hahn']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0398.mp3", 'u', 0, 'a', 'Hand']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0400.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0413.mp3", 'a', 0, 'u', 'Kult']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0414.mp3", 'a', 0, 'ei', 'Keim']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0429.mp3", 'ei', 1, 'a', 'Kamm']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0434.mp3", 'i', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0435.mp3", 'i', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0438.mp3", 'i', 0, 'ei', 'kein']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0455.mp3", 'e', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0456.mp3", 'ei', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0461.mp3", 'o', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0462.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0465.mp3", 'i', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0467.mp3", 'o', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0472.mp3", 'o', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0484.mp3", 'ei', 1, 'au', 'kraus']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0486.mp3", 'eu', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0488.mp3", 'i', 1, 'u', 'Krug']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0492.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0494.mp3", 'u', 0, 'a', 'kalt']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0500.mp3", 'a', 0, 'o', 'Lok']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0502.mp3", 'a', 0, 'ei', 'Leim']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0510.mp3", 'a', 0, 'u', 'Lust']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0512.mp3", 'au', 1, 'ei', 'Leib']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0513.mp3", 'au', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0514.mp3", 'au', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0516.mp3", 'au', 1, 'ei', 'Leid']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0521.mp3", 'ei', 1, 'i', 'Licht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0522.mp3", 'ei', 1, 'i', 'Lied']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0523.mp3", 'ei', 1, 'e', 'Lehm']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0524.mp3", 'i', 0, 'ei', 'leicht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0526.mp3", 'i', 1, 'ei', 'Leib']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0529.mp3", 'i', 1, 'o', 'Lot']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0534.mp3", 'o', 0, 'au', 'Lauch']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0538.mp3", 'o', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0539.mp3", 'o', 0, 'a', 'Lack']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0541.mp3", 'o', 1, 'au', 'Laus']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0545.mp3", 'u', 0, 'a', 'Last']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0550.mp3", 'ei', 1, 'au', 'Maus']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0561.mp3", 'a', 1, 'o', 'Moos']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0565.mp3", 'au', 1, 'ei', 'Mais']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0566.mp3", 'au', 1, 'u', 'Mut']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0569.mp3", 'e', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0570.mp3", 'e', 1, 'a', 'Mahl']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0574.mp3", 'e', 0, 'a', 'matt']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0578.mp3", 'i', 0, 'o', 'Molch']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0582.mp3", 'i', 0, 'a', 'Mast']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0589.mp3", 'ö', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0590.mp3", 'o', 1, 'u', 'Mund']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0592.mp3", 'o', 1, 'e', 'Meer']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0593.mp3", 'o', 1, 'a', 'Maß']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0602.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0605.mp3", 'a', 0, 'i', 'nicht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0610.mp3", 'a', 1, 'o', 'Not']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0614.mp3", 'a', 0, 'u', 'Nuss']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0616.mp3", 'ei', 1, 'o', 'Not']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0618.mp3", 'e', 0, 'ei', 'Neid']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0621.mp3", 'i', 0, 'a', 'Nacht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0626.mp3", 'o', 1, 'a', 'Naht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0629.mp3", 'u', 0, 'a', 'nass']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0633.mp3", 'ö', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0656.mp3", 'e', 0, 'i', 'Pilz']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0657.mp3", 'e', 0, 'o', 'Post']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0658.mp3", 'a', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0659.mp3", 'a', 0, 'u', 'Pfund']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0661.mp3", 'au', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0662.mp3", 'ei', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0669.mp3", 'u', 0, 'a', 'Pfand']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0670.mp3", 'i', 0, 'e', 'Pelz']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0672.mp3", 'a', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0677.mp3", 'o', 0, 'e', 'Pest']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0685.mp3", 'u', 0, 'i', 'Pilz']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0693.mp3", 'a', 1, 'o', 'rot']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0699.mp3", 'a', 0, 'u', 'rund']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0700.mp3", 'a', 0, 'i', 'Ring']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0704.mp3", 'a', 0, 'o', 'Rost']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0708.mp3", 'au', 1, 'ei', 'Reim']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0711.mp3", 'e', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0718.mp3", 'e', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0719.mp3", 'ei', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0720.mp3", 'ei', 1, 'au', 'Raum']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0721.mp3", 'ei', 1, 'i', 'Riss']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0724.mp3", 'ei', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0725.mp3", 'e', 0, 'a', 'Rast']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0729.mp3", 'i', 0, 'au', 'rauf']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0730.mp3", 'i', 0, 'u', 'rund']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0731.mp3", 'i', 0, 'a', 'Rang']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0733.mp3", 'o', 0, 'e', 'Reck']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0735.mp3", 'o', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0740.mp3", 'o', 0, 'a', 'Rast']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0741.mp3", 'o', 1, 'a', 'Rat']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0744.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0745.mp3", 'u', 1, 'a', 'Rahm']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0755.mp3", 'a', 0, 'i', 'Sitz']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0756.mp3", 'au', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0760.mp3", 'a', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0764.mp3", 'a', 0, 'u', 'Schutz']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0766.mp3", 'au', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0767.mp3", 'e', 0, 'i', 'schick']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0768.mp3", 'ei', 1, 'a', 'Schach']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0769.mp3", 'ei', 1, 'o', 'schon']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0774.mp3", 'i', 0, 'a', 'Schacht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0776.mp3", 'i', 0, 'e', 'Chef']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0777.mp3", 'i', 0, 'u', 'Schuld']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0779.mp3", 'a', 0, 'u', 'Schlucht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0782.mp3", 'a', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0783.mp3", 'a', 0, 'ei', 'Schleim']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0785.mp3", 'au', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0786.mp3", 'ei', 1, 'i', 'schlimm']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0787.mp3", 'o', 0, 'u', 'Schluss']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0788.mp3", 'u', 0, 'a', 'Schlacht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0789.mp3", 'u', 0, 'o', 'Schloss']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0791.mp3", 'a', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0794.mp3", 'i', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0797.mp3", 'e', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0800.mp3", 'i', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0802.mp3", 'o', 1, 'ei', 'Schein']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0810.mp3", 'o', 0, 'i', 'Schritt']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0811.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0814.mp3", 'u', 0, 'i', 'Schild']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0820.mp3", 'u', 0, 'a', 'Schatz']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0824.mp3", 'ei', 1, 'a', 'Schwan']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0825.mp3", 'ei', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0832.mp3", 'e', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0841.mp3", 'i', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0842.mp3", 'i', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0843.mp3", 'i', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0845.mp3", 'i', 0, 'a', 'Satz']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0850.mp3", 'o', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0858.mp3", 'a', 1, 'i', 'Spieß']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0861.mp3", 'e', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0863.mp3", 'i', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0868.mp3", 'i', 1, 'a', 'Spaß']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0879.mp3", 'a', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0880.mp3", 'a', 1, 'au', 'Staub']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0886.mp3", 'au', 1, 'a', 'Stab']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0889.mp3", 'ei', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0891.mp3", 'i', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0892.mp3", 'i', 1, 'u', 'Stuhl']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0893.mp3", 'i', 1, 'u', 'stur']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0894.mp3", 'i', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0898.mp3", 'o', 1, 'ei', 'Steiß']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0905.mp3", 'e', 0, 'au', 'Strauß']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0907.mp3", 'o', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0908.mp3", 'o', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0909.mp3", 'ü', 0, 'o', 'Stock']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0910.mp3", 'u', 1, 'a', 'Stahl']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0913.mp3", 'u', 0, 'i', 'Sicht']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0920.mp3", 'a', 1, 'ei', 'Teig']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0922.mp3", 'a', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0931.mp3", 'a', 1, 'o', 'Tod']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0935.mp3", 'e', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0938.mp3", 'e', 1, 'i', 'Tier']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0939.mp3", 'ei', 1, 'u', 'Tuch']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0940.mp3", 'ei', 1, 'a', 'Tag']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0941.mp3", 'ei', 1, 'a', 'Tal']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0949.mp3", 'o', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0951.mp3", 'i', 1, 'o', 'Tor']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0952.mp3", 'i', 0, 'u', 'Tusch']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0955.mp3", 'o', 1, 'a', 'Tat']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0957.mp3", 'o', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0963.mp3", 'au', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0971.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0978.mp3", 'u', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0983.mp3", 'i', 1, 'e', 'Fell']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0989.mp3", 'a', 0, 'ei', 'weich']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0991.mp3", 'a', 1, 'o', 'Wohl']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0992.mp3", 'a', 1, 'ei', 'Wein']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0993.mp3", 'a', 0, 'i', 'wild']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m0995.mp3", 'a', 0, 'u', 'wund']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1006.mp3", 'e', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1009.mp3", 'ei', 1, 'a', 'Wahn']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1012.mp3", 'ei', 1, 'u', 'Wut']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1014.mp3", 'e', 0, 'a', 'Wald']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1021.mp3", 'i', 0, 'e', 'Welt']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1022.mp3", 'i', 0, 'u', 'wund']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1027.mp3", 'o', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1031.mp3", 'u', 0, 'a', 'Wand']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1036.mp3", 'u', 1, 'ei', 'weit']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1038.mp3", 'a', 1, 'i', 'Ziel']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1040.mp3", 'a', 1, 'e', 'zehn']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1046.mp3", 'e', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1047.mp3", 'ei', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1051.mp3", 'eu', 1, 'u', 'Zug']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1052.mp3", 'i', 1, 'a', 'Zahl']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1055.mp3", 'o', 0, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1056.mp3", 'o', 1, null, null]
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1058.mp3", 'u', 1, 'eu', 'Zeug']
      );
      await txn.execute(
          "INSERT INTO vowels ('res_id', 'vowel', 'length', 'alternative', 'minimalpair') values (?, ?, ?, ?, ?)",
          ["raw/m1061.mp3", 'e', 0, 'ei', 'Zweig']
      );
      await txn.execute(
          "INSERT INTO vowels SELECT females.res_id, males.vowel, males.length, males.alternative, males.minimalpair FROM "
              "(SELECT * FROM samples NATURAL JOIN vowels WHERE speaker = 0) AS males "
              "JOIN (SELECT res_id, content FROM samples WHERE speaker <> 0) AS females ON males.content = females.content"
      );
    });
  }
}