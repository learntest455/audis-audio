import 'package:sqflite/sqflite.dart';

class SampleThemes{

  Future insertSampleThemes(Database db) async {
    await db.transaction((txn) async {
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0041.mp3', 0] //Abendkleid Kleidung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0059.mp3', 1] //Apfelsaft Lebensmittel
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0083.mp3',  2] //Backofen Küche
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0107.mp3',  3] //Baustelle Stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0148.mp3',  4] //Briefkasten Kommunikation
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0172.mp3',  5] //Dachboden Wohnung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0178.mp3',  6] //Dezember Zeit
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0211.mp3',  7] //Eisenbahn Verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0221.mp3',  5] //Erdgeschoss Wohung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0249.mp3',  5] //Fensterbrett Wohnung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0266.mp3',  8] //Flughafen Reisen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0280.mp3',  4] //Fremdsprache Kommunikation
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0318.mp3',  9] //Geschichte Märchen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0363.mp3',  10] //Handwerker Berufe
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0399.mp3',  11] //Hürdenlauf Sport
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0402.mp3',  12] //Instrument Musik
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0485.mp3',  7] //Kreisverkehr Verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0507.mp3', 13] //Landwirtschaft Landwirtschaft
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0533.mp3', 14] //Lippenstift Badezimmer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0555.mp3', 9] //Märchenbuch Märchen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0584.mp3', 6] //Mitternacht Zeit
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0604.mp3', 4] //Nachrichten Kommunikation
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0609.mp3', 14] //Nagellack Badezimmer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0630.mp3', 13] //Obstwiese Landwirtschaft
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0646.mp3', 15] //Papierkorb Büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0664.mp3', 13] //Pferdestall Landwirtschaft
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0702.mp3', 14] //Rasierschaum Badezimmer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0717.mp3', 16] //Regenwurm Tiere
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0793.mp3', 16] //Schmetterling Tiere
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0904.mp3', 7] //Straßenbahn Verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0915.mp3', 3] //Supermarkt Stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0923.mp3', 7] //Tankstelle Verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0953.mp3', 17] //Tischdecke Restaurant
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0981.mp3', 8] //Urlaubszeit Reisen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1000.mp3', 14] //Waschbecken Badezimmer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1010.mp3', 18] //Weintrauben Früchte
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0039.mp3', 19] //Aal Meer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0042.mp3', 15] //Abteilung Büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0044.mp3', 20] //Afrika Geografie
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0046.mp3', 10] //Akustiker Berufe
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0048.mp3', 10] //Altenpfleger Berufe
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0049.mp3', 3] //Altstadt Stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0050.mp3', 20] //Amerika Geografie
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0051.mp3', 7] //Ampel Verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0052.mp3', 3] //Amt Stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0053.mp3', 18] //Ananas Früchte
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0055.mp3', 10] //Anwalt Berufe
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0057.mp3', 0] //Anzug Kleidung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0058.mp3', 18] //Apfel Früchte
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0060.mp3', 3] //Apotheke Stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0061.mp3', 6] //April Zeit
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0062.mp3', 21] //Arm Körper
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0066.mp3', 10] //Arzt Berufe
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0067.mp3', 3] //Arztpraxis Stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0068.mp3', 20] //Asien Geografie
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0069.mp3', 22] //Ast Natur
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0070.mp3', 10] //Astronaut Berufe
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0073.mp3', 21] //Auge Körper
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0074.mp3', 6] //August Zeit
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0076.mp3', 8] //Ausflugsziel Reisen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0077.mp3', 20] //australien geografie
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0078.mp3', 7] //auto verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0079.mp3', 7] //autobahn verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0081.mp3', 22] //bach natur
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0082.mp3', 10] //bäcker berufe
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0084.mp3', 3] //badesee stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0085.mp3', 7] //bahn verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0086.mp3', 3] //bahnhof stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0088.mp3', 23] //ball kinder
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0089.mp3', 18] //banane früchte
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0091.mp3', 24] //bank geld
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0092.mp3', 24] //bar geld
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0093.mp3', 16] //bär tiere
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0094.mp3', 21] //bart körper
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0096.mp3', 11] //basketball sport
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0097.mp3', 12] //bass musik
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0099.mp3', 10] //bauarbeiter berufe
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0100.mp3', 21] //bauch körper
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0102.mp3', 13] //bauer landwirtschaft
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0103.mp3', 13] //bauernhof landwirtschaft
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0104.mp3', 23] //bauklötze kinder
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0105.mp3', 22] //baum natur
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0106.mp3', 24] //bausparen geld
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0108.mp3', 21] //bein körper
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0109.mp3', 20] //berg geografie
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0110.mp3', 8] //bergsteigen reisen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0111.mp3', 5] //bett wohnung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0114.mp3', 16] //beuteltier tiere
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0115.mp3', 15] //bewerbung büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0116.mp3', 3] //bibliothek stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0117.mp3', 22] //bienenkorb natur
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0118.mp3', 17] //bier restaurant
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0119.mp3', 23] //bild kinder
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0120.mp3', 23] //bilderbuch kinder
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0121.mp3', 15] //bildschirm büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0122.mp3', 18] //birne früchte
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0124.mp3', 22] //blatt natur
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0125.mp3', 25] //blau farben
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0126.mp3', 26] //blech werkstatt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0128.mp3', 27] //blitz wetter
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0133.mp3', 5] //blumentopf wohnung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0137.mp3', 26] //bohrmaschine werkstatt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0138.mp3', 19] //boot meer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0140.mp3', 9] //böse märchen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0141.mp3', 15] //boss büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0144.mp3', 28] //braut personen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0146.mp3', 26] //brett werkstatt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0147.mp3', 4] //brief kommunikation
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0150.mp3', 1] //brot lebensmittel
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0151.mp3', 1] //brötchen lebensmittel
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0152.mp3', 2] //brotkorb küche
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0154.mp3', 28] //bruder personen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0155.mp3', 21] //brust körper
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0156.mp3', 9] //buch märchen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0157.mp3', 29] //bücher schule
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0158.mp3', 19] //bucht meer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0161.mp3', 23] //buntstift kinder
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0162.mp3', 30] //burg mittelalter
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0163.mp3', 3] //bürgersteig stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0164.mp3', 30] //burggraben mittelalter
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0165.mp3', 5] //büro wohnung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0166.mp3', 7] //bus verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0167.mp3', 22] //busch natur
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0168.mp3', 15] //chef büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0169.mp3', 12] //chor musik
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0170.mp3', 15] //computer büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0171.mp3', 5] //dach wohnung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0173.mp3', 16] //dachs tiere
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0176.mp3', 21] //darm körper
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0177.mp3', 19] //deich meer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0181.mp3', 6] //dienstag zeit
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0183.mp3', 11] //disziplin sport
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0186.mp3', 15] //dokument büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0187.mp3', 3] //dom stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0188.mp3', 27] //donner wetter
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0189.mp3', 6] //donnerstag zeit
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0190.mp3', 3] //dorf stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0191.mp3', 2] //dose küche
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0192.mp3', 9] //drache märchen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0193.mp3', 26] //draht werkstatt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0195.mp3', 26] //dreck werkstatt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0196.mp3', 15] //drehstuhl büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0197.mp3', 23] //dreirad kinder
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0203.mp3', 14] //dusche badezimmer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0204.mp3', 14] //duschvorhang badezimmer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0205.mp3', 5] //ehebett wohnung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0206.mp3', 1] //ei lebensmittel
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0208.mp3', 1] //eis lebensmittel
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0209.mp3', 16] //eisbär tiere
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0210.mp3', 3] //eisdiele stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0213.mp3', 11] //eiskunstlauf sport
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0214.mp3', 10] //elektriker berufe
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0215.mp3', 9] //elfen märchen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0217.mp3', 28] //enkel personen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0218.mp3', 16] //ente tiere
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0220.mp3', 18] //ertxneere früchte
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0222.mp3', 10] //erzieherin berufe
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0224.mp3', 20] //europa geografie
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0225.mp3', 3] //fabrik stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0226.mp3', 29] //fach schule
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0227.mp3', 7] //fahrbahn verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0228.mp3', 7] //fahrradständer verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0235.mp3', 21] //faust körper
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0236.mp3', 15] //fax büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0237.mp3', 6] //februar zeit
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0238.mp3', 9] //fee märchen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0239.mp3', 15] //feierabend büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0240.mp3', 6] //feiertag zeit
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0241.mp3', 18] //feige früchte
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0242.mp3', 22] //feigenbaum natur
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0243.mp3', 28] //feind personen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0244.mp3', 13] //feld landwirtschaft
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0245.mp3', 1] //feldsalat lebensmittel
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0246.mp3', 16] //fell tiere
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0247.mp3', 22] //fels natur
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0248.mp3', 5] //fenster wohnung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0250.mp3', 29] //ferien schule
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0252.mp3', 4] //fernseher Kommunikation
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0255.mp3', 3] //feuerwehr stadt
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0256.mp3', 4] //film Kommunikation
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0257.mp3', 21] //finger körper
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0258.mp3', 19] //fisch meer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0259.mp3', 23] //Fleck kinder
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0260.mp3', 1] //Fleisch Lebensmittel
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0262.mp3', 12] //Flöte Musik
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0263.mp3', 9] //fluch märchen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0265.mp3', 8] //flug reisen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0267.mp3', 7] //flugzeug verkehr
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0268.mp3', 5] //flur wohnung
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0269.mp3', 20] //fluss geografie
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0271.mp3', 19] //flut meer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0272.mp3', 14] //föhn badezimmer
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0274.mp3', 10] //fotograf berufe
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0276.mp3', 28] //frau personen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0278.mp3', 6] //freitag zeit
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0279.mp3', 23] //freizeit kinder
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0281.mp3', 28] //freund personen
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0282.mp3', 1] //frisch lebensmittel
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0283.mp3', 15] //frist büro
    );
    await txn.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0285.mp3', 16] //frosch tiere
    );
    insertSampleThemes2(txn);
  });

}
  Future insertSampleThemes2(Transaction db) async {
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0286.mp3', 27] //frost wetter
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0287.mp3', 1] //frucht lebensmittel
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0288.mp3', 5] //frühjahrsputz wohnung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0289.mp3', 6] //frühling zeit
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0290.mp3', 2] //frühstück küche
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0291.mp3', 16] //fuchs tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0292.mp3', 29] //füller schule
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0293.mp3', 4] //funk kommunikation
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0295.mp3', 21] //fuß körper
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0296.mp3', 11] //fußball sport
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0297.mp3', 13] //futter landwirtschaft
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0298.mp3', 17] //gabel restaurant
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0300.mp3', 16] //gans tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0302.mp3', 10] //gärtner berufe
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0305.mp3', 17] //gast restaurant
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0309.mp3', 11] //gegner sport
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0310.mp3', 12] //geige musik
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0311.mp3', 9] //geist märchen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0312.mp3', 25] //gelb farben
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0313.mp3', 24] //geld geld
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0314.mp3', 17] //gericht restaurant
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0316.mp3', 12] //gesang musik
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0317.mp3', 24] //geschäft geld
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0321.mp3', 17] //getränk restaurant
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0322.mp3', 11] //gewichtheben sport
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0327.mp3', 17] //glas restaurant
  );

  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0329.mp3', 7] //gleis verkehr
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0330.mp3', 9] //glück im märchen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0332.mp3', 9] //gold märchen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0333.mp3', 10] //goldschmied berufe
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0334.mp3', 11] //golf sport
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0337.mp3', 27] //grad wetter
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0341.mp3', 22] //gras natur
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0342.mp3', 25] //grau farben
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0346.mp3', 25] //grün farben
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0351.mp3', 9] //gut märchen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0352.mp3', 21] //haar körper
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0353.mp3', 14] //haarklammer badezimmer
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0354.mp3', 19] //hafen meer
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0355.mp3', 15] //haftnotiz büro
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0356.mp3', 27] //hagelkorn wetter
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0357.mp3', 16] //hahn tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0359.mp3', 26] //hammer werkstatt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0360.mp3', 21] //hand körper
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0362.mp3', 14] //handtuch badezimmer
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0366.mp3', 16] //hase tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0368.mp3', 17] //hauptgang restaurant
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0369.mp3', 5] //haus wohnung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0370.mp3', 29] //hausaufgaben schule
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0371.mp3', 5] //hausschlüssel wohnung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0372.mp3', 21] //haut körper
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0373.mp3', 16] //hecht tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0374.mp3', 29] //heft schule
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0375.mp3', 5] //heizung wohnung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0376.mp3', 9] //held märchen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0378.mp3', 30] //helm mittelalter
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0379.mp3', 0] //hemd kleidung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0380.mp3', 6] //herbst zeit
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0381.mp3', 2] //herd küche
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0382.mp3', 28] //herr personen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0383.mp3', 21] //herz körper
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0384.mp3', 13] //heu landwirtschaft
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0385.mp3', 9] //hexe märchen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0387.mp3', 18] //himbeere früchte
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0388.mp3', 22] //himmel natur
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0389.mp3', 21] //hirn körper
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0390.mp3', 27] //hitze wetter
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0391.mp3', 26] //holz werkstatt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0393.mp3', 0] //hose kleidung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0394.mp3', 8] //hotel reisen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0395.mp3', 8] //hotelbett reisen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0396.mp3', 7] //hubschrauber verkehr
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0397.mp3', 13] //huhn landwirtschaft
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0398.mp3', 16] //hund tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0400.mp3', 0] //hut kleidung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0401.mp3', 16] //insekt tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0403.mp3', 4] //internet kommunikation
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0404.mp3', 0] //jacke kleidung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0406.mp3', 6] //januar zeit
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0407.mp3', 6] //juli zeit
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0408.mp3', 6] //juni zeit
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0410.mp3', 15] //kaffee büro
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0412.mp3', 2] //kaffeemaschine küche
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0413.mp3', 27] //kalt wetter
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0414.mp3', 14] //kamm badezimmer
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0416.mp3', 15] //kantine büro
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0417.mp3', 17] //karte restaurant
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0419.mp3', 1] //kartoffel lebensmittel
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0420.mp3', 1] //käse lebensmittel
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0421.mp3', 24] //kasse geld
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0422.mp3', 24] //kassenbon geld
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0424.mp3', 16] //katze tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0425.mp3', 24] //kauf geld
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0426.mp3', 10] //kauffrau berufe
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0427.mp3', 3] //kaufhaus stadt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0428.mp3', 10] //kaufmann berufe
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0430.mp3', 5] //keller wohnung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0431.mp3', 17] //kellner restaurant
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0433.mp3', 17] //kerze restaurant
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0434.mp3', 22] //kies natur
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0435.mp3', 28] //kind personen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0436.mp3', 23] //kindergarten kinder
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0437.mp3', 23] //kinderlied kinder
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0438.mp3', 21] //kinn körper
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0439.mp3', 3] //kino stadt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0440.mp3', 3] //kirche stadt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0441.mp3', 3] //kirchturmuhr stadt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0442.mp3', 18] //kirsche früchte
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0443.mp3', 1] //kirschkuchen lebensmittel
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0447.mp3', 18] //kiwi früchte
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0449.mp3', 12] //klang musik
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0451.mp3', 29] //klasse schule
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0452.mp3', 12] //klassik musik
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0453.mp3', 12] //klavier musik
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0455.mp3', 22] //klee natur
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0456.mp3', 0] //kleid kleidung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0457.mp3', 5] //kleiderschrank wohnung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0459.mp3', 24] //kleingeld geld
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0461.mp3', 1] //kloß lebensmittel
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0464.mp3', 30] //knecht mittelalter
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0465.mp3', 21] //knie körper
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0466.mp3', 9] //kobold märchen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0467.mp3', 17] //koch restaurant
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0468.mp3', 2] //kochlöffel küche
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0470.mp3', 15] //kollege büro
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0471.mp3', 20] //kompass geografie
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0472.mp3', 21] //kopf körper
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0477.mp3', 23] //krach kinder
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0478.mp3', 3] //kran stadt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0480.mp3', 3] //krankenhaus stadt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0481.mp3', 10] //krankenschwester berufe
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0483.mp3', 19] //krebs meer
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0487.mp3', 8] //kreuzfahrtschiff reisen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0488.mp3', 30] //krieg mittelalter
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0489.mp3', 5] //küche Wohnung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0490.mp3', 2] //küchentisch küche
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0491.mp3', 15] //kugelschreiber büro
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0492.mp3', 13] //kuh landwirtschaft
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0493.mp3', 2] //kühlschrank küche
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0495.mp3', 29] //kunst schule
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0496.mp3', 8] //kur reisen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0497.mp3', 9] //kuss märchen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0498.mp3', 19] //küste meer
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0499.mp3', 16] //lachs tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0500.mp3', 26] //lack werkastatt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0502.mp3', 16] //lamm tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0503.mp3', 5] //lampenschirm wohnung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0504.mp3', 20] //land geografie
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0505.mp3', 8] //landebahn reisen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0506.mp3', 20] //landkarte geografie
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0509.mp3', 26] //lärm werkstatt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0511.mp3', 7] //lastwagen verkehr
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0512.mp3', 22] //laub natur
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0513.mp3', 1] //lauch lebensmittel
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0514.mp3', 11] //lauf sport
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0515.mp3', 16] //laus tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0517.mp3', 12] //lautstärke musik
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0518.mp3', 1] //leberwurst lebensmittel
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0520.mp3', 29] //lehrer schule
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0523.mp3', 26] //leim werkstatt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0524.mp3', 22] //licht natur
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0525.mp3', 5] //lichtschalter wohnung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0527.mp3', 9] //liebe märchen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0529.mp3', 12] //lied musik
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0530.mp3', 8] //liegestuhl reisen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0531.mp3', 5] //lift wohnung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0532.mp3', 29] //lineal schule
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0535.mp3', 15] //locher büro
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0536.mp3', 17] //löffel restaurant
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0538.mp3', 15] //lohn büro
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0539.mp3', 7] //lok verkehr
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0540.mp3', 10] //lokführer berufe
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0543.mp3', 22] //luft natur
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0546.mp3', 23] //lutscher kinder
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0548.mp3', 9] //magie märchen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0549.mp3', 6] //mai zeit
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0550.mp3', 1] //mais lebensmittel
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0551.mp3', 18] //mandarine früchte
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0552.mp3', 18] //mango früchte
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0553.mp3', 28] //mann personen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0554.mp3', 11] //marathon sport
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0556.mp3', 24] //mark geld
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0557.mp3', 3] //markt stadt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0558.mp3', 3] //matktplatz stadt
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0559.mp3', 6] //märz zeit
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0560.mp3', 9] //maskenball märchen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0563.mp3', 13] //mast landwirtschaft
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0564.mp3', 5] //matratze wohnung
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0565.mp3', 16] //maus tiere
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0566.mp3', 7] //maut verkehr
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0567.mp3', 4] //medien kommunikation
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0568.mp3', 10] //mediziner berufe
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0569.mp3', 8] //meer reisen
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0570.mp3', 1] //mehl lebensmittel
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0571.mp3', 12] //melodie musik
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0572.mp3', 18] //melone früchte
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0573.mp3', 17] //messer restaurant
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0574.mp3', 1] //mett lebensmittel
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0575.mp3', 10] //metzger berufe
  );
  await db.execute(
      "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
      ['raw/m0577.mp3', 2] //mikrowelle küche
  );
  insertSampleThemes3(db);
}

  Future insertSampleThemes3(Transaction db) async {
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0578.mp3', 1] //milch lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0582.mp3', 13] //mist landwirtschaft
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0583.mp3', 15] //mittagspause büro
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0585.mp3', 6] //mittwoch zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0586.mp3', 2] //mixer küche
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0587.mp3', 4] //moderation kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0588.mp3', 6] //monat zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0590.mp3', 22] //mond natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0591.mp3', 6] //montag zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0592.mp3', 22] //moor natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0593.mp3', 22] //moos natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0595.mp3', 7] //motorrad verkehr
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0596.mp3', 19] //möwe meer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0598.mp3', 2] //mülleimer küche
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0599.mp3', 21] //mund körper
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0600.mp3', 24] //münze geld
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0601.mp3', 19] //muschel meer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0603.mp3', 0] //mütze kleidung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0605.mp3', 6] //nacht zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0606.mp3', 17] //nachtisch restaurant
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0608.mp3', 26] //nagel werkstatt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0610.mp3', 0] //naht kleidung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0612.mp3', 30] //narr mittelalter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0613.mp3', 21] //nase körper
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0614.mp3', 14] //nass badezimmer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0615.mp3', 28] //neffe personen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0622.mp3', 28] //nichte personen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0625.mp3', 20] //norden geografie
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0627.mp3', 6] //november zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0628.mp3', 1] //nudel lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0629.mp3', 1] //nuss lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0631.mp3', 21] //ohr körper
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0632.mp3', 6] //oktober zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0633.mp3', 1] //öl lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0634.mp3', 28] //oma personen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0635.mp3', 28] //onkel personen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0636.mp3', 28] //opa personen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0637.mp3', 25] //orange farben
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0639.mp3', 20] //ort geografie
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0640.mp3', 20] //osten geografie
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0641.mp3', 20] //ozean geografie
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0642.mp3', 4] //päckchen Kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0643.mp3', 4] //paket kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0648.mp3', 3] //park stadt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0649.mp3', 3] //parkdeck stadt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0651.mp3', 8] //pass reisen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0653.mp3', 28] //patenkind personen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0654.mp3', 29] //pause schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0655.mp3', 9] //pech märchen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0657.mp3', 30] //pest mittelalter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0660.mp3', 2] //pfanne küche
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0661.mp3', 16] //pfau tiere
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0663.mp3', 16] //pferd tiere
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0665.mp3', 11] //pfiff sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0666.mp3', 18] //pfirsich früchte
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0668.mp3', 18] //pflaume früchte
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0669.mp3', 24] //pfund geld
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0670.mp3', 1] //pilz lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0674.mp3', 1] //plätzchen lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0675.mp3', 3] //polizei stadt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0677.mp3', 4] //post kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0678.mp3', 24] //preis geld
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0679.mp3', 10] //priester berufe
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0680.mp3', 9] //prinz märchen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0681.mp3', 30] //prinzessin mittelalter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0683.mp3', 29] //prüfung schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0684.mp3', 29] //prüfungsangst schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0685.mp3', 21] //puls körper
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0686.mp3', 29] //pult schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0687.mp3', 4] //punkt kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0688.mp3', 23] //puppe kinder
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0689.mp3', 23] //puppenhaus kinder
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0690.mp3', 24] //quittung geld
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0691.mp3', 4] //quizsendung kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0692.mp3', 24] //rabatt geld
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0693.mp3', 7] //rad verkehr
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0694.mp3', 4] //radio kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0696.mp3', 11] //radrennen sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0697.mp3', 3] //radweg stadt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0705.mp3', 3] //rathaus stadt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0708.mp3', 5] //raum wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0714.mp3', 5] //regalbrett wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0715.mp3', 27] //regen wetter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0716.mp3', 0] //regenjacke kleidung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0721.mp3', 1] //reis lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0722.mp3', 8] //reisen reisen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0723.mp3', 8] //reisepass reisen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0726.mp3', 9] //rettung märchen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0727.mp3', 10] //richter berufe
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0729.mp3', 19] //riff meer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0730.mp3', 13] //rind landwirtschaft
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0732.mp3', 30] //ritter mittelalter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0733.mp3', 0] //rock kleidung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0735.mp3', 14] //rohr badezimmer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0737.mp3', 25] //rosa farben
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0738.mp3', 22] //rose natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0739.mp3', 1] //rosenkohl lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0741.mp3', 25] //rot farben
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0742.mp3', 29] //rucksack schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0743.mp3', 11] //ruderboot sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0748.mp3', 1] //Saft Lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0749.mp3', 9] //Sage Märchen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0750.mp3', 1] //salz lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0751.mp3', 17] //salzstreuer restaurant
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0752.mp3', 6] //samstag zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0753.mp3', 19] //sand meer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0755.mp3', 4] //satz kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0756.mp3', 13] //sau landwirtschaft
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0760.mp3', 16] //schaf tiere
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0761.mp3', 12] //schall musik
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0764.mp3', 9] //schatz märchen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0765.mp3', 23] //schaukelpferd kinder
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0766.mp3', 14] //schaum badezimmer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0767.mp3', 24] //scheck geld
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0769.mp3', 24] //schein geld
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0770.mp3', 15] //schere büro
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0773.mp3', 13] //scheune landwirtschaft
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0776.mp3', 19] //schiff meer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0777.mp3', 7] //schild verkehr
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0778.mp3', 19] //schilf meer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0779.mp3', 30] //schlacht mittelalter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0781.mp3', 5] //schlafzimmer wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0787.mp3', 30] //schloss mittelalter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0790.mp3', 5] //schlüssel wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0794.mp3', 30] //schmied mittelalter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0797.mp3', 27] //schnee wetter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0798.mp3', 2] //schneebesen küche
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0801.mp3', 1] //schokolade lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0803.mp3', 5] //schornstein wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0804.mp3', 5] //schrank wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0805.mp3', 26] //schraube werkstatt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0807.mp3', 23] //schrei kinder
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0808.mp3', 15] //schreibtisch büro
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0809.mp3', 29] //schrift schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0811.mp3', 0] //schuh kleidung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0812.mp3', 3] //schuhgeschäft stadt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0813.mp3', 29] //schulabschluss schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0815.mp3', 23] //schule kinder
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0816.mp3', 29] //schulhof schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0817.mp3', 29] //schulleiter schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0823.mp3', 25] //schwarz farben
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0824.mp3', 13] //schwein landwirtschaft
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0826.mp3', 30] //schwert mittelalter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0827.mp3', 28] //schwester personen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0828.mp3', 28] //schwiegereltern personen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0829.mp3', 3] //schwimmbad stadt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0830.mp3', 11] //schwimmen sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0832.mp3', 20] //see geografie
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0833.mp3', 19] //seefahrt meer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0834.mp3', 14] //seife badezimmer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0836.mp3', 10] //sekretärin berufe
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0837.mp3', 1] //sekt lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0838.mp3', 4] //sender kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0839.mp3', 6] //september zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0847.mp3', 29] //sitznachbar schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0848.mp3', 11] //ski sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0849.mp3', 5] //sofa wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0850.mp3', 28] //sohn personen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0851.mp3', 6] //sommer zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0853.mp3', 27] //sonne wetter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0854.mp3', 8] //sonnenschirm reisen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0855.mp3', 6] //sonnenuhr zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0856.mp3', 6] //sonntag zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0858.mp3', 23] //spaß kinder
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0859.mp3', 16] //specht tiere
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0860.mp3', 1] //speck lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0862.mp3', 11] //speerwurf sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0863.mp3', 23] //spiel kinder
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0864.mp3', 11] //spielfeld sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0865.mp3', 23] //spielplatz kinder
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0866.mp3', 3] //spielstraße stadt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0867.mp3', 23] //spielzeug kinder
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0871.mp3', 29] //sport schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0872.mp3', 11] //sportverein sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0873.mp3', 4] //sprache kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0876.mp3', 2] //spüle küche
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0877.mp3', 2] //Spülmittel küche
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0879.mp3', 20] //staat geografie
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0881.mp3', 11] //stabhochsprung sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0882.mp3', 20] //stadt geografie
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0883.mp3', 22] //stamm natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0885.mp3', 11] //start sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0887.mp3', 5] //steckdose wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0889.mp3', 22] //stein natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0890.mp3', 22] //stern natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0893.mp3', 16] //stier tiere
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0894.mp3', 29] //stift schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0895.mp3', 12] //stimmgabel musik
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0896.mp3', 22] //stock natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0900.mp3', 19] //strand meer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0901.mp3', 8] //strandkorb reisen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0902.mp3', 8] //strandurlaub reisen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0903.mp3', 3] //straße stadt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0905.mp3', 15] //stress büro
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0907.mp3', 13] //stroh landwirtschaft
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0910.mp3', 17] //stuhl restaurant
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0911.mp3', 29] //stundenplan schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0912.mp3', 27] //sturm wetter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0914.mp3', 20] //süden geografie
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0916.mp3', 1] //suppe lebensmittel
    );
    insertSampleThemes4(db);
  }

Future insertSampleThemes4(Transaction db) async {
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0917.mp3', 23] //süßigkeit kinder
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0918.mp3', 15] //tacker büro
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0919.mp3', 29] //tafel schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0920.mp3', 6] //tag zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0921.mp3', 12] //takt musik
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0922.mp3', 20] //Tal Geografie
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0924.mp3', 22] //tanne natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0926.mp3', 28] //tante personen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0929.mp3', 2] //tasse küche
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0930.mp3', 15] //tastatur büro
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0933.mp3', 11] //team sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0935.mp3', 1] //tee lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0937.mp3', 2] //teekanne küche
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0938.mp3', 7] //teer verkehr
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0939.mp3', 22] //teich natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0940.mp3', 1] //teig lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0942.mp3', 4] //telefon kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0943.mp3', 17] //teller restaurant
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0944.mp3', 27] //temperatur wetter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0945.mp3', 11] //tennis sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0946.mp3', 5] //teppich wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0948.mp3', 15] //textmarker büro
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0949.mp3', 30] //thron mittelalter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0950.mp3', 2] //tiefkühlfach küche
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0951.mp3', 13] //tier landwirtschaft
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0952.mp3', 17] //tisch restaurant
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0954.mp3', 28] //tochter personen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0955.mp3', 9] //tod märchen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0956.mp3', 1] //tomate lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0957.mp3', 12] //ton musik
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0959.mp3', 2] //topf küche
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0960.mp3', 11] //tor sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0961.mp3', 1] //tortenguss lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0962.mp3', 13] //traktor landwirtschaft
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0967.mp3', 12] //trommel musik
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0968.mp3', 12] //trompete musik
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0970.mp3', 23] //trotz kinder
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0971.mp3', 0] //tuch kleidung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0972.mp3', 22] //tulpe natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0973.mp3', 5] //tür wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0974.mp3', 25] //türkis farben
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0975.mp3', 5] //türklinke wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0976.mp3', 30] //turm mittelalter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0977.mp3', 11] //turnen sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0978.mp3', 6] //uhr zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0979.mp3', 29] //unterricht schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0980.mp3', 15] //urlaub büro
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0984.mp3', 25] //violett farben
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0985.mp3', 16] //vogel tiere
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0987.mp3', 15] //vorlage büro
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0988.mp3', 17] //vorspeise restaurant
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0993.mp3', 22] //wald natur
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0995.mp3', 5] //wand wohnung
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0996.mp3', 8] //wandern reisen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0997.mp3', 14] //wanne badezimmer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m0998.mp3', 27] //warm wetter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1001.mp3', 14] //wäschekorb badezimmer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1002.mp3', 19] //wasser meer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1003.mp3', 14] //wasserhahn badezimmer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1004.mp3', 2] //wasserkocher küche
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1005.mp3', 26] //wasserwaage werkstatt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1008.mp3', 13] //waidenzaun landwirtschaft
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1009.mp3', 17] //wein restaurant
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1011.mp3', 25] //weiß farben
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1013.mp3', 19] //wellen meer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1014.mp3', 8] //welt reisen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1016.mp3', 26] //werkzeug werkstatt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1017.mp3', 24] //wert geld
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1019.mp3', 20] //westen geografie
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1020.mp3', 11] //wettkampf sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1022.mp3', 27] //wind wetter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1024.mp3', 6] //winter zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1027.mp3', 16] //wolf tiere
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1028.mp3', 27] //wolken wetter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1029.mp3', 4] //wort kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1030.mp3', 19] //wrack meer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1032.mp3', 9] //wunsch märchen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1033.mp3', 16] //wurm tiere
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1034.mp3', 1] //wurst lebensmittel
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1035.mp3', 20] //wüste geogafie
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1037.mp3', 11] //yoga sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1038.mp3', 29] //zahl schule
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1040.mp3', 21] //zahn körper
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1041.mp3', 14] //zahnbürste badezimmer
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1043.mp3', 26] //zange werkstatt
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1045.mp3', 9] //zauberer märchen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1046.mp3', 21] //zeh körper
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1047.mp3', 6] //zeit zeit
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1048.mp3', 4] //zeitung kommunikation
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1049.mp3', 8] //zelt reisen
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1050.mp3', 15] //zettel büro
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1052.mp3', 11] //ziel sport
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1054.mp3', 18] //zitrone früchte
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1055.mp3', 24] //zoll geld
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1056.mp3', 16] //zoo tiere
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1057.mp3', 21] //zopf körper
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1058.mp3', 7] //zug verkehr
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1059.mp3', 30] //zugbrücke mittelalter
    );
    await db.execute(
        "INSERT INTO samplethemes ('res_id', 'theme_id') values (?, ?)",
        ['raw/m1062.mp3', 9] //zwerg märchen
    );
    //insert all themes for female samples based on the male ones by matching content...
    await db.execute("INSERT INTO samplethemes SELECT females.res_id, males.theme_id FROM (SELECT * FROM samples NATURAL "
        "JOIN samplethemes WHERE speaker = 0 AND type = 0) AS males JOIN (SELECT * FROM samples WHERE speaker != 0 AND type = 0) "
        "AS females ON males.content = females.content");
  }

}