import 'package:sqflite/sqflite.dart';

class Theme{

  Future insertTheme(Database db) async {
    await db.transaction((txn) async {
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [14, 'Badezimmer']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [10, 'Berufe']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [15, 'Büro']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [25, 'Farben']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [18, 'Früchte']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [24, 'Geld']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [20, 'Geografie']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [23, 'Kinder']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [0, 'Kleidung']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [4, 'Kommunikation']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [21, 'Körper']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [2, 'Küche']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [13, 'Landwirtschaft']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [1, 'Lebensmittel']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [9, 'Märchen']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [19, 'Meer']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [30, 'Mittelalter']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [12, 'Musik']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [22, 'Natur']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [28, 'Personen']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [8, 'Reisen']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [17, 'Restaurant']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [29, 'Schule']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [11, 'Sport']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [3, 'Stadt']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [16, 'Tiere']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [7, 'Verkehr']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [26, 'Werkstatt']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [27, 'Wetter']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [5, 'Wohnung']
      );
      await txn.execute(
          "INSERT INTO theme ('theme_id', 'theme') values (?, ?)",
          [6, 'Zeit']
      );
    });
  }
}