import 'package:audis/views/home.dart';
import 'package:flutter/material.dart';

import 'appearance/theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: AppTheme.global,
        home: HomeScreen()
    );
  }
}

