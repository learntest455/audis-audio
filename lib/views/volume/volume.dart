import 'package:audioplayers/audioplayers.dart';
import 'package:audis/utils/shared_preferences.dart';
import 'package:audis/utils/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:volume_control/volume_control.dart';

typedef void OnError(Exception exception);

class VolumeSet extends StatefulWidget {
  const VolumeSet({Key? key}) : super(key: key);

  @override
  _VolumeSetState createState() => _VolumeSetState();
}

class _VolumeSetState extends State<VolumeSet> {
  late AudioPlayer advancedPlayer;
  late AudioCache audioCache;
  double _val = 0.1;

  @override
  void initState() {
    super.initState();
    initPlayer();
  }

  void initPlayer() async {
    advancedPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    _val = (await getVolume()) ?? 0.1;
    setState(() {});
    print("initPlayer=> $_val");
  }

  @override
  void dispose() {
    super.dispose();
    advancedPlayer.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        advancedPlayer.stop();
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          body: Container(
            color: Colors.orangeAccent,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                      child: Text(
                    Strings.volumeMenuText,
                    style: TextStyle(fontSize: 20),
                  )),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('0', style: TextStyle(fontSize: 15)),
                      Expanded(
                        child: Slider(
                            value: _val,
                            min: 0,
                            max: 1,
                            onChanged: (val) {
                              _val = val;
                              advancedPlayer.setVolume(rvolume:val);
                              VolumeControl.setVolume(val);
                              setState(() {});
                              print("val=> $_val");
                            }),
                      ),
                      Text('100', style: TextStyle(fontSize: 15)),
                    ],
                  ),
                  SizedBox(height: 10),
                  ElevatedButton(
                      onPressed: () async {
                        audioCache.play('raw/white_noise.mp3',
                            lvolume: _val, rvolume: _val);
                        VolumeControl.setVolume(_val);
                      },
                      child: Text(Strings.test_abspielen)),
                  SizedBox(height: 10),
                  ElevatedButton(
                      onPressed: () {
                        advancedPlayer.stop();
                        storeVolume(_val);
                      },
                      child: Text(Strings.save_changes))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
