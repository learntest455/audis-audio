import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/speaker.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding1.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechWordUnderstanding2 extends SpeechWordUnderstanding1{

  List<Sample> list2 = [];
  List<Sample> samplesDistinctContent2 = [];
  AudioPlayer advancedPlayer = new AudioPlayer();

  String mediaFile = "";

  SpeechWordUnderstanding2({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_WORDUNDERSTANDING_B;
    category = Category.SPEECH;
    name = Strings.wordunderstandingTaskBName;
    taskText = Strings.wordunderstandingTaskADescr;
  }

  Future<void> setUpTask() async {
    advancedPlayer.release();
    Random rnd = new Random();

    //choosing one sample to play (and remove from list: it is not played twice in a exercise)
    int correctIdx = rnd.nextInt(list2.length);
    Sample correctSample = list2[correctIdx];
    list2.remove(correctIdx);

    Set<String> answerContents = new Set(); //set cant hold equal objects... => not showing one answer multiple times
    int correctPos = rnd.nextInt(4); //the position of the correct answer

    //this is all for not showing one answer multiple times
    while (answerContents.length < 4) {
      if (answerContents.length == correctPos) {
        answerContents.add(correctSample.getContent());
      } else {
        String ans = samplesDistinctContent2[rnd.nextInt(samplesDistinctContent2.length)].getContent();
        if (ans != correctSample.getContent())
          answerContents.add(ans);
      }
    }

    answers = [];
    for (String ans in answerContents) {
      answers.add(Answer.constructor2(ans, ans == correctSample.getContent()));
    }
    mediaFile = correctSample.getRes_id();
}

  @override
  Future<void> resetTask() async {
    score = 0;
    list2.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT * FROM samples WHERE syllables < 4 and samples.type = ?', [Type.WORD]);
    list2.addAll(results.map((json) => Sample.fromJson(json)).toList());
    final results2 = await db.rawQuery(
        'SELECT * FROM samples WHERE syllables < 4 AND samples.speaker = ? And samples.type = ?', [Speaker.MALE, Type.WORD]);
    samplesDistinctContent2.addAll(results2.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list2.length}");
    setUpTask();

  }

}