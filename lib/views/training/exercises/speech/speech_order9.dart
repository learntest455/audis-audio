import 'dart:math';

import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/views/training/exercises/speech/speech_order8.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechOrder9 extends SpeechOrder8 {

  SpeechOrder9({Function()? onReset}) {
    this.onResetFun = onReset;
    exerciseId = Level.SPEECH_ORDER_I;
  }

  @override
  Future<void> setUpTask() async {
    answerVisible = false;
    onResetFun!();
    advancedPlayer.release();

    Random rnd = new Random();
    Sample rhymeStart = list[rnd.nextInt(list.length)];
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT samples.* FROM samples NATURAL JOIN rhymes WHERE rhyme_group = '
        '(SELECT rhyme_group FROM rhymes WHERE res_id = ? )',
        [rhymeStart.getRes_id()]);
    List<Sample> rhyming = [];
    rhyming.addAll(results.map((json) => Sample.fromJson(json)).toList());

    //we have to ensure that in one task no word is spoken by the man AND the women (distinct content)
    playSamplesList = [];
    while (playSamplesList.length < 4) {
      Sample? sample = rhyming[rnd.nextInt(rhyming.length)];
      for (int i = 0; i < playSamplesList.length; i++) {
        if (playSamplesList[i].getContent() == sample!.getContent()) {
          sample =
              null; //this word is already in the list... set null and dont add
          break;
        }
      }
      if (sample != null) {
        playSamplesList.add(sample);
        rhyming.remove(sample);
      }
    }

    currentIndex = 0;
    mediaFile = playSamplesList[currentIndex].getRes_id();
    currentTask = playSamplesList[currentIndex].getContent();
    onResetFun!();
    advancedPlayer.onPlayerCompletion.listen((event) {
      play(playSamplesList);
    });

    List<Sample> tmp = [];
    tmp.addAll(playSamplesList);
    answers = [];
    while (tmp.length > 0) {
      Sample s = tmp.removeAt(rnd.nextInt(tmp.length));
      answers.add(Answer.constructor1(s.getContent(), playSamplesList.indexOf(s)));
    }
  }
}
