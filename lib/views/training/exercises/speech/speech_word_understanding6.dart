
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/speaker.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding2.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechWordUnderstanding6 extends SpeechWordUnderstanding2{

  SpeechWordUnderstanding6({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_WORDUNDERSTANDING_F;
    category = Category.SPEECH;
    name = Strings.wordunderstandingTaskFName;
    taskText = Strings.wordunderstandingTaskADescr;
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list2.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT * FROM samples WHERE syllables == 1 and samples.type != ?', [Type.NONSENSE]);
    list2.addAll(results.map((json) => Sample.fromJson(json)).toList());
    final results2 = await db.rawQuery(
        'SELECT * FROM samples WHERE syllables == 1 AND samples.speaker = ? And samples.type != ?', [Speaker.MALE, Type.NONSENSE]);
    samplesDistinctContent2.addAll(results2.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list2.length}");
    setUpTask();
  }
}