import 'dart:math';

import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/answer_model.dart';
import 'package:audis/database/model/gapsentece_model.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence1.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:sprintf/sprintf.dart';

class SpeechGapSentence3 extends SpeechGapSentence1{

  SpeechGapSentence3({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    exerciseId = Level.SPEECH_SENTENCEUNDERSTANDING_C;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    answerVisible = false;
    onResetFun!();
    setUpTask();
  }

  @override
  Future<void> setUpTask() async {
    advancedPlayer.release();
    currentTask = "";
    Random rnd = new Random();

    //choosing one sample to play (and remove from list: it is not played twice in a exercise)
    int correctIdx = rnd.nextInt(list.length);
    GapSentenceModel correctSample = list.removeAt(correctIdx);
    String correctSentence = correctSample.getContent();
    String? gapSentence = correctSample.getGapsentece();

    RegExp regexp = RegExp(gapSentence!.replaceAll("%s", "(.+)"));
    final match = regexp.firstMatch(correctSentence);
    String firstGap = match!.group(1).toString();
    String secondGap = match.group(2).toString();

    String correctGapFill;
    List<String> strAnswers = [];

    //ask for the first or second gap?
    if (rnd.nextBool()){
      currentTask = sprintf(gapSentence, [gap,secondGap]); //first
      correctGapFill = firstGap;
      final db = await SqliteDB.instance.database;
      final results = await db.rawQuery(
          'SELECT answer FROM answer WHERE answer_id = ?', [correctSample.getAnswer_id1()]);
      strAnswers.addAll(results.map((json) => AnswerModel.fromJson(json)).toList());
    } else {
      currentTask = sprintf(gapSentence,[firstGap,gap]); //second
      correctGapFill = secondGap;
      final db = await SqliteDB.instance.database;
      final results = await db.rawQuery(
          'SELECT answer FROM answer WHERE answer_id = ?', [correctSample.getAnswer_id2()]);
      strAnswers.addAll(results.map((json) => AnswerModel.fromJson(json)).toList());
    }

    int correctPos = rnd.nextInt(4);
    Set<String> answerContents = new Set();

    while (answerContents.length < 4) {
      if (answerContents.length == correctPos) {
        answerContents.add(correctGapFill);
      } else {
        String ans = strAnswers[rnd.nextInt(strAnswers.length)];
        if (ans != correctGapFill)
          answerContents.add(ans);
      }
    }

    answers = [];
    for (String ans in answerContents)
    {
      answers.add(Answer.constructor2(ans, ans == correctGapFill));
    }
    mediaFile = correctSample.getRes_id();

    // mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
    //     {
    //     @Override
    //     public void onCompletion(MediaPlayer mp) {
    //     currentTask = currentTaskText;
    //     mListener.OnTaskFinishedPlay();
    //     mListener.OnCurrentTaskUpdated();
    //     }
    //     });
  }
}