import 'dart:core';

import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic2.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechDichotic4 extends SpeechDichotic2 {


  SpeechDichotic4({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_DICHOTIC_D;
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT * FROM samples Where type = ?', [Type.NUMBER]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

}
