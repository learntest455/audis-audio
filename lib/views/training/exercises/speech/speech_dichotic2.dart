import 'dart:core';
import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic1.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class SpeechDichotic2 extends SpeechDichotic1 {

  SpeechDichotic2({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_DICHOTIC_B;

  }
  @override
  void setAnswers() {
    bool leftAsked = new Random().nextBool();
    if (leftAsked) {
      currentTask = Strings.left;
    } else {
      currentTask = Strings.right;
    }
    answers = [];
    List<Answer> lAnswers = [];
    int k;
    //add both played samples to the answers
    for (k=0; k < playSamplesList.length; k++) {
      if (leftAsked) {
        lAnswers.add(Answer.constructor2(playSamplesList[k].getContent(), k == 0));
      } else {
        lAnswers.add(Answer.constructor2(playSamplesList[k].getContent(), k == 1));
      }
    }

    //add other DISTINCT samples to the answers
    //for (k=playsampleslist.size(); k < answers.length; k++) {
    //  lAnswers.add(new Answer(list.get(rnd.nextInt(list.size())).getContent(),false));
    //}
    while (lAnswers.length < 4) {
      Sample? sample = list[rnd.nextInt(list.length)];
      //check if the text is already in answerlist
      for (int j = 0; j < lAnswers.length; j++){
        if (sample!.getContent() == lAnswers[j].getText()){
          sample = null;
          break;
        }
      }
      if (sample != null){
        lAnswers.add(Answer.constructor2(sample.getContent(), false));
      }
    }

    int i = 0;
    while (lAnswers.length > 0) {
      answers.add(lAnswers.removeAt(rnd.nextInt(lAnswers.length)));
    }
  }
}
