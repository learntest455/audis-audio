import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/answer_model.dart';
import 'package:audis/database/model/gapsentece_model.dart';
import 'package:audis/database/model/speed.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';
import 'package:sprintf/sprintf.dart';

class SpeechGapSentence1 extends Level{

  List<GapSentenceModel> list = [];
  String gap = "____";
  AudioPlayer advancedPlayer = new AudioPlayer();
  void Function()? onResetFun;
  String mediaFile = "";

  SpeechGapSentence1({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_SENTENCEUNDERSTANDING_A;
    taskGroup = TaskGroup.SPEECH_SENTENCEUNDERSTANDING;
    category = Category.SPEECH;
    name = Strings.sentenceUnderstandingTaskAName;
    taskText = Strings.sentenceUnderstandingTaskADesc;
  }

  @override
  int getMaxScore() {
    return 25;
  }

  @override
  String getStatus() {
    return score.toString() + "/" + getMaxScore().toString();
  }

  @override
  bool hasMoreExercises() {
    score++;
    if (score >= 25) {
      return false;
    }
    return true;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    onResetFun!();
    setUpTask();
  }

  @override
  Future<void> playSound() async {
    AudioCache audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      answerVisible = true;
      onResetFun!();
    });
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT * FROM samples natural inner join samplesentence natural inner join gapsentence WHERE TYPE = ? AND speed = ?', [Type.SENTENCE, Speed.SLOW]);
    list.addAll(results.map((json) => GapSentenceModel.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

  Future<void> setUpTask() async {
    advancedPlayer.release();
    Random rnd = new Random();

    //choosing one sample to play (and remove from list: it is not played twice in a exercise)
    int correctIdx = rnd.nextInt(list.length);
    GapSentenceModel correctSample = list.removeAt(correctIdx);
    String correctSentence = correctSample.getContent();
    String? gapSentence = correctSample.getGapsentece();

    RegExp regexp = RegExp(gapSentence!.replaceAll("%s", "(.+)"));
    final match = regexp.firstMatch(correctSentence);
    String firstGap = match!.group(1).toString();
    String secondGap = match.group(2).toString();

    String correctGapFill;
    // Cursor otherAnswers;

    List<String> strAnswers = [];
    //ask for the first or second gap?
    if (rnd.nextBool()){
      currentTask = sprintf(gapSentence,[gap,secondGap]); //first
      correctGapFill = firstGap;
      final db = await SqliteDB.instance.database;
      final results = await db.rawQuery(
          'SELECT answer FROM answer WHERE answer_id = ?', [correctSample.getAnswer_id1()]);
      strAnswers.addAll(results.map((json) => AnswerModel.fromJson(json)).toList());
    } else {
      currentTask = sprintf(gapSentence, [firstGap,gap]); //second
      correctGapFill = secondGap;
      final db = await SqliteDB.instance.database;
      final results = await db.rawQuery(
          'SELECT answer FROM answer WHERE answer_id = ?', [correctSample.getAnswer_id2()]);
      strAnswers.addAll(results.map((json) => AnswerModel.fromJson(json)).toList());
    }

    int correctPos = rnd.nextInt(4);
    Set<String> answerContents = new Set();

    while (answerContents.length < 4) {
      if (answerContents.length == correctPos) {
        answerContents.add(correctGapFill);
      } else {
        String ans = strAnswers[rnd.nextInt(strAnswers.length)];
        if (ans != correctGapFill)
          answerContents.add(ans);
      }
    }
    answers = [];
    for (String ans in answerContents)
    {
      answers.add(Answer.constructor2(ans, ans == correctGapFill));
    }
    mediaFile = correctSample.getRes_id();
  }
}