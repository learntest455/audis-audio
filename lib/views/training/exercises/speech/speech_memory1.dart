import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';
import 'package:audis/database/model/type.dart';

class SpeechMemory1 extends Level {
  List<Sample> samples = [];
  List<Sample> list = [];
  int currentIndex = 0;
  int correctPos = 0;
  List<Sample> playSamplesList = [];
  AudioPlayer advancedPlayer = new AudioPlayer();
  bool isPlayFinish = false;
  void Function()? onResetFun;
  String mediaFile = "";

  SpeechMemory1({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_MEMORY_A;
    taskGroup = TaskGroup.SPEECH_MEMORY;
    category = Category.SPEECH;
    name = Strings.memory;
    taskText = Strings.memoryTaskDesc;
  }

  @override
  Future<void> playSound() async {
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    audioCache.play(mediaFile).then((value) => isPlayFinish = false);
  }

  @override
  String getStatus() {
    return score.toString() + "/" + getMaxScore().toString();
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    onResetFun!();
    setUpTask();
  }

  @override
  bool hasMoreExercises() {
    score++;
    if (score >= getMaxScore()) {
      return false;
    }
    return true;
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results =
        await db.rawQuery('SELECT * FROM samples Where type = ?', [Type.WORD]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

  @override
  int getMaxScore() {
    return 25;
  }

  void setUpTask() {
    advancedPlayer.release();
    Random rnd = new Random();

    playSamplesList = [];
    //choosing one sample to play (and remove from list: it is not played twice in a exercise)
    while (playSamplesList.length < 3) {
      Sample? sample = list[rnd.nextInt(list.length)];
      for (int i = 0; i < playSamplesList.length; i++) {
        if (playSamplesList[i].getContent() == sample!.getContent()) {
          sample =
              null; //this word is already in the list... set null and dont add
          break;
        }
      }
      if (sample != null) {
        playSamplesList.add(sample);
        list.remove(sample);
      }
    }
    currentIndex = 0;
    mediaFile = playSamplesList[currentIndex].getRes_id();
    advancedPlayer.onPlayerCompletion.listen((event) async {
      play(playSamplesList);
    });

    correctPos = new Random().nextInt(playSamplesList.length);
    currentTask = playSamplesList[correctPos].getContent();
    answers = [];

    for (int k = 0; k < playSamplesList.length; k++) {
      answers.add(Answer.constructor2("${k + 1}", k == correctPos));
    }
  }

  void play(List<Sample> playSamplesList) {
    if (!isPlayFinish) {
      isPlayFinish = true;
      currentIndex++;
      if (currentIndex < playSamplesList.length) {
        mediaFile = playSamplesList[currentIndex].getRes_id();
        playSound();
      }else{
        isEnableTile = true;
        onResetFun!();
      }
    }
  }
}
