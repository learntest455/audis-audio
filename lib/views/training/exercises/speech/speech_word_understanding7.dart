import 'dart:math';

import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding2.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechWordUnderstanding7 extends SpeechWordUnderstanding2{

  SpeechWordUnderstanding7({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_WORDUNDERSTANDING_G;
    category = Category.SPEECH;
    name = Strings.highConfuse;
    taskText = Strings.wordunderstandingTaskADescr;
  }

  Future<void> setUpTask() async {
    advancedPlayer.release();
    Random rnd = new Random();

    //choosing one sample to play (and remove from list: it is not played twice in a exercise)
    int correctIdx = rnd.nextInt(list2.length);
    Sample correctSample = list2[correctIdx];
    list2.remove(correctIdx);

    final db = await SqliteDB.instance.database;
    final results2 = await db.rawQuery(
        'SELECT samples.* FROM samples NATURAL JOIN rhymes WHERE speaker = 0 AND rhyme_group = (SELECT rhyme_group FROM rhymes WHERE res_id = ?)', [correctSample.getRes_id()]);
    samplesDistinctContent2.addAll(results2.map((json) => Sample.fromJson(json)).toList());

    Set<String> answerContents = new Set(); //set cant hold equal objects... => not showing one answer multiple times
    int correctPos = rnd.nextInt(4); //the position of the correct answer

    //this is all for not showing one answer multiple times
    while (answerContents.length < 4) {
      if (answerContents.length == correctPos) {
        answerContents.add(correctSample.getContent());
      } else {
        String ans = samplesDistinctContent2[rnd.nextInt(samplesDistinctContent2.length)].getContent();
        if (ans != correctSample.getContent())
          answerContents.add(ans);
      }
    }

    answers = [];
    for (String ans in answerContents) {
      answers.add(Answer.constructor2(ans, ans == correctSample.getContent()));
    }
    mediaFile = correctSample.getRes_id();

}

  @override
  Future<void> resetTask() async {
    score = 0;
    list2.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT samples.* FROM samples NATURAL JOIN rhymes');
    list2.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list2.length}");
    setUpTask();
  }

}