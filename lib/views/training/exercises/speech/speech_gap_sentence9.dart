import 'package:audis/database/model/gapsentece_model.dart';
import 'package:audis/database/model/speed.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence6.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechGapSentence9 extends SpeechGapSentence6{

  SpeechGapSentence9({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    exerciseId = Level.SPEECH_SENTENCEUNDERSTANDING_I;
  }
  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT * FROM samples natural inner join samplesentence natural inner join gapsentence WHERE TYPE = ? AND speed = ?', [Type.SENTENCE, Speed.FAST]);
    list.addAll(results.map((json) => GapSentenceModel.fromJson(json)).toList());
    print("List => ${list.length}");
    askingFirstGap = true;
    setUpFirstGap();
  }
}