import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/views/training/exercises/speech/speech_order1.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechOrder4 extends SpeechOrder1{

  SpeechOrder4({Function()? onReset}) {
    this.onResetFun = onReset;
    currentTask = "";
    exerciseId = Level.SPEECH_ORDER_D;
  }

  void play(List<Sample> playSamplesList) {
    if (!isPlayFinish) {
      isPlayFinish = true;
      currentIndex++;
      if (currentIndex < playSamplesList.length) {
        currentTask = "";
        mediaFile = playSamplesList[currentIndex].getRes_id();
        playSound();
      } else {
        currentTask = "";
        answerVisible = true;
        isEnableTile = true;
        isVisibleCounter = true;
      }
      onResetFun!();
    }
  }

  void setUpTask() {
    answerVisible = false;
    onResetFun!();
    advancedPlayer.release();
    Random rnd = new Random();

    //we have to ensure that in one task no word is spoken by the man AND the women (distinct content)
    playSamplesList = [];
    while (playSamplesList.length < 3) {
      Sample? sample = list[rnd.nextInt(list.length)];
      for (int i = 0; i < playSamplesList.length; i++) {
        if (playSamplesList[i].getContent() == sample!.getContent()) {
          sample =
          null; //this word is already in the list... set null and dont add
          break;
        }
      }
      if (sample != null) {
        playSamplesList.add(sample);
        list.remove(sample);
      }
    }

    currentIndex = 0;
    mediaFile = playSamplesList[currentIndex].getRes_id();
    advancedPlayer.onPlayerCompletion.listen((event) {
      play(playSamplesList);
    });

    answers = [];
    List<Answer> lAnswers = [];
    for (int k = 0; k < playSamplesList.length; k++) {
      lAnswers.add(Answer.constructor1(playSamplesList[k].getContent(), k));
    }
    while (lAnswers.length > 0) {
      answers.add(lAnswers.removeAt(rnd.nextInt(lAnswers.length)));
    }
  }

}