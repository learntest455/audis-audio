import 'dart:math';

import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/speech/speech_memory1.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechMemory3 extends SpeechMemory1 {

  SpeechMemory3({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_MEMORY_C;
    taskText = Strings.memoryTaskDesc;
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT * FROM samples Where type = ? and syllables == 1', [Type.WORD]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

  void setUpTask() {
    advancedPlayer.release();
    Random rnd = new Random();
    //choosing one sample to play (and remove from list: it is not played twice in a exercise)
    playSamplesList = [];
    while (playSamplesList.length < 3) {
      Sample? sample = list[rnd.nextInt(list.length)];
      for (int i = 0; i < playSamplesList.length; i++) {
        if (playSamplesList[i].getContent() == sample!.getContent()) {
          sample = null; //this word is already in the list... set null and dont add
          break;
        }
      }
      if (sample != null) {
        playSamplesList.add(sample);
        list.remove(sample);
      }
    }
    currentIndex = 0;
    mediaFile = playSamplesList[currentIndex].getRes_id();
    advancedPlayer.onPlayerCompletion.listen((event) async {
      play(playSamplesList);
    });

    correctPos = new Random().nextInt(playSamplesList.length);
    currentTask = playSamplesList[correctPos].getContent();
    answers = [];

    for (int k = 0; k < playSamplesList.length; k++) {
      answers.add(Answer.constructor2("${k+1}", k == correctPos));
    }
  }

}