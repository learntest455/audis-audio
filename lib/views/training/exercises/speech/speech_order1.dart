import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';
import 'package:audis/database/model/type.dart';

class SpeechOrder1 extends Level {
  List<Sample> list = [];
  int currentIndex = 0;
  List<Sample> playSamplesList = [];
  AudioPlayer advancedPlayer = new AudioPlayer();
  bool isPlayFinish = false;
  void Function()? onResetFun;
  String mediaFile = "";

  SpeechOrder1({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    exerciseId = Level.SPEECH_ORDER_A;
    taskGroup = TaskGroup.SPEECH_ORDER;
    category = Category.SPEECH;
    name = Strings.order;
    taskText = Strings.orderTaskDesc;
    isVisibleCounter = true;
  }

  @override
  void playSound() {
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    audioCache.play(mediaFile).then((value) => isPlayFinish = false);
  }

  @override
  String getStatus() {
    return score.toString() + "/" + getMaxScore().toString();
  }

  @override
  void nextExercise() {
    answerVisible = false;
    onResetFun!();
    setUpTask();
  }

  @override
  bool hasMoreExercises() {
    score++;
    if (score >= 25) {
      return false;
    }
    return true;
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT * FROM samples Where type = ? AND syllables > 1', [Type.WORD]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

  @override
  int getMaxScore() {
    return 25;
  }

  void setUpTask() {
    answerVisible = false;
    onResetFun!();
    advancedPlayer.release();
    Random rnd = new Random();

    //we have to ensure that in one task no word is spoken by the man AND the women (distinct content)
    playSamplesList = [];
    while (playSamplesList.length < 3) {
      Sample? sample = list[rnd.nextInt(list.length)];
      for (int i = 0; i < playSamplesList.length; i++) {
        if (playSamplesList[i].getContent() == sample!.getContent()) {
          sample =
              null; //this word is already in the list... set null and dont add
          break;
        }
      }
      if (sample != null) {
        playSamplesList.add(sample);
        list.remove(sample);
      }
    }

    currentIndex = 0;
    mediaFile = playSamplesList[currentIndex].getRes_id();
    currentTask = playSamplesList[currentIndex].getContent();
    onResetFun!();
    advancedPlayer.onPlayerCompletion.listen((event) {
      play(playSamplesList);
    });

    answers = [];
    List<Answer> lAnswers = [];
    for (int k = 0; k < playSamplesList.length; k++) {
      lAnswers.add(Answer.constructor1(playSamplesList[k].getContent(), k));
    }
    while (lAnswers.length > 0) {
      answers.add(lAnswers.removeAt(rnd.nextInt(lAnswers.length)));
    }
  }

  void play(List<Sample> playSamplesList) {
    if (!isPlayFinish) {
      isPlayFinish = true;
      currentIndex++;
      if (currentIndex < playSamplesList.length) {
        currentTask = playSamplesList[currentIndex].getContent();
        mediaFile = playSamplesList[currentIndex].getRes_id();
        playSound();
      } else {
        currentTask = "";
        answerVisible = true;
        isEnableTile = true;
        isVisibleCounter = true;
      }
      onResetFun!();
    }
  }
}
