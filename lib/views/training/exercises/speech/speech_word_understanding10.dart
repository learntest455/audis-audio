import 'package:audioplayers/audioplayers.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding7.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechWordUnderstanding10 extends SpeechWordUnderstanding7 {
  SpeechWordUnderstanding10({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_WORDUNDERSTANDING_J;
    category = Category.SPEECH;
    name = Strings.wordunderstandingTaskFName + " (" + Strings.lessVolume + ")";
    taskText = Strings.wordunderstandingTaskADescr;
  }

  @override
  Future<void> playSound() async {
    AudioCache audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.setVolume(rvolume: 0.25);
    advancedPlayer.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
  }
}
