import 'dart:core';
import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/rhyme_model.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class SpeechDichotic7 extends Level {
  List<int> rhymeGroups = [];
  int numOfAnswers = 2;
  AudioPlayer advancedPlayerLeft = new AudioPlayer();
  AudioPlayer advancedPlayerRight = new AudioPlayer();
  String mediaFileLeft = "";
  String mediaFileRight = "";
  void Function()? onResetFun;

  SpeechDichotic7({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_DICHOTIC_G;
    taskGroup = TaskGroup.SPEECH_DICHOTIC;
    category = Category.SPEECH;
    name = Strings.dichotic;
    taskText = Strings.dichoticTaskDesc;
  }

  @override
  Future<void> playSound() async {
    AudioCache audioCacheLeft = new AudioCache(fixedPlayer: advancedPlayerLeft);
    await audioCacheLeft.play(mediaFileLeft);
    AudioCache audioCacheRight =
        new AudioCache(fixedPlayer: advancedPlayerRight);
    await audioCacheRight.play(mediaFileRight);
    advancedPlayerLeft.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
    advancedPlayerRight.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
    // if (mediaPlayerLeft != null && mediaPlayerRight != null) {
    // mediaPlayerLeft.start();
    // mediaPlayerRight.start();
  }

  @override
  int getMaxScore() {
    return 25;
  }

  @override
  String getStatus() {
    return score.toString() + "/" + getMaxScore().toString();
  }

  @override
  bool hasMoreExercises() {
    score++;
    if (score >= 25) {
      return false;
    }
    return true;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    onResetFun!();
    setUpTask();
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    rhymeGroups.clear();
    final db = await SqliteDB.instance.database;
    final results =
        await db.rawQuery('SELECT DISTINCT rhyme_group FROM rhymes');
    rhymeGroups.addAll(results.map((json) => Rhyme.fromJson(json)).toList());

    for (int i = 0; i < rhymeGroups.length; i++) {
      print("rhymeGroups data => ${rhymeGroups[i]}");
    }
    print("rhymeGroups List => ${rhymeGroups.length}");
    setUpTask();
  }

  Future<void> setUpTask() async {
    advancedPlayerLeft.release();
    advancedPlayerRight.release();

    Random rnd = new Random();
    //select a rhyme_group by random
    int rhymeGroup = rhymeGroups[rnd.nextInt(rhymeGroups.length)];

    //get all samples of this rhymegroup
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT samples.* FROM samples NATURAL JOIN rhymes WHERE rhyme_group = ?',
        [rhymeGroup]);
    List<Sample> rhymes = [];
    rhymes.addAll(results.map((json) => Sample.fromJson(json)).toList());

    Sample left = rhymes[rnd.nextInt(rhymes.length)];
    Sample right = left;
    while (right.getContent() == left.getContent()) {
      right = rhymes[(rnd.nextInt(rhymes.length))];
    }

    bool askLeft = rnd.nextBool();
    Sample correct;

    if (askLeft) {
      currentTask = Strings.left;
      correct = left;
    } else {
      currentTask = Strings.right;
      correct = right;
    }

    List<Answer> ans = [];
    int correctPos = rnd.nextInt(numOfAnswers);

    while (ans.length < numOfAnswers) {
      if (ans.length == correctPos) {
        ans.add(Answer.constructor2(correct.getContent(), true));
      } else {
        Answer? x = Answer.constructor2(
            rhymes[rnd.nextInt(rhymes.length)].getContent(), false);
        for (Answer a in ans) {
          if (x!.getText() == a.getText()) {
            x = null;
            break;
          }
        }
        if (x != null && x.getText() != correct.getContent()) {
          ans.add(x);
        }
      }
    }

    answers = ans;

    mediaFileLeft = left.getRes_id();
    mediaFileRight = right.getRes_id();

    advancedPlayerLeft.setVolume(rvolume: 0.0);
    advancedPlayerRight.setVolume(rvolume: 0.9);

    // mediaPlayerLeft.setVolume(1.0f, 0);
    // mediaPlayerRight.setVolume(0, 1.0f);
  }
}
