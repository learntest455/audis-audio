import 'dart:math';

import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/views/training/exercises/speech/speech_order4.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechOrder5 extends SpeechOrder4{

  SpeechOrder5({Function()? onReset}) {
    this.onResetFun = onReset;
    exerciseId = Level.SPEECH_ORDER_E;
  }

  @override
  void setUpTask() {
    answerVisible = false;
    onResetFun!();
    advancedPlayer.release();

    Random rnd = new Random();

    //we have to ensure that in one task no word is spoken by the man AND the women (distinct content)
    playSamplesList = [];
    while (playSamplesList.length < 5) {
      Sample? sample = list[rnd.nextInt(list.length)];
      for (int i = 0; i < playSamplesList.length; i++) {
        if (playSamplesList[i].getContent() == sample!.getContent()) {  //don't compare strings with '=='     !!!
          sample = null; //this word is already in the list... set null and dont add
          break;
        }
      }
      if (sample != null) {
        playSamplesList.add(sample);
        list.remove(sample);
      }
    }

    currentIndex = 0;
    mediaFile = playSamplesList[currentIndex].getRes_id();
    currentTask = playSamplesList[currentIndex].getContent();
    onResetFun!();
    advancedPlayer.onPlayerCompletion.listen((event) {
      play(playSamplesList);
    });

    answers = [];
    List<Answer> lAnswers = [];
    for (int k = 0; k < playSamplesList.length; k++) {
      lAnswers.add(Answer.constructor1(playSamplesList[k].getContent(), k));
    }

    while (lAnswers.length > 0) {
      answers.add(lAnswers.removeAt(rnd.nextInt(lAnswers.length)));
    }
  }

}