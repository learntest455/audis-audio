import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/views/training/exercises/speech/speech_order5.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechOrder7 extends SpeechOrder5{

  SpeechOrder7({Function()? onReset}) {
    this.onResetFun = onReset;
    exerciseId = Level.SPEECH_ORDER_G;
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results =
    await db.rawQuery('SELECT * FROM samples Where type = ? AND syllables = 1', [Type.WORD]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

}