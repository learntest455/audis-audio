import 'package:audis/views/training/exercises/speech/speech_gap_sentence1.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechGapSentence2 extends SpeechGapSentence1{

  SpeechGapSentence2({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    exerciseId = Level.SPEECH_SENTENCEUNDERSTANDING_B;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    answerVisible = false;
    onResetFun!();
    setUpTask();
  }
}