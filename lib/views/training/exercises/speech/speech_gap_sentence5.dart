import 'package:audis/views/training/exercises/speech/speech_gap_sentence4.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechGapSentence5 extends SpeechGapSentence4{

  SpeechGapSentence5({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    exerciseId = Level.SPEECH_SENTENCEUNDERSTANDING_E;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    answerVisible = false;
    onResetFun!();
    if (askingFirstGap) {
      setUpFirstGap();
    } else {
      setUpSecondGap();
    }
  }
}