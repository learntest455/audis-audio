import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/speaker.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding2.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechWordUnderstanding9 extends SpeechWordUnderstanding2 {
  SpeechWordUnderstanding9({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_WORDUNDERSTANDING_I;
    category = Category.SPEECH;
    name = Strings.multiSyllable + " (" + Strings.lessVolume + ")";
    taskText = Strings.wordunderstandingTaskADescr;
  }

  @override
  Future<void> playSound() async {
    AudioCache audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    advancedPlayer.setVolume(rvolume: 0.25);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list2.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT * FROM samples WHERE syllables > 1 and samples.type = ?',
        [Type.WORD]);
    list2.addAll(results.map((json) => Sample.fromJson(json)).toList());
    final results2 = await db.rawQuery(
        'SELECT * FROM samples WHERE syllables > 1 AND samples.speaker = ? And samples.type = ?',
        [Speaker.MALE, Type.WORD]);
    samplesDistinctContent2
        .addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list2.length}");
    setUpTask();
  }
}
