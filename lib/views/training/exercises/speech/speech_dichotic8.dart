import 'dart:core';

import 'package:audis/views/training/exercises/speech/speech_dichotic7.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechDichotic8 extends SpeechDichotic7 {

  SpeechDichotic8({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_DICHOTIC_H;
    numOfAnswers = 4;
  }
}
