
import 'package:audis/database/model/sample.dart';
import 'package:audis/views/training/exercises/speech/speech_memory2.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechMemory6 extends SpeechMemory2 {

  SpeechMemory6({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    exerciseId = Level.SPEECH_MEMORY_F;
  }

  @override
  void setUpTask() {
    super.setUpTask();
    currentTask="";
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    answerVisible = false;
    onResetFun!();
    setUpTask();
  }

  @override
  void play(List<Sample> playSamplesList) {
    if (!isPlayFinish) {
      isPlayFinish = true;
      currentIndex++;
      if (currentIndex < playSamplesList.length) {
        mediaFile = playSamplesList[currentIndex].getRes_id();
        playSound();
      }else{
        currentTask = playSamplesList[correctPos].getContent();
        print("currentTask=========== $currentTask");
        isEnableTile = true;
        answerVisible = true;
        onResetFun!();
      }
    }
  }

}