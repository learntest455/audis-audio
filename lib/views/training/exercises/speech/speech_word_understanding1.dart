import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample_theme.dart';
import 'package:audis/database/model/speaker.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class SpeechWordUnderstanding1 extends Level{

  List<SampleTheme> list = [];
  List<SampleTheme> samplesDistinctContent = [];
  AudioPlayer advancedPlayer = new AudioPlayer();
  void Function()? onResetFun;
  String mediaFile = "";

  SpeechWordUnderstanding1({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_WORDUNDERSTANDING_A;
    taskGroup = TaskGroup.SPEECH_WORDUNDERSTANDING;
    category = Category.SPEECH;
    name = Strings.wordunderstandingTaskAName;
    taskText = Strings.wordunderstandingTaskADescr;
  }

  @override
  int getMaxScore() {
    return 25;
  }

  @override
  String getStatus() {
    return score.toString() +"/" + getMaxScore().toString();
  }

  @override
  bool hasMoreExercises() {
    score++;
    if(score >= getMaxScore()){ return false;}
    return true;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    onResetFun!();
    setUpTask();
  }

  @override
  Future<void> playSound() async {
    AudioCache audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
  }

  Future<void> setUpTask() async {
    advancedPlayer.release();
    Random rnd = new Random();

   //choosing one sample to play (and remove from list: it is not played twice in a exercise)
   int correctIdx = rnd.nextInt(list.length);
   SampleTheme correctSample = list[correctIdx];
   list.remove(correctIdx);
   mediaFile = correctSample.getRes_id();

    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT * FROM samples natural inner join samplethemes natural inner join theme '
        'WHERE theme_id = ? AND samples.speaker = ?', [correctSample.getThemeId(), Speaker.MALE]);
    samplesDistinctContent.addAll(results.map((json) => SampleTheme.fromJson(json)).toList());

    Set<String> answerContents = new Set(); //set cant hold equal objects... => not showing one answer multiple times
    int correctPos = rnd.nextInt(4); //the position of the correct answer

  // //this is all for not showing one answer multiple times
    while (answerContents.length < 4) {
      if (answerContents.length == correctPos) {
        answerContents.add(correctSample.getContent());
      } else {
        String ans = samplesDistinctContent[rnd.nextInt(samplesDistinctContent.length)].getContent();
        if (ans != correctSample.getContent())
          answerContents.add(ans);
      }
    }

    currentTask = correctSample.getTheme()!;
    answers = [];
    for (String ans in answerContents) {
      answers.add(Answer.constructor2(ans, ans == correctSample.getContent()));
    }
}

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT * FROM samples natural inner join samplethemes natural inner join theme');
    list.addAll(results.map((json) => SampleTheme.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

}