import 'dart:core';
import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class SpeechDichotic1 extends Level {
  List<Sample> list = [];
  int currentIndex = 0;
  List<Sample> playSamplesList = [];
  AudioPlayer advancedPlayerLeft = new AudioPlayer();
  AudioPlayer advancedPlayerRight = new AudioPlayer();
  Random rnd = new Random();
  String mediaFileLeft = "";
  String mediaFileRight = "";
  // MediaPlayer mediaPlayerLeft;
  // MediaPlayer mediaPlayerRight;
  void Function()? onResetFun;

  SpeechDichotic1({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_DICHOTIC_A;
    taskGroup = TaskGroup.SPEECH_DICHOTIC;
    category = Category.SPEECH;
    name = Strings.dichotic;
    taskText = Strings.dichoticTaskDesc;
  }

  @override
  Future<void> playSound() async {
    AudioCache audioCacheLeft = AudioCache(fixedPlayer: advancedPlayerLeft);
    await audioCacheLeft.play(mediaFileLeft);
    Future.delayed(const Duration(milliseconds: 200)).then((value) async {
      AudioCache audioCacheRight = AudioCache(fixedPlayer: advancedPlayerRight);
      await audioCacheRight.play(mediaFileRight);
    });
    advancedPlayerLeft.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
    advancedPlayerRight.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
    // if (mediaPlayerLeft != null && mediaPlayerRight != null) {
    // mediaPlayerLeft.start();
    // mediaPlayerRight.start();

    advancedPlayerLeft.setVolume(lvolume: 0.9, rvolume: 0.0);
    advancedPlayerRight.setVolume(lvolume: 0.0, rvolume: 0.9);
  }

  @override
  int getMaxScore() {
    return 25;
  }

  @override
  String getStatus() {
    return score.toString() + "/" + getMaxScore().toString();
  }

  @override
  bool hasMoreExercises() {
    score++;
    if (score >= 25) {
      return false;
    }
    return true;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    onResetFun!();
    setUpTask();
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT * FROM samples Where type = ? AND syllables > 1', [Type.WORD]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

  void setUpTask() {
    advancedPlayer.release();
    // if (mediaPlayerLeft != null) {
    // mediaPlayerLeft.release();
    // }
    // if (mediaPlayerRight != null) {
    // mediaPlayerRight.release();
    // }

    rnd = new Random();
    // //we have to ensure that in one task no word is spoken by the man AND the women (distinct content)
    playSamplesList = [];
    while (playSamplesList.length < 2) {
      Sample? sample = list[rnd.nextInt(list.length)];
      for (int i = 0; i < playSamplesList.length; i++) {
        if (playSamplesList[i].getContent() == sample!.getContent()) {
          sample =
              null; //this word is already in the list... set null and dont add
          break;
        }
      }
      if (sample != null) {
        playSamplesList.add(sample);
        list.remove(sample);
      }
    }
    currentIndex = 0;
    mediaFileLeft = playSamplesList[0].getRes_id();
    mediaFileRight = playSamplesList[1].getRes_id();
    // advancedPlayerLeft.setVolume(0.9, 0.0);
    // advancedPlayerRight.setVolume(0.0, 0.9);

    // mediaPlayerLeft.setVolume(1.0f, 0);
    // mediaPlayerRight.setVolume(0, 1.0f);
    //
    setAnswers();
  }

  void setAnswers() {
    bool leftAsked = new Random().nextBool();
    if (leftAsked) {
      currentTask = Strings.left;
    } else {
      currentTask = Strings.right;
    }
    answers = [];
    List<Answer> lAnswers = [];
    for (int k = 0; k < playSamplesList.length; k++) {
      if (leftAsked) {
        lAnswers
            .add(Answer.constructor2(playSamplesList[k].getContent(), k == 0));
      } else {
        lAnswers
            .add(Answer.constructor2(playSamplesList[k].getContent(), k == 1));
      }
    }
    int i = 0;
    while (lAnswers.length > 0) {
      answers.add(lAnswers.removeAt(rnd.nextInt(lAnswers.length)));
    }
  }
}
