import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/answer_model.dart';
import 'package:audis/database/model/gapsentece_model.dart';
import 'package:audis/database/model/speed.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';
import 'package:sprintf/sprintf.dart';

class SpeechGapSentence4 extends Level{

  List<GapSentenceModel> list = [];
  String gap = "____";
  bool askingFirstGap = false;
  GapSentenceModel? currentGapSentence;
  String? gap1, gap2;
  void Function()? onResetFun;
  AudioPlayer advancedPlayer = new AudioPlayer();

  String mediaFile = "";

  SpeechGapSentence4({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_SENTENCEUNDERSTANDING_D;
    taskGroup = TaskGroup.SPEECH_SENTENCEUNDERSTANDING;
    category = Category.SPEECH;
    name = Strings.sentenceUnderstandingTaskAName;
    taskText = Strings.sentenceUnderstandingTaskADesc;
  }

  @override
  int getMaxScore() {
    return 25;
  }

  @override
  String getStatus() {
    return score.toString() + "/" + getMaxScore().toString();
  }

  @override
  bool hasMoreExercises() {
    if (askingFirstGap) {
      askingFirstGap = false; //do second gap next...
      return true;
    } else {
      score++;
      askingFirstGap = true;
      if (score >= 25) {
        return false;
      }
      return true;
    }
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    onResetFun!();
    if (askingFirstGap) {
      setUpFirstGap();
    } else {
      setUpSecondGap();

    }
  }

  @override
  Future<void> playSound() async {
    if(mediaFile != ''){
      AudioCache audioCache = new AudioCache(fixedPlayer: advancedPlayer);
      await audioCache.play(mediaFile);
      advancedPlayer.onPlayerCompletion.listen((event) {
        isEnableTile = true;
        answerVisible = true;
        onResetFun!();
      });
    }
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT * FROM samples natural inner join samplesentence natural inner join gapsentence WHERE TYPE = ? AND speed = ?', [Type.SENTENCE, Speed.SLOW]);
    list.addAll(results.map((json) => GapSentenceModel.fromJson(json)).toList());
    print("List => ${list.length}");
    askingFirstGap = true;
    setUpFirstGap();
  }

  Future<void> setUpFirstGap() async {
    advancedPlayer.release();

    taskText = Strings.sentenceUnderstandingTaskADesc;

    Random rnd = new Random();

    //choosing one sample to play (and remove from list: it is not played twice in a exercise)
    int correctIdx = rnd.nextInt(list.length);
    currentGapSentence = list.removeAt(correctIdx);

    String correctSentence = currentGapSentence!.getContent();
    String? gapSentence = currentGapSentence!.getGapsentece();

    RegExp regexp = RegExp(gapSentence!.replaceAll("%s", "(.+)"));
    final match = regexp.firstMatch(correctSentence);
    gap1 = match!.group(1).toString();
    gap2 = match.group(2).toString();

    currentTask = sprintf(gapSentence, [gap, gap]);

    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT answer FROM answer WHERE answer_id = ?', [currentGapSentence!.getAnswer_id1()]);
    List<String> strAnswers = [];
    strAnswers.addAll(results.map((json) => AnswerModel.fromJson(json)).toList());

    int correctPos = rnd.nextInt(4);
    Set<String> answerContents = new Set();
    
    while (answerContents.length < 4) {
      if (answerContents.length == correctPos) {
        answerContents.add(gap1!);
      } else {
        String ans = strAnswers[rnd.nextInt(strAnswers.length)];
        if (ans != gap1)
          answerContents.add(ans);
      }
    }

    answers = [];
    for (String ans in answerContents)
    {
      answers.add(Answer.constructor2(ans, ans == gap1));
    }
    mediaFile = currentGapSentence!.getRes_id();
    taskText = Strings.askingForFirst;

    // mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
    //     {
    //     @Override
    //     public void onCompletion(MediaPlayer mp) {
    //     mListener.OnTaskFinishedPlay();
    //     taskText = ((Context)mListener).getString(R.string.askingForFirst);
    //     mListener.OnTaskTextUpdated();
    //     }
    //     });
  }

  Future<void> setUpSecondGap() async {
    mediaFile = '';
    advancedPlayer.release();
    Random rnd = new Random();
    currentTask = sprintf(currentGapSentence!.getGapsentece()!, ["<u>$gap1</u>", gap]);
    taskText = Strings.askingForSecond;

    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT answer FROM answer WHERE answer_id = ?', [currentGapSentence!.getAnswer_id2()]);
    List<String> strAnswers = [];
    strAnswers.addAll(results.map((json) => AnswerModel.fromJson(json)).toList());

    int correctPos = rnd.nextInt(4);
    Set<String> answerContents = new Set();
    while (answerContents.length < 4) {
      if (answerContents.length == correctPos) {
        answerContents.add(gap2!);
      } else {
        String ans = strAnswers[rnd.nextInt(strAnswers.length)];
        if (ans != gap2)
          answerContents.add(ans);
      }
    }

    answers = [];
    for (String ans in answerContents)
    {
      answers.add(Answer.constructor2(ans, ans == gap2));
    }

    isEnableTile = true;
    answerVisible = true;
    onResetFun!();
  }
}