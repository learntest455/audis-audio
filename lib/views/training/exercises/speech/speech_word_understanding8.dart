
import 'package:audioplayers/audioplayers.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding1.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';

class SpeechWordUnderstanding8 extends SpeechWordUnderstanding1{

  SpeechWordUnderstanding8({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.SPEECH_WORDUNDERSTANDING_H;
    category = Category.SPEECH;
    name = Strings.wordunderstandingTaskAName + " (" + Strings.lessVolume + ")";
    taskText = Strings.wordunderstandingTaskADescr;
  }

  @override
  Future<void> playSound() async {
    AudioCache audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    advancedPlayer.setVolume(rvolume: 0.25);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
  }
}