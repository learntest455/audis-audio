import 'dart:math';

import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/answer_model.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence5.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:sprintf/sprintf.dart';

class SpeechGapSentence6 extends SpeechGapSentence5{

  SpeechGapSentence6({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    exerciseId = Level.SPEECH_SENTENCEUNDERSTANDING_F;
  }

  @override
  Future<void> setUpFirstGap() async {
    advancedPlayer.release();
    taskText = Strings.sentenceUnderstandingTaskADesc;

    Random rnd = new Random();

    //choosing one sample to play (and remove from list: it is not played twice in a exercise)
    int correctIdx = rnd.nextInt(list.length);
    currentGapSentence = list.removeAt(correctIdx);

    String correctSentence = currentGapSentence!.getContent();
    String? gapSentence = currentGapSentence!.getGapsentece();

    RegExp regexp = RegExp(gapSentence!.replaceAll("%s", "(.+)"));
    final match = regexp.firstMatch(correctSentence);
    gap1 = match!.group(1).toString();
    gap2 = match.group(2).toString();


    currentTask = sprintf(gapSentence,[gap,gap]);
    List<String> strAnswers = [];

    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT answer FROM answer WHERE answer_id = ?', [currentGapSentence!.getAnswer_id1()]);
    strAnswers.addAll(results.map((json) => AnswerModel.fromJson(json)).toList());

    int correctPos = rnd.nextInt(4);
    Set<String> answerContents = new Set();
    while (answerContents.length < 4) {
      if (answerContents.length == correctPos) {
        answerContents.add(gap1!);
      } else {
        String ans = strAnswers[rnd.nextInt(strAnswers.length)];
        if (ans != gap1)
          answerContents.add(ans);
      }
    }

    answers = [];
    for (String ans in answerContents)
    {
      answers.add(Answer.constructor2(ans, ans == gap1));
    }
    mediaFile = currentGapSentence!.getRes_id();
    taskText = Strings.askingForFirst;

    // mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
    //     {
    //     @Override
    //     public void onCompletion(MediaPlayer mp) {
    //     currentTask = currentTaskText;
    //     taskText = ((Context)mListener).getString(R.string.askingForFirst);
    //     mListener.OnTaskTextUpdated();
    //     mListener.OnTaskFinishedPlay();
    //     mListener.OnCurrentTaskUpdated();
    //     }
    //     });
  }
}