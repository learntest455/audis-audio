import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A0.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class VowelsInRealTaskC4 extends VowelsInNonsenseTaskA0 {
  AudioPlayer advancedPlayer = new AudioPlayer();
  late AudioCache audioCache;
  String mediaFile = "";

  VowelsInRealTaskC4({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_VOWELS_REAL_C4;
    taskGroup = TaskGroup.LUTE_VOWELS_REAL;
    name = Strings.vowelsInRealwords;
    taskText = Strings.vowelsInNonsense1Desc;
  }

  @override
  Future<void> playSound() async {
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
  }

  @override
  Future<void> setUpTask() async {
    advancedPlayer.release();

    Random rnd = new Random();
    Sample correctSample = list[rnd.nextInt(list.length)];
    list.remove(correctSample);

    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT minimalpair FROM vowels WHERE vowels.res_id = ?',
        [correctSample.getRes_id()]);
    print("results=> $results");

    String miniPair =
        results.map((json) => json['minimalpair']).first.toString();
    print("miniPair=> $miniPair");

    mediaFile = correctSample.getRes_id();

    answers = [];

    if (rnd.nextBool()) {
      answers.insert(0, Answer.constructor2(correctSample.getContent(), true));
      answers.insert(1, Answer.constructor2(miniPair, false));
    } else {
      answers.insert(0, Answer.constructor2(miniPair, false));
      answers.insert(1, Answer.constructor2(correctSample.getContent(), true));
    }

  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT samples.* FROM samples JOIN vowels ON samples.res_id = vowels.res_id WHERE samples.syllables = 1 AND samples.type = ? AND vowels.minimalpair IS NOT NULL AND vowels.minimalpair != ""',
        [Type.WORD]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }
}
