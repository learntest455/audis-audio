
import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample_consonant.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

import 'consonants_In_real_task_0.dart';

class ConsonantsInReal4 extends ConsonantsInReal0{

  AudioPlayer advancedPlayer = new AudioPlayer();
  late AudioCache audioCache;
  String mediaFile = "";

  ConsonantsInReal4({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_CONSONANTS_REAL_4;
    taskGroup = TaskGroup.LUTE_CONSONANTS_REAL;
    category = Category.LUTE;
    name = Strings.consonantsInReal;
    taskText = Strings.startMiniPairDesc;
  }

  @override
  void setUpTask() {
    advancedPlayer.release();

    Random rnd = new Random();
    SampleConsonant sample = list[rnd.nextInt(list.length)];
    list.remove(sample);

    answers = [];
    if (rnd.nextBool()) {
      answers.add(Answer.constructor2(sample.getContent(), true));
      answers.add(Answer.constructor2(sample.getMinPair(), false));
    } else {
      answers.add(Answer.constructor2(sample.getMinPair(), false));
      answers.add(Answer.constructor2(sample.getContent(), true));
    }
    mediaFile = sample.getRes_id();

    // mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
    // @Override
    // public void onCompletion(MediaPlayer mp) {
    // mListener.OnTaskFinishedPlay();
    // }
    // });
    //
  }

  @override
  Future<void> resetTask() async{
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT samples.*, consonants.* FROM samples NATURAL JOIN consonants WHERE startEnd = ? AND minPair != ?', [0,'']);
    list.addAll(results.map((json) => SampleConsonant.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }
}