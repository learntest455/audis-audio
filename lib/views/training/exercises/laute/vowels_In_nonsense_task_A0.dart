import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class VowelsInNonsenseTaskA0 extends Level{
  List<Sample> list = [];
  AudioPlayer advancedPlayer = new AudioPlayer();
  late AudioCache audioCache;
  String mediaFile = "";
  void Function()? onResetFun;

  VowelsInNonsenseTaskA0({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_VOWELS_A0;
    taskGroup = TaskGroup.LUTE_VOWELS_NONSENSE;
    category = Category.LUTE;
    name = Strings.vowelsInNonsense0Name;
    taskText = Strings.vowelsInNonsense0Desc;
  }

  @override
  int getMaxScore() {
    return 22;
  }

  @override
  String getStatus() {
    return "" + score.toString() + " / " + getMaxScore().toString();
  }

  @override
  bool hasMoreExercises() {
    score++;
    if (score >= getMaxScore()) return false;
    return true;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    onResetFun!();
    setUpTask();
  }

  @override
  Future<void> playSound() async {
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
  }

  void setUpTask()  {
    advancedPlayer.release();

    int rnd = new Random().nextInt(list.length);
    mediaFile = list[rnd].getRes_id();
    answers = [Answer.constructor2(list[rnd].getContent(), true)];
    print("answers => ${answers.length}");
    list.remove(rnd);
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT * FROM samples JOIN vowels ON samples.res_id = vowels.res_id WHERE samples.type = ?',[Type.NONSENSE]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

}