import 'package:audioplayers/audioplayers.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_real_task_C4.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class VowelsInRealTaskC5 extends VowelsInRealTaskC4 {

  VowelsInRealTaskC5({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    isEnableTile = true;
    exerciseId = Level.LUTE_VOWELS_REAL_C5;
    taskGroup = TaskGroup.LUTE_VOWELS_REAL;
  }

  @override
  Future<void> playSound() async {
    answerVisible = false;
    isEnableTile = true;
    onResetFun!();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      answerVisible = true;
      onResetFun!();
    });
  }
}