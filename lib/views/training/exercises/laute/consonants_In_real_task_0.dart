import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample_consonant.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class ConsonantsInReal0 extends Level{
  List<SampleConsonant> list = [];
  List<String> consonants = [];

  AudioPlayer advancedPlayer = new AudioPlayer();
  late AudioCache audioCache;
  String mediaFile = "";
  void Function()? onResetFun;

  ConsonantsInReal0({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_CONSONANTS_REAL_0;
    taskGroup = TaskGroup.LUTE_CONSONANTS_REAL;
    category = Category.LUTE;
    name = Strings.consonantsInReal;
    taskText = Strings.startConsDescr;
  }

  @override
  int getMaxScore() {
    return 25;
  }

  @override
  String getStatus() {
    return "" + score.toString() + " / " + getMaxScore().toString();
  }

  @override
  bool hasMoreExercises() {
    score++;
    if (score >= getMaxScore()) return false;
    return true;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    onResetFun!();
    setUpTask();
  }

  @override
  Future<void> playSound() async {
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
  }

  void setUpTask()  {
    advancedPlayer.release();
    Random rnd = new Random();

    SampleConsonant sample = list[rnd.nextInt(list.length)];
    list.remove(sample);

    Set<String> answerContents = new Set(); //set cant hold equal objects... => not showing one answer multiple times
    int correctPos = rnd.nextInt(4); //the position of the correct answer

    //this is all for not showing one answer multiple times
    while (answerContents.length < 4) {
      if (answerContents.length == correctPos) {
        answerContents.add(sample.getConsonant());
      } else {
        String ans = consonants[rnd.nextInt(consonants.length)];
        if (ans != sample.getConsonant())
          answerContents.add(ans);
      }
    }

    answers = [];
    for (String ans in answerContents) {
      answers.add(Answer.constructor2(ans, ans == sample.getConsonant()));
    }
    mediaFile = sample.getRes_id();

    // mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
    // @Override
    // public void onCompletion(MediaPlayer mp) {
    // mListener.OnTaskFinishedPlay();
    // }
    // });
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    consonants.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT samples.*, consonants.* FROM samples NATURAL JOIN consonants WHERE startEnd = 0');
    list.addAll(results.map((json) => SampleConsonant.fromJson(json)).toList());
    final results2 = await db.rawQuery('SELECT DISTINCT consonant FROM consonants');
    consonants.addAll(results2.map((json) => SampleConsonant.fromJson2(json)).toList());
    print("List => ${list.length}");
    print("List => ${consonants.length}");
    setUpTask();

  }

}