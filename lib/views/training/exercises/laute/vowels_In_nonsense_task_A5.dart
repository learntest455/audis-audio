
import 'package:audioplayers/audioplayers.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A4.dart';
import 'package:audis/views/training/model/level.dart';

class VowelsInNonsenseTaskA5 extends VowelsInNonsenseTaskA4{

  VowelsInNonsenseTaskA5({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    isEnableTile = true;
    exerciseId =  Level.LUTE_VOWELS_A5;
    name = Strings.vowelsInNonsense0Name;
  }

  @override
  Future<void> playSound() async {
    answerVisible = false;
    isEnableTile = true;
    onResetFun!();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      answerVisible = true;
      onResetFun!();
    });
  }
}