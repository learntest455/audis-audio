import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/speaker.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A1.dart';
import 'package:audis/views/training/model/level.dart';

class VowelsInNonsenseTaskA4 extends VowelsInNonsenseTaskA1 {

  VowelsInNonsenseTaskA4({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_VOWELS_A4;
    name = Strings.vowelsInNonsense4Name;
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT samples.* FROM samples JOIN vowels ON samples.res_id = vowels.res_id WHERE samples.type = ?',
        [Type.NONSENSE]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());

    final results2 = await db.rawQuery(
        'SELECT samples.* FROM samples JOIN vowels ON samples.res_id = vowels.res_id WHERE samples.type = ?  AND samples.speaker = ?',
        [Type.NONSENSE, Speaker.FEMALE]);
    samplesDistinctContent.addAll(
        results2.map((json) => Sample.fromJson(json)).toList());

    print("List => ${list.length}");
    print("samplesDistinctContent => ${samplesDistinctContent.length}");
    setUpTask();
  }
}