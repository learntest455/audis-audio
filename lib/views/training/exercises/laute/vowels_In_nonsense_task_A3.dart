
import 'dart:math';
import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class VowelsInNonsenseTaskA3 extends Level{
  List<Sample> list = [];
  AudioPlayer advancedPlayer = new AudioPlayer();
  late AudioCache audioCache;
  String mediaFile = "";
  void Function()? onResetFun;

  VowelsInNonsenseTaskA3({Function()? onReset}) {
    // updateContext(context);
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_VOWELS_A3;
    taskGroup = TaskGroup.LUTE_VOWELS_NONSENSE;
    category = Category.LUTE;
    name = Strings.vowelsInNonsense4Name;
    taskText = Strings.vowelsInNonsenseA3Desc;
  }

  @override
  int getMaxScore() {
    return 22;
  }

  @override
  String getStatus() {
    return "" + score.toString() + " / " + getMaxScore().toString();
  }

  @override
  bool hasMoreExercises() {
    score++;
    if (score >= getMaxScore()) return false;
    return true;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    onResetFun!();
    setUpTask();
  }

  @override
  Future<void> playSound() async {
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
  }


  Future<void> setUpTask() async {
    advancedPlayer.release();

    int rnd = new Random().nextInt(list.length);

    Sample sample = list[rnd];
    list.remove(rnd);
    mediaFile = sample.getRes_id();

    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT length FROM vowels WHERE res_id = ?', [sample.getRes_id()]);
    int length = int.parse(results.map((json) => json['length']).toString().substring(1, 2));

    answers = [];
    answers.add(Answer.constructor2(Strings.short_, length == 0));
    answers.add(Answer.constructor2(Strings.long_, length == 1));
  }


  @override
  Future<void> resetTask() async{
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT samples.* FROM samples JOIN vowels ON samples.res_id = vowels.res_id WHERE samples.type = ?',
        [Type.NONSENSE]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

}