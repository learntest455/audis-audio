import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class ConsonantsInNonsenseB0 extends Level{
  List<Sample> list = [];
  AudioPlayer advancedPlayer = new AudioPlayer();
  late AudioCache audioCache;
  String mediaFile = "";
  void Function()? onResetFun;

  ConsonantsInNonsenseB0({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_CONSONANTS_B0;
    category = Category.LUTE;
    taskGroup = TaskGroup.LUTE_CONSONANTS_NONSENSE;
    name = Strings.consonantsInNonsense;
    taskText = Strings.vowelsInNonsense0Desc;
  }

  @override
  int getMaxScore() {
    return 25;
  }

  @override
  String getStatus() {
    return "" + score.toString() + " / " + getMaxScore().toString();
  }

  @override
  bool hasMoreExercises() {
    score++;
    if (score >= getMaxScore()) return false;
    return true;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    onResetFun!();
    setUpTask();
  }

  @override
  Future<void> playSound() async {
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });

  }

  void setUpTask()  {
    advancedPlayer.release();

    int rnd = new Random().nextInt(list.length);
    Sample sample = list[rnd];
    list.remove(rnd);
    mediaFile = sample.getRes_id();
    answers = [Answer.constructor2(sample.getContent(), true)];
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT samples.* FROM samples JOIN consonant_similarity ON samples.res_id = consonant_similarity.res_id');
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

}