
import 'package:audis/database/model/sample_consonant.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

import 'consonants_In_real_task_0.dart';

class ConsonantsInReal2 extends ConsonantsInReal0{

  ConsonantsInReal2({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_CONSONANTS_REAL_2;
    taskGroup = TaskGroup.LUTE_CONSONANTS_REAL;
    category = Category.LUTE;
    name = Strings.consonantsInReal;
    taskText = Strings.endConsDescr;
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    consonants.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT samples.*, consonants.* FROM samples NATURAL JOIN consonants WHERE startEnd = 1');
    list.addAll(results.map((json) => SampleConsonant.fromJson(json)).toList());
    final results2 = await db.rawQuery('SELECT DISTINCT consonant FROM consonants');
    consonants.addAll(results2.map((json) => SampleConsonant.fromJson2(json)).toList());
    print("List => ${list.length}");
    print("List => ${consonants.length}");
    setUpTask();
  }

}