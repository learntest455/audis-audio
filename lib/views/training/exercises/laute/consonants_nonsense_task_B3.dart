import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';

import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A1.dart';
import 'package:audis/views/training/model/level.dart';

class ConsonantsInNonsenseB3 extends VowelsInNonsenseTaskA1{
  List<Sample> list = [];
  AudioPlayer advancedPlayer = new AudioPlayer();
  late AudioCache audioCache;
  String mediaFile = "";

  ConsonantsInNonsenseB3({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_CONSONANTS_B3;
    name = Strings.consonantsInNonsense;
  }

  @override
  int getMaxScore() {
    return 25;
  }

  Future<void> setUpTask()  async {
    advancedPlayer.release();
    Random rnd = new Random();

    //choosing one sample to play (and remove from list: it is not played twice in a exercise)
    int correctIdx = rnd.nextInt(list.length);
    Sample correctSample = list[correctIdx];
    list.remove(correctIdx);

    final db = await SqliteDB.instance.database;
    final results2 = await db.rawQuery(
        'SELECT samples.* FROM samples JOIN consonant_similarity ON samples.res_id = consonant_similarity.res_id WHERE samples.speaker = 0 AND consonant_similarity.sim_group = (SELECT sim_group FROM consonant_similarity WHERE res_id = ?)',
        [correctSample.getRes_id()]);
    samplesDistinctContent
        .addAll(results2.map((json) => Sample.fromJson(json)).toList());

    Set<String> answerContents = new Set(); //set cant hold equal objects... => not showing one answer multiple times
    int correctPos = rnd.nextInt(4); //the position of the correct answer

    //this is all for not showing one answer multiple times
    while (answerContents.length < 4) {
      if (answerContents.length == correctPos) {
        answerContents.add(correctSample.getContent());
      } else {
        String ans = samplesDistinctContent[rnd.nextInt(samplesDistinctContent.length)].getContent();
        if (ans != correctSample.getContent()) answerContents.add(ans);
      }
    }

    answers = [];
    for (String ans in answerContents) {
      answers.add(Answer.constructor2(ans, ans == correctSample.getContent()));
    }
    mediaFile = correctSample.getRes_id();

    // mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
    // @Override
    // public void onCompletion(MediaPlayer mp) {
    // mListener.OnTaskFinishedPlay();
    // }
    // });
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT samples.* FROM samples JOIN consonant_similarity ON samples.res_id = consonant_similarity.res_id');
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

}