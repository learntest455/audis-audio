import 'package:audis/database/model/sample.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A3.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class VowelsInRealTaskC1 extends VowelsInNonsenseTaskA3 {

  VowelsInRealTaskC1({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_VOWELS_REAL_C1;
    taskGroup = TaskGroup.LUTE_VOWELS_REAL;
    category = Category.LUTE;
    name = Strings.vowelsInRealwordsShortLong;
    taskText = Strings.vowelsInNonsenseA3Desc;
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery(
        'SELECT samples.* FROM samples JOIN vowels ON samples.res_id = vowels.res_id WHERE samples.syllables = 1 AND samples.type = ?',
        [Type.WORD]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }
}
