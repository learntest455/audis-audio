
import 'package:audis/database/model/sample_consonant.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';
import 'consonants_In_real_task_4.dart';

class ConsonantsInReal6 extends ConsonantsInReal4{

  ConsonantsInReal6({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_CONSONANTS_REAL_6;
    taskGroup = TaskGroup.LUTE_CONSONANTS_REAL;
    category = Category.LUTE;
    name = Strings.consonantsInReal;
    taskText = Strings.endMiniPairDesc;
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT samples.*, consonants.* FROM samples NATURAL JOIN consonants WHERE startEnd = ? AND minPair != ? ', [1,'']);
    list.addAll(results.map((json) => SampleConsonant.fromJson(json)).toList());
    print("List => ${list.length}");
    setUpTask();
  }

}