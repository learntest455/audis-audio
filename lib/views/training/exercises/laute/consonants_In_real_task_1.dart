
import 'package:audioplayers/audioplayers.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

import 'consonants_In_real_task_0.dart';

class ConsonantsInReal1 extends ConsonantsInReal0{

  ConsonantsInReal1({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    isEnableTile = true;
    exerciseId = Level.LUTE_CONSONANTS_REAL_1;
    taskGroup = TaskGroup.LUTE_CONSONANTS_REAL;
    category = Category.LUTE;
    name = Strings.consonantsInReal;
    taskText = Strings.startConsDescr;
  }

  @override
  Future<void> playSound() async {
    answerVisible = false;
    isEnableTile = true;
    onResetFun!();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      answerVisible = true;
      onResetFun!();
    });
  }

}