
import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/speaker.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A1.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class ConsonantsInNonsenseB1 extends VowelsInNonsenseTaskA1{
  AudioPlayer advancedPlayer = new AudioPlayer();
  late AudioCache audioCache;
  String mediaFile = "";

  ConsonantsInNonsenseB1({Function()? onReset}) {
    this.onResetFun = onReset;
    exerciseId = Level.LUTE_CONSONANTS_B1;
    taskGroup = TaskGroup.LUTE_CONSONANTS_NONSENSE;
    name = Strings.consonantsInNonsense;
    taskText = Strings.vowelsInNonsense1Desc;
    //TODO: these two lines can be deletetd when >>VowelsInNonsenseTaskA1<< is modified to run sql in resetTask()-method
    list = [];
    samplesDistinctContent = [];
  }

  @override
  int getMaxScore() {
    return 25;
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    final results = await db.rawQuery('SELECT samples.* FROM samples JOIN consonant_similarity ON samples.res_id = consonant_similarity.res_id');
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    final results2 = await db.rawQuery(
        'SELECT samples.* FROM samples JOIN consonant_similarity ON samples.res_id = consonant_similarity.res_id WHERE samples.speaker = ?',
        [Speaker.FEMALE]);
    samplesDistinctContent.addAll(results2.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    print("samplesDistinctContent => ${samplesDistinctContent.length}");
    setUpTask();

  }

}