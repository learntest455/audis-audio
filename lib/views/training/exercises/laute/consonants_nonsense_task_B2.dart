
import 'package:audioplayers/audioplayers.dart';
import 'package:audis/views/training/model/level.dart';

import 'consonants_nonsense_task_B1.dart';

class ConsonantsInNonsenseB2 extends ConsonantsInNonsenseB1{

  ConsonantsInNonsenseB2({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    isEnableTile = true;
    exerciseId = Level.LUTE_CONSONANTS_B2;

  }

  @override
  Future<void> playSound() async {
    answerVisible = false;
    isEnableTile = true;
    onResetFun!();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      answerVisible = true;
      onResetFun!();
    });
  }

  // @Override
  // public boolean getHideAnswersWhilePlay() {
  //   return true;
  // }
}