import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/model/sample.dart';
import 'package:audis/database/model/speaker.dart';
import 'package:audis/database/model/type.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class VowelsInNonsenseTaskA1 extends Level {
  List<Sample> list = [];
  List<Sample> samplesDistinctContent = [];
  AudioPlayer advancedPlayer = new AudioPlayer();
  late AudioCache audioCache;
  String mediaFile = "";
  void Function()? onResetFun;

  VowelsInNonsenseTaskA1({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = true;
    exerciseId = Level.LUTE_VOWELS_A1;
    taskGroup = TaskGroup.LUTE_VOWELS_NONSENSE;
    category = Category.LUTE;
    name = Strings.vowelsInNonsense0Name;
    taskText = Strings.vowelsInNonsense1Desc;
  }

  @override
  int getMaxScore() {
    return 22;
  }

  @override
  String getStatus() {
    return "" + score.toString() + " / " + getMaxScore().toString();
  }

  @override
  bool hasMoreExercises() {
    score++;
    if (score >= getMaxScore()) return false;
    return true;
  }

  @override
  void nextExercise() {
    isEnableTile = false;
    onResetFun!();
    setUpTask();
  }

  @override
  Future<void> playSound() async {
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      isEnableTile = true;
      onResetFun!();
    });
  }

  void setUpTask() {
    advancedPlayer.release();
    Random rnd = new Random();

    //choosing one sample to play (and remove from list: it is not played twice in a exercise)
    int correctIdx = rnd.nextInt(list.length);
    Sample correctSample = list[correctIdx];
    list.remove(correctIdx);

    // HashSet<String> answerContents = new HashSet<>(4); //set cant hold equal objects... => not showing one answer multiple times
    Set<String> answerContents =
        new Set(); //set cant hold equal objects... => not showing one answer multiple times
    int correctPos = rnd.nextInt(4); //the position of the correct answer

    //this is all for not showing one answer multiple times
    while (answerContents.length < 4) {
      if (answerContents.length == correctPos) {
        answerContents.add(correctSample.getContent());
      } else {
        String ans =
            samplesDistinctContent[rnd.nextInt(samplesDistinctContent.length)]
                .getContent();
        if (ans != correctSample.getContent()) answerContents.add(ans);
      }
    }
    answers = [];
    for (String ans in answerContents) {
      answers.add(Answer.constructor2(ans, ans == correctSample.getContent()));
    }

    mediaFile = correctSample.getRes_id();
  }

  @override
  Future<void> resetTask() async {
    score = 0;
    list.clear();
    final db = await SqliteDB.instance.database;
    // final results = await db.rawQuery('SELECT * FROM samples JOIN vowels ON samples.res_id = vowels.res_id WHERE samples.type = ?',[Type.NONSENSE]);
    final results = await db.rawQuery(
        'SELECT samples.* FROM samples JOIN vowels ON samples.res_id = vowels.res_id WHERE samples.type = ? AND vowels.length = ?',
        [Type.NONSENSE, 1]);
    list.addAll(results.map((json) => Sample.fromJson(json)).toList());
    final results2 = await db.rawQuery(
        'SELECT samples.* FROM samples JOIN vowels ON samples.res_id = vowels.res_id WHERE samples.type = ? AND vowels.length = ? AND samples.speaker = ?',
        [Type.NONSENSE, 1, Speaker.FEMALE]);
    samplesDistinctContent
        .addAll(results2.map((json) => Sample.fromJson(json)).toList());
    print("List => ${list.length}");
    print("samplesDistinctContent => ${samplesDistinctContent.length}");
    setUpTask();
  }
}
