
import 'package:audioplayers/audioplayers.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A1.dart';
import 'package:audis/views/training/model/level.dart';

class VowelsInNonsenseTaskA2 extends VowelsInNonsenseTaskA1{

  VowelsInNonsenseTaskA2({Function()? onReset}) {
    this.onResetFun = onReset;
    answerVisible = false;
    isEnableTile = true;
    exerciseId =  Level.LUTE_VOWELS_A2;
  }

  @override
  Future<void> playSound() async {
    answerVisible = false;
    isEnableTile = true;
    onResetFun!();
    audioCache = new AudioCache(fixedPlayer: advancedPlayer);
    await audioCache.play(mediaFile);
    advancedPlayer.onPlayerCompletion.listen((event) {
      answerVisible = true;
      onResetFun!();
    });
  }
}