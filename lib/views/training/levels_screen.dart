import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercise_screen.dart';
import 'package:audis/views/training/model/level.dart';
import 'package:audis/views/training/model/task_group_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:http/http.dart';

class LevelsScreen extends StatefulWidget {
  final int groupID;

  LevelsScreen({Key? key, required this.groupID}) : super(key: key);

  @override
  _LevelsScreenState createState() => _LevelsScreenState();
}

class _LevelsScreenState extends State<LevelsScreen> {
  double presentScore = 5;
  double _userRating = 0.0;
  bool isFirstLoad = true;

  @override
  Widget build(BuildContext context) {
    List<Level> tasks = TaskGroup(widget.groupID).getLevels();

     if(isFirstLoad) {
      Future.delayed(Duration(milliseconds: 2000)).then((value) {
        setState(() {});
        isFirstLoad = false;
      });
    }
    return Scaffold(
      body: Center(
        child: Container(
          height: 400,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: tasks.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ExerciseScreen(
                              exerciseId: tasks[index].exerciseId,
                            ),
                          ));
                    },
                    child: Container(
                      height: 50,
                      color: Colors.yellow[800],
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                Strings.exercise + " " + (index + 1).toString(),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                              )),
                          RatingBar(
                            ignoreGestures: true,
                            itemSize: 30,
                            initialRating: rating(tasks[index].getExerciseId(),
                                tasks[index].getMaxScore()),
                            direction: Axis.horizontal,
                            allowHalfRating: false,
                            itemCount: 3,
                            ratingWidget: RatingWidget(
                              full: Icon(Icons.star, color: Colors.red),
                              half: Icon(Icons.star_border, color: Colors.red),
                              empty: Icon(Icons.star_border, color: Colors.red),
                            ),
                            onRatingUpdate: (value) {},
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  double rating(int exerciseId, int maxScore) {
    int highestScore = 0;
    SqliteDB.instance.getHighestScore(exerciseId).then((value) {
      highestScore = value;
      double prozentRatio = highestScore / maxScore;
      if (prozentRatio > 0.9) {
        _userRating = 3.0;
      } else if (prozentRatio > 0.6) {
        _userRating = 2.0;
      } else if (prozentRatio > 0.33) {
        _userRating = 1.0;
      } else {
        _userRating = 0.0;
      }
      print("$exerciseId,$_userRating");
    });

    return _userRating;
  }
}
