import 'dart:async';

import 'package:audis/appearance/theme.dart';
import 'package:audis/database/model/answer.dart';
import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_0.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_1.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_2.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_4.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_5.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_7.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic2.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic6.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic7.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic8.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence3.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence4.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence5.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence6.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence7.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence8.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence9.dart';
import 'package:audis/views/training/exercises/speech/speech_memory1.dart';
import 'package:audis/views/training/exercises/speech/speech_memory4.dart';
import 'package:audis/views/training/exercises/speech/speech_memory5.dart';
import 'package:audis/views/training/exercises/speech/speech_memory6.dart';
import 'package:audis/views/training/exercises/speech/speech_memory7.dart';
import 'package:audis/views/training/exercises/speech/speech_order4.dart';
import 'package:audis/views/training/exercises/speech/speech_order5.dart';
import 'package:audis/views/training/exercises/speech/speech_order7.dart';
import 'package:audis/views/training/exercises/speech/speech_order9.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding10.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding11.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding2.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding3.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding4.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding5.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding6.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding7.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding8.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding9.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import 'exercises/laute/consonants_In_real_task_3.dart';
import 'exercises/laute/consonants_In_real_task_6.dart';
import 'exercises/laute/consonants_nonsense_task_B0.dart';
import 'exercises/laute/consonants_nonsense_task_B1.dart';
import 'exercises/laute/consonants_nonsense_task_B2.dart';
import 'exercises/laute/consonants_nonsense_task_B3.dart';
import 'exercises/laute/consonants_nonsense_task_B4.dart';
import 'exercises/laute/vowels_In_nonsense_task_A0.dart';
import 'exercises/laute/vowels_In_nonsense_task_A1.dart';
import 'exercises/laute/vowels_In_nonsense_task_A2.dart';
import 'exercises/laute/vowels_In_nonsense_task_A3.dart';
import 'exercises/laute/vowels_In_nonsense_task_A4.dart';
import 'exercises/laute/vowels_In_nonsense_task_A5.dart';
import 'exercises/laute/vowels_In_real_task_C1.dart';
import 'exercises/laute/vowels_In_real_task_C2.dart';
import 'exercises/laute/vowels_In_real_task_C3.dart';
import 'exercises/laute/vowels_In_real_task_C4.dart';
import 'exercises/laute/vowels_In_real_task_C5.dart';
import 'exercises/speech/speech_dichotic1.dart';
import 'exercises/speech/speech_dichotic3.dart';
import 'exercises/speech/speech_dichotic4.dart';
import 'exercises/speech/speech_dichotic5.dart';
import 'exercises/speech/speech_gap_sentence1.dart';
import 'exercises/speech/speech_gap_sentence2.dart';
import 'exercises/speech/speech_memory2.dart';
import 'exercises/speech/speech_memory3.dart';
import 'exercises/speech/speech_memory8.dart';
import 'exercises/speech/speech_order1.dart';
import 'exercises/speech/speech_order2.dart';
import 'exercises/speech/speech_order3.dart';
import 'exercises/speech/speech_order6.dart';
import 'exercises/speech/speech_order8.dart';
import 'exercises/speech/speech_word_understanding1.dart';
import 'model/level.dart';

class ExerciseScreen extends StatefulWidget {
  final int? exerciseId;

  ExerciseScreen({Key? key, this.exerciseId}) : super(key: key);

  @override
  _ExerciseScreenState createState() => _ExerciseScreenState();
}

class _ExerciseScreenState extends State<ExerciseScreen> {
  late Level exercise;
  bool hidePlayButton = false, isFirstLoad = true, isAnswerView = true, multiAnswer = false;
  bool isSelectedView = false;
  String correctAnswerLog = '' ;
  String givenAnswerLog = '';
  String name = "", taskText = "", status = "", currentTask = "";
  int columnCount = 1;
  double aspectRatio = 6.0;
  bool answerVisible = false;

  @override
  void initState() {
    super.initState();
    switch(widget.exerciseId){
      case Level.LUTE_VOWELS_A0:
        exercise = VowelsInNonsenseTaskA0(onReset: resetScreen);
        break;
      case Level.LUTE_VOWELS_A1:
        exercise = VowelsInNonsenseTaskA1(onReset: resetScreen);
        break;
      case Level.LUTE_VOWELS_A2:
        exercise = VowelsInNonsenseTaskA2(onReset: resetScreen);
        break;
      case Level.LUTE_VOWELS_A3:
        columnCount = 2;
        aspectRatio = 3.0;
        exercise = VowelsInNonsenseTaskA3(onReset: resetScreen);
        break;
      case Level.LUTE_VOWELS_A4:
        exercise = VowelsInNonsenseTaskA4(onReset: resetScreen);
        break;
      case Level.LUTE_VOWELS_A5:
        exercise = VowelsInNonsenseTaskA5(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_B0:
        exercise = ConsonantsInNonsenseB0(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_B1:
        exercise = ConsonantsInNonsenseB1(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_B2:
        exercise = ConsonantsInNonsenseB2(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_B3:
        exercise = ConsonantsInNonsenseB3(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_B4:
        exercise = ConsonantsInNonsenseB4(onReset: resetScreen);
        break;
      case Level.LUTE_VOWELS_REAL_C1:
        columnCount = 2;
        aspectRatio = 3.0;
        exercise = VowelsInRealTaskC1(onReset: resetScreen);
        break;
      case Level.LUTE_VOWELS_REAL_C2:
        exercise = VowelsInRealTaskC2(onReset: resetScreen);
        break;
      case Level.LUTE_VOWELS_REAL_C3:
        exercise = VowelsInRealTaskC3(onReset: resetScreen);
        break;
      case Level.LUTE_VOWELS_REAL_C4:
        columnCount = 2;
        aspectRatio = 3.0;
        exercise = VowelsInRealTaskC4(onReset: resetScreen);
        break;
      case Level.LUTE_VOWELS_REAL_C5:
        columnCount = 2;
        aspectRatio = 3.0;
        exercise = VowelsInRealTaskC5(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_REAL_0:
        exercise = ConsonantsInReal0(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_REAL_1:
        exercise = ConsonantsInReal1(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_REAL_2:
        exercise = ConsonantsInReal2(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_REAL_3:
        exercise = ConsonantsInReal3(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_REAL_4:
        exercise = ConsonantsInReal4(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_REAL_5:
        exercise = ConsonantsInReal5(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_REAL_6:
        exercise = ConsonantsInReal6(onReset: resetScreen);
        break;
      case Level.LUTE_CONSONANTS_REAL_7:
        exercise = ConsonantsInReal7(onReset: resetScreen);
        break;
      case Level.SPEECH_WORDUNDERSTANDING_A:
        exercise = SpeechWordUnderstanding1(onReset: resetScreen);
        break;
      case Level.SPEECH_WORDUNDERSTANDING_B:
        exercise = SpeechWordUnderstanding2(onReset: resetScreen);
        break;
      case Level.SPEECH_WORDUNDERSTANDING_C:
        exercise = SpeechWordUnderstanding3(onReset: resetScreen);
        break;
      case Level.SPEECH_WORDUNDERSTANDING_D:
        exercise = SpeechWordUnderstanding4(onReset: resetScreen);
        break;
      case Level.SPEECH_WORDUNDERSTANDING_E:
        exercise = SpeechWordUnderstanding5(onReset: resetScreen);
        break;
      case Level.SPEECH_WORDUNDERSTANDING_F:
        exercise = SpeechWordUnderstanding6(onReset: resetScreen);
        break;
      case Level.SPEECH_WORDUNDERSTANDING_G:
        exercise = SpeechWordUnderstanding7(onReset: resetScreen);
        break;
      case Level.SPEECH_WORDUNDERSTANDING_H:
        exercise = SpeechWordUnderstanding8(onReset: resetScreen);
        break;
      case Level.SPEECH_WORDUNDERSTANDING_I:
        exercise = SpeechWordUnderstanding9(onReset: resetScreen);
        break;
      case Level.SPEECH_WORDUNDERSTANDING_J:
        exercise = SpeechWordUnderstanding10(onReset: resetScreen);
        break;
      case Level.SPEECH_WORDUNDERSTANDING_K:
        exercise = SpeechWordUnderstanding11(onReset: resetScreen);
        break;
      case Level.SPEECH_MEMORY_A:
        exercise = SpeechMemory1(onReset: resetScreen);
        break;
      case Level.SPEECH_MEMORY_B:
        exercise = SpeechMemory2(onReset: resetScreen);
        break;
      case Level.SPEECH_MEMORY_C:
        exercise = SpeechMemory3(onReset: resetScreen);
        break;
      case Level.SPEECH_MEMORY_D:
        exercise = SpeechMemory4(onReset: resetScreen);
        break;
      case Level.SPEECH_MEMORY_E:
        exercise = SpeechMemory5(onReset : resetScreen);
        break;
      case Level.SPEECH_MEMORY_F:
        exercise = SpeechMemory6(onReset : resetScreen);
        break;
      case Level.SPEECH_MEMORY_G:
        exercise = SpeechMemory7(onReset : resetScreen);
        break;
      case Level.SPEECH_MEMORY_H:
        exercise = SpeechMemory8(onReset : resetScreen);
        break;
      case Level.SPEECH_DICHOTIC_A:
        columnCount = 2;
        aspectRatio = 3.0;
        exercise = SpeechDichotic1(onReset : resetScreen);
        break;
      case Level.SPEECH_DICHOTIC_B:
        exercise = SpeechDichotic2(onReset : resetScreen);
        break;
      case Level.SPEECH_DICHOTIC_C:
        columnCount = 2;
        aspectRatio = 3.0;
        exercise = SpeechDichotic3(onReset : resetScreen);
        break;
      case Level.SPEECH_DICHOTIC_D:
        exercise = SpeechDichotic4(onReset : resetScreen);
        break;
      case Level.SPEECH_DICHOTIC_E:
        columnCount = 2;
        aspectRatio = 3.0;
        exercise = SpeechDichotic5(onReset : resetScreen);
        break;
      case Level.SPEECH_DICHOTIC_F:
        exercise = SpeechDichotic6(onReset : resetScreen);
        break;
      case Level.SPEECH_DICHOTIC_G:
        columnCount = 2;
        aspectRatio = 3.0;
        exercise = SpeechDichotic7(onReset : resetScreen);
        break;
      case Level.SPEECH_DICHOTIC_H:
        exercise = SpeechDichotic8(onReset : resetScreen);
        break;
      case Level.SPEECH_SENTENCEUNDERSTANDING_A:
        exercise = SpeechGapSentence1(onReset : resetScreen);
        break;
      case Level.SPEECH_SENTENCEUNDERSTANDING_B:
        exercise = SpeechGapSentence2(onReset : resetScreen);
        break;
      case Level.SPEECH_SENTENCEUNDERSTANDING_C:
        exercise = SpeechGapSentence3(onReset : resetScreen);
        break;
      case Level.SPEECH_SENTENCEUNDERSTANDING_D:
        exercise = SpeechGapSentence4(onReset : resetScreen);
        break;
      case Level.SPEECH_SENTENCEUNDERSTANDING_E:
        exercise = SpeechGapSentence5(onReset : resetScreen);
        break;
      case Level.SPEECH_SENTENCEUNDERSTANDING_F:
        exercise = SpeechGapSentence6(onReset : resetScreen);
        break;
      case Level.SPEECH_SENTENCEUNDERSTANDING_G:
        exercise = SpeechGapSentence7(onReset : resetScreen);
        break;
      case Level.SPEECH_SENTENCEUNDERSTANDING_H:
        exercise = SpeechGapSentence8(onReset : resetScreen);
        break;
      case Level.SPEECH_SENTENCEUNDERSTANDING_I:
        exercise = SpeechGapSentence9(onReset : resetScreen);
        break;
      case Level.SPEECH_ORDER_A:
        exercise = SpeechOrder1(onReset : resetScreen);
        break;
      case Level.SPEECH_ORDER_B:
        exercise = SpeechOrder2(onReset : resetScreen);
        break;
      case Level.SPEECH_ORDER_C:
        exercise = SpeechOrder3(onReset : resetScreen);
        break;
      case Level.SPEECH_ORDER_D:
        exercise = SpeechOrder4(onReset : resetScreen);
        break;
      case Level.SPEECH_ORDER_E:
        exercise = SpeechOrder5(onReset : resetScreen);
        break;
      case Level.SPEECH_ORDER_F:
        exercise = SpeechOrder6(onReset : resetScreen);
        break;
      case Level.SPEECH_ORDER_G:
        exercise = SpeechOrder7(onReset : resetScreen);
        break;
      case Level.SPEECH_ORDER_H:
        exercise = SpeechOrder8(onReset : resetScreen);
        break;
      case Level.SPEECH_ORDER_I:
        exercise = SpeechOrder9(onReset : resetScreen);
        break;
    }
    name = exercise.getName();
    taskText = exercise.getTaskText();
    status = exercise.getStatus();
    currentTask = exercise.getCurrentTask();
    answerVisible = exercise.getHideAnswersWhilePlay();
    exercise.resetTask();
  }

  @override
  void dispose() {
    super.dispose();
    exercise.advancedPlayer.dispose();
  }

  @override
  Widget build(BuildContext context) {
    currentTask = exercise.getCurrentTask();
    print("answers:: ${exercise.getAnswers().length}");
    print("currentTask:: $currentTask");

    if(isFirstLoad) {
      Future.delayed(Duration(milliseconds: 400)).then((value) {
        resetScreen();
        // print("answers isFirstLoad:: ${exercise.getAnswers().length}");
        isFirstLoad = false;
      });
    }
    return WillPopScope(
      onWillPop: () => showExitPopup(context),
      child: SafeArea(
        child: Scaffold(
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.all(10),
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.topRight,
                          child: Text(status, style: TextStyle(fontSize: 15))),
                      Text(name, style: TextStyle(fontSize: 20)),
                      SizedBox(height: 30),
                      Text(taskText, style: TextStyle(fontSize: 18)),
                      SizedBox(height: 20),
                      Visibility(visible: hidePlayButton ,child: Html(data: currentTask, style: {"body": Style(fontSize: FontSize(18.0), textAlign: TextAlign.center)})),
                      SizedBox(height: 20),
                      Center(
                        child: Visibility(
                          visible: hidePlayButton ? false : true,
                          child: FloatingActionButton(onPressed: (){
                            exercise.playSound();
                            hidePlayButton = true;
                            resetScreen();
                          },child: Icon(Icons.play_arrow)),
                        ),
                      ),
                      SizedBox(height: 10)
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: answersListView()),
            ],
          ),
        ),
      ),
    );
  }

  Widget answersListView(){
    for(int i = 0; i < exercise.getAnswers().length; i++){
      print("exercise.getAnswers():: ${exercise.getAnswers()[i].getText()}");
    }
    // print("isAnswerView=> $isAnswerView");
    return isAnswerView ? Center(
      child: Visibility(
        visible: answerVisible,
        child: GridView.count(
              shrinkWrap: true,
              crossAxisCount: columnCount,
              childAspectRatio: aspectRatio,
              children: List.generate(exercise.getAnswers().length, (index) {
                return  Card(
                  child: ListTile(
                    tileColor: exercise.getIsEnableTile() ? AppColors.lightGrey : AppColors.lightGrey2,
                    title:Center(child: Row(
                      children: [
                        Visibility(visible: exercise.getIsVisibleCounter(), child:
                        Text((exercise.getAnswers()[index].getOrder() != -1) ?
                        (exercise.getAnswers()[index].getOrder() + 1).toString() : "", style: TextStyle(color: Colors.black))),
                        Flexible(child: Center(child: Text(exercise.getAnswers()[index].getText().toString(), style: TextStyle(color: Colors.black),))),
                      ],
                    )),
                    enabled: exercise.getIsEnableTile(),
                    onTap: (){
                      Answer answer = exercise.getAnswers()[index];
                      if(answer.getOrder() == -1 ){
                        if (multiAnswer && answer.isSelected()){
                          //reset all given answers
                          for (int i = 0; i < exercise.getAnswers().length; i++){
                            exercise.getAnswers()[i].setSelected(false);
                          }
                        } else {
                          answer.setOrder(nextClick());
                          isSelectedView = true;
                          resetScreen();
                        }
                        if (areAllAnswersGiven()) {
                          if (areAllAnswersCorrect() && exercise.hasMoreExercises()) {
                            exercise.nextExercise();
                            Future.delayed(Duration(milliseconds: 400)).then((value) {
                              setUpTask();
                              hidePlayButton = true;
                              exercise.playSound();
                            });
                          } else {
                            // status = STATUS_ENDED;
                            endTaskAndShowResults();
                          }
                        }
                      }else{
                        isSelectedView = false;
                        for (int i = 0; i < exercise.getAnswers().length; i++){
                          exercise.getAnswers()[i].setOrder(answer.order);
                          exercise.getAnswers()[i].setSelected(false);
                        }
                        resetScreen();
                      }
                      // resetScreen();
                    },
                  ),
                );
              }),
            ),
      )
    ) : taskSolvedButtons();
  }

  Widget taskSolvedButtons(){
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
                onPressed: () async {
                  // status = STATUS_NOT_STARTED;
                  onAgainClicked();
                },
                child: Text(Strings.again)),
            ElevatedButton(
                onPressed: () async {
                  Navigator.of(context).pop();
                },
                child: Text(Strings.ready))
          ],
        ),
      ),
    );
  }

  bool areAllAnswersGiven() {
    if (exercise.getAnswers() == null) return false;
    int numOfClickedAnswers = 0;
    int expNumOfAnswers = 0;
    for (Answer answer in exercise.getAnswers()) {
      if (answer.isSelected()) {
        numOfClickedAnswers++;
      }
      if (answer.getCorrectOrder() != -1) {
        expNumOfAnswers++;
      }
    }
    if (numOfClickedAnswers >= expNumOfAnswers) {
      return true;
    } else {
      return false;
    }
  }

  bool areAllAnswersCorrect() {
    if (exercise.getAnswers() == null) return false;
    bool isCorrect = true;
    for (Answer answer in exercise.getAnswers()) {
      if(answer.getCorrectOrder() == 0){
        correctAnswerLog = answer.getText();
      }
      if (answer.getOrder() == 0) {
        givenAnswerLog = answer.getText();
      }
      if(!answer.isCorrect()){
        isCorrect = false;
      }
    }
    return isCorrect;
  }

  int nextClick() {
    int next = 0;
    for (int i = 0; i < exercise.getAnswers().length; i++){
      if (exercise.getAnswers()[i].isSelected())
        next++;
    }
    return next;
  }

  void onAgainClicked() {
    // status = STATUS_NOT_STARTED;
    switch(widget.exerciseId){
      case Level.LUTE_VOWELS_A2:
      case Level.LUTE_VOWELS_REAL_C3 :
      case Level.LUTE_VOWELS_REAL_C5 :
      case Level.LUTE_CONSONANTS_B2 :
      case Level.LUTE_CONSONANTS_B4 :
      case Level.LUTE_CONSONANTS_REAL_1 :
      case Level.LUTE_CONSONANTS_REAL_3 :
      case Level.LUTE_CONSONANTS_REAL_5 :
      case Level.LUTE_CONSONANTS_REAL_7 :
      case Level.SPEECH_MEMORY_E :
      case Level.SPEECH_MEMORY_F :
      case Level.SPEECH_MEMORY_G :
      case Level.SPEECH_MEMORY_H :
      case Level.SPEECH_SENTENCEUNDERSTANDING_B :
      case Level.SPEECH_SENTENCEUNDERSTANDING_C :
      case Level.SPEECH_SENTENCEUNDERSTANDING_E :
      case Level.SPEECH_SENTENCEUNDERSTANDING_F :
      case Level.SPEECH_SENTENCEUNDERSTANDING_H :
      case Level.SPEECH_SENTENCEUNDERSTANDING_I :
      case Level.SPEECH_ORDER_A :
      case Level.SPEECH_ORDER_B :
      case Level.SPEECH_ORDER_C :
      case Level.SPEECH_ORDER_D :
      case Level.SPEECH_ORDER_E :
      case Level.SPEECH_ORDER_F :
      case Level.SPEECH_ORDER_G :
      case Level.SPEECH_ORDER_H :
      case Level.SPEECH_ORDER_I :
      exercise.answerVisible = false;
      break;
    }
    exercise.isEnableTile = false;
    exercise.resetTask();
    isAnswerView = true;
    hidePlayButton = false;
    // fragmentAnswers.startNewExercise();
    // finishedPlayback = false;
    setUpTask();
    resetScreen();
  }

  Future<bool> showExitPopup(context) async{
    exercise.advancedPlayer.release();
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(Strings.backBtnPressedDialog, style: TextStyle(fontSize: 20, fontStyle: FontStyle.normal),),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(Strings.no, style: TextStyle(fontSize: 18, fontStyle: FontStyle.normal))),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                  child: Text(Strings.yes, style: TextStyle(fontSize: 18, fontStyle: FontStyle.normal)))
            ],
          );
        });
  }

  void endTaskAndShowResults() {
    isAnswerView = false;
    // hideLightButtons();
    if (areAllAnswersCorrect()) {
      status = exercise.getStatus();
      name = Strings.congratulations;
      taskText = Strings.taskComplete + "\nerreichte Punkte: " + exercise.getScore().toString() + "/" + exercise.getMaxScore().toString();
      currentTask = "";
    //   fragmentTask.setCurrentTask("");
     SqliteDB.instance.writeToHistory(exercise.getExerciseId(), exercise.getScore(), exercise.getMaxScore());
    } else {
      name = Strings.wrong;
      taskText = Strings.taskFailed + "\nerreichte Punkte: " + exercise.getScore().toString() + "/" + exercise.getMaxScore().toString();
      currentTask = "";
    //   fragmentTask.setCurrentTask("");
      SqliteDB.instance.writeToHistoryWithErrorLog(exercise.getExerciseId(),exercise.getScore(),exercise.getMaxScore(),exercise.getTaskText(),
          givenAnswerLog, correctAnswerLog);
    }
    resetScreen();
    uploadHistory();
    // answerHolder.setVisibility(View.GONE);
  }

  void setUpTask() {

      // if (exercise.getHideAnswersWhilePlay() && !finishedPlayback) {
      //   fragmentAnswers.hideAnswerButtons();
      // }
      taskText = exercise.getTaskText();
      name = exercise.getName();
      status = exercise.getStatus();
      currentTask = exercise.getCurrentTask();
      setAnswers(exercise.getAnswers());
      // fragmentTask.setTaskHeadLineLeft(Category.getNameByID(this, exercise.getCategory())+" > "+TaskGroup.getNameByID(this,exercise.getTaskgroup()));
      // fragmentTask.setCurrentTask(exercise.getCurrentTask());
      // fragmentAnswers.setAnswers(currentAnswers);
  }

  void setAnswers(List<Answer> answersList) {
    int expNum = 0;
    for (Answer a in answersList) {
      if (a.getCorrectOrder() != -1) expNum++;
    }

    multiAnswer = expNum > 1;
    // exercise.getAnswers().clear();
    // exercise.answers.addAll(answersList);
    // if(answersList.length == 2){
    //   columnCount = 2;
    // }else{
    //   columnCount = 1;
    // }

    // answerListAdapter.clear();
    // answerListAdapter.addAll(answers);
    // if (answers.length == 2) {
    //   answerListView.setNumColumns(2);
    // } else {
    //   answerListView.setNumColumns(1);
    // }
    // answersEnabled = false;
  }

  void uploadHistory(){
    // HistoryUploader historyUploader = new HistoryUploader();

    // if (uploadThread != null) {
    //   uploadThread = null;
    // }
    // if(uploadThread == null){
    //   uploadThread = new Thread(historyUploader);
    // }
    //
    // if (uploadThread.isAlive() && historyUploader.isPaused())
    //   historyUploader.onResume();
    //
    // if (!uploadThread.isAlive())
    //   uploadThread.start();
  }

  void resetScreen(){
    setState(() {
      answerVisible = exercise.getHideAnswersWhilePlay();
    });
  }
}
