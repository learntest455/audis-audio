import 'package:audis/views/training/levels_screen.dart';
import 'package:audis/views/training/model/task_group_model.dart';
import 'package:flutter/material.dart';

import 'model/category_model.dart';

class TasksScreen extends StatefulWidget {
  final int categoryId;

  TasksScreen({Key? key, required this.categoryId}) : super(key: key);

  @override
  _TasksScreenState createState() => _TasksScreenState();
}

class _TasksScreenState extends State<TasksScreen> {
  @override
  Widget build(BuildContext context) {
    List<TaskGroup> taskGroups = Category(widget.categoryId).getTaskGroups();
    return Scaffold(
      body: Center(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: taskGroups.length,
              itemBuilder: (context, index) {
                return Card(
                  margin: EdgeInsets.symmetric(vertical: 2),
                  child: ListTile(
                    tileColor: Colors.orangeAccent,
                    title:Center(child: Text(taskGroups[index].getName())),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => LevelsScreen(groupID: taskGroups[index].getTaskGroupId())));
                    },
                  ),
                );
              },),
          ),
        ),
      ),
    );
  }
}
