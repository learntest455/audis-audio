
import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/model/task_group_model.dart';

class Category{

  static const int LUTE = 9;               //Laute
  static const int NOISES = 1;             //Geräusche
  static const int SPEECH = 2;             //Sprache
  static const int SPEECH_DISTORTION = 3;  //Sprache im Störschall
  static const int INTONATION = 4;         //Betonung und Stimme
  static const int SIGNALS = 5;          //Richtung

  int category = -1;

  Category(int cat) {
    category = cat;
  }
  int getCategoryId() {
    return category;
  }

  String getName() {
    switch (category) {
      case LUTE:
        return Strings.laute;
      case NOISES:
        return Strings.noises;
      case SPEECH:
        return Strings.language;
      case SPEECH_DISTORTION:
        return Strings.language_with_distortion;
      case INTONATION:
        return Strings.intonation;
      case SIGNALS:
        return Strings.signalDiff;
    }
    return "Doh!!!";
  }

  List<TaskGroup> getTaskGroups() {
    //TODO: we have to check the database for available tasks/exercises in this category...
    List<TaskGroup> taskGroups = [];

    if (category == LUTE) {
      taskGroups = [ TaskGroup(TaskGroup.LUTE_CONSONANTS_NONSENSE),
        TaskGroup(TaskGroup.LUTE_VOWELS_NONSENSE),
        TaskGroup(TaskGroup.LUTE_VOWELS_REAL), TaskGroup(TaskGroup.LUTE_CONSONANTS_REAL)];
    } else if (category == SIGNALS) {
      taskGroups = [ TaskGroup(TaskGroup.DIRECTION_ALL), TaskGroup(TaskGroup.PITCH_ALL),
        TaskGroup(TaskGroup.DURATION_ALL),TaskGroup(TaskGroup.VOLUME_ALL)];
    } else if (category == SPEECH){
    taskGroups = [ TaskGroup(TaskGroup.SPEECH_WORDUNDERSTANDING), TaskGroup(TaskGroup.SPEECH_MEMORY),
      TaskGroup(TaskGroup.SPEECH_ORDER), TaskGroup(TaskGroup.SPEECH_DICHOTIC),
      TaskGroup(TaskGroup.SPEECH_SENTENCEUNDERSTANDING)];
    } else if (category == SPEECH_DISTORTION){
    taskGroups = [ TaskGroup(TaskGroup.SPEECHDISTORTION_WORDUNDERSTANDING),
      TaskGroup(TaskGroup.SPEECHDISTORTION_ORDER),
      TaskGroup(TaskGroup.SPEECHDISTORTION_SENTENCEUNDERSTANDING)];
    } else if (category == INTONATION) {
      taskGroups = [
        TaskGroup(TaskGroup.INTONATION_Q_OR_S),
        TaskGroup(TaskGroup.INTONATION_GENDER),
        TaskGroup(TaskGroup.INTONATION_AGE),
        TaskGroup(TaskGroup.INTONATION_MOOD)
      ];
    }
    return taskGroups;
  }
}