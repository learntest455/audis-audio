import 'package:audis/utils/strings.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_0.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_1.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_2.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_3.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_4.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_5.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_6.dart';
import 'package:audis/views/training/exercises/laute/consonants_In_real_task_7.dart';
import 'package:audis/views/training/exercises/laute/consonants_nonsense_task_B0.dart';
import 'package:audis/views/training/exercises/laute/consonants_nonsense_task_B1.dart';
import 'package:audis/views/training/exercises/laute/consonants_nonsense_task_B2.dart';
import 'package:audis/views/training/exercises/laute/consonants_nonsense_task_B3.dart';
import 'package:audis/views/training/exercises/laute/consonants_nonsense_task_B4.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A0.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A1.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A2.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A3.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A4.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_nonsense_task_A5.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_real_task_C1.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_real_task_C2.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_real_task_C3.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_real_task_C4.dart';
import 'package:audis/views/training/exercises/laute/vowels_In_real_task_C5.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic1.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic2.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic3.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic4.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic5.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic6.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic7.dart';
import 'package:audis/views/training/exercises/speech/speech_dichotic8.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence1.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence2.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence3.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence4.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence5.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence6.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence7.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence8.dart';
import 'package:audis/views/training/exercises/speech/speech_gap_sentence9.dart';
import 'package:audis/views/training/exercises/speech/speech_memory1.dart';
import 'package:audis/views/training/exercises/speech/speech_memory2.dart';
import 'package:audis/views/training/exercises/speech/speech_memory3.dart';
import 'package:audis/views/training/exercises/speech/speech_memory4.dart';
import 'package:audis/views/training/exercises/speech/speech_memory5.dart';
import 'package:audis/views/training/exercises/speech/speech_memory6.dart';
import 'package:audis/views/training/exercises/speech/speech_memory7.dart';
import 'package:audis/views/training/exercises/speech/speech_memory8.dart';
import 'package:audis/views/training/exercises/speech/speech_order1.dart';
import 'package:audis/views/training/exercises/speech/speech_order2.dart';
import 'package:audis/views/training/exercises/speech/speech_order3.dart';
import 'package:audis/views/training/exercises/speech/speech_order4.dart';
import 'package:audis/views/training/exercises/speech/speech_order5.dart';
import 'package:audis/views/training/exercises/speech/speech_order6.dart';
import 'package:audis/views/training/exercises/speech/speech_order7.dart';
import 'package:audis/views/training/exercises/speech/speech_order8.dart';
import 'package:audis/views/training/exercises/speech/speech_order9.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding1.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding10.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding11.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding2.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding3.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding4.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding5.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding6.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding7.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding8.dart';
import 'package:audis/views/training/exercises/speech/speech_word_understanding9.dart';

import '../exercises/speech/speech_gap_sentence1.dart';
import 'level.dart';

class TaskGroup{
  //Speech
  static const int SPEECH_WORDUNDERSTANDING = 20;
  static const int SPEECH_MEMORY = 21;
  static const int SPEECH_ORDER = 22;
  static const int SPEECH_DICHOTIC = 23;
  static const int SPEECH_SENTENCEUNDERSTANDING = 24;

  static const int SPEECHDISTORTION_WORDUNDERSTANDING = 30;
  static const int SPEECHDISTORTION_ORDER = 31;
  static const int SPEECHDISTORTION_SENTENCEUNDERSTANDING = 32;

  //Intonation & Voice
  static const int INTONATION_Q_OR_S = 40; //question or statement
  static const int INTONATION_GENDER = 41; //male or female
  static const int INTONATION_AGE = 42; //child or grownup
  static const int INTONATION_MOOD = 43; //sad or happy

  //Lute
  static const int LUTE_VOWELS_NONSENSE = 90;
  static const int LUTE_CONSONANTS_NONSENSE = 91;
  static const int LUTE_VOWELS_REAL = 92;
  static const int LUTE_CONSONANTS_REAL = 93;

  //Direction
  static const int DIRECTION_ALL = 50; //TODO: const introduce different direction-taskgroups with level (=>level-support in the corresponding level-classes

  //Pitch
  static const int PITCH_ALL = 60;

  //Duration
  static const int DURATION_ALL = 70;

  //VolumeFragment
  static const int VOLUME_ALL = 80;

  int groupID = -1;

  TaskGroup(int groupID) {
    this.groupID = groupID;
  }

  int getTaskGroupId() {
    return groupID;
  }

  String getName() {
    switch (groupID) {
      case LUTE_VOWELS_NONSENSE:
        return Strings.vowelsInNonsense0Name;
      case LUTE_CONSONANTS_NONSENSE:
        return Strings.consonantsInNonsense;
      case LUTE_VOWELS_REAL:
        return Strings.vowelsInRealwords;
      case LUTE_CONSONANTS_REAL:
        return Strings.consonantsInRealwords;
      case DIRECTION_ALL:
        return Strings.orientation;
      case PITCH_ALL:
        return Strings.pitch;
      case DURATION_ALL:
        return Strings.duration;
      case VOLUME_ALL:
        return Strings.volume;
      case SPEECH_WORDUNDERSTANDING:
        return Strings.wordUnderstanding;
      case SPEECHDISTORTION_WORDUNDERSTANDING:
        return Strings.wordUnderstanding;
      case SPEECH_MEMORY:
        return Strings.memory;
      case SPEECH_ORDER:
        return Strings.order;
      case SPEECH_DICHOTIC:
        return Strings.dichotic;
      case SPEECH_SENTENCEUNDERSTANDING:
        return Strings.sentenceUnderstandingTaskAName;
      case SPEECHDISTORTION_SENTENCEUNDERSTANDING:
        return Strings.sentenceUnderstandingTaskAName;
      case SPEECHDISTORTION_ORDER:
        return Strings.order;
      case INTONATION_Q_OR_S:
        return Strings.intonationQorS;
      case INTONATION_GENDER:
        return Strings.intonationGender;
      case INTONATION_AGE:
        return Strings.intonationAge;
      case INTONATION_MOOD:
        return Strings.intonationMood;
    }
    return "Doh!!!";
  }

  List<Level> getLevels() {
    //TODO: we have to check the database for available tasks/exercises in this category...
    List<Level> tasks = [];
    switch(groupID){
      case LUTE_VOWELS_NONSENSE:
        tasks = [
          VowelsInNonsenseTaskA0(),
          VowelsInNonsenseTaskA1(),
          VowelsInNonsenseTaskA2(),
          VowelsInNonsenseTaskA3(),
          VowelsInNonsenseTaskA4(),
          VowelsInNonsenseTaskA5(),
        ];
        break;
      case LUTE_CONSONANTS_NONSENSE:
        tasks = [
          ConsonantsInNonsenseB0(),
          ConsonantsInNonsenseB1(),
          ConsonantsInNonsenseB2(),
          ConsonantsInNonsenseB3(),
          ConsonantsInNonsenseB4(),
        ];
        break;
      case LUTE_VOWELS_REAL:
        tasks = [
          VowelsInRealTaskC1(),
          VowelsInRealTaskC2(),
          VowelsInRealTaskC3(),
          VowelsInRealTaskC4(),
          VowelsInRealTaskC5(),
        ];
        break;
      case LUTE_CONSONANTS_REAL:
        tasks = [
          ConsonantsInReal0(),
          ConsonantsInReal1(),
          ConsonantsInReal2(),
          ConsonantsInReal3(),
          ConsonantsInReal4(),
          ConsonantsInReal5(),
          ConsonantsInReal6(),
          ConsonantsInReal7(),
        ];
        break;
      case SPEECH_WORDUNDERSTANDING:
        tasks = [
          SpeechWordUnderstanding1(),
          SpeechWordUnderstanding2(),
          SpeechWordUnderstanding3(),
          SpeechWordUnderstanding4(),
          SpeechWordUnderstanding5(),
          SpeechWordUnderstanding6(),
          SpeechWordUnderstanding7(),
          SpeechWordUnderstanding8(),
          SpeechWordUnderstanding9(),
          SpeechWordUnderstanding10(),
          SpeechWordUnderstanding11()
        ];
        break;
      case SPEECH_MEMORY:
        tasks = [
          SpeechMemory1(),
          SpeechMemory2(),
          SpeechMemory3(),
          SpeechMemory4(),
          SpeechMemory5(),
          SpeechMemory6(),
          SpeechMemory7(),
          SpeechMemory8(),
        ];
        break;
      case SPEECH_ORDER:
        tasks = [
          SpeechOrder1(),
          SpeechOrder2(),
          SpeechOrder3(),
          SpeechOrder4(),
          SpeechOrder5(),
          SpeechOrder6(),
          SpeechOrder7(),
          SpeechOrder8(),
          SpeechOrder9(),
        ];
        break;
      case SPEECH_DICHOTIC:
        tasks = [
          SpeechDichotic1(),
          SpeechDichotic2(),
          SpeechDichotic3(),
          SpeechDichotic4(),
          SpeechDichotic5(),
          SpeechDichotic6(),
          SpeechDichotic7(),
          SpeechDichotic8(),
        ];
        break;
      case SPEECH_SENTENCEUNDERSTANDING:
        tasks = [
          SpeechGapSentence1(),
          SpeechGapSentence2(),
          SpeechGapSentence3(),
          SpeechGapSentence4(),
          SpeechGapSentence5(),
          SpeechGapSentence6(),
          SpeechGapSentence7(),
          SpeechGapSentence8(),
          SpeechGapSentence9(),
        ];
        break;
    }

    return tasks;

    // } else if (groupID == DIRECTION_ALL) {
    // tasks = new Level[]{
    // new DirectionTaskA(context),
    // new DirectionTaskB(context),
    // new DirectionTaskC(context),
    // new DirectionTaskD(context),
    // new DirectionTaskE(context)
    // };
    // } else if (groupID == PITCH_ALL) {
    // tasks = new Level[]{
    // new PitchTaskA(context),
    // new PitchTaskB(context),
    // new PitchTaskC(context),
    // new PitchTaskD(context)
    // };
    // } else if (groupID == DURATION_ALL) {
    // tasks = new Level[]{
    // new DurationTaskA(context),
    // new DurationTaskB(context),
    // new DurationTaskC(context),
    // new DurationTaskD(context)
    // };
    // } else if (groupID == VOLUME_ALL) {
    // tasks = new Level[]{
    // new VolumeTaskA(context),
    // new VolumeTaskB(context),
    // new VolumeTaskC(context),
    // new VolumeTaskD(context)
    // };
    // } else if (groupID == SPEECH_WORDUNDERSTANDING) {
    // tasks = new Level[]{
    // new SpeechWordUnderstanding1(context),
    // new SpeechWordUnderstanding2(context),
    // new SpeechWordUnderstanding3(context),
    // new SpeechWordUnderstanding4(context),
    // new SpeechWordUnderstanding5(context),
    // new SpeechWordUnderstanding6(context),
    // new SpeechWordUnderstanding7(context),
    // new SpeechWordUnderstanding8(context),
    // new SpeechWordUnderstanding9(context),
    // new SpeechWordUnderstanding10(context),
    // new SpeechWordUnderstanding11(context)
    // };
    // } else if (groupID == SPEECHDISTORTION_WORDUNDERSTANDING) {
    // tasks = new Level[]{
    // new SpeechNoiseWordUnderstanding1(context),
    // new SpeechNoiseWordUnderstanding2(context),
    // new SpeechNoiseWordUnderstanding3(context),
    // new SpeechNoiseWordUnderstanding4(context),
    // new SpeechNoiseWordUnderstanding5(context),
    // new SpeechNoiseWordUnderstanding6(context),
    // new SpeechNoiseWordUnderstanding7(context),
    // new SpeechNoiseWordUnderstanding8(context),
    // new SpeechNoiseWordUnderstanding9(context),
    // new SpeechNoiseWordUnderstanding10(context)
    // };
    // } else if (groupID == SPEECH_MEMORY) {
    // tasks = new Level[]{
    // new SpeechMemory1(context),
    // new SpeechMemory2(context),
    // new SpeechMemory3(context),
    // new SpeechMemory4(context),
    // new SpeechMemory5(context),
    // new SpeechMemory6(context),
    // new SpeechMemory7(context),
    // new SpeechMemory8(context)
    // };
    // } else if (groupID == SPEECH_ORDER) {
    // tasks = new Level[]{
    // new SpeechOrder1(context),
    // new SpeechOrder2(context),
    // new SpeechOrder3(context),
    // new SpeechOrder4(context),
    // new SpeechOrder5(context),
    // new SpeechOrder6(context),
    // new SpeechOrder7(context),
    // new SpeechOrder8(context),
    // new SpeechOrder9(context)
    // };
    // }else if (groupID == SPEECHDISTORTION_ORDER) {
    // tasks = new Level[]{
    // new SpeechNoiseOrder1(context),
    // new SpeechNoiseOrder2(context),
    // new SpeechNoiseOrder3(context),
    // new SpeechNoiseOrder4(context),
    // new SpeechNoiseOrder5(context),
    // new SpeechNoiseOrder6(context)
    // };
    // }
    // else if (groupID == SPEECH_DICHOTIC) {
    // tasks = new Level[]{
    // new SpeechDichotic1(context),
    // new SpeechDichotic2(context),
    // new SpeechDichotic3(context),
    // new SpeechDichotic4(context),
    // new SpeechDichotic5(context),
    // new SpeechDichotic6(context),
    // new SpeechDichotic7(context),
    // new SpeechDichotic8(context)
    // };
    // } else if (groupID == SPEECH_SENTENCEUNDERSTANDING) {
    // tasks = new Level[]{
    // new SpeechGapSentence1(context),
    // new SpeechGapSentence2(context),
    // new SpeechGapSentence3(context),
    // new SpeechGapSentence4(context),
    // new SpeechGapSentence5(context),
    // new SpeechGapSentence6(context),
    // new SpeechGapSentence7(context),
    // new SpeechGapSentence8(context),
    // new SpeechGapSentence9(context)
    // };
    // }else if (groupID == SPEECHDISTORTION_SENTENCEUNDERSTANDING) {
    // tasks = new Level[]{
    // new SpeechNoiseGapSentence1(context),
    // new SpeechNoiseGapSentence2(context),
    // new SpeechNoiseGapSentence3(context),
    // new SpeechNoiseGapSentence4(context),
    // new SpeechNoiseGapSentence5(context),
    // new SpeechNoiseGapSentence6(context)
    // };
    // }
    // else if (groupID == INTONATION_Q_OR_S) {
    // tasks = new Level[]{
    // new IntonationQuestionOrStatement1(context),
    // new IntonationQuestionOrStatement2(context),
    // new IntonationQuestionOrStatement3(context)
    // };
    // } else if (groupID == INTONATION_GENDER) {
    // tasks = new Level[]{
    // new IntonationGender1(context),
    // new IntonationGender2(context),
    // new IntonationGender3(context)
    // };
    // } else if (groupID == INTONATION_AGE) {
    // tasks = new Level[]{
    // new IntonationAge1(context),
    // new IntonationAge2(context),
    // new IntonationAge3(context)
    // };
    // } else if (groupID == INTONATION_MOOD) {
    // tasks = new Level[]{
    // new IntonationMood1(context),
    // new IntonationMood2(context),
    // new IntonationMood3(context)
    // };
    // }
    // else {
    // tasks = new Level[]{};
    // }
    // return Arrays.asList(tasks);
  }
}