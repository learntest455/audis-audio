import 'package:audioplayers/audioplayers.dart';
import 'package:audis/database/model/answer.dart';

abstract class Level{
  /// Constants (IDs) to identifiy the single Tasks/Exercises...
  /// these values are set in the constructor of an implementation and assigned to 'exerciseId'
  /// trying (category-id * 100) + incrementing number
  
  static const int SPEECH_WORDUNDERSTANDING_A = 201;
  static const int SPEECH_WORDUNDERSTANDING_B = 202;
  static const int SPEECH_WORDUNDERSTANDING_C = 203;
  static const int SPEECH_WORDUNDERSTANDING_D = 204;
  static const int SPEECH_WORDUNDERSTANDING_E = 205;
  static const int SPEECH_WORDUNDERSTANDING_F = 206;
  static const int SPEECH_WORDUNDERSTANDING_G = 207;
  static const int SPEECH_WORDUNDERSTANDING_H = 208;
  static const int SPEECH_WORDUNDERSTANDING_I = 209;
  static const int SPEECH_WORDUNDERSTANDING_J = 210;
  static const int SPEECH_WORDUNDERSTANDING_K = 260; // <- 211 is in use allready...

  static const int SPEECH_TESTALLSAMPLES = 299;

  static const int SPEECH_MEMORY_A = 211;
  static const int SPEECH_MEMORY_B = 212;
  static const int SPEECH_MEMORY_C = 213;
  static const int SPEECH_MEMORY_D = 214;
  static const int SPEECH_MEMORY_E = 215;
  static const int SPEECH_MEMORY_F = 216;
  static const int SPEECH_MEMORY_G = 217;
  static const int SPEECH_MEMORY_H = 218;

  static const int SPEECH_ORDER_A = 221;
  static const int SPEECH_ORDER_B = 222;
  static const int SPEECH_ORDER_C = 223;
  static const int SPEECH_ORDER_D = 224;
  static const int SPEECH_ORDER_E = 225;
  static const int SPEECH_ORDER_F = 226;
  static const int SPEECH_ORDER_G = 227;
  static const int SPEECH_ORDER_H = 228;
  static const int SPEECH_ORDER_I = 229;

  static const int SPEECH_DICHOTIC_A = 231;
  static const int SPEECH_DICHOTIC_B = 232;
  static const int SPEECH_DICHOTIC_C = 233;
  static const int SPEECH_DICHOTIC_D = 234;
  static const int SPEECH_DICHOTIC_E = 235;
  static const int SPEECH_DICHOTIC_F = 236;
  static const int SPEECH_DICHOTIC_G = 237;
  static const int SPEECH_DICHOTIC_H = 238;

  static const int SPEECH_SENTENCEUNDERSTANDING_A = 241;
  static const int SPEECH_SENTENCEUNDERSTANDING_B = 242;
  static const int SPEECH_SENTENCEUNDERSTANDING_C = 243;
  static const int SPEECH_SENTENCEUNDERSTANDING_D = 244;
  static const int SPEECH_SENTENCEUNDERSTANDING_E = 245;
  static const int SPEECH_SENTENCEUNDERSTANDING_F = 246;
  static const int SPEECH_SENTENCEUNDERSTANDING_G = 247;
  static const int SPEECH_SENTENCEUNDERSTANDING_H = 248;
  static const int SPEECH_SENTENCEUNDERSTANDING_I = 249;

  static const int SPEECHNOISE_WORDUNDERSTANDING_A = 301;
  static const int SPEECHNOISE_WORDUNDERSTANDING_B = 302;
  static const int SPEECHNOISE_WORDUNDERSTANDING_C = 303;
  static const int SPEECHNOISE_WORDUNDERSTANDING_D = 304;
  static const int SPEECHNOISE_WORDUNDERSTANDING_E = 305;
  static const int SPEECHNOISE_WORDUNDERSTANDING_F = 306;
  static const int SPEECHNOISE_WORDUNDERSTANDING_G = 307;
  static const int SPEECHNOISE_WORDUNDERSTANDING_H = 308;
  static const int SPEECHNOISE_WORDUNDERSTANDING_I = 309;
  static const int SPEECHNOISE_WORDUNDERSTANDING_J = 310;

  static const int SPEECHNOISE_ORDER_A = 311;
  static const int SPEECHNOISE_ORDER_B = 312;
  static const int SPEECHNOISE_ORDER_C = 313;
  static const int SPEECHNOISE_ORDER_D = 314;
  static const int SPEECHNOISE_ORDER_E = 315;
  static const int SPEECHNOISE_ORDER_F = 316;

  static const int SPEECHNOISE_SENTENCEUNDERSTANDING_A = 321;
  static const int SPEECHNOISE_SENTENCEUNDERSTANDING_B = 322;
  static const int SPEECHNOISE_SENTENCEUNDERSTANDING_C = 323;
  static const int SPEECHNOISE_SENTENCEUNDERSTANDING_D = 324;
  static const int SPEECHNOISE_SENTENCEUNDERSTANDING_E = 325;
  static const int SPEECHNOISE_SENTENCEUNDERSTANDING_F = 326;

  static const int INTONATION_Q_OR_S_A = 401;
  static const int INTONATION_Q_OR_S_B = 402;
  static const int INTONATION_Q_OR_S_C = 403;
  static const int INTONATION_GENDER_A = 404;
  static const int INTONATION_GENDER_B = 405;
  static const int INTONATION_GENDER_C = 406;
  static const int INTONATION_AGE_A = 407;
  static const int INTONATION_AGE_B = 408;
  static const int INTONATION_AGE_C = 409;
  static const int INTONATION_MOOD_A = 410;
  static const int INTONATION_MOOD_B = 411;
  static const int INTONATION_MOOD_C = 412;

  static const int DIRECTION_TASK_A = 510;
  static const int DIRECTION_TASK_B = 511;
  static const int DIRECTION_TASK_C = 521;
  static const int DIRECTION_TASK_D = 531;
  static const int DIRECTION_TASK_E = 541;

  static const int PITCH_TASK_A = 601;
  static const int PITCH_TASK_B = 611;
  static const int PITCH_TASK_C = 621;
  static const int PITCH_TASK_D = 631;

  static const int DURATION_LONGEST_TONE = 701;
  static const int DURATION_LONGEST_TONE_HALF = 711;
  static const int DURATION_LONGEST_TONE_II = 721;
  static const int DURATION_LONGEST_TONE_II_HALF = 731;
  static const int DURATION_TASK_A = 741;
  static const int DURATION_TASK_B = 751;
  static const int DURATION_TASK_C = 761;
  static const int DURATION_TASK_D = 771;

  static const int VOLUME_TASK_A = 801;
  static const int VOLUME_TASK_B = 811;
  static const int VOLUME_TASK_C = 821;
  static const int VOLUME_TASK_D = 831;

  static const int LUTE_VOWELS_A0 = 901;
  static const int LUTE_VOWELS_A1 = 902;
  static const int LUTE_VOWELS_A2 = 903;
  static const int LUTE_VOWELS_A3 = 904;
  static const int LUTE_VOWELS_A4 = 905;
  static const int LUTE_VOWELS_A5 = 906;

  static const int LUTE_CONSONANTS_B0 = 911;
  static const int LUTE_CONSONANTS_B1 = 912;
  static const int LUTE_CONSONANTS_B2 = 913;
  static const int LUTE_CONSONANTS_B3 = 914;
  static const int LUTE_CONSONANTS_B4 = 915;

  static const int LUTE_VOWELS_REAL_C1 = 921;
  static const int LUTE_VOWELS_REAL_C2 = 922;
  static const int LUTE_VOWELS_REAL_C3 = 923;
  static const int LUTE_VOWELS_REAL_C4 = 924;
  static const int LUTE_VOWELS_REAL_C5 = 925;

  static const int LUTE_CONSONANTS_REAL_0 = 931;
  static const int LUTE_CONSONANTS_REAL_1 = 932;
  static const int LUTE_CONSONANTS_REAL_2 = 933;
  static const int LUTE_CONSONANTS_REAL_3 = 934;
  static const int LUTE_CONSONANTS_REAL_4 = 935;
  static const int LUTE_CONSONANTS_REAL_5 = 936;
  static const int LUTE_CONSONANTS_REAL_6 = 937;
  static const int LUTE_CONSONANTS_REAL_7 = 938;

  late int exerciseId;
  late int category;
  late int taskGroup;
  late String name;
  late String taskText;
  int score = 0;
  String currentTask = '';
  AudioPlayer advancedPlayer = new AudioPlayer();
  late AudioCache audioCache;
  List<Answer> answers = [];
  bool isVisibleCounter = false;
  bool answerVisible = false;
  bool isEnableTile = false;
  void playSound();
  String getStatus();
  void nextExercise();
  bool hasMoreExercises();
  void resetTask();
  int getMaxScore();
  getScore(){return score;}

  List<Answer> getAnswers() {
    return answers;
  }

  int getExerciseId(){
    return exerciseId;
  }
  int getCategory(){
    return category;
  }
  int getTaskGroup(){ return taskGroup;}

  String getName() {
    return name;
  }
  String getTaskText() {
    return taskText;
  }
  String getCurrentTask(){ return currentTask; }

  bool getIsVisibleCounter() {
    return isVisibleCounter;
  }

  bool getHideAnswersWhilePlay() {
    return answerVisible;
  }
  bool getIsEnableTile() {
    return isEnableTile;
  }

}