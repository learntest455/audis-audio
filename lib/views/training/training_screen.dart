import 'package:audis/views/training/model/category_model.dart';
import 'package:audis/views/training/tasks_group_screen.dart';
import 'package:flutter/material.dart';

class TrainingScreen extends StatefulWidget {
  const TrainingScreen({Key? key}) : super(key: key);

  @override
  _TrainingScreenState createState() => _TrainingScreenState();
}

class _TrainingScreenState extends State<TrainingScreen> {
  List<Category> categories = [Category(Category.LUTE), Category(Category.SPEECH),
  Category(Category.SPEECH_DISTORTION),
  Category(Category.INTONATION), Category(Category.SIGNALS)];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: categories.length,
                itemBuilder: (context, index) {
                  return Card(
                    margin: EdgeInsets.symmetric(vertical: 2),
                    child: ListTile(
                      tileColor: Colors.orangeAccent,
                        title:Center(child: Text(categories[index].getName())),
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => TasksScreen(categoryId: categories[index].getCategoryId())));
                      },
                    ),
                  );
                },),
          ),
        ),
      ),
    );
  }
}
