class DoctorsDataModel {
  Doctors? doctors;

  DoctorsDataModel({this.doctors});

  DoctorsDataModel.fromJson(Map<String, dynamic> json) {
    doctors =
    json['Doctors'] != null ? new Doctors.fromJson(json['Doctors']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.doctors != null) {
      data['Doctors'] = this.doctors!.toJson();
    }
    return data;
  }
}

class Doctors {
  List<Doctor>? doctor;

  Doctors({this.doctor});

  Doctors.fromJson(Map<String, dynamic> json) {
    if (json['Doctor'] != null) {
      doctor = [];
      json['Doctor'].forEach((v) {
        doctor!.add(new Doctor.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.doctor != null) {
      data['Doctor'] = this.doctor!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Doctor {
  String? name;
  String? address;
  String? zip;
  String? city;
  String? tel;
  String? eMail;
  String? uRL;
  String? lat;
  String? lng;
  String? isAdmin;
  String? endOfAbo;
  String? isStudySupervisor;

  Doctor(
      {this.name,
        this.address,
        this.zip,
        this.city,
        this.tel,
        this.eMail,
        this.uRL,
        this.lat,
        this.lng,
        this.isAdmin,
        this.endOfAbo,
        this.isStudySupervisor});

  Doctor.fromJson(Map<String, dynamic> json) {
    name = json['Name'];
    address = json['Address'];
    zip = json['Zip'];
    city = json['City'];
    tel = json['Tel'];
    eMail = json['eMail'];
    uRL = json['URL'];
    lat = json['lat'];
    lng = json['lng'];
    isAdmin = json['isAdmin'];
    endOfAbo = json['endOfAbo'];
    isStudySupervisor = json['isStudySupervisor'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Name'] = this.name;
    data['Address'] = this.address;
    data['Zip'] = this.zip;
    data['City'] = this.city;
    data['Tel'] = this.tel;
    data['eMail'] = this.eMail;
    data['URL'] = this.uRL;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['isAdmin'] = this.isAdmin;
    data['endOfAbo'] = this.endOfAbo;
    data['isStudySupervisor'] = this.isStudySupervisor;
    return data;
  }
}