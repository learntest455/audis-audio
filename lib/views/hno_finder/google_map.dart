import 'dart:async';
import 'package:audis/network/api_client.dart';
import 'package:clippy_flutter/clippy_flutter.dart';
import 'package:custom_info_window/custom_info_window.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

import 'model/doctors.dart';

class GoogleMapScreen extends StatefulWidget {
  GoogleMapScreen({Key? key}) : super(key: key);

  @override
  _GoogleMapScreenState createState() => _GoogleMapScreenState();
}

class _GoogleMapScreenState extends State<GoogleMapScreen> {
  CustomInfoWindowController _customInfoWindowController =
      CustomInfoWindowController();

  final Set<Marker> listMarkers = {};
  MapType currentMapType = MapType.normal;
  BitmapDescriptor? customIcon;
  String? tel, url, email, lat, lng;
  bool onTap = false;

  static final LatLngBounds bounds = LatLngBounds(
    southwest: LatLng(47.2701115, 5.8663425),
    northeast: LatLng(55.0815000, 15.0418962),
  );
// calculating centre of the bounds
  static final LatLng centerBounds = LatLng(
      (bounds.northeast.latitude + bounds.southwest.latitude) / 2,
      (bounds.northeast.longitude + bounds.southwest.longitude) / 2);

  static final CameraPosition initCameraPosition =
      CameraPosition(bearing: 30, target: centerBounds, tilt: 45, zoom: 6);

  @override
  void initState() {
    super.initState();
    setCustomMarker();
  }

  void dispose() {
    _customInfoWindowController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<Doctor>?>(
        future: ApiClient().fetchDoctorsList(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Text('An error has occurred!'),
            );
          } else if (snapshot.hasData) {
            for (int i = 0; i < snapshot.data!.length; i++) {
              Doctor data = snapshot.data![i];

              String snippetData = "";
              if (data.address != null) snippetData = data.address!;
              if (data.zip != null)
                snippetData = snippetData + "\n" + data.zip!;
              if (data.city != null)
                snippetData = snippetData + " " + data.city!;
              if (data.tel != null)
                snippetData = snippetData + "\n\n" + data.tel!;
              if (data.eMail != null)
                snippetData = snippetData + "\n" + data.eMail!; 
              if (data.uRL != null)
                snippetData = snippetData + "\n" + data.uRL!;

              listMarkers.add(
                Marker(
                  icon: customIcon ?? BitmapDescriptor.defaultMarker,
                  markerId: MarkerId(i.toString()),
                  position: LatLng(double.parse(data.lat.toString()),
                      double.parse(data.lng.toString())),
                  onTap: () {
                    _customInfoWindow(data, snippetData);
                    setState(() {});
                    tel = data.tel;
                    email = data.eMail;
                    url = data.uRL;
                    lat = data.lat;
                    lng = data.lng;
                    onTap = true;
                  },
                ),
              );
            }
            return _buildMap();
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Widget _buildMap() {
    return SafeArea(
        child: Stack(
      children: [
        GoogleMap(
            onTap: (latlng) {
              onTap = false;
              tel = null;
              email = null;
              url = null;
              _customInfoWindowController.hideInfoWindow!();
              setState(() {});
            },
            onCameraMove: (latlng) {
              _customInfoWindowController.onCameraMove!();
            },
            mapToolbarEnabled: false,
            mapType: currentMapType,
            zoomControlsEnabled: false,
            onMapCreated: (GoogleMapController controller) {
              _customInfoWindowController.googleMapController = controller;
            },
            initialCameraPosition: initCameraPosition,
            compassEnabled: false,
            markers: listMarkers),
        Container(
          padding: EdgeInsets.all(15),
          alignment: Alignment.topRight,
          child: FloatingActionButton(
            child: Icon(Icons.map, size: 30),
            onPressed: _onMapTypeChanged,
          ),
        ),
        Container(
          padding: EdgeInsets.all(15),
          alignment: Alignment.bottomCenter,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Visibility(
                visible: email != null ? true : false,
                child: Expanded(
                  child: FloatingActionButton(
                    backgroundColor: Colors.orange,
                    onPressed: () {
                      openEmail();
                    },
                    child: const Icon(Icons.mail_outline, color: Colors.white),
                  ),
                ),
              ),
              Visibility(
                visible: tel != null ? true : false,
                child: Expanded(
                  child: FloatingActionButton(
                    backgroundColor: Colors.orange,
                    onPressed: () {
                      launch(('tel://$tel'));
                    },
                    child: const Icon(Icons.call, color: Colors.white),
                  ),
                ),
              ),
              Visibility(
                visible: url != null ? true : false,
                child: Expanded(
                  child: FloatingActionButton(
                    backgroundColor: Colors.orange,
                    onPressed: () async {
                      final Uri uri = Uri(
                        scheme: 'https',
                        path: url,
                      );
                      await launch(uri.toString());
                    },
                    child: const Icon(Icons.open_in_browser_sharp,
                        color: Colors.white),
                  ),
                ),
              ),
              Visibility(
                visible: onTap ? true : false,
                child: Expanded(
                  child: FloatingActionButton(
                    backgroundColor: Colors.orange,
                    onPressed: () {
                      openMap(double.parse(lat.toString()),
                          double.parse(lng.toString()));
                    },
                    child: const Icon(Icons.alt_route_outlined,
                        color: Colors.white),
                  ),
                ),
              )
            ],
          ),
        ),
        CustomInfoWindow(
          controller: _customInfoWindowController,
          height: 125,
          width: 200,
        )
      ],
    ));
  }

  _customInfoWindow(Doctor data, String snippetData) {
    return _customInfoWindowController.addInfoWindow!(
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          data.name!,
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ),
                        Text(snippetData,
                            style: TextStyle(color: Colors.grey, fontSize: 11))
                      ],
                    ),
                  ),
                ),
                width: double.infinity,
                height: double.infinity,
              ),
            ),
            Triangle.isosceles(
              edge: Edge.BOTTOM,
              child: Container(
                color: Colors.white,
                width: 20.0,
                height: 10.0,
              ),
            ),
          ],
        ),
        LatLng(double.parse(data.lat!), double.parse(data.lng!)));
  }

  void openEmail() {
    String? encodeQueryParameters(Map<String, String> params) {
      return params.entries
          .map((e) =>
              '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
          .join('&');
    }

    final Uri emailLaunchUri = Uri(
      scheme: 'mailto',
      path: email,
      query: encodeQueryParameters(<String, String>{
        'subject': 'Example Subject & Symbols are allowed!'
      }),
    );

    launch(emailLaunchUri.toString());
  }

  Future<void> openMap(double lat, double lng) async {
    var uri = Uri.parse("google.navigation:q=$lat,$lng&mode=d");
    if (await canLaunch(uri.toString())) {
      await launch(uri.toString());
    } else {
      throw 'Could not launch ${uri.toString()}';
    }
  }

  void _onMapTypeChanged() {
    setState(() {
      currentMapType =
          currentMapType == MapType.normal ? MapType.satellite : MapType.normal;
    });
  }

  void setCustomMarker() async {
    if (customIcon == null) {
      BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.5),
              'assets/images/auric_yellow.png')
          .then((value) => setState(() {
                customIcon = value;
              }));
    }
  }
}
