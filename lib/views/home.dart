import 'package:audis/database/sqlite.dart';
import 'package:audis/utils/strings.dart';
import 'package:audis/views/hno_finder/google_map.dart';
import 'package:audis/views/training/training_screen.dart';
import 'package:audis/views/volume/volume.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final db = SqliteDB.instance.database;

  @override
  void initState() {
    super.initState();
    // SqliteDB.instance.readAllVowels();
  }

  @override
  void dispose() {
    super.dispose();
    SqliteDB.instance.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => TrainingScreen()));
                },
                child: Text("Training"),
                style: ElevatedButton.styleFrom(primary: Colors.orangeAccent)),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GoogleMapScreen()));
                },
                child: Text("HNO FInder"),
                style: ElevatedButton.styleFrom(primary: Colors.orangeAccent)),
            ElevatedButton(
                onPressed: () {},
                child: Text("Login"),
                style: ElevatedButton.styleFrom(primary: Colors.orangeAccent)),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => VolumeSet()));
                },
                child: Text(Strings.lautst_rke),
                style: ElevatedButton.styleFrom(primary: Colors.orangeAccent))
          ],
        ),
      ),
    );
  }
}
