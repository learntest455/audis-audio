import 'dart:convert';

import 'package:audis/views/hno_finder/model/doctors.dart';
import 'package:xml2json/xml2json.dart';
import 'package:http/http.dart' as http;

class ApiClient{
  final String baseUrl = 'https://hoertraining.auric.de/';
  final successCode = 200;
  final myTransformer = Xml2Json();

  Future unlockChecker(String userId) async {
    var response = await http.post(Uri.parse(baseUrl + "REST/getUnlocks.ashx"),
    body: jsonEncode("userid:" + userId));
    print("response=> ${response.body}");
    // myTransformer.parse(response.body);
    // var jsonResponse = myTransformer.toParker();
    // print("json=> $jsonResponse");
    // final responseString = jsonDecode(jsonResponse);

  }

  Future<List<Doctor>?> fetchDoctorsList() async {
    var response = await http.get(Uri.parse(baseUrl + "REST/getAllDoctors.ashx"));
    myTransformer.parse(response.body);
    var jsonResponse = myTransformer.toParker();
    print("json=> $jsonResponse");
    final responseString = jsonDecode(jsonResponse);

    if (response.statusCode == successCode) {
      DoctorsDataModel parsed = DoctorsDataModel.fromJson(responseString);
      return parsed.doctors!.doctor;
    } else {
      throw Exception('failed to load');
    }
  }

}
