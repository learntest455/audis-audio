// class HistoryUploader implements Runnable {
//
//   final Object pauseLock;
//   int connectingState;
//   bool threadPaused;
//
//   HistoryUploader() {
//     pauseLock = new Object();
//     threadPaused = false;
//   }
//
//   bool isPaused() {
//     return threadPaused;
//   }
//
//   @Override
//   void run() {
//     connectingState = preferences.getInt(HtConstants.KEY_PEERING_STATUS, 0);
//     if (connectingState != 3) {
//       return;
//     } else {
//       History[] histories = SQLiteDBHandler.getHistorysToSynchronize(context);
//       //only continue if there is something to upload
//       if (histories.length > 0){
//         String request = createRequestString(histories);
//         sendHistoryToServer(request);
//       }
//     }
//   }
//
//   //call to pause this thread
//   void onPause() {
//     synchronized (pauseLock) {
//       threadPaused = true;
//     }
//   }
//
//   //call to resume this thread
//   void onResume() {
//     synchronized (pauseLock) {
//       threadPaused = false;
//       pauseLock.notifyAll();
//     }
//   }
//
//   String createRequestString(History[] histories) {
//   String request = "";
//   for (int i = 0; i < histories.length; i++) {
//   request += "userid:" + preferences.getString(HtConstants.KEY_USERID, "") + ";timestamp:" + histories[i].getTimestamp().getTime() + ";level_id:" + histories[i].getLevel_id() + ";score:" + histories[i].getScore() + ";maxScore:" + histories[i].getMaxScore() + ";content:" + histories[i].getContent() + ";answer:" + histories[i].getAnswer() + ";correctAnswer:" + histories[i].getCorrectAnswer() + ";";
//   if (i != histories.length - 1) {
//   request += "|";
//   }
//   }
//   return request;
//   }
//
//
//   void sendHistoryToServer(String request) {
//     RequestQueue queue = Volley.newRequestQueue(context);
//     String url = HtConstants.SERVER_HOST + "/REST/postHistory.ashx";
//     final String encrypted;
//     try {
//       encrypted = CryptoHelper.encryptAndFormatMsg(request, HtConstants.SERVER_PUBLIC_RSA);
//     } catch (Exception e) {
//     //TODO: a better way of errorhandling
//     Log.d("EXEPTION", e.getMessage());
//     return;
//     }
//     StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//     @Override
//     void onResponse(String response) {
//     if (!CryptoHelper.verifyServerMessageSignature(response)) {
//     return;
//     }
//     if (response.contains("succes")) {
//     SharedPreferences sp = context.getSharedPreferences("history", 0);
//     SharedPreferences.Editor ed = sp.edit();
//     long lastTimestamp = new Date().getTime();
//     ed.putLong("timestamp", lastTimestamp);
//     ed.commit();
//     }
//
//     }
//     }, new Response.ErrorListener() {
//     @Override
//     void onErrorResponse(VolleyError error) {
//     Log.e("-l-l-l", "ERROR");
//     error.printStackTrace();
//     }
//     }) {
//     @Override
//     byte[] getBody() {
//
//     return encrypted.getBytes();
//     }
//
//     };
//     // Add the request to the RequestQueue.
//     stringRequest.setRetryPolicy(new DefaultRetryPolicy(2000, 3, 1f));
//     onPause();  //this doesnt directly pause the thread, its brefore the request to ensure that
//     //onResume is DEFENITLY after onPause
//     queue.add(stringRequest);
//   }
// }