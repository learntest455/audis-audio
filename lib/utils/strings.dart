class Strings{

  //Volume
  static String volumeMenuText = "Stellen Sie die Lautstärke so ein, dass das Rauschen für Sie gut hörbar ist. Diese Einstellung wird zu Beginn jeder Übung angewendet.";
  static String lautst_rke = "Lautstärke:";
  static String test_abspielen = "Test abspielen";
  static String save_changes = "Änderungen speichern";
  static String laute = "Laute";
  static String noises = "-Geräusche-";
  static String language = "Sprache";
  static String language_with_distortion = "Sprache im Störschall";
  static String intonation = "Betonung und Stimme";
  static String signalDiff = "Signaldifferenzierung";
  static String vowelsInNonsense0Name = "Vokale in Nonsense Wörtern";
  static String consonantsInNonsense = "Konsonanten in Nonsense Wörtern";
  static String vowelsInRealwords = "Vokale in Realwörter";
  static String consonantsInRealwords = "Konsonanten in Realwörter";
  static String orientation = "Richtung";
  static String pitch = "Tonhöhe";
  static String duration = "Tonlänge";
  static String volume = "Lautstärke";
  static String wordUnderstanding = "Wortverständnis";
  static String memory = "Memory";
  static String order = "Reihenfolge";
  static String orderTaskDesc = "Geben Sie die Wörter in gehörter Reihenfolge an";
  static String dichotic = "Dichotisches Hören";
  static String sentenceUnderstandingTaskAName = "Satzverständnis";
  static String intonationQorS = "Frage oder Aussage";
  static String intonationGender = "Mann oder Frau";
  static String intonationAge = "Kind oder Erwachsener";
  static String intonationMood = "Emotionen";
  static String exercise = "Übung";
  static String again = "Nochmal";
  static String ready = "Fertig";
  static String congratulations = "Glückwunsch!";
  static String taskComplete = "Sie haben alle Übungen in dieser Aufgabe erfolgreich gelöst!";
  static String wrong = "Falsch";
  static String taskFailed = "Das war leider falsch. Beim nächsten Versuch klappt es bestimmt besser!";
  static String backBtnPressedDialog = "Möchten Sie die aktuelle Übung abbrechen? Alle nicht gespeicherten Fortschritte gehen verloren.";
  static String yes = "Ja";
  static String no = "Nein";
  static String short_ = "kurz";
  static String long_ = "lang";
  static String left = "links";
  static String right = "rechts";

  //vowels
  static String vowelsInNonsense0Desc = "Um sich mit den Stimmen der Sprecher vertraut zu machen, sollen Sie erstmal nur das Klangbeispiel hören und das entsprechende Wort dazu lesen.";
  static String vowelsInNonsense1Desc = "Geben Sie das Wort an das Sie gehört haben.";
  static String vowelsInNonsenseA3Desc = "Geben Sie an ob es sich um eine langes oder kurzes Vokal handelt. (Die Laute 'au', 'ei' &amp; 'eu' zählen als lang)";
  static String vowelsInNonsense4Name = "Vokale in Nonsense Wörtern(lang/kurz)";
  static String vowelsInRealwordsShortLong = "Vokale in Realwörter (kurz/lang)";

  //consonants
  static String consonantsInReal = "Konsonanten in Realwörtern";
  static String startConsDescr = "Hören Sie sich die Aufnahme an und bestimmen Sie den Anlaut.";
  static String endConsDescr = "Hören Sie sich die Aufnahme an und bestimmen Sie den Auslaut.";
  static String startMiniPairDesc = "Bestimmen Sie, welches Wort Sie gehört haben. Als Möglichkeiten wird ihnen ein Minimalpaar vorgegeben, das sind zwei sehr änhliche Wörter die sich nur im Anlaut unterscheiden.";
  static String endMiniPairDesc = "Bestimmen Sie, welches Wort Sie gehört haben. Als Möglichkeiten wird ihnen ein Minimalpaar vorgegeben, das sind zwei sehr änhliche Wörter die sich nur im Auslaut unterscheiden.";

  //speech
  static String wordunderstandingTaskAName = "Wörter zu Thema";
  static String wordunderstandingTaskBName = "1-3-Silbige Wörter";
  static String wordunderstandingTaskCName = "3-Silbige Wörter";
  static String wordunderstandingTaskDName = "zweistellige Zahlen";
  static String wordunderstandingTaskEName = "2-Silbige Wörter";
  static String wordunderstandingTaskFName = "1-Silbige Wörter";
  static String highConfuse = "Hohe Verwechselbarkeit";
  static String lessVolume = "Lautstärke verringert";
  static String multiSyllable = "mehrsilbige Wörter";
  static String wordunderstandingTaskADescr = "Geben Sie das Wort an das Sie gehört haben.";
  static String wordunderstandingTaskDDescr = "Geben Sie die gehörte Zahl an.";

  //memory
  static String memoryTaskDesc = "An welcher Stelle wurde das folgende Wort genannt?";

  //dichotic
  static String dichoticTaskDesc = "Welches Wort haben sie folgender Seite gehört?";

  //Satzverständnis
  static String sentenceUnderstandingTaskADesc = "Vervollständigen Sie den Satz.";
  static String askingForFirst = "Füllen Sie die erste Lücke";
  static String askingForSecond = "Füllen Sie die zweite Lücke";
}