import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<double?> getVolume() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = await SharedPreferences.getInstance();
  return prefs.getDouble(volumeSettings);
}

Future<void> storeVolume(double value) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setDouble(volumeSettings, value);
}

Future<int?> getTimestamp() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = await SharedPreferences.getInstance();
  return prefs.getInt(timestamp);
}

Future<void> storeTimestamp(int value) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setInt(timestamp, value);
}

String volumeSettings = "volume_settings";
String timestamp = "timestamp";